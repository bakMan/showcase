#! /usr/bin/env python3
import asyncpbs.core
from asyncpbs.core import AsyncPBS
from asyncpbs.filemanager import FileManager
from asyncpbs.fileTask import FileTask
import asyncio
from asyncpbs.core.common import visualizeTaskTree


con = asyncpbs.core.createConnectorFromConfig()
manager = AsyncPBS(con)
file_manager = FileManager("/storage/brno3-cerit/home/xsmata01/.asynciopbs",con)

task  = FileTask(manager,file_manager,"cat some_file | wc -l > line_num",inports = ["some_file"],outports = ["line_num"],name = "psax") 
task2 = FileTask(manager,file_manager,"echo The number is: `cat count` > msg.txt",inports = ["count"],outports = ["msg"],dynamic_connections = {"line_num":"count"},name ="count") 

task2.addSubtasks(task)

async def job():
    await task2.run({"some_file":"/storage/brno3-cerit/home/xsmata01/barcode.txt"})
    await task2.barrier()

asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(job()))