import os.path as pth
import os

class RemoteFile:
    def __init__(self,file_manager,path):

        self.path = path
        self.file_manager = file_manager

    async def exists(self):
        return await self.file_manager.pathExists(self.path)

    async def getPath(self):
        return self.path

    async def upload(self):
        return self

    def translate(trans_map):
        return self

        


class TaskFile:
    def __init__(self,file_manager,task,name):
        self.task = task
        self.name = name
        self.file_manager = file_manager

    async def exists(self):
        return await self.file_manager.pathExists(await self.getPath())

    async def getPath(self):
        return pth.join(await self.file_manager.getTaskDir(self.task.tid),self.name)
    
    async def upload(self):
        return self

    async def download(self,target_path = None):
        
        if target_path is None:
            cwd         = os.getcwd()
            file_name   = pth.basename(await self.getPath())
            target_path = pth.join(cwd,file_name)

        await self.file_manager.download(await self.getPath(),target_path)
        return LocalFile(target_path)


    def translate(trans_map):
        name = trans_map.get(self.name,self.name)
        return TaskFile(self.file_manager,self.task,name)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self,other):
        return self.name == other.name


class LocalFile:
    def __init__(self,path):
        self.path = path
    
    async def exists(self):
        return pth.exists(self.path)

    async def getPath(self):
        return self.path

    async def upload(self):
        pass
    
    def translate(trans_map):
        return self

    def open(self,mode):
        return open(self.path,mode)

class NullFile:
    def __init__(self):
        pass
    
    async def exists(self):
        return True

    async def getPath(self):
        return ""

    async def upload(self):
        pass
    
    def translate(trans_map):
        return self


class FileFactory:
    def __init__(self,file_manager):
        self.file_manager = file_manager

    def fileFromPath(self,task,path):
        if pth.isabs(path):
            return RemoteFile(self.file_manager,path)
        elif path.startsWith("local:"):
            _,true_path = path.split(":",2)
            return LocalFile(true_path)
        else:
            return TaskFile(self.file_manager,task,path)

