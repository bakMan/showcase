import unittest
from .functional import *
from .core.connector import *
from .core.manager import *
from .fileTask import *
from .filemanager import *
import tempfile
import shutil

class TestFunctional(unittest.TestCase):

    def setUp(self):
        self.con = localConnector("somebody")
        self.manager = AsyncPBS(self.con)
        self.dir = tempfile.mkdtemp()

        self.file_manager = FileManager(self.dir,self.con)


    def tearDown(self):
        shutil.rmtree(self.dir)

        
    def test_map(self):
        inpt = FileTask(self.manager,self.file_manager,"bla",["bla"],["out1","out2"])
        mapper = FileTask(self.manager,self.file_manager,"bla",["some_port"],["many doge","very doge"])

        res = taskMap(inpt,mapper)

        self.assertEqual(len(res),len(inpt.outports))
        self.assertEqual(res[0].dynamic_connections,{"out1":"some_port"})
        self.assertEqual(res[1].dynamic_connections,{"out2":"some_port"})
    
    def test_map_single(self):
        inpt = FileTask(self.manager,self.file_manager,"bla",["bla"],["out1"])
        mapper = FileTask(self.manager,self.file_manager,"bla",["some_port"],["many doge","very doge"])

        res = taskMap(inpt,mapper)

        self.assertEqual(len(res),len(inpt.outports))
        self.assertEqual(res[0].dynamic_connections,{"out1":"some_port"})

    def test_map_zero(self):
        inpt = FileTask(self.manager,self.file_manager,"bla",["bla"],[])
        mapper = FileTask(self.manager,self.file_manager,"bla",["some_port"],["many doge","very doge"])

        res = taskMap(inpt,mapper)

        self.assertEqual(len(res),0)

    def test_reduce_even(self):
        inpt1 = FileTask(self.manager,self.file_manager,"bla",["bla"],["out1"])
        inpt2 = FileTask(self.manager,self.file_manager,"bla",["bla"],["out2"])
        reducer = FileTask(self.manager,self.file_manager,"bla",["in1","in2"],["out"])

        res = taskReduce([inpt1,inpt2],reducer)
        sub1,sub2 = res.subtasks

        self.assertListEqual([inpt1,inpt2],[sub1,sub2])
        
    def test_reduce_odd(self):
        inpt1 = FileTask(self.manager,self.file_manager,"bla",["bla"],["out1"])
        inpt2 = FileTask(self.manager,self.file_manager,"bla",["bla"],["out2"])
        inpt3 = FileTask(self.manager,self.file_manager,"bla",["bla"],["out3"])
        reducer = FileTask(self.manager,self.file_manager,"bla",["in1","in2"],["out"])

        res = taskReduce([inpt1,inpt2,inpt3],reducer)
        sub1,sub2 = res.subtasks
        ssub1,ssub2 = sub1.subtasks
        
        self.assertListEqual([inpt1,inpt2],[ssub1,ssub2])
        self.assertEqual(inpt3,sub2)