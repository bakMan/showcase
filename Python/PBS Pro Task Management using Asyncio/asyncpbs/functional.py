from .core.common import PBSException
from copy import deepcopy
from itertools import islice
from .fileTask import *

def taskMap(input_task,mapping_prototype):
    if not mapping_prototype.getType() == TaskType(1,N):
        raise PBSException("Mapping task has to have one free port and one output port")

    output_tasks = []
    for p in input_task.outports:
        new = mapping_prototype.clone()

        new.addSubtasks(input_task)
        prototype_ports = list(mapping_prototype.iterateDisconnectedPorts())
        new.dynamic_connections = {prototype_ports[0]:p}
        
        output_tasks.append(new)

    return output_tasks

def createReductor(prototype,input1,input2):
    new = prototype.clone()
    new.addSubtasks(input1,input2)
    dynamic_connections = {inp:outp for outp,inp in zip(itertools.chain(input1.outports,input2.outports),new.iterateDisconnectedPorts())}
    new.dynamic_connections = dynamic_connections

    return new

def createReductionStructure(inputs,reductor_prototype):
    #extra_item = inputs[-1] if len(inputs) % 2 else None

    top_reducer = None
    carry = None
    input_tasks = inputs
    

    while len(input_tasks) != 1:
        
        carry = [input_tasks[-1]] if len(input_tasks) % 2 else []
        reduction_list = list(iterateNeighbours(input_tasks))

        new_input = []
        for input1,input2 in reduction_list:
            reductor = createReductor(reductor_prototype,input1,input2)           
            new_input.append(reductor)
        
        input_tasks = new_input + carry
        carry = []
        

    return input_tasks[0] 


def taskReduce(input_tasks,reductor_prototype):
    inputs = [i for i in input_tasks]

    #reducer has to process input of two tasks -> double the output ports of input
    #also it must be connectable to itself -> output of lower level reducers
    reductor_type = reductor_prototype.getType()
    required_type = TaskType(N,int(reductor_type.inports/2))

    if not all(t.getType() == required_type for t in inputs):
        raise PBSException("Uncompatible input -> reducer on reduce()")

    if not reductor_prototype.getType().couldBeConnectedTo(reductor_prototype.getType()):
        raise PBSException("Reductor in reduce must be connectible to itself !")
    
    return createReductionStructure(inputs,reductor_prototype)

        