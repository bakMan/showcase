from .task import *
from .manager import *
from .common import *
import unittest
from .parsers import *

MOCK_SEP = "".join(["-"]*200)

class MockConnRes:
    def __init__(self):
        self.exit_status = 0
        self.stderr = ""
        self.stdout = ""

class MockSshConn:
    
    DEF_LIFETIME = 2
    TEMPLATE_QSTAT = "{} banana queue name session 10 20 res_memory res_time {} time"
    TEMPLATE_JOBNAME = "{}.banana"

    def gen_id(self):
        self.id += 1
        return self.id

    def __init__(self,freeze=False):
        self.joblist = []
        self.id = 1
        self.freeze = freeze
        
        self.user = "blabla"
        self.password = "blabla"
        self.port = 200
        self.server = "bla.blabla.bla"

    def getDependencies(self):
        for _,_,_,args in self.joblist:
            yield args

    async def connect(self,server,port,username,password):
        #errprint("Connect to sw {}, port {}, user {}, pw {}".format(server,port,username,password))
        await asyncio.sleep(1)
        return self
    
    async def run(self,cmd):      
        await asyncio.sleep(0.5)
        if "qsub" in cmd:

            name = self.gen_id()

            #save dependencies
            args = []
            if "-W depend=afterany:" in cmd:
                args = cmd.split("-W depend=afterany:")[1]
                args = args.split(" ")[0]
                args = args.split(":")
            
            self.joblist.append((name,self.DEF_LIFETIME,cmd,args))
            res = MockConnRes()
            res.stdout = self.TEMPLATE_JOBNAME.format(str(name)) + "\n"
            
            return res

        elif "qstat" in cmd:

            if not self.freeze:
                self.joblist = [(j,cnt -1,cmd,args) for j,cnt,cmd,args in self.joblist]
            
            #jobs with expired will not be included
            
            self.joblist = [(i,cnt,cmd,args) for i,cnt,cmd,args in self.joblist if cnt > 0]


            sout = "\n".join(self.TEMPLATE_QSTAT.format(self.TEMPLATE_JOBNAME.format(str(i)),"R") for i,_,_,_ in self.joblist)
            res = MockConnRes()
            res.stdout = " ".join(PBS_ALT_FIELDS) + "\n" + MOCK_SEP + "\n" + sout
            return res
            
        else:
            return MockConnRes()

class TestPbs(unittest.TestCase):

    def setUp(self):
        self.con = MockSshConn()
        self.manager = AsyncPBS(con = self.con)

    def tearDown(self):
        for t in asyncio.Task.all_tasks():
            t.cancel()

    def test_job_iter(self):
        t1 = PBSTask(None,"bla")
        t2 = PBSTask(None,"bla")
        t3 = PBSTask(None,"bla")
        t4 = PBSTask(None,"bla")
        t5 = PBSTask(None,"bla")

        t1.jobids = ["A"]
        t2.jobids = ["B"]
        t3.jobids = ["C"]
        t4.jobids = ["D"]
        t5.jobids = ["E"]

        t4.addSubtasks(t2,t1)
        t3.addSubtasks(t4,t5)

        jobids = list(getJIDsFromSubtree(t3))
        self.assertEqual(jobids,["B","A","D","E","C"])

    def test_single_submit(self):
        t = self.manager.createTask("qsub")
        asyncio.get_event_loop().run_until_complete(t.run())
        self.assertEqual(self.con.joblist,[(2,2,"cd ./;qsub ",[])])


    def test_single_barrier(self):
        t = self.manager.createTask("qsub")

        async def task():
            await t.run()
            await t.barrier()

        asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(task()))
        self.assertEqual(self.con.joblist,[])
        self.assertEqual(self.manager.future_map,{})
    

    def test_chain(self):
        t1 = self.manager.createTask("A")
        t2 = self.manager.createTask("B")
        t3 = self.manager.createTask("C")

        res = chain(t3,t2,t1)
        sublist = list(map(operator.attrgetter("cmd"),iterateTaskSubtree(res)) )
        self.assertEqual(sublist,["C","B","A"])

        res = chain(t3)
        sublist = list(map(operator.attrgetter("cmd"),iterateTaskSubtree(res)) )
        self.assertEqual(sublist,["C"])      
    

# class ChainTests(unittest.TestCase):
    
#     def setUp(self):
#         self.con = MockSshConn(freeze=True)
#         self.manager = AsyncPBS("bla","bla","bla",con = self.con,interval=10000)

#     def tearDown(self):
#         for t in asyncio.Task.all_tasks():
#             t.cancel()


#     def test_chain_depends(self):
#         t1 = self.manager.createTask("qsub A")
#         t2 = self.manager.createTask("qsub B")

#         res = chain(t1,t2)
#         asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(res.run()))
#         deps = list(self.con.getDependencies());
#         self.assertEqual(deps,[[],["2"]])
