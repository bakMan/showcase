import operator
import itertools
from graphviz import Digraph

class PBSException(Exception):
    """Base of all PBS exceptions"""
    pass

splitter = lambda x: x.split(".")[0] if len(x.split(".")) > 1 else x

def flatten(iter):
    yield from (x for sub in iter for x in sub)

def reverseItemToCollectionMap(d):
    #mapping file -> tid
    out = {}
    for key,items in d.items():
        for x in items:
            val = out.get(x,[])
            val.append(key)
            out[x] = val

    return out

def reverseItemToItemMap(d):
    #mapping file -> tid
    out = {}
    for key,item in d.items():
        val = out.get(item,[])
        val.append(key)
        out[item] = val

    return out

def iterateEveryOther(collection):
    truefalse = itertools.cycle([True,False])
    yield from (x for x,is_output in zip(collection,truefalse) if is_output)

def iterateNeighbours(lst):
    if len(lst) < 2:
        return

    yield from  iterateEveryOther(zip(lst,lst[1:]))

def iterateTaskSubtree(root):
    """Iterator over subtasks of a task, children first"""
    
    #if I would always output children and than parent I would have duplicities
    #if I output children excluding parent I will always miss the topmost root node
    #therefore division -> inner iter outputs excluding parent + topmost level outputs also parent
    def _innerIter(r):
        for task in r.subtasks:
            yield from _innerIter(task)
    
        yield from r.subtasks
    
    yield from _innerIter(root)
    yield root

def iterateLeafTasks(root):
    tasks = iterateTaskSubtree(root)
    yield from (t for t in tasks if t.isLeafTask())

def getJIDsFromSubtree(root):
    for t in iterateTaskSubtree(root):
        yield from t.jobids

def getJIDsOfDirectChildren(root):
    for t in root.subtasks:
        if t.jobids is not None:
            yield from t.jobids


def taskTreeToGraph(root):

    def _createNode(task,graph):
        graph.node(task.tid,task.name)

        for t in task.subtasks:
            _createNode(t,graph)
            graph.edge(t.tid,task.tid)

    
    graph = Digraph()
    _createNode(root,graph)
    return graph

        
def visualizeTaskTree(root):
    graph = taskTreeToGraph(root)
    graph.render("{}.gv".format(root.tid),view=True)