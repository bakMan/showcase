from .common import PBSException,splitter

def parseJIDs(input):
    lines          = [l for l in input.split("\n") if l != ""]
    splitted_lines = [l.split(".") for l in lines]

    if not all(len(l) >= 1 for l in splitted_lines):
        raise PBSException("Invalid joblist was returned\n{}".format(input))
    
    return [leftmost for leftmost,*rest in splitted_lines]


#Method parses PBS Qstat output to python objects
identity = lambda x:x


QSTAT_NFIELDS=11
PBS_STATUSES=['Q','R','E','F','H']
PBS_ALT_FIELDS=["id","user","queue","name","sessID","NDS","TSK","res_memory","res_time","status","time"]
PBS_CONVERTORS=[splitter ,identity,identity,identity,identity,int,int,identity,identity,identity,identity]

def parseQstat(data):

    #no string --> no current jobs
    if data=="":
        return []

    #first two lines are headers
    lines = data.split("\n")

    #otazka - co sa tu moze a nemoze stat ?
    #ignore untill start of the header
    line = lines.pop(0)

    while not line.startswith("-"):
        line = lines.pop(0)


    #split on whitespace, delete blanks
    lines = [x.split() for x in lines if x.strip() != ""]
    
    if not all(map(lambda x: len(x) == QSTAT_NFIELDS,lines)):
        stats = Counter(map(len,lines))
        print(lines)
        raise ValueError("Qstat output is expected to contain 11 fields. Check PBS version. N fields found: {}".format(stats))

    #translate to dict

    return [dict( (f,conv(x)) for f,x,conv in zip(PBS_ALT_FIELDS,x,PBS_CONVERTORS)) for x in lines]    

def parseJobs(jobstr):
    #expecting one job per newline number.anything
    jobs = [x.strip().split(".")[0] for x in jobstr.split("\n") if x != ""]

    if not all(map(lambda x: re.match("^[0-9]+$",x),jobs)):
        raise ValueError("Command have not outputted list of jobs.")

    return [{"id" : x, "status" : "SENT"} for x in jobs]