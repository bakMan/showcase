import sys
import os.path as pth
from os.path import expanduser
import os
from .common import PBSException
import asyncssh as ssh
from .manager import AsyncPBS
import json
from .connector import *
from .task import QsubTask
import logging

def createConfigDirIfNotExists():
    if not pth.exists(APBS_DIR):
        os.mkdir(APBS_DIR)
    else:
        if not pth.isdir(APBS_DIR):
            raise FileExistsError("File {} exists but is not a directory".format(APBS_DIR))

def readConfig(conf_path):

    if pth.exists(conf_path):
        conf = None

        with open(conf_path,"r") as f:
            conf = json.load(f)

        if any(map(lambda x: x not in conf,["server","port","user","password","remote_basedir"])):
            raise PBSException("Configuration has to contain values for server port user and password and base directory on the remote in json format")

        return conf
    else:
        return None


#check for library home dir
APBS_DIR=pth.join(expanduser("~"),".asynciopbs")
SETTINGS_FILE="config.json"


createConfigDirIfNotExists()
DEFAULT_CONFIG = readConfig(pth.join(APBS_DIR,SETTINGS_FILE))

