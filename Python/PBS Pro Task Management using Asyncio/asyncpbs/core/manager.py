import asyncssh as ssh
import datetime
from collections import Counter
import json
import re
import asyncio
import threading
import operator
import logging as log
from asyncio.futures import Future
from copy import deepcopy
import random
import sys
from .common import *
from .task import PBSTask,QsubTask
from copy import deepcopy
from functools import reduce
from .parsers import *
from .connector import localConnector


class PBSFuture(Future):
    """Special future of AsyncioPBS, as an addition contains also task"""
    def __init__(self,pbstask,loop=None):
        super().__init__(loop=loop)
        self.pbstask = pbstask
        
        
        self.jobs_to_do = list(getJIDsFromSubtree(pbstask))
        if any(x is None for x in self.jobs_to_do):
            raise PBSException("Encountered task with no jobid on future registration")

    def setJobCompleted(self,jid):
        log.info("[PBSFUTURE] Setting job {} finished".format(jid))
        self.jobs_to_do = [x for x in self.jobs_to_do if x != jid]


    def jobsRemaining(self):
        return self.jobs_to_do != []


def errprint(str):
    sys.stderr.write(str + "\n")    


class AsyncPBS:

    def __init__(self,con,interval=4):
        self.connector = con
        self.monitoring_daemon = None
        self.interval=interval
            
        #dict of monitored jobs - will contain only jobs sent by AsyncPBS
        self.future_map={}

    def runDaemonIfNeeded(self):
        if self.monitoring_daemon is None:
            log.info("[ASYNCPBS] Starting the monitoring daemon")
            self.monitoring_daemon = MonitoringDaemon(self)

    def insertFutureIntoMap(self,future):
        #mapping job -> future
        self.future_map.update({j:(future,{"status" : "SENT","absent_counter" : 0}) for j in future.jobs_to_do})  
    
    def deleteFutureFromMap(self,future):
        self.future_map = {j:(f,info) for j,(f,info) in self.future_map.items() if f != future}

    def deleteJobFromMap(self,jid):
        self.future_map ={j:(f,info) for j,(f,info) in self.future_map.items() if j != jid}

    def register(self,task):
        """Registers given future with monitoring daemon"""
        log.info("[MANAGER] Registred new task {} with command {} ".format(task.tid,task.cmd))
        
        future = PBSFuture(task,loop=asyncio.get_event_loop())
        self.insertFutureIntoMap(future)
        self.runDaemonIfNeeded()

        return future

    def getRegistredJIDs(self):
        return set(self.future_map.keys())

    async def _getStdOE(self,dir,name,ident):
        """Fetches job's stdout and stderr"""

        sout = await self.connector.run("cd {};cat {}.o{}".format(dir,name,ident))
        serr = await self.connector.run("cd {};cat {}.e{}".format(dir,name,ident))
        return (sout.stdout,serr.stdout)

    def cancelFuture(self,fut):
        if fut is None:
            return

        #cancle future and filter it out from pool
        fut.cancel()
        self.deleteFutureFromMap(fut)

    async def qstat(self,user=None):
        if user is None:
            user = self.connector.user

        user = "-u {}".format(user) if user != "" else ""
        res  = await self.connector.run("qstat -a {}".format(user))

        if res.exit_status != 0:
            raise ValueError("Qstat ended with non zero status, error: {}".format(res.stderr))
        return parseQstat(res.stdout)

    async def qdel(self,*jobs):
        res  = await self.connector.run("qdel {}".format(" ".join(jobs)))

    async def _sendPBSCmd(self,cmd,pwd="./",additional=""):
        log.info("[ASYNCPBS] Sending command {} to remote".format(cmd))

        res = await self.connector.run("cd {};{} {}".format(pwd,cmd,additional))
        return res.stdout

        if res.exit_status != 0:
            raise ValueError("Command {} failed with error '{}'".format(cmd,res.stderr))

    def createTask(self,cmd):
        """ Wrapper to inject this manager instance """
        return QsubTask(self,cmd)

    def anyJobsToWatch(self):
        return len(self.future_map) != 0

    def notifyNoMoreJobs(self):
        log.info("[ASYNCPBS] No more jobs to monitor, shutting down daemon")
        if self.monitoring_daemon is not None:
            self.monitoring_daemon.terminate()
            self.monitoring_daemon = None

class MonitoringDaemon:
    def __init__(self,PBSManager):
        self.PBSManager = PBSManager
        self.task       = None
        self.startMonitoring()

    def startMonitoring(self):
        loop = asyncio.get_event_loop()
        self.task = loop.create_task(self.doPeriodicMonitoring())
    
    
    async def doPeriodicMonitoring(self):
        
        while True:
            num_jobs = len(self.PBSManager.future_map)
            log.info("[MONITORING DAEMON] Starting watch, {} registred jobs to watch".format(num_jobs))
            
            jobs_pbs = await self.getJobsFromPBS()

            self.updateRunningJobs(jobs_pbs)
            self.processCompletedJobs(jobs_pbs)
            self.processSentJobs(jobs_pbs)
            
            if not self.PBSManager.anyJobsToWatch():
                self.PBSManager.notifyNoMoreJobs()

            log.info("[MONITORING DAEMON] Going to sleep")
            await asyncio.sleep(self.PBSManager.interval)

    
    def updateRunningJobs(self,pbs_jobs):
        for jid in self.getRunningJIDs(pbs_jobs):
            future,jobinfo = self.PBSManager.future_map[jid]
            jobinfo.update(pbs_jobs[jid])


    def processCompletedJobs(self,pbs_jobs):
        for jid in self.getCompletedJIDs(pbs_jobs):
            future,info = self.PBSManager.future_map[jid]

            future.setJobCompleted(jid)
            self.PBSManager.deleteJobFromMap(jid)

            if not future.jobsRemaining():
                self.markFutureCompleted(future)

    def processSentJobs(self,pbs_jobs):
        for jid in self.getSentNonCompleteJIDs(pbs_jobs):
            future,info = self.PBSManager.future_map[jid]

            info["absent_counter"] +=1

    def getRunningJIDs(self,pbs_jobs):
        pbs_jids       = set(pbs_jobs.keys())
        monitored_jids = self.PBSManager.getRegistredJIDs()
        
        yield from monitored_jids.intersection(pbs_jids)

    def getCompletedOrSentJIDs(self,pbs_jobs):
        pbs_jids       = set(pbs_jobs.keys())
        monitored_jids = self.PBSManager.getRegistredJIDs()

        yield from monitored_jids.difference(pbs_jids)
    
    def getCompletedJIDs(self,pbs_jobs):
        for jid in self.getCompletedOrSentJIDs(pbs_jobs):
            future,info = self.PBSManager.future_map[jid]

            if info["status"] != "SENT":
                yield jid
    
    def markFutureCompleted(self,future):
        self.PBSManager.deleteFutureFromMap(future)
        future.set_result(True)


    def getSentNonCompleteJIDs(self,pbs_jobs):
        completed_or_sent = set(self.getCompletedOrSentJIDs(pbs_jobs))
        completed         = set(self.getCompletedJIDs(pbs_jobs))
        yield from completed_or_sent.difference(completed)


    def terminate(self):
        log.info("[MONITORING DAEMON] Terminating, nothing to do")
        self.task.cancel()

    async def getJobsFromPBS(self):
        res = await self.PBSManager.qstat()
        resmap = {r["id"]:r for r in res}
        return resmap

def chain(*args):
    """ Chain multiple PBS tasks in linear way """

    if len(args) == 1:
        return args[0]

    #reversed -> we want A -> B -> C -> D, so 
    #A is subtask of B, B of C, ...
    root, *rest = deepcopy(reversed(args))

    for c,n in zip(rest,rest[1:]):
        c.addSubtasks(n)
    
    return root.addSubtasks(rest[0])