import asyncssh as ssh
import os
import os.path as pth
import subprocess
from .common import PBSException
import logging as log
import shutil

class SSHConnector:

    def __init__(self,server,user,password,port=22):
        self.server=server
        self.user=user
        self.password=password
        self.port=port
        self.conn      = None
        self.sftp_conn = None
    
    async def getConnection(self):

        if self.conn is None:
            self.conn = await ssh.connect(self.server,self.port,username=self.user,password=self.password)
            self.sftp_conn = await self.conn.start_sftp_client()

        return (self.conn, self.sftp_conn)

    async def run(self,cmd):
        (ssh,sftp) = await self.getConnection()
        return await ssh.run(cmd)

    async def mkdir(self,path):
        (ssh,sftp) = await self.getConnection()
        await sftp.mkdir(path)
    
    async def isDir(self,path):
        (ssh,sftp) = await self.getConnection()
        return await sftp.isdir(path)
    
    async def pathExists(self,path):
        (ssh,sftp) = await self.getConnection()
        return await sftp.exists(path)

    async def getAbsPath(self,path):
        (ssh,sftp) = await self.getConnection()
        return await sftp.realpath(path)

    async def open(self,filename,mode):
        (ssh,sftp) = await self.getConnection()
        return await sftp.open(filename,mode)

    async def download(self,remote,local):
        await self.sftp_conn.get(remote,local)

class localConnectorProcRes():
    def __init__(self,sout,serr,stat):
        self.stdout = sout.decode()
        self.stderr = serr.decode()
        self.exit_status = stat

class localFileWrapper:
    def __init__(self,file):
        self.file = file

    async def close(self):
        self.file.close()

    async def write(self,str):
        self.file.write(str)


class localConnector:
    def __init__(self,user):
        self.user = user

    async def run(self,cmd):        
        proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        out, err = proc.communicate()

        if proc.returncode != 0:
            raise PBSException("{} returned non zero exit status ! Error: {}".format(cmd,err.decode()))
        
        return localConnectorProcRes(out,err,proc.returncode)

    async def mkdir(self,path):
        os.mkdir(path)
    
    async def pathExists(self,path):
        return pth.exists(path)

    async def getAbsPath(self,path):
        pth.abspath(path)

    async def open(self,filename,mode):
        return localFileWrapper(open(filename,mode))

    async def isDir(self,path):
        return pth.isdir(path)

    async def download(self,remote,local):
        shutil.copy(remote,local)