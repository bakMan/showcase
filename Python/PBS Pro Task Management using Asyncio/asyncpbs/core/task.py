import asyncio
import operator
import logging as log
from .common import *
import uuid
from .parsers import parseJIDs

class PBSTask:
    """ 
    Class for tasks at PBS, first instanced, the task 
    is inactive, just ready to run
    """

    def __init__(self,manager,cmd,dir="./",name="STDIN"):
        self.cmd           =   cmd
        self.subtasks      =   []
        self.jobids        =   None
        self.PBSmanager    =   manager
        self.future        =   None
        self.dir           =   dir
        self.tid           =   self.genTID()
        self.name          = name

    def genTID(self):
        return str(uuid.uuid4())

    def addSubtasks(self,*tasks):

        #Typecheck
        if not all(isinstance(task,PBSTask) for task in tasks):
            raise PBSException("Subtask can be only of PBSTask type")
        
        self.subtasks += tasks
        return self

    async def cancel(self):

        for t in iterateTaskSubtree(self):
            self.manager.cancelFuture(t.future)

        subtask_jobs = list(getJIDsFromSubtree(self))
        all_jobs     = subtask_jobs.append(self.jobid)
        await self.manager.qdel(*all_jobs)

    async def run(self):
        """Run this task -> send it and all its subtasks to the PBS.
           NOTE: This completes when task have been submitted. To wait for their completion
                 use barrier()
        """
        log.info("[TASK] Executing root task {}".format(self.cmd))
        
        await self._run(0)
        self.future = self.PBSmanager.register(self)

    async def _run(self,level=0):        
        log.info("[TASK] Executing subtask {}".format(self.cmd))

        if not self.hasCommand():
            return 
        
        for t in self.subtasks:
            await t._run(level = level + 1)
        
        for r in self.subtasks:
            if not r.hasJobids():
                raise PBSException("Task with command '{}' have failed to start".format(r.cmd))

        if self.hasJobids(): return

        await self.goingToExecute()
        await self.sendToPBS()
        
    
    async def goingToExecute(self):
        pass


    async def barrier(self):
        """Results in callee waiting until this pbs task is completed"""
        
        if not self.isAwaitable():
            raise PBSException("Called barrier on non-root task")

        log.info("[TASK] Waiting on completion of {}".format(self.cmd))
        await asyncio.ensure_future(self.future)

    def hasCommand(self):
        return self.cmd != ""

    def hasJobids(self):
        return self.jobids is not None

    def isAwaitable(self):
        return self.future is not None

    def isLeafTask(self):
        return len(self.subtasks) == 0

    async def sendToPBS(self):
        dependlist  = self.dependstringFromSubtasks()
        res         = await self.PBSmanager._sendPBSCmd(self.cmd,pwd=self.dir)
        self.jobids = parseJIDs(res)


class QsubTask(PBSTask):
    """Special instance of task which treats given command as an input to qsub"""

    QSUB_TEMPLATE="qsub -l select={nodes}:ncpus={ncpus}:mem={mem} -l walltime={walltime} -N {name} {depends} {script}"
    SCRIPT_NAME = "recipe-{tid}.sh"    
    DEPEND_PREFIX = "-W depend=afterany:"


    def __init__(self,manager,file_manager,cmd,dir="./",name="STDIN",walltime="2:00:00",mem="2g",ncpus="1",scratch="5gb",nodes=1):
        super().__init__(manager,cmd,dir="./",name=name)

        self.qsub_args = {
            "nodes" : nodes,
            "ncpus" : ncpus,
            "mem"   : mem,
            "walltime" : walltime,
            "name"     : self.name,
            "depends"    : ""
        }
        self.file_manager = file_manager

    async def uploadCmdAsFile(self,cmd):
        script_name = self.SCRIPT_NAME.format(tid = self.tid)
        return await self.file_manager.saveStringAsFile(self.cmd,script_name)

    async def goingToExecute(self):      
        script = await self.uploadCmdAsFile(self.cmd)
        self.qsub_args["depends"] = self.dependstringFromSubtasks()
        self.cmd = self.QSUB_TEMPLATE.format(script = script,**self.qsub_args)

    
    def dependstringFromSubtasks(self):
        deps = ":".join(getJIDsOfDirectChildren(self))
        return self.DEPEND_PREFIX + deps if deps != "" else ""

        
