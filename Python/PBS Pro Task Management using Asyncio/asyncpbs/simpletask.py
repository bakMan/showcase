#! /usr/bin/env python3
import core
import asyncio
from core.common import visualizeTaskTree

factory = core.createTaskFactoryFromConfig()
task  = factory.createTask(cmd = "sleep 1m; ls"); 
task2 = factory.createTask(cmd = "sleep 1m; ls"); 
task3 = factory.createTask(cmd = "sleep 1m; ls"); 
task4 = factory.createTask(cmd = "sleep 1m; ls"); 
task5 = factory.createTask(cmd = "sleep 1m; ls"); 
task4.addSubtasks(task2,task3)

task5.addSubtasks(task4,task)
visualizeTaskTree(task5)


async def job():
    await task5.run()
    await task5.barrier()

asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(job()))