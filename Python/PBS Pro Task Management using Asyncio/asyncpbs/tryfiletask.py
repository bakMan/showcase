#! /usr/bin/env python3
import core
from core import AsyncPBS
from filemanager import FileManager
from fileTask import FileTask
import asyncio
from core.common import visualizeTaskTree


con = core.createConnectorFromConfig()
manager = AsyncPBS(con)
file_manager = FileManager("/storage/brno3-cerit/home/xsmata01/.asynciopbs",con)

task  = FileTask(manager,file_manager,"ps -ax > processes.txt",[],["processes.txt"],name = "psax") 
task2 = FileTask(manager,file_manager,"cat procs.txt | wc -l > proccount.txt",["procs.txt"],["proccount.txt"],{"procs.txt":"processes.txt"},name ="count") 

task2.addSubtasks(task)

async def job():
    await task2.run({})
    await task2.barrier()

asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(job()))