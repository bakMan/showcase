from .core.task import QsubTask
from .core.common import *
import os.path
from .file import *
from copy import deepcopy

class ManyType:
    pass

N = ManyType()

class TaskType:

    def compareWithMany(self,num1,num2):
        if isinstance(num1,ManyType) or isinstance(num2,ManyType):
            return True
        else:
            return num1 == num2


    def __init__(self,inports,outports):
        self.inports, self.outports = inports, outports

    def __eq__(self,other):
        return self.compareWithMany(self.inports,other.inports) and self.compareWithMany(self.outports,other.outports)

    def couldBeConnectedTo(self,other):
        if isinstance(other.inports,ManyType):
            return True
        else:
            return other.inports >= self.outports

    def couldOtherBeConnected(self,other):
        return other.couldBeConnectedTo(self)

    


class FileTask(QsubTask):
    def __init__(self,pbs_manager,file_manager,cmd,inports = [],outports = [],static_connections = {},dynamic_connections  = {},name="STDIN"):
        super().__init__(pbs_manager,file_manager,cmd,name = name)

        #Input files can be of any type, output files can be only task files
        self.inports = inports
        self.outports = outports
        self.dynamic_connections = dynamic_connections
        self.connections = {p:None for p in self.inports}
        

        self.insertStaticConnections(static_connections)

    def getType(self):
        num_inports = len(list(self.iterateDisconnectedPorts()))
        num_outports = len(self.outports)
        return TaskType(num_inports,num_outports)

    def clone(self):
        new = FileTask(self.PBSmanager,self.file_manager,self.cmd,self.inports,self.outports,self.static_connections,name=self.name)
        return new

    def iterateConnectedPorts(self):
        yield from (p for p in self.inports if self.connections[p] is not None)

    def iterateDisconnectedPorts(self):
        yield from (p for p in self.inports if self.connections[p] is None)

    def hasPorts(self,portlist):
        if len(set(portlist).difference(set(self.inports))) != 0:
            return False
        else:
            return True

    def isFullyConnected(self):
        return len(list(self.iterateDisconnectedPorts())) == 0

    def getPortFile(self,port):

        if not port in self.inports and not port in self.outports:
            raise PBSException("Trying to get file of non-existing port {}".format(port))

        return TaskFile(self.file_manager,self,port)


    def insertStaticConnections(self,static_connections):
        
        if not self.hasPorts(static_connections.keys()):
            raise PBSException("Static connection of task {} with non-existing port ".format(self.tid))

        self.connections.update(static_connections)
        self.static_connections = static_connections

    
    async def doStaticConnectionsExist(self):
        for f in self.static_connections.values():
            if not await f.exists():
                return False
        
        return True

    def addSemicolonIfNeeded(self,string):
        if string == "": return ""
        
        if string.endswith(";"):
            return string
        else:
            return string + ";"


    async def createCopyToPortCmd(self,p):
        port_file = self.getPortFile(p)
        return await self.file_manager.createCopyCmd(self.connections[p],port_file)

    async def createCopyCmds(self):
        cmds = []
        for p in self.iterateConnectedPorts():
            cmds.append(await self.createCopyToPortCmd(p))

        return " ".join(cmds) 

    async def scheduleDependencyCopying(self):
        copy_cmd = await self.createCopyCmds()
        self.cmd = self.addSemicolonIfNeeded(copy_cmd) + self.cmd
    
    async def scheduleSubtaskDirRemoval(self):
        
        #MUST BE LOOP, await in comprehensions is not supported
        cmds = []
        for t in self.subtasks:
            cmds.append(await self.file_manager.createDeleteTaskDirCmd(t.tid))

        self.cmd = self.addSemicolonIfNeeded(self.cmd) + self.addSemicolonIfNeeded(" ".join(cmds))

    
    def injectInputToLeaves(self,input_files):
        for t in iterateLeafTasks(self):
            t.insertStaticConnections(input_files)

            if not t.isFullyConnected():
                raise PBSException("Task {} has unconnected ports: {}".format(t.tid,set(t.iterateDisconnectedPorts())))

    async def run(self,input_paths):
        
        input_files = {port:RemoteFile(self.file_manager,path) for port,path in input_paths.items()}

        for f in input_files.values():
            if not await f.exists():
                raise PBSException("Input file {} does not exist".format(f))

        self.injectInputToLeaves(input_files)
        await super().run()

    def scheduleCdIntoWorkdir(self):
        self.cmd = "cd $PBS_O_WORKDIR;" + self.cmd

    def iteratePortsProvidedBySubtasks(self):
        for t in self.subtasks:
            yield from ( (t,p) for p in t.outports)

    def getExpectedInputPortNames(self):
        return set(self.dynamic_connections.get(p,p) for p in self.iterateDisconnectedPorts())

    def canPortBeAssigned(self,port):
        expected_ports = self.getExpectedInputPortNames()
        return port in expected_ports
    
    def getPossibleDestsForForeignPort(self,p):
        dests = []
        open_ports = list(self.iterateDisconnectedPorts())
        inverse_dynamic = reverseItemToItemMap({k:v for k,v in self.dynamic_connections.items() if k in open_ports})

        if p in self.inports:
            dests.append(p)
        
        return dests + inverse_dynamic.get(p,[])

    def assignPort(self,p,portfile):
        if not p in self.connections:
            raise PBSException("Trying to assign non-existing port {}".format(p))
        
        self.connections[p] = portfile

    def assignPortsFromSubtasks(self):
        for t,p in self.iteratePortsProvidedBySubtasks():
            
            if self.canPortBeAssigned(p):
                dests = self.getPossibleDestsForForeignPort(p)
                self.assignPort(dests[0],t.getPortFile(p))


    async def goingToExecute(self):

        if not await self.doStaticConnectionsExist():
            errmsg = "Task {} has unsatisified dependencies: some static file missing\n".format(tid)
            raise PBSException(errmsg.format(self.tid,depstr))
        
        self.assignPortsFromSubtasks()

        if not self.isFullyConnected():
            errmsg = "Task {} has unconnected ports: {}\n".format(self.tid,set(self.iterateDisconnectedPorts()))
            raise PBSException(errmsg)

        await self.file_manager.makeTaskDir(self.tid)
        self.dir = await self.file_manager.getTaskDir(self.tid)
        

        self.scheduleCdIntoWorkdir()
        await self.scheduleDependencyCopying()
        #await self.scheduleSubtaskDirRemoval()
        
        await super().goingToExecute()

    def __eq__(self,other):
        ports_matching = self.inports == other.inports and self.outports == other.outports
        cmds_matching  = self.cmd == other.cmd
        closures_matching = self.static_connections == other.static_connections

        return cmds_matching and ports_matching and closures_matching

