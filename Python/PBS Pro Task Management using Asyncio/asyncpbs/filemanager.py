import asyncio
from asyncpbs.core.common import PBSException
import os
import os.path as pth 
from .file import FileFactory,NullFile

class FileManager:

    async def tryToMakeDir(self,dir):
        err = "[LOCAL_FILE_MANAGER] Unable to create directory '{}': {}"
        try:
            await self.connector.mkdir(dir)
        except Exception as e:
            raise PBSException(err.format(dir,str(e)))

    def __init__(self,basedir,connector):
        #Basedir can be created only in local file manager case
        #in ssh version, this would require await on constructor

        self.connector = connector
        self.basedir   = basedir
        self.file_facotry = FileFactory(self)

    async def getTaskDir(self,tid):
        return pth.join(await self.getBaseDir(),tid)

    async def pathExists(self,path):
        return await self.connector.pathExists(path)

    async def getBaseDir(self):

        if await self.connector.pathExists(self.basedir):
            if not await self.connector.isDir(self.basedir):
                raise PBSException("Staging directory {} exists and is not a directory".format(self.dir))
        else:
            await self.tryToMakeDir(self.basedir)
            self.basedir = await self.connector.getAbsPath(self.basedir)

        
        return self.basedir

    async def makeTaskDir(self,tid):
        tdir = await self.getTaskDir(tid)

        if await self.connector.pathExists(tdir):
            raise PBSException("Directory for task {} already exists".format(tid))

        await self.tryToMakeDir(tdir)


    def copyCmd(self,input,output):
        return "cp -r {} {};".format(input,output)

    def rmCmd(self,file):
        return "rm -r -f {};".format(file)

    async def createDeleteTaskDirCmd(self,tid):
        return self.rmCmd(await self.getTaskDir(tid))

    async def createCopyCmd(self,file1,file2):
        if isinstance(file1,NullFile) or isinstance(file2,NullFile):
            return ""

        return self.copyCmd(await file1.getPath(),await file2.getPath())
        
    async def saveStringAsFile(self,str,filename):
        path = pth.join(await self.getBaseDir(),filename)

        f = await self.connector.open(path,"w")
        await f.write(str)
        await f.close()
        return path

    async def download(self,remote_path,local_path):
        try:
            await self.connector.download(remote_path,local_path)
        except Exception as e:
            raise PBSException("Error while downloading '{}': {}".format(remote_path,str(e)))         

    def fileFromTidAndPath(tid,path):
        self.file_facotry.fileFromTidAndPath(tid,path)