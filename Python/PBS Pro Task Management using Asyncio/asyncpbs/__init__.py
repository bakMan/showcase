from .core import *
from .core.manager import *
from .filemanager import *
from .fileTask import *
from .file import FileFactory


class TaskFactoryClass:
    
    def __init__(self,config):
        if config is None:
            self.is_usable = False

        self.is_usable = True
        self.con      = SSHConnector(config["server"],config["user"],config["password"],config["port"])

        self.PBSManager = AsyncPBS(self.con)
        self.file_manager = FileManager(config["remote_basedir"],self.con)
        self.file_factory = FileFactory(self.file_manager)
    

    def failIfNoConfig(self):
        if not self.is_usable:
            raise PBSException("Configuration from configuration file could not be loaded - could not create task. Please check that ~/.asynciopbs/config.json exists.")


    def createTask(self,**kwargs):
        self.failIfNoConfig()

        return FileTask(pbs_manager=self.PBSManager,file_manager=self.file_manager,**kwargs)

    def fileFromPath(self,path):
        self.failIfNoConfig()

        return self.file_factory.fileFromPath(None,path)




TaskFactory = TaskFactoryClass(core.DEFAULT_CONFIG)