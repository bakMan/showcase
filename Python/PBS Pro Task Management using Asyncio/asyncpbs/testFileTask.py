from .fileTask import *
from .filemanager import FileManager
from .core.manager import AsyncPBS
from .core.connector import localConnector
import os
import os.path as pth 
import tempfile
import unittest
import asyncio
import shutil

class testFileTask(unittest.TestCase):

    def setUp(self):
        self.dir = tempfile.mkdtemp()
        self.loop = asyncio.get_event_loop()
        self.con  = localConnector("random_user")
        self.file_manager = FileManager(self.dir,self.con)
        self.pbs_manager  = AsyncPBS(self.con)

        self.testfile_name = tempfile.mktemp()

        self.teststr="Hello partner !\n"

        with open(self.testfile_name,"w") as f:
            f.write(self.teststr)

    def tearDown(self):
        shutil.rmtree(self.dir)

        for t in asyncio.Task.all_tasks():
            t.cancel()
        
        os.remove(self.testfile_name)
    

    def getFileInTaskPath(self,task,file):
        return pth.join(task.file_manager.basedir,task.tid,file)

    def checkFileInTask(self,task,file):
        path = self.getFileInTaskPath(task,file)
        self.assertTrue(pth.exists(path))

    def checkTaskFileContents(self,task,file,expected_cont):
        path = self.getFileInTaskPath(task,file)
        file_cont = None

        with open(path,"r") as f:
            file_cont = "\n".join(line for line in f)

        self.assertEqual(file_cont,expected_cont)


    def test_single(self):
        exp_cont = self.teststr.strip() + " Howdy ?\n"
        filemap = {
            "test.txt" : self.testfile_name
        }
        
        t1 = FileTask(self.pbs_manager,self.file_manager,inports = ["test.txt"],outports = [],cmd = "echo `cat test.txt` Howdy ? > out.txt")
        asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(t1.run(filemap)))

        self.checkFileInTask(t1,"test.txt")
        self.checkTaskFileContents(t1,"out.txt",exp_cont)        

    def test_chain(self):
        exp_cont = self.teststr.strip() + " Howdy ? Howdy ?\n"
        
        filemap = {
            "test.txt" : self.testfile_name
        }
        
        t1 = FileTask(self.pbs_manager,self.file_manager,inports = ["test.txt"],outports = ["out.txt"],cmd = "echo `cat test.txt` Howdy ? > out.txt")
        t2 = FileTask(self.pbs_manager,self.file_manager,inports = ["out.txt"],outports = [],cmd = "echo `cat out.txt` Howdy ? > res.txt")
        t2.addSubtasks(t1)

        asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(t2.run(filemap)))

        self.checkFileInTask(t2,"out.txt")
        self.checkTaskFileContents(t2,"res.txt",exp_cont)     

    
    def test_tree(self):
        one_cont = self.teststr.strip() + " Howdy ?"
        exp_cont = one_cont + "\n\n"  + one_cont + "\n"
        
        filemap = {
            "test.txt" : self.testfile_name
        }
        
        t1 = FileTask(self.pbs_manager,self.file_manager,inports = ["test.txt"],outports = ["out.txt"],cmd = "echo `cat test.txt` Howdy ? > out.txt")
        t2 = FileTask(self.pbs_manager,self.file_manager,inports = ["test.txt"],outports = ["mnau.txt"],cmd = "echo `cat test.txt` Howdy ? > mnau.txt")
        t3 = FileTask(self.pbs_manager,self.file_manager,inports = ["out.txt","mnau.txt"],outports = [],cmd = "cat out.txt mnau.txt > res.txt")
        t3.addSubtasks(t1,t2)

        asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(t3.run(filemap)))

        self.checkFileInTask(t3,"out.txt")
        self.checkTaskFileContents(t3,"res.txt",exp_cont)     


    def test_unbalanced_tree(self):
        howdy = self.teststr.strip() + " Howdy ?"
        mnau  = self.teststr.strip() + " Mnau ?"
        stek  = self.teststr.strip() + " Stek ?"

        exp_cont = "\n\n".join([howdy,mnau,stek]) + "\n"
        
        filemap = {
            "test.txt" : self.testfile_name
        }
        
        t1 = FileTask(self.pbs_manager,self.file_manager,inports = ["test.txt"],outports = ["out.txt"],cmd = "echo `cat test.txt` Howdy ? > out.txt")
        t2 = FileTask(self.pbs_manager,self.file_manager,inports = ["test.txt"],outports = ["mnau.txt"],cmd = "echo `cat test.txt` Mnau ? > mnau.txt")
        t3 = FileTask(self.pbs_manager,self.file_manager,inports = ["out.txt","mnau.txt"],outports = ["haf.txt"],cmd = "cat out.txt mnau.txt > haf.txt")
        t4 = FileTask(self.pbs_manager,self.file_manager,inports = ["test.txt"],outports = ["stek.txt"],cmd = "echo `cat test.txt` Stek ? > stek.txt")
        t5 = FileTask(self.pbs_manager,self.file_manager,inports = ["stek.txt","haf.txt"],outports = [],cmd = "cat haf.txt stek.txt > res.txt")
        
        t3.addSubtasks(t1,t2)
        t5.addSubtasks(t3,t4)

        asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(t5.run(filemap)))

        self.checkFileInTask(t5,"res.txt")
        self.checkTaskFileContents(t5,"res.txt",exp_cont)     


    def test_multiple_choice_dep(self):
        pass


    def test_translation(self):
        exp_cont = self.teststr.strip() + " Howdy ? Howdy ?\n"
        
        filemap = {
            "test.txt" : self.testfile_name
        }
        
        t1 = FileTask(self.pbs_manager,self.file_manager,inports = ["test.txt"],outports = ["out.txt"],cmd = "echo `cat test.txt` Howdy ? > out.txt")
        t2 = FileTask(self.pbs_manager,self.file_manager,inports = ["renamed.txt"],outports = ["res.txt"],cmd = "echo `cat renamed.txt` Howdy ? > res.txt",dynamic_connections={"out.txt" : "renamed.txt"})
        t2.addSubtasks(t1)

        asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(t2.run(filemap)))

        self.checkFileInTask(t2,"renamed.txt")
        self.checkTaskFileContents(t2,"res.txt",exp_cont)     


