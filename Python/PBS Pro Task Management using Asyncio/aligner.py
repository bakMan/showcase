#! /usr/bin/env python3
from asyncpbs import *
from asyncpbs.functional import *
from asyncpbs.fileTask import *
from asyncpbs.filemanager import *
from asyncpbs.core import *
from asyncpbs.core.common import visualizeTaskTree
from optparse import OptionParser
import sys
import asyncio
import logging as log 

def errExit(msg):
    sys.stderr.write(msg + "\n")
    sys.exit(1)


def mkAlignTask():
    code = """module add emboss-6.5.7;
              needleall -auto query.fasta reference.fasta res.fasta;
              cat res.fasta | sed /#/d | sed /^$/d > alignment_result;"""

    inports = ["query.fasta","reference.fasta"]
    outports = ["alignment_result"]

    return TaskFactory.createTask(cmd = code,inports = inports, outports = outports,name="aligner")   

def mkSplitterTask(num_splits):
    code = """cat input_fasta | awk -v RS='>' 'BEGIN{{CNT=0;}}NF{{print ">"$1"\\n"$2 > CNT ;CNT = (CNT + 1) % {num_splits}}}' """
    inports = ["input_fasta"]
    outports = [str(i) for i in range(num_splits)]

    return TaskFactory.createTask(cmd = code.format(num_splits=num_splits),inports = inports,outports=outports,name="splitter")  

def mkAggregator(topn = 10):

    code = "cat needle1 needle2 | sort -n -k 3 | tail -n {topn} > toptable".format(topn=topn)
    inports = ["needle1","needle2"]
    outports = ["toptable"]
    return TaskFactory.createTask(cmd=code,inports=inports,outports=outports,name="aggregator")


def main():
    log.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

    parser = OptionParser(usage="aligner.py options")
    parser.add_option("-n","--number",dest="n_nodes",action="store",type="int",default=10, help="Number of nodes to use for database search.");
    parser.add_option("-d","--database",dest="database",action="store", help="Database fasta file to search in.");
    parser.add_option("-q","--query",dest="query",action="store", help="File containing the query sequence");


    (options,args) = parser.parse_args();

    if not options.n_nodes or not options.database or not options.query:
        errExit("Aligner requires three arguments -> number of nodes, database file and query file ")

    
    asyncio.get_event_loop().run_until_complete(asyncio.ensure_future(run(options)))

def dieIfNotFileExists(f,msg):
    if not f.exists():
        log.error(msg)
        sys.exit(1)

def printFile(file_obj):
    print("".join(file_obj.open("r")))

async def run(options):
    #load files
    
    query_file = TaskFactory.fileFromPath(options.query)
    database_file = TaskFactory.fileFromPath(options.database)

    #create tasks
    splitter = mkSplitterTask(options.n_nodes)
    
    aligner = mkAlignTask()
    aligner.insertStaticConnections({"query.fasta" : query_file})
    
    aggregator = mkAggregator()

    #produce execution plan
    mapped = taskMap(splitter,aligner)
    reduced = taskReduce(mapped,aggregator)

    #vizualize for good feeling ;)
    visualizeTaskTree(reduced)

    #fire !
    await reduced.run({"input_fasta":options.database})
    await reduced.barrier()

    #download resulting file
    top_scores = reduced.getPortFile("toptable")
    local_top_scores = await top_scores.download()
    
    dieIfNotFileExists(local_top_scores,"Download of top scores file have failed")

    printFile(local_top_scores)
    
    


if __name__ == '__main__':
    main()

