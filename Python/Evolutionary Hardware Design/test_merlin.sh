#! /bin/bash

ssh xsmata01@merlin.fit.vutbr.cz 'rm -rf /homes/eva/xs/xsmata01/TESTS/BIN;mkdir /homes/eva/xs/xsmata01/TESTS/BIN'
scp xsmata01.zip xsmata01@merlin.fit.vutbr.cz:/homes/eva/xs/xsmata01/TESTS/BIN/
ssh xsmata01@merlin.fit.vutbr.cz 'cd /homes/eva/xs/xsmata01/TESTS/BIN;unzip xsmata01.zip;make everything;rm -rf /homes/eva/xs/xsmata01/TESTS/BIN'