I. Program genetic.py - implementation of evolutionary algorithm for selection in HLS

	This program implements the selection stage of  High Level Synthesis process.
	In this stage, desired design template is populated by circuits avalible from
	the component library. This is done in fashion to meet specified timing constraints,
	while minimizing overall space occupied on resulting chip. Internally, program uses
	genetic algorithm to perform this optimization.

	Program was created using Python 3 programming language with no dependencies on external
	libraries. However, to fully use automating scripts provided with this project,
	users should also have Scipy library, Make, Awk, Bash and Gnuplot installed on their system.

	Following listing shows allowed program options (also accessible via -h parameter):

	Usage: genetic.py [options] cl_file design_file

	Options:
	  -h, --help            show this help message and exit
	  -g GRAPH_FILE, --graph-output=GRAPH_FILE
	                        Output evolution process statistics for graph
	                        plotting.
	  -c CHILDREN, --children-in-gen=CHILDREN
	                        Number of children per generation, defaults to 100.
	  -n GENERATIONS, --number-of-gen=GENERATIONS
	                        Number of generations, defaults to 100.
	  -p CROSSOVER, --probability-crossover=CROSSOVER
	                        Probability of crossover, 1 - mutation probability,
	                        defaults to 0.6.
	  -e ELITE, --elite-rate=ELITE
	                        Rate of elitism,percentual part of species that are
	                        preserved between generations, defaults to 0.1
	  -m, --make-example    Dump example JSON problem description into
	                        example_cl.json and example_design.json and exit
	                        program.
	  -t DELAY, --target-delay=DELAY
	                        REQUIRED, target delay of evolutionary design.

	To make things more clear, I will provide couple of examples. First, let's
	consider this minimal working example:

	src/genetic.py -t 500 input/example_cl.json input/example_design.json

	Command above will run selection on example_design.json using components from
	example_cl.json component library, with timing constraint of 500ns. Please note
	that the '-t' parameter is required and program will show error message if it 
	is not supplied. If user wants to control parameters of evolution more precisely, 
	command may look something like this:

	src/genetic.py -t 500 -c 40 -n 100 -e 0.1 -p 0.6 input/example_cl.json input/example_design.json

	Now, algorithm will run for 100 generations, each containing 40 species, with
	crossover probability of 60% and elitism rate of 1%. 

	Finally, user may be interested in developement of evolution process itself. 
	In that case option '-g' will provide output of time developement (in generation number) 
	of average population fitness level and fitness level of best specimen.

	src/genetic.py -t 500 -g "output/out.txt" input/example_cl.json input/example_design.json  

	Output can be ploted for instance using the gnuplot environment. Example script
	for plotting can be found in plot/plot_graphs.plt. However, as you can find 
	in next section, I have already prepared some automated commands to support this
	process.


II. Helper scripts and Makefile targets
	
	User may wish to skip the process of manually invoking 'genetic.py' and use some
	of these predefined Makefile targets instead: 

	make run - 				run one instance of algorithm with EWF design (files input/example_cl.json, 
				  			input/example_design.json) and default parameters. Developement of evolution
				  			is saved in output/out.txt and plotted using gnuplot (plot can be found
				  			in graph/ subdirectory). Timing constraint can be adjusted in
				  			runs.conf.

	make experiment - 		run set of experiments resulting in plots corresponding to the
							ones that could be found in the 'slides.pdf'. Parameters for 
							experiments, like number of runs or target design delay can
							be found in runs.conf.

	make run_test - 		evaluate corectness of genetic algorithm using the Ackley function
