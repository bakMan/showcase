load "imports/box_var.plt"
set output "../graph/mutation_box.pdf"
set terminal pdf
set key off

set xlabel "Mutation rate"
set ylabel "Fitness"

set title "Box plot of different mutation rates, ".gen." generations, ".children." children, ".runs." runs"

#set border 2 front lt black linewidth 1.000 dashtype solid
set boxwidth 0.5 absolute
set style fill solid 0.25 border lt -1

set style data boxplot
set style boxplot nooutliers
plot "../output/mutation_box.txt" using (0.0):1:(0.3):2

set output "../graph/elitism_box.pdf"
set terminal pdf
set key off

set xlabel "Elitism rate"
set ylabel "Fitness"

set title "Box plot of different elitism rates, ".gen." generations, ".children." children, ".runs." runs"

plot "../output/elitism_box.txt" using (0.0):1:(0.3):2

