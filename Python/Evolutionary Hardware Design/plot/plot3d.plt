load "imports/3dplot_var_mut.plt"
set output "../graph/3dplot_mutation.pdf"
set terminal "pdf"
set key right bottom
set title "Best average fitness for different pool sizes and crossover rates, p*g=".pg

set xlabel "Pool size"
set ylabel "Crossover rate (inverse of mutation rate)"

plot "../output/3dplot.txt" using 1:2:3 title "Average best fitness" with image

load "imports/3dplot_var_elite.plt"
set output "../graph/3dplot_elite.pdf"
set terminal "pdf"
set key right bottom
set title "Best average fitness for different pool sizes and elitism rates, p*g=".pg

set xlabel "Pool size"
set ylabel "Elitism rate (inverse of mutation rate)"

plot "../output/3dplot_elite.txt" using 1:2:3 title "Average best fitness" with image
