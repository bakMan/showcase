# XML Query - program na ziskavanie informacii z XML pomocou dotazovacieho # jazyka.
# Modul pre parsovanie pseudo-SQL jazyka
#XQR:xsmata01
# Stanislav Smatana, xsmata01 16.4.2013

import copy;

class SyntaxError(Exception):
   pass;

class RuntimeError(Exception):
   pass;

class NoElementError(Exception):
   pass;

#Trieda parsera jazyka podobneho SQL
class Parser:

   operators = ["<",">","=","CONTAINS","NOT","AND","OR"];
   #Konstruktor nastavi stack tokenov.
   def __init__(self,query):
      self.clean();
      self.tokenize(query);

   def clean(self):
      #instancne premenne
      self.what = "";
      self.limit = -1;
      self.from_str = "";
      self.condition = [];
      self.order_by = "";
      self.order_type = "ASC";

      self.keywords = ["FROM","SELECT","WHERE","ORDER BY","LIMIT","ASC","DESC"];

   def tokenize(self,query):
      self.query=[];
      tmp = [];

      #Vytvorenie zoznamu tokenov s respektovanim uvodzoviek
      in_brackets = False;
      token ="";
      space = False;
      for char in query:
         if char.isspace():
            if space == False and in_brackets == False:
               tmp.append(token);
               token = "";
               space = True;

            elif in_brackets:
               token += char;

         elif char == "\"":
            space = False;
            token += char;
            in_brackets = not in_brackets;
         else:
            space = False;
            token += char;

      #mohol po prejdeni retazca zostat token
      if token:
         tmp.append(token);

      #rozdelenie tokenov podla aritmetickych operacii
      arithm = ["=",">","<"];
      tmp2 = [];
      for tok in tmp:
         if len(tok) > 1:
            for op in arithm:
               if op in tok:
                  tok = tok.split(op);
                  tok.insert(1,op);
         tmp2.append(tok);

      
      #vyzehlenie zoznamu
      tmp=[]
      for tok in tmp2:
         if isinstance(tok,list):
            for x in tok:
               tmp.append(x);
         else:
            tmp.append(tok);




      #spojenie ORDER BY
      for token in tmp:
         #order by je dvojslovny ! nutne spojit
         if token == "BY" and self.query[-1] == "ORDER":
            self.query[-1] = self.query[-1] + " " + "BY";
            continue;


         #pri zatvorkach je nutne tokenizovat rucne
         if "(" in token or ")" in token:

            tmp_str = "";
            for char in token:
               if char == "(":
                  if tmp_str:
                     self.query.append(tmp_str);
                     tmp_str = "";
                  self.query.append("(");
               elif char == ")":
                  if tmp_str:
                     self.query.append(tmp_str);
                     tmp_str = "";
                  self.query.append(")");
               else:
                  tmp_str += char;

            #ak este nieco zostalo -> prihod - pripad (bla_bla
            if tmp_str:
               self.query.append(tmp_str);
         else:
            self.query.append(token);
         
   #vstupny bod parsera
   def parse(self):
      if not self.query:
         raise SyntaxError();
      token = self.query.pop(0);

      #prvy token musi byt select
      if token == "SELECT":
         try:
            syntax_ok = self.select();
         except IndexError:
            syntax_ok = False;

         if not syntax_ok:
            raise SyntaxError();
      else:
            raise SyntaxError();

   #delect statement
   def select(self):
      token = self.query.pop(0);

      #nazov elementu nesmie byt keyword
      if token in self.keywords:
         return False;

      self.what = token;

      token = self.query.pop(0);
      if token == "LIMIT":
         return self.limit_expr();
      elif token == "FROM":
         return self.from_expr();
      else:
         return False;

   #limit
   def limit_expr(self):
      token = self.query.pop(0);

      #limit musi byt cislo
      if not token.isdigit():
         return False;

      self.limit = int(token);
      token = self.query.pop(0);

      if token == "FROM":
         return self.from_expr();
      else:
         return False;

   #from
   def from_expr(self):
      token = self.query.pop(0);

      #nazov elementu nesmie byt keyword
      if token in self.keywords:
         return False;

      self.from_str = token;

      #po from moze a nemusi vyraz koncit
      if len(self.query) == 0:
         return True;

      token = self.query.pop(0);

      if token == "WHERE":
         return self.where();
      elif token == "ORDER BY":
         return self.order_by_expr();
      else:
         return False;

   def where(self):
      return self.to_postfix();


   def order_by_expr(self):
      token = self.query.pop(0);

      #nazov elementu nesmie byt keyword
      if token in self.keywords:
         return False;

      self.order_by = token;

      #implicitne order by je ASC -> nemusi byt uvedene
      if len(self.query) == 0:
         return True;

      token = self.query.pop(0);

      if not token in ["ASC","DESC"]:
         return False;

      self.order_type = token;

      if len(self.query) == 0:
         return True;
      else:
         return False;


   #Procedura vytvori postfixovu notaciu pre vyhodnocovanie podmienky.
   def to_postfix(self):
      token = "";
      stack = [];

      #az pokial nenarazime na nejake klucove slovo
      #alebo nespracujeme cely retazec
      while self.query:
         token = self.query.pop(0);
         if token in self.keywords:
            break;

         tmp = "";
         
         if token == "(":
            stack.append(token);
         elif token == ")":
            #pri ) zatvorke odstranujeme zo zasobniku a pridavame na koniec
            #vystupnej sekvencie az kym nenarazime na lavu
            while not tmp == "(":
               tmp = stack.pop();
               self.condition.append(tmp);
            #na konci je naviac pridana lava zatvorka
            self.condition.pop();
         elif not token in self.operators:
            #operand
            self.condition.append(token);
         else:
            #je to operator
            pushed = False;

            while not pushed:
               if not stack:
                  stack.append(token);
                  pushed = True;
               #vrchol zasobniku je posledna polozka zoznamu
               elif stack[-1] == "(":
                  stack.append(token);
                  pushed = True;
               #ak je na vrchole operator s rovnakou alebo vyssou prioritou
               #ide na koniec vystupu a pokracujeme v cykle
               elif self.isOpPriorEqual(stack[-1],token):
                  tmp = stack.pop();
                  self.condition.append(tmp);
               else:
                  stack.append(token);
                  pushed = True;

      #akonahle som dorazil na koniec vstupu -> vyprazdnim zasobnik a davam na
      #koniec
      while stack:
         tmp = stack.pop();
         self.condition.append(tmp);


      if token == "ORDER BY":
         return self.order_by_expr();
      elif len(self.query) == 0:
         return True;
      else:
         return False;
      


               

   #pomocna funkcia ktora zisti ci je operator op1 rovnako prioritny alebo
   #prioritnejsi nez op2
   def isOpPriorEqual(self,op1,op2):
      #v zozname operatorov su prioritnejsie na NIZSOM indexe
      return self.operators.index(op1) <= self.operators.index(op2);




      
      
