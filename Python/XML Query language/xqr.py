# XML Query - program na ziskavanie informacii z XML pomocou dotazovacieho # jazyka.
# Stanislav Smatana, xsmata01 16.4.2013
#XQR:xsmata01

import argparse;
import sys;
import xml.etree.ElementTree as etree;

#moje moduly
import queryparse;

#navratove kody programu
RET_OK = 0;
RET_E_ARGS = 1;
RET_E_FILE = 2;
RET_E_INPUT_FILE = 4;
RET_E_SYNTAX = 80;


#Pomocna funkcia na vypis chyby a ukoncenie programu.
def error(description,retval):
   sys.stderr.write(description + "\n");
   sys.exit(retval);

# Funkcia spracuje parametre a vrati argparse objekt.
# 
# V pripade ze prikazovy riadok neobsahuje argument --qf ani --query, hadze
# vyjnimku ValueError. Ak sa nepodari otvorit queryfile, hadze vyjnimku IOError.
def processArgs():
   parser = argparse.ArgumentParser();
   parser.add_argument('--input');
   parser.add_argument('--output');
   parser.add_argument('--query');
   parser.add_argument('--qf');
   parser.add_argument("-n",action='store_false',dest='gen_header');
   parser.add_argument('--root');

   args = parser.parse_args();

   #musi byt zadane query alebo qf
   if args.query == None and args.qf == None:
      raise ValueError();

   #qf a query nesmie byt zadane naraz
   if args.qf != None and args.query != None:
      raise ValueError();

   #query moze byt zadane ako parameter alebo zo suboru, v oboch pripadoch
   #bude ulozene v atribute query_str
   if args.query != None:
      args.query_str = args.query;
   else:
      #bude nacitane zo suboru
      with open(args.qf,'r') as qfile:
         args.query_str = qfile.read();

   return args;


#Funkcia najde v zadanom xml subore ziadane elementy.
#
def select(xmlfile, root, element):

   #ak nebol parameter zadany -> stdin
   if xmlfile == None:
      xmlfile = sys.stdin;
   else:
      xmlfile = open(xmlfile,"r");

   tree = etree.parse(xmlfile);
   xmlfile.close();

   #nad existujuci root pripojim novy falosny root, aby som povodny pohodlne
   #zahrnul do vyhladavania. XPath implicitne neuvazuje pri hladani root.
   new_root = etree.Element('tmp_root');
   new_root.append(tree.getroot());
   tree = etree.ElementTree(new_root);

   #uz root moze zodpovedat from -> nutne skontrolovat samostatne
   if root == "ROOT":
      search_elem = new_root;
   else:
      search_elem = tree.find(".//" + to_xpath(root));

   if search_elem == None:
      return []; 

   results = search_elem.findall(".//" + to_xpath(element));


   return results;
   
def slice_elem(elem):
   if "." in elem:
      splits = elem.split(".");

      #vsetky elementy s danym atributom v xpath znacime s hviezdickou
      if splits[0] == "":
         splits[0] = "*";

      return (splits[0],splits[1]);

   else:
      return (elem,"");

def to_xpath(elem):
   (el,attrib) = slice_elem(elem);

   if attrib:
      return el + "[@" + attrib + "]";
   else:
      return el;


#Funkcia ziska textovu hodnotu elementu. Pokial je operator text, tak ho len
#vrati.
def to_text(element,op):
   if len(op) > 1 and op[0] == "\"" and op[-1] == "\"":
      return op.strip("\"");
   #cislo prevediem na text -> vsetky porovnania unifikujem na textove
   elif not [num for num in op if not (num in ["0","1","2","3","4","5","6","7","8","9","."])]:
      return op;
   #inak musi ist o element/atribut v xml
   else:
      new_root = etree.Element('tmp_root');
      new_root.append(element);
      tree = etree.ElementTree(new_root);
      elem = new_root.find(".//" + to_xpath(op));

      if elem == None:
         raise queryparse.NoElementError("Unable to find element '" + op + "' during condition evaluation");

      (el,attr) = slice_elem(op);

      #ak je v poziadavku uvedeny atribut -> chceme hodnotu atributu nie elementu
      if attr:
         text = elem.get(attr);
         if text == None:
            raise queryparse.NoElementError("Element '" + el +"' does not contain attribute '" + attr + "'");
         else:
            return text;
      #inak vratime text v elemente
      else:
         if list(elem):
            raise queryparse.RuntimeError("Requested text from non text element");
         else:
            return elem.text;
            

      

#slovnik zastupnych funkcii pre operatory
#zastupne funkcie pre operatory
def equals(element,op1,op2):
   return to_text(element,op1) == to_text(element,op2);

#less aj greater mozu byt float alebo text
def less(element,op1,op2):
   try:
      return float(to_text(element,op2)) < float(to_text(element,op1));
   except ValueError:
      return to_text(element,op2) < to_text(element,op1);

def greater(element,op1,op2):
   try:
      return float(to_text(element,op2)) > float(to_text(element,op1));
   except ValueError:
      return to_text(element,op2) > to_text(element,op1);

def and_op(element,op1,op2):
   return op1 and op2;

def or_op(element,op1,op2):
   return op1 or op2;

def not_op(element,op1):
   return not op1;

def contains(element,op1,op2):
   if op1.isdigit() or op2.isdigit():
      raise queryparse.RuntimeError("Operator contains can only process text argument");

   op1_text = to_text(element,op1).strip("\"");
   op2_text = to_text(element,op2).strip("\"");

   return op2_text.find(op1_text) != -1;



#v slovniku je ulozena arita operacie a zodpovedajuca funkcia
op_dict = {"CONTAINS":(contains,2), "=":(equals,2), "<":(less,2),
">":(greater,2), "AND":(and_op,2),
           "OR":(or_op,2), "NOT":(not_op,1)};

#Funkcia overi ci dany element vyhovuje podmienke
def isValid(element,condition):
   stack = [];
   
   for op in condition:
      #ak ide o operator -> skontrolujeme ci mame dost operandov a spustime
      #operaciu
      if op in queryparse.Parser.operators:
         (op_func,arity) = op_dict[op];
         
         #je dost operandov ?
         if len(stack) < arity:
            raise queryparse.RuntimeError("Wrong arity for operator " + op);

         #zo zasobnika premiestnim potrebny pocet operandov do zoznamu
         #argumentov pre funkciu
         operands = [];
         for i in range(arity):
            operands.append(stack.pop());

         #spracovanie operatorom a vysledok na stack
         try:
            stack.append(op_func(element,*operands));
         except queryparse.NoElementError:
            return False;
      else:
         #operand
         stack.append(op);

   if len(stack) == 1:
      return stack[0];
   else:
      raise queryparse.RuntimeError("Extra items on stack during condition evaluation " + str(stack));
      





#Funkcia vyberie len zaznami vyhovujuce podmienke
def filter(condition, element_list):
   if condition:
      return [ x for x in element_list if isValid(x,condition) ];
   else:
      return element_list;

#Funkcia zoradi zaznamy (quicksort)
def sort(order_by, order_type,elements):
    if elements == []: 
        return []
    else:
        pivot = elements[0];
        pivot_text = to_text(pivot,order_by);


        #ascending vs descending
        if order_type == "ASC":
           lesser = sort(order_by,order_type,[x for x in elements[1:] if to_text(x,order_by) < pivot_text])
           greater = sort(order_by,order_type,[x for x in elements[1:] if to_text(x,order_by) >= pivot_text])
        else:
           lesser = sort(order_by,order_type,[x for x in elements[1:] if to_text(x,order_by) >= pivot_text])
           greater = sort(order_by,order_type,[x for x in elements[1:] if to_text(x,order_by) < pivot_text])

        return lesser + [pivot] + greater;


def printOut(output,elements,root_node,gen_header):
   out_str="";
   if root_node:
      #pridanie root node z prikazovej riadky
      root_elem = etree.Element(root_node);
      for node in elements:
         root_elem.append(node);

      out_str = etree.tostring(root_elem,encoding="utf-8").decode();

   else:
      for node in elements:
         out_str += etree.tostring(node).decode();

   if output == None:
      output = sys.stdout;
   else:
      output = open(output,"w");

   with output as file:
      if gen_header:
         file.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

      lines = out_str.split("\n");

      if len(lines) == 1:
         file.write(lines[0].lstrip() + "\n");
      else:
         for line in lines:
            file.write(line.lstrip());



   

################################################################################
##################################### MAIN #####################################
################################################################################
if __name__ == "__main__":

   #Spracovanie parametrov
   try:
      args = processArgs();
   except ValueError:
      error("Exactly one option of --qf or --query is required",RET_E_ARGS);
   except IOError:
      error("Unable to open given queryfile",RET_E_FILE);

   #Parsovanie query
   try:
      parser = queryparse.Parser(args.query_str);
      parser.parse();
   except queryparse.SyntaxError:
      error("Bad syntax of query",RET_E_SYNTAX);

   #Hladanie v xml
   try:
      elements = select(args.input, parser.from_str, parser.what);
   except etree.ParseError:
      error("Corrupted input file",RET_E_INPUT_FILE);

   #Filtracia 
   try:
      elements = filter(parser.condition,elements);
   except queryparse.RuntimeError as r:
      error("Runtime error in condition: " + r.args[0],RET_E_SYNTAX);
   except TypeError as t:
      error("Runtime error in condition: bad types in condition",RET_E_SYNTAX);

   #Zoradenie
   try: 
      if parser.order_by:
         elements = sort(parser.order_by, parser.order_type,elements);
         #kazdemu zoradenemu elementu je nutne pridat atribut order
         cnt = 1;
         for elem in elements:
            elem.attrib['order'] = str(cnt);
            cnt = cnt + 1;
   except queryparse.RuntimeError as r:
      error("Runtime error in ORDER BY: " + r.args[0],RET_E_SYNTAX);
   except queryparse.NoElementError as n:
      error("Element search error in ORDER BY: " + n.args[0],RET_E_SYNTAX);

   #ak je nastaveny limit orezeme vysledky
   if parser.limit >= 0 and len(elements) > parser.limit:
      elements = elements[:parser.limit];

   #Vypis
   try:
      printOut(args.output,elements,args.root,args.gen_header);
   except IOError:
      error("Unable to open output file",RET_E_FILE);


