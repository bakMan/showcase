from lib.io.fasta import *
from lib.util.taxxi import *
import logging

class DatasetError(Exception):
    pass

class Dataset:
    def __init__(self, sequences, taxonomies):
        self.sequences = sequences
        self.taxonomies = taxonomies

    def __eq__(self, other):
        return self.sequences == other.items and self.taxonomies == other.labels



class DatasetReader:
    def __init__(self,fasta_reader=FASTAReader()):
        self.fasta_reader = fasta_reader

    def __call__(self,path):
        id_sequence_tuples       = self.fasta_reader(path)
        taxonomy_sequence_tuples = list(self._parseTaxonomies(id_sequence_tuples))

        if len(taxonomy_sequence_tuples) == 0:
            return self._emptyDataset()
        else:
            return self._makeDataset(taxonomy_sequence_tuples)

    def _parseTaxonomies(self, id_sequence_tuples):
        taxonomy_parser = TAXXISequenceDescriptionParser()
        for id, sequence in id_sequence_tuples:
            yield from self._maybeParseTaxonomy(id, sequence, taxonomy_parser)

    def _maybeParseTaxonomy(self, id, sequence, taxonomy_parser):
        try:
            yield (taxonomy_parser.parse(id), sequence)
        except Exception as e:
            logging.warning("Unable to parse taxonomy '{}'. Error: {}".format(id, str(e)))

    def _emptyDataset(self):
        return Dataset([],[])

    def _makeDataset(self, taxonomy_sequence_tuples):
        taxonomies, sequences = zip(*taxonomy_sequence_tuples)
        return Dataset(
            sequences=list(sequences),
            taxonomies=list(taxonomies)
        )


class FileDatasetSplit:
    def __init__(self, training_set_path, testing_set_path,dataset_reader = DatasetReader()):
        self.training_set_path = training_set_path
        self.testing_set_path  = testing_set_path
        self.dataset_reader    = dataset_reader

        self._training_set = None
        self._testing_set  = None

    @property
    def testing_set(self):
        self._loadDatasetsIfNeeded()
        return self._testing_set

    def _loadDatasetsIfNeeded(self):
        if not self._datasetsLoaded():
            self._loadDatasets()

    def _loadDatasets(self):
        self._testing_set = self.dataset_reader(self.testing_set_path)
        self._training_set = self.dataset_reader(self.training_set_path)

    def _datasetsLoaded(self):
        return self._testing_set is not None or self._training_set is not None

    @property
    def training_set(self):
        self._loadDatasetsIfNeeded()
        return self._training_set

