import os.path as pth
import re
from lib.data.dataset import *
import logging

INNER_CV_SUBDIRECTORY = "inner"
TRAINING_SET_FILENAME = "train.fasta"
TESTING_SET_FILENAME = "test.fasta"

TRAINING_SPLIT_TEMPLATE = "{}_train.fasta"
TESTING_SPLIT_TEMPLATE = "{}_test.fasta"


CV_DIR_REGEX = r"^[0-9][0-9]*$"

NUM_REGEX = r"^([0-9]|[1-9][0-9][0-9]*)$"

class CVDataset:
    def __init__(self,root_path, split_factory, subdata_loader):
        self.root_path      = root_path
        self.split_factory  = split_factory
        self.subdata_loader = subdata_loader

        self._populate()

    def _populate(self):
        self._createMasterSplit()
        self._createSubDatasets()

    def _createMasterSplit(self):
        self.master_split = self.split_factory(
            pth.join(self.root_path, TRAINING_SET_FILENAME),
            pth.join(self.root_path, TESTING_SET_FILENAME)
        )

    def _createSubDatasets(self):
        self.subdatasets = self.subdata_loader(self.root_path)

    def __iter__(self):
        yield from self.subdatasets

    def __getitem__(self, item):
        return self.subdatasets[item]

class InnerCVDatasetLoader:
    def __init__(self,filesystem=Filesystem(),cv_dataset_factory=lambda x : CVDataset(x,FileDatasetSplit,CVSplitsLoader())):
        self.filesystem         = filesystem
        self.cv_dataset_factory = cv_dataset_factory

    def __call__(self, path):
        cv_directories = self.listCVDirectoriesIn(path)
        return [
            self.cv_dataset_factory(directory) for directory in sorted(cv_directories)
        ]

    def listCVDirectoriesIn(self, path):
        candidates = self.filesystem.list(path)
        return [
            candidate for candidate in candidates
            if self._isValidCVDir(candidate)
        ]

    def _isValidCVDir(self,candidate):
        is_dir = self.filesystem.isDir(candidate)
        basename = pth.basename(candidate)
        has_correct_name = re.match(CV_DIR_REGEX,basename) is not None
        return is_dir and has_correct_name

class CVSplitsLoader:
    def __init__(self,filesystem=Filesystem(),split_factory = lambda args : FileDatasetSplit(*args)):
        self.filesystem = filesystem
        self.split_factory = split_factory

    def __call__(self,path):
        path_to_cv_splits = self._cvSplitPathFromRoot(path)
        files_in_directory = self.filesystem.list(path_to_cv_splits)
        dataset_numbers = self._getDatasetNumbers(files_in_directory)
        dataset_numbers_of_existing = self._onlyWithBothExistingFiles(dataset_numbers,path)
        return [
            self.split_factory(self._makeFullDatasetPaths(number,path))
            for number in sorted(dataset_numbers_of_existing)
        ]

    def _makeFullDatasetPaths(self, number,base):
        train,test = self._makeDatasetFilenames(number)
        return (
            pth.join(self._cvSplitPathFromRoot(base),train),
            pth.join(self._cvSplitPathFromRoot(base),test)
        )

    def _cvSplitPathFromRoot(self, path):
        return pth.join(path, INNER_CV_SUBDIRECTORY)

    def _getDatasetNumbers(self,files_in_directory):
        only_filenames = [pth.basename(file) for file in files_in_directory]
        valid_dataset_files = self._filterValidFilenames(only_filenames)
        return set(
            self._numberFromFilename(file) for file in valid_dataset_files
        )

    def _filterValidFilenames(self,files):
        for file in files:

            if self.filesystem.isDir(file):
                logging.warning("Ignoring directory '{}' in CV split loading.".format(file))
                continue

            if not self._hasValidName(file):
                logging.warning("Ignoring file '{}' in CV split loading because of invalid filename.".format(file))
                continue

            yield file

    def _numberFromFilename(self, filename):
        return filename.split("_")[0]

    def _makeDatasetFilenames(self,dataset_number):
        return (
            TRAINING_SPLIT_TEMPLATE.format(dataset_number),
            TESTING_SPLIT_TEMPLATE.format(dataset_number)
        )

    def _hasValidName(self, filname):
        splits = filname.split("_")

        if len(splits) != 2:
            return False

        if splits[1] not in {"train.fasta","test.fasta"}:
            return False

        if re.match(NUM_REGEX,splits[0]) is None:
            return False

        return True

    def _onlyWithBothExistingFiles(self,numbers,path):
        return set(
            number for number in numbers
            if self._datasetFilesExist(number,path)
        )

    def _datasetFilesExist(self,number,path):
        return all(
            self.filesystem.exists(pth.join(self._cvSplitPathFromRoot(path),filename))
            for filename in self._makeDatasetFilenames(number)
        )

def openNestedCVDataset(path):
    return CVDataset(path,FileDatasetSplit,InnerCVDatasetLoader())