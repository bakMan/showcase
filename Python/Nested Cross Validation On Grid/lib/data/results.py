import lib.util.command_executor as cmd_exec
from lib.io.filesystem import *
from lib.io.filesystem import Filesystem
from lib.util import command_executor as cmd_exec
from lib.util.definitions import *
from io import StringIO
from pandas import read_csv, DataFrame,isnull

from lib.util.definitions import TSV_SEPARATOR, TYPES


class InvalidResultsFormat(Exception):
    pass


HEADER = [HEADER_NAME,HEADER_COMMAND,HEADER_OUTER_INDEX,HEADER_INNER_INDEX] + METRICS_ORDERING


class AbstractResultsStore:
    def __init__(self,root,filesystem= Filesystem(),command_executor = cmd_exec):
        self.root = root
        self.filesystem = filesystem
        self.command_executor = command_executor

    def resultsOfOuterFold(self,oindex):
        tsv = self._getTSVStringForOindex(oindex)
        return self._TSVToDataFrame(tsv)

    def getAllResults(self):
        tsv = self._getTSVStringForOindex("*")
        return self._TSVToDataFrame(tsv)

    def _TSVToDataFrame(self, tsv):
        df = self.__dataframeFromTSVString(tsv)
        self._checkDataFrame(df)
        return df

    def _getTSVStringForOindex(self, oindex):
        cat_template = "cat " + self._getQueryFilesTemplate()
        query_command = cat_template.format(root=self.root, outer_index=oindex)
        result = self.command_executor.executeCommand(query_command)
        return result

    def __dataframeFromTSVString(self,tsv_string):
        if tsv_string.strip() == "":
            return DataFrame()

        df = self._tryToMakeDataframe(tsv_string)

        return df

    def _tryToMakeDataframe(self, tsv_string):
        try:
            header = self._getHeader()
            with_header = TSV_SEPARATOR.join(header) + "\n" + tsv_string
            source = StringIO(with_header)
            df = read_csv(source, sep=TSV_SEPARATOR, dtype=TYPES)
            return df
        except Exception as e:
            raise InvalidResultsFormat("Malformed results tsv string:\n{}\nError: {}".format(tsv_string,str(e)))

    def _checkTypes(self,df):
        try:

            for col in df.columns:
                for e in df.loc[:,col]:
                    if col in TYPES:
                        TYPES[col](e)

        except Exception as e:
            raise InvalidResultsFormat("Results is invalid. Check wether cell values have correct type.\nError: {}".format(str(e)))

    def _checkDataFrame(self,df):
        if df.empty:
            return

        n_missing = isnull(df).sum().sum()

        if n_missing != 0:
            raise InvalidResultsFormat("Results contains NA or null values, this is not allowed:\n {}".format(df))

        self._checkTypes(df)


    def _getQueryFilesTemplate(self):
        raise NotImplementedError()

    def _getHeader(self):
        raise NotImplementedError()

class InnerResultsStore(AbstractResultsStore):
    def _getQueryFilesTemplate(self):
        return "{root}/{outer_index}" + FILENAME_SEPARATOR + "*" + FILENAME_SEPARATOR + "*"

    def _getHeader(self):
        return [HEADER_NAME, HEADER_COMMAND, HEADER_OUTER_INDEX, HEADER_INNER_INDEX] + METRICS_ORDERING

class OuterResultsStore(AbstractResultsStore):
    def _getQueryFilesTemplate(self):
        return "{root}/{outer_index}.tsv"

    def _getHeader(self):
        return [HEADER_NAME, HEADER_COMMAND, HEADER_OUTER_INDEX] + METRICS_ORDERING
