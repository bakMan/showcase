from lib.io.filesystem import *
from lib.evaluation.classifier_program import *
import logging


class ClassifierFileReader:
    ILLEGAL_CHARACTERS = "./\\?%*:|\"<>. _"
    def __init__(self,filesystem,classifier_program_factory):
        self.filesystem = filesystem
        self.classifier_program_factory = classifier_program_factory

    def read(self,path):
        for i,line in enumerate(self.filesystem.openForReading(path)):
            n = i + 1
            if line.strip() == "":
                continue

            values = line.split("\t")

            if len(values) < 2:
                logging.warning("Record in file '{}' at line {} does not have enough columns, ignoring.".format(path, n))
                continue

            name, command,*rest = values

            if len(rest) != 0:
                logging.warning("Ignoring extra columns in file '{}' at line {}, classifier '{}'".format(path,n,name))

            name = name.strip()
            command = command.strip()

            if name == "" or command == "":
                logging.warning(
                    "Record in file '{}' at line {} has empty name or command, ignoring.".format(path, n))
                continue

            if self._containsIllegalCharacters(name):
                self._logNameWithIllegalChars(name)
                continue

            yield self.classifier_program_factory(name,command)

    def _logNameWithIllegalChars(self,name):
        logging.warning("Ignoring classifier '{}' because its name contains one or more of illegal characters '{}'.".format(name,self.ILLEGAL_CHARACTERS))

    def _containsIllegalCharacters(self,name):
        return any(c in self.ILLEGAL_CHARACTERS for c in name)

def readClassifierFile(path):
    return ClassifierFileReader(
        filesystem=Filesystem(),
        classifier_program_factory=ClassifierProgram
    ).read(path)