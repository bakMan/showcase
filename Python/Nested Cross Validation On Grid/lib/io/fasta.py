from lib.io.filesystem import *
from io import StringIO

class WrongFastaFileException(Exception):
    pass

class FASTAReader:

    def __init__(self, fasta_repository=Filesystem()):
        self.fasta_repository = fasta_repository

    def __call__(self, uri):
        try:
            yield from self.__doParsing(uri)
        except WrongFastaFileException:
            raise
        except Exception as e:
            raise WrongFastaFileException("Unable to parse fasta file '{}'. Error: {}".format(uri, str(e)))

    def __doParsing(self, uri):
        content = ""

        with self.fasta_repository.openForReading(uri) as f:
            content = f.read()

        memfile = StringIO(content)

        yield from self._parseFastaLineSequence(memfile)

    def _parseFastaLineSequence(self, f):
        return ((id,sequence) for sequence,id in FastaLineStreamParser().parse(f))


class FastaLineStreamParser:
    RECORD_DELIMITER = ">"

    def __init__(self):
        self.__initializeEmptyRecord()

    def __initializeEmptyRecord(self):
        self.sequence = ""
        self.seq_id = None

    def parse(self, lines):
        for line in lines:
            line = line.strip()
            if self.__isBlank(line):
                continue

            yield from self.__parseLine(line)

        if self.__currentRecordIsNotEmpty():
            yield self.__getCurrentRecord()

        self.__initializeEmptyRecord()

    def __isBlank(self, line):
        return line == ""

    def __parseLine(self, line):
        if self.__newRecordStartsOnLine(line):
            if self.__currentRecordIsNotEmpty():
                yield self.__getCurrentRecord()
            self.__startNewRecord(line)
        else:
            self.__addLineToSequenceOfCurrentRecord(line)

    def __newRecordStartsOnLine(self, line):
        return line[0] == self.RECORD_DELIMITER

    def __currentRecordIsNotEmpty(self):
        return self.seq_id is not None

    def __getCurrentRecord(self):
        return (self.sequence, self.seq_id)

    def __startNewRecord(self, line):
        self.seq_id = self.__parseRecordId(line)
        self.sequence = ""

    def __parseRecordId(self, line):
        return line[1:]

    def __addLineToSequenceOfCurrentRecord(self, line):
        self.sequence += line.strip().upper()