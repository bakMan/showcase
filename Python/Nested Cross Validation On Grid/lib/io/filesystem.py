import os
import shutil
from os.path import *

class Filesystem:

    def openForWriting(self, file_path):
        return open(file_path, "w")

    def openForReadingBinary(self,file_path):
        return open(file_path, "rb")

    def openForReading(self, file_path):
        return open(file_path,"r")

    def list(self,directory_path):
        return ( join(directory_path,file) for file in os.listdir(directory_path))

    def remove(self,path):
        if self.isDir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)

    def isDir(self,path):
        return isdir(path)

    def exists(self,path):
        return os.path.exists(path)

    def mkDir(self,path):
        os.mkdir(path)

    def abspath(self,path):
        return abspath(expanduser(path))