import subprocess
from subprocess import PIPE

def executeCommand(command):
    result = subprocess.run(command,shell=True,check=True,stdout=PIPE)
    return result.stdout.decode("utf-8").strip()