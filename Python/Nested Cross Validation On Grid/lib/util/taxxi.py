from lib.util.taxonomy import *
import string

class WrongDescriptionSyntax(Exception):
    pass

class TAXXISequenceDescriptionParser:
    def parse(self, sequence_description):
        try:
            return self.__extractTaxonomy(sequence_description)
        except Exception as e:
            raise WrongDescriptionSyntax("Sequence description '{}' has wrong syntax".format(sequence_description))

    def __splitSequenceIDString(self, sequence_id):
        sequence_id, taxonomy_definition,_ = sequence_id.split(";")
        return (sequence_id,taxonomy_definition)


    def __extractTaxonomy(self,sequence_id):
        _,taxonomy_definition = self.__splitSequenceIDString(sequence_id)
        return self.__parseTaxonomyDefinition(taxonomy_definition)

    def __parseTaxonomyDefinition(self, taxonomy_definition):
        if  self.__taxonomyDefinitionEmpty(taxonomy_definition):
            return Taxonomy([])

        _, comma_separated_taxonomy_assignment = taxonomy_definition.split("=")
        return TAXXITaxonomyParser().parse(comma_separated_taxonomy_assignment)

    def __taxonomyDefinitionEmpty(self,taxonomy_definition):
        splitted_definition = taxonomy_definition.split("=")

        if len(splitted_definition) != 2:
            return False

        taxonomy_field,assignment = splitted_definition
        if splitted_definition[0] != taxonomy_field or assignment != "":
            return False

        return True


class TAXXITaxonomyParser:
    ALLOWED_TAXON_CHARS = set(string.ascii_lowercase + string.ascii_uppercase + ":_-[]" + string.digits)

    def parse(self,comma_separated_taxonomy_assignment):
        if comma_separated_taxonomy_assignment == "":
            return Taxonomy([])

        taxonomy_assignment_sequence = comma_separated_taxonomy_assignment.split(",")
        self._checkTaxons(taxonomy_assignment_sequence)
        taxonomy_assignment_sequence_without_prefix = list(
            self.__removeTaxonPrefixIfNeeded(taxon_with_prefix) for taxon_with_prefix in taxonomy_assignment_sequence)
        return Taxonomy(taxonomy_assignment_sequence_without_prefix)

    def __removeTaxonPrefixIfNeeded(self,taxon_with_prefix):
        if ":" in taxon_with_prefix:
            return taxon_with_prefix.split(":")[1]
        else:
            return taxon_with_prefix

    def _checkTaxons(self,taxons):
        for taxon in taxons:
            self._checkTaxon(taxon)

    def _checkTaxon(self,taxon):
        if not all(x in self.ALLOWED_TAXON_CHARS for x in taxon):
            raise WrongDescriptionSyntax("Taxon '{}' contains illegal characters.".format(taxon))

        if taxon == "":
            raise WrongDescriptionSyntax("Empty taxon is not allowed")