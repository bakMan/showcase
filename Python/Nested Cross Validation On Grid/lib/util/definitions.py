DEFAULT_RANK = 5

FILENAME_SEPARATOR = "_"

TSV_SEPARATOR = "\t"

HEADER_NAME = "name"
HEADER_COMMAND = "command"

HEADER_OUTER_INDEX = "OI"
HEADER_INNER_INDEX = "II"

METRICS_ORDERING = ["OC","MC","UC","TP","UMC","UUC","TPR","MCR","UCR","OCR","UMCR","UUCR","ACC","SCR","MCC","SCR_2"]

TYPES = {
    HEADER_NAME : str,
    HEADER_COMMAND : str,
    "OI" : int,
    "II" : int,
    "OC" : int,
    "MC" : int,
    "UC" : int,
    "TP" : int,
    "UMC": int,
    "UUC": int,
    "TPR": float,
    "MCR": float,
    "UCR": float,
    "OCR": float,
    "UMCR": float,
    "UUCR": float,
    "ACC": float,
    "SCR": float,
    "MCC" : float,
    "SCR_2" : float
}

DEFAULT_SELECTION_METRIC = "SCR_2"

EVALUATE_CLASSIFIER_COMMAND = "evaluate_classifier"
