import logging
import sys


def error(msg):
    logging.error(msg)
    sys.exit(1)