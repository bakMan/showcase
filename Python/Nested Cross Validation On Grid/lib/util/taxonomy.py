from abc import ABCMeta, abstractmethod

PRINTING_LIMIT = 49
PART_LENGTH = 23




class BaseTaxonomy(metaclass=ABCMeta):
    UNASSIGNED = "Unassigned"

    def __getitem__(self, key):
        if type(key) == slice:
            return self._indexBySlice(key)
        elif type(key) == int:
            return self._indexByInt(key)
        else:
            raise TypeError("Taxonomy indices must be integers or slices, not {}".format(type(key)))

    @abstractmethod
    def _indexBySlice(self, slice):
        pass

    @abstractmethod
    def _indexByInt(self, key):
        pass

    @abstractmethod
    def nCommonTaxa(self, other):
        pass

    @abstractmethod
    def getCommonTaxonomy(self, other):
        pass

    @abstractmethod
    def __str__(self):
        pass

    @abstractmethod
    def __repr__(self):
        pass

    @abstractmethod
    def __eq__(self, other):
        pass

    @abstractmethod
    def isParentOf(self, other):
        pass


class Taxonomy(BaseTaxonomy):

    def __init__(self,taxa):
        self.taxa = list(self.__upToFirstUnassigned(taxa))

    def __upToFirstUnassigned(self, taxa):
        for taxon in taxa:
            if self._isUnassigned(taxon):
                return
            yield taxon

    def _isUnassigned(self,taxon):
        return taxon is None or taxon == "" or taxon == self.UNASSIGNED

    def __iter__(self):
        if len(self.taxa) == 0:
            yield from [self.UNASSIGNED]
        else:
            yield from self.taxa

    def _indexBySlice(self, slice):
        if slice.start is not None:
            raise KeyError("Taxonomy is not slicable with start")

        if slice.step is not None:
            raise KeyError("Taxonomy is not slicable with custom step")

        return Taxonomy(self.taxa[:slice.stop])

    def _indexByInt(self, key):
        if key < 0 or key >= len(self.taxa):
            return self.UNASSIGNED
        return self.taxa[key]

    def __len__(self):
        return len(self.taxa)

    def nCommonTaxa(self,other):
        return len(self.getCommonTaxonomy(other))

    def getCommonTaxonomy(self,other):
        return Taxonomy(list(self._commonPrefix(other)))

    def _commonPrefix(self,other):
        for a,b in zip(self.taxa,other.taxa):
            if a == b:
                yield a
            else:
                return

    def __str__(self):
        return ";".join(self.taxa)

    def __repr__(self):
        string = str(self)

        if string == "":
            return "EMPTY_TAXONOMY;"

        if len(string) > PRINTING_LIMIT:
            return string[:PART_LENGTH] + "..." + string[-PART_LENGTH:]
        else:
            return string

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        if type(self) != type(other):
            return False

        if len(self) != len(other):
            return False

        return len(self) == len(self.getCommonTaxonomy(other))

    def isParentOf(self,other : BaseTaxonomy):
        if type(other) == WrongTaxonomy:
            return False

        if len(self) == 0:
            return True

        return len(self) < len(other) and self.nCommonTaxa(other) == len(self)


class WrongTaxonomy(BaseTaxonomy):

    def _indexBySlice(self, slice):
        return WrongTaxonomy()

    def _indexByInt(self, key):
        return self.UNASSIGNED

    def nCommonTaxa(self, other):
        return 0

    def getCommonTaxonomy(self, other):
        return WrongTaxonomy()


    def __str__(self):
        return "WRONG_TAXONOMY;"

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if type(other) == WrongTaxonomy:
            return True
        else:
            return False

    def isParentOf(self, other):
        return False