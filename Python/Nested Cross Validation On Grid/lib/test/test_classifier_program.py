from lib.evaluation.classifier_program import *
from lib.util.definitions import *
from itertools import product
import unittest

class TestClassifierProgram(unittest.TestCase):
    def test_classifier_program_has_to_have_name_and_command(self):
        for arguments in product([None,""],[None,""]):
            with self.assertRaises(ValueError):
                ClassifierProgram(*arguments)

        with self.assertRaises(ValueError):
            ClassifierProgram("","bla")

        with self.assertRaises(ValueError):
            ClassifierProgram("bla","")

    def test_classifier_program_can_create_command(self):
        self.assertEqual(
            ClassifierProgram("bla","bla.sh").makeCommandline("train","test"),
            "bla.sh --rank 5 --train \"train\" --test \"test\""
        )

    def test_classifier_program_can_create_command_with_different_rank(self):
        self.assertEqual(
            ClassifierProgram("bla","bla.sh").makeCommandline("train","test",rank=2),
            "bla.sh --rank 2 --train \"train\" --test \"test\""
        )

    def test_command_deletes_extra_quotes(self):
        self.assertEqual(
            ClassifierProgram("bla","bla.sh").makeCommandline("\"train\"","'te\"st'"),
            "bla.sh --rank 5 --train \"train\" --test \"test\""
        )

    def test_train_and_test_sets_cant_be_empty(self):
        for arguments in product([None,""],[None,""]):
            with self.assertRaises(ValueError):
                ClassifierProgram("bla","bla.sh").makeCommandline(*arguments)

        with self.assertRaises(ValueError):
            ClassifierProgram("bla", "bla.sh").makeCommandline("bla","")

        with self.assertRaises(ValueError):
            ClassifierProgram("bla", "bla.sh").makeCommandline("","bla")


class MockFileDatasetSplit:
    def __init__(self, training_set_path, testing_set_path):
        self.training_set_path = training_set_path
        self.testing_set_path  = testing_set_path

class TestClassifierProgramRunner(unittest.TestCase):
    def test_program_runner_executes_correct_commandline(self):
        self.executeClassifierProgram(
            ClassifierProgram("bla","bla.sh"),
            MockFileDatasetSplit(
                training_set_path="training",
                testing_set_path="testing",
            )

        )

        self.assertCommandlineRecieved("bla.sh --rank {} --train \"training\" --test \"testing\"".format(DEFAULT_RANK))

    def test_program_runner_does_taxonomy_parsing(self):
        self.givenProgramOutputs(
            "d:Bacteria,p:Bacteroidetes,c:Chitinophagia,o:Chitinophagales,f:Chitinophagaceae,g:Parafilimonas,s:Parafilimonas_terrae",
            "d:Bacteria,p:Firmicutes,c:Bacilli,o:Bacillales,f:Alicyclobacillaceae,g:Alicyclobacillus,s:Alicyclobacillus_shizuokensis"
        )

        self.executeArbitraryClassifierProgram()

        self.assertResultingTaxonomies(
            Taxonomy(["Bacteria","Bacteroidetes","Chitinophagia","Chitinophagales","Chitinophagaceae","Parafilimonas","Parafilimonas_terrae"]),
            Taxonomy(["Bacteria","Firmicutes","Bacilli","Bacillales","Alicyclobacillaceae","Alicyclobacillus","Alicyclobacillus_shizuokensis"])
        )

    def test_malformed_record_results_in_wrong_taxonomy(self):
        self.givenProgramOutputs(
            "3218907831-=;dqwee,sdas,s",
            "d:Bacteria,p:Bacteroidetes,c:Chitinophagia,o:Chitinophagales,f:Chitinophagaceae,g:Parafilimonas,s:Parafilimonas_terrae",
            "d:Bacteria;,;;,",
            ",",
            "",
            ","
        )

        self.executeArbitraryClassifierProgram()

        self.assertResultingTaxonomies(
            WrongTaxonomy(),
            Taxonomy(["Bacteria","Bacteroidetes","Chitinophagia","Chitinophagales","Chitinophagaceae","Parafilimonas","Parafilimonas_terrae"]),
            WrongTaxonomy(),
            WrongTaxonomy(),
            Taxonomy([]),
            WrongTaxonomy()
        )

    def test_newline_at_the_eof_is_ignored(self):
        self.givenProgramOutputs(
            "d:Bacteria,p:Bacteroidetes,c:Chitinophagia,o:Chitinophagales,f:Chitinophagaceae,g:Parafilimonas,s:Parafilimonas_terrae",
            ""
        )

        self.executeArbitraryClassifierProgram()

        self.assertResultingTaxonomies(
            Taxonomy(
                ["Bacteria", "Bacteroidetes", "Chitinophagia", "Chitinophagales", "Chitinophagaceae", "Parafilimonas",
                 "Parafilimonas_terrae"]),
        )

    def test_only_last_newline_is_ignored(self):
        self.givenProgramOutputs(
            "d:Bacteria,p:Bacteroidetes,c:Chitinophagia,o:Chitinophagales,f:Chitinophagaceae,g:Parafilimonas,s:Parafilimonas_terrae",
            "",
            "",
            ""
        )

        self.executeArbitraryClassifierProgram()

        self.assertResultingTaxonomies(
            Taxonomy(
                ["Bacteria", "Bacteroidetes", "Chitinophagia", "Chitinophagales", "Chitinophagaceae", "Parafilimonas",
                 "Parafilimonas_terrae"]),
            Taxonomy([]),
            Taxonomy([])
        )


    def setUp(self):
        self.recieved_command = None
        self.result = None
        self.output = ""

    def executeArbitraryClassifierProgram(self):
        self.executeClassifierProgram(ClassifierProgram("bla","bla.sh"),MockFileDatasetSplit("x","y"))

    def executeClassifierProgram(self,program,data):
        self.result = ClassifierProgramRunner(self.executeCommand).runClassifier(program,data)

    #command executor impl
    def executeCommand(self,command):
        self.recieved_command = command
        return self.output

    def assertCommandlineRecieved(self,expected):
        self.assertEqual(expected,self.recieved_command)

    def givenProgramOutputs(self,*lines):
        self.output = "\n".join(lines)

    def assertResultingTaxonomies(self,*taxonomies):
        self.assertEqual(
            list(self.result),
            list(taxonomies)
        )

if __name__ == '__main__':
    unittest.main()
