import unittest
from lib.io.fasta import *
from lib.test.mock_filesystem import *


class TestFastaReader(unittest.TestCase):
    def test_fasta_parser_is_buildable(self):
        FASTAReader()

    def test_fasta_reader_reads(self):
        contents = ">some_sequence\nACTGTGTGTCGGCTAGCTA"

        reader = self.makeReader(contents)

        self.assertEqual(
            list(reader("some_file")),
            [("some_sequence","ACTGTGTGTCGGCTAGCTA")]
        )

    def makeReader(self, contents):
        return FASTAReader(mockConstantFilesystem(contents))

    def test_fasta_reader_with_empty_file_reads_nothing(self):
        contents = ""

        reader = self.makeReader(contents)

        self.assertEqual(
            list(reader("some_file")),
            []
        )

    def test_fasta_reader_with_empty_sequence_reads_only_id(self):
        contents = ">banana"

        reader = self.makeReader(contents)

        self.assertEqual(
            list(reader("some_file")),
            [("banana","")]
        )

        contents = ">banana\n"

        reader = self.makeReader(contents)

        self.assertEqual(
            list(reader("some_file")),
            [("banana", "")]
        )


    def test_is_able_to_read_two_empty_records(self):
        contents = ">\n>"

        reader = self.makeReader(contents)

        self.assertEqual(
            list(reader("some_file")),
            [
                ("",""),
                ("", "")
             ]
        )

    def test_doubled_record_delimiter_is_handled(self):
        contents = ">>"

        reader = self.makeReader(contents)

        self.assertEqual(
            list(reader("some_file")),
            [
                (">","")
             ]
        )


    def test_blanks_are_ignored(self):
        contents = ">seq\nACTG\n\nTGCA\n\n"

        reader = self.makeReader(contents)

        self.assertEqual(
            list(reader("some_file")),
            [
                ("seq","ACTGTGCA")
             ]
        )

    def test_fasta_reader_with_wrong_input_fails(self):
        contents = ">ome_sequence\nACTGTCTGTCGTC\n>"
        reader = self.makeReader(contents)

        self.assertEqual(
            list(reader("some_file")),
            [("ome_sequence","ACTGTCTGTCGTC"),("","")]
        )

if __name__ == '__main__':
    unittest.main()