import unittest
from lib.util.taxonomy import *

class TestTaxonomy(unittest.TestCase):
    def test_empty_taxonomy_is_unassigned_in_each_rank(self):
        self.givenTaxonomy()

        for i in range(10):
            self.assertTaxonomyAtRank(
                i,
                Taxonomy.UNASSIGNED
            )

    def test_taxonomy_is_padded_by_unassigned(self):
        self.givenTaxonomy("bla","hla")

        self.assertTaxonomyAtRank(-1, Taxonomy.UNASSIGNED)
        self.assertTaxonomyAtRank(0,"bla")
        self.assertTaxonomyAtRank(1,"hla")

        for i in range(2,10):
            self.assertTaxonomyAtRank(i,Taxonomy.UNASSIGNED)

    def test_taxonomy_can_be_iterated(self):
        self.givenTaxonomy("bla", "hla")
        self.assertTaxonomyIteration("bla","hla")

    def test_taxonomy_ends_at_first_unassigned(self):
        self.givenTaxonomy("bla", "hla",Taxonomy.UNASSIGNED,"kla")
        self.assertTaxonomyIteration("bla","hla")

    def test_empty_taxonomy_has_one_unassigned(self):
        self.givenTaxonomy()
        self.assertTaxonomyIteration(Taxonomy.UNASSIGNED)

        self.givenTaxonomy(Taxonomy.UNASSIGNED,Taxonomy.UNASSIGNED)
        self.assertTaxonomyIteration(Taxonomy.UNASSIGNED)

        self.givenTaxonomy(Taxonomy.UNASSIGNED, "bla")
        self.assertTaxonomyIteration(Taxonomy.UNASSIGNED)

    def test_empty_string_or_none_is_unassigned(self):
        self.givenTaxonomy("bla", "hla","","kla")
        self.assertTaxonomyIteration("bla","hla")

        self.givenTaxonomy("bla", "hla", None, "kla")
        self.assertTaxonomyIteration("bla", "hla")

        self.givenTaxonomy("", "", None, "")
        self.assertTaxonomyIteration(Taxonomy.UNASSIGNED)

    def test_same_taxonomies_have_similarity_one(self):
        self.givenTaxonomy("bla", "hla","kla")
        self.assertHasNCommonTaxaWith(
            ["bla","hla","kla"],
            3
        )

    def test_similarity_differs_based_on_length(self):
        self.givenTaxonomy("bla", "hla","kla")

        self.assertHasNCommonTaxaWith(
            ["bla", "hla", "kla","ula"],
            3
        )

        self.assertHasNCommonTaxaWith(
            ["bla","hla","kla"],
            3
        )

        self.assertHasNCommonTaxaWith(
            ["bla", "hla"],
            2
        )

        self.assertHasNCommonTaxaWith(
            ["bla"],
            1
        )

        self.assertHasNCommonTaxaWith(
            [],
            0
        )

    def test_similarity_differs_based_on_common_taxa(self):

        self.givenTaxonomy("bla", "hla", "kla")

        self.assertHasNCommonTaxaWith(
            ["bla", "hla", "kla", "ula"],
            3
        )

        self.assertHasNCommonTaxaWith(
            ["bla", "hla", "ola", "ula"],
            2
        )

        self.assertHasNCommonTaxaWith(
            ["bla", "zla", "ola", "ula"],
            1
        )

        self.assertHasNCommonTaxaWith(
            ["dla", "hla", "ola", "ula"],
            0
        )


    def test_empty_taxonomies_have_zero_taxa_in_common(self):

        self.givenTaxonomy()

        self.assertHasNCommonTaxaWith(
            [],
            0
        )


    def test_empty_taxonomy_has_zero_taxa_in_common_with_anything(self):

        self.givenTaxonomy()

        self.assertHasNCommonTaxaWith(
            ["bla"],
            0
        )

        self.assertHasNCommonTaxaWith(
            ["bla","baklava"],
            0
        )


    def test_taxonomy_equality_works(self):

        self.givenTaxonomy("bla","kla","hla")

        self.assertIsEqualTo(
            ["bla","kla","hla"],
        )

        self.assertIsNOTEqualTo(
            ["bla", "kla"],
        )

        self.assertIsNOTEqualTo(
            ["bla"],
        )

        self.assertIsNOTEqualTo(
            [],
        )

        self.assertIsNOTEqualTo(
            ["bla", "kla", "hla","ula"],
        )

    def test_taxonomy_parent_relation_works(self):

        self.givenTaxonomy("bla", "kla", "hla")

        self.assertIsParentOf(
            ["bla", "kla", "hla","ula"],
        )

        self.assertIsNOTParentOf(
            ["bla", "kla", "hla"],
        )


        self.assertIsNOTParentOf(
            ["bla", "kla"],
        )

        self.assertIsNOTParentOf(
            ["bla"],
        )

        self.assertIsNOTParentOf(
            [],
        )

    def test_empty_taxonomy_is_parent_of_each_taxonomy(self):

        self.givenTaxonomy()

        self.assertIsParentOf(
            ["bla", "kla", "hla","ula"],
        )

        self.assertIsParentOf(
            ["bla", "kla", "hla"],
        )


        self.assertIsParentOf(
            ["bla", "kla"],
        )

        self.assertIsParentOf(
            ["bla"],
        )

        self.assertIsParentOf(
            [],
        )

    def test_taxonomy_is_sliceable(self):
        self.assertEqual(
            Taxonomy(["bla","hla","kla"])[:0],
            Taxonomy([])
        )

        self.assertEqual(
            Taxonomy(["bla", "hla", "kla"])[:1],
            Taxonomy(["bla"])
        )

        self.assertEqual(
            Taxonomy(["bla", "hla", "kla"])[:2],
            Taxonomy(["bla","hla"])
        )

        self.assertEqual(
            Taxonomy(["bla", "hla", "kla"])[:3],
            Taxonomy(["bla", "hla","kla"])
        )

        self.assertEqual(
            Taxonomy(["bla", "hla", "kla"])[:4],
            Taxonomy(["bla", "hla", "kla"])
        )


    def test_taxonomy_is_not_sliceable_with_start_or_step(self):
        with self.assertRaises(KeyError):
            Taxonomy(["bla","hla","kla"])[2:3]

        with self.assertRaises(KeyError):
            Taxonomy(["bla","hla","kla"])[1:0]

        with self.assertRaises(KeyError):
            Taxonomy(["bla","hla","kla"])[0:1:4]

    def test_two_empty_taxonomies_are_always_equal(self):
        self.givenTaxonomy()
        self.assertIsEqualTo([])
        self.assertIsEqualTo(["",""])
        self.assertIsEqualTo([None])

    def test_wrong_taxonomy_is_equal_to_nothing_but_itself(self):

        self.givenWrongTaxonomy()

        self.assertIsNOTEqualTo(
            ["bla", "kla", "hla"],
        )

        self.assertIsNOTEqualTo(
            ["bla", "kla"],
        )

        self.assertIsNOTEqualTo(
            ["bla"],
        )

        self.assertIsNOTEqualTo(
            [],
        )

        self.assertIsEqualTo(
            WrongTaxonomy(),
        )

        self.givenTaxonomy("bla", "kla", "hla")

        self.assertIsNOTEqualTo(
            WrongTaxonomy(),
        )

        self.givenTaxonomy()

        self.assertIsNOTEqualTo(
            WrongTaxonomy(),
        )



    def test_wrong_taxonomy_is_parent_of_nothing(self):

        self.givenWrongTaxonomy()

        self.assertIsNOTParentOf(
            ["bla", "kla", "hla"],
        )

        self.assertIsNOTParentOf(
            ["bla", "kla"],
        )

        self.assertIsNOTParentOf(
            ["bla"],
        )

        self.assertIsNOTParentOf(
            [],
        )

    def test_nothing_is_parent_of_wrong_taxonomy(self):

        self.givenTaxonomy("bla", "kla", "hla")

        self.assertIsNOTParentOf(
            WrongTaxonomy(),
        )

        self.givenTaxonomy()

        self.assertIsNOTParentOf(
            WrongTaxonomy(),
        )




    def givenWrongTaxonomy(self):
        self.taxonomy = WrongTaxonomy()

    def givenTaxonomy(self,*taxa):
        self.taxonomy = self._makeTaxonomy(taxa)

    def assertTaxonomyAtRank(self,rank,taxon):
        self.assertEqual(
            taxon,
            self.taxonomy[rank]
        )

    def assertTaxonomyIteration(self,*exp):
        self.assertEqual(
            list(exp),
            list(self.taxonomy)
        )

    def assertHasNCommonTaxaWith(self, taxa, val):
        self.assertEqual(
            self.taxonomy.nCommonTaxa(Taxonomy(taxa)),
            val
        )

    def assertIsEqualTo(self,taxa):
        self.assertEqual(
            self.taxonomy,
            self._makeTaxonomy(taxa)
        )

    def _makeTaxonomy(self, taxa):
        if type(taxa) == WrongTaxonomy:
            return taxa
        else:
            return Taxonomy(taxa)

    def assertIsNOTEqualTo(self,taxa):
        self.assertNotEqual(
            self.taxonomy,
            self._makeTaxonomy(taxa)
        )

    def assertIsParentOf(self,taxa):
        self.assertTrue(
            self.taxonomy.isParentOf(self._makeTaxonomy(taxa))
        )

    def assertIsNOTParentOf(self,taxa):
        self.assertFalse(
            self.taxonomy.isParentOf(self._makeTaxonomy(taxa))
        )
