from lib.evaluation.evaluate import *
from pandas import DataFrame
from pandas.testing import assert_frame_equal
import unittest

class TestTopClassifierSelection(unittest.TestCase):
    def test_if_table_does_not_contain_target_metric_it_is_an_error(self):
        self.givenResultsTable(
            DataFrame([
                {"name" : "bla", "command" : "bla.sh","M1" : 1, "M2" : 3}
            ])
        )

        self.givenTargetMetric("M3")

        with self.assertRaises(ValueError):
            self.selectTopClassifier()


    def test_if_table_does_not_contain_name_or_command_it_is_error(self):
        self.givenTargetMetric("M1")

        self.givenResultsTable(
            DataFrame([
                { "command" : "bla.sh","M1" : 1, "M2" : 3}
            ])
        )

        with self.assertRaises(ValueError):
            self.selectTopClassifier()

        self.givenResultsTable(
            DataFrame([
                {"name": "bla.sh", "M1": 1, "M2": 3}
            ])
        )

        with self.assertRaises(ValueError):
            self.selectTopClassifier()

        self.givenResultsTable(
            DataFrame([
                {"M1": 1, "M2": 3}
            ])
        )

        with self.assertRaises(ValueError):
            self.selectTopClassifier()

    def test_one_entry_is_the_winner(self):
        self.givenResultsTable(
            DataFrame([
                {"name" : "bla", "command" : "bla.sh","M1" : 1, "M2" : 3}
            ])
        )

        self.givenTargetMetric("M2")

        self.assertWinnerIs(
            ClassifierProgram("bla","bla.sh")
        )

    def test_if_more_entries_best_entry_is_the_winner(self):
        self.givenResultsTable(
            DataFrame([
                {"name" : "bla", "command" : "bla.sh","M1" : 1, "M2" : 3},
                {"name" : "hla", "command" : "hla.sh","M1" : 1, "M2" : 10},
                {"name" : "kla", "command" : "kla.sh","M1" : 1, "M2" : 4},
            ])
        )

        self.givenTargetMetric("M2")

        self.assertWinnerIs(
            ClassifierProgram("hla","hla.sh")
        )

    def test_selector_can_handle_NA(self):
        self.givenTargetMetric("M2")

        self.givenResultsTable(
            DataFrame([
                {"name" : "bla", "command" : "bla.sh","M1" : 1, "M2" : 3},
                {"name" : "hla", "command" : "hla.sh","M1" : 1, "M2" : 10},
                {"name" : "kla", "command" : "kla.sh","M1" : 1, "M2" : None},
            ])
        )

        self.assertWinnerIs(
            ClassifierProgram("hla","hla.sh")
        )

        self.givenResultsTable(
            DataFrame([
                {"name": "hla", "command": "hla.sh", "M1": 1, "M2": None},
            ])
        )

        self.assertWinnerIs(
            ClassifierProgram("hla", "hla.sh")
        )


    def test_if_table_is_empty_it_is_error(self):
        self.givenResultsTable(
            DataFrame([])
        )

        with self.assertRaises(ValueError):
            self.selectTopClassifier()

    def givenResultsTable(self,table):
        self.table  = table

    def givenTargetMetric(self,metric):
        self.metric = metric

    def selectTopClassifier(self):
        return TopClassifierSelector(self.metric).select(self.table)

    def assertWinnerIs(self,other):
        self.assertEqual(
            self.selectTopClassifier(),
            other
        )

    def setUp(self):
        self.metric = None

class TestAverageCalculation(unittest.TestCase):
    def test_average_can_be_calculated_for_one_classifier(self):
        self.givenResultsTable(
            DataFrame([
                {"name" : "bla","OI" : 0,"II" : 0, "command" : "bla.sh","M1" : 1, "M2" : 3},
                {"name": "bla", "OI": 0, "II": 1, "command": "bla.sh", "M1": 10, "M2": 30},
                {"name": "bla", "OI": 0, "II": 2, "command": "bla.sh", "M1": 4, "M2": 13},
            ])
        )

        self.assertAverageIs(
            DataFrame([
                {"name": "bla", "OI": 0, "command": "bla.sh", "M1": 5, "M2": 15.33333},
            ])
        )

    def test_average_can_be_calculated_for_multiple_classifiers(self):
        self.givenResultsTable(
            DataFrame([
                {"name" : "bla","OI" : 0,"II" : 0, "command" : "bla.sh","M1" : 1, "M2" : 3},
                {"name": "bla", "OI": 0, "II": 1, "command": "bla.sh", "M1": 10, "M2": 30},
                {"name": "bla", "OI": 0, "II": 2, "command": "bla.sh", "M1": 4, "M2": 13},
                {"name": "hla", "OI": 0, "II": 1, "command": "hla.sh", "M1": 1, "M2": 3},
                {"name": "hla", "OI": 0, "II": 2, "command": "hla.sh", "M1": 4, "M2": 13},
            ])
        )

        self.assertAverageIs(
            DataFrame([
                {"name": "bla", "OI": 0, "command": "bla.sh", "M1": 5, "M2": 15.33333},
                {"name": "hla", "OI": 0, "command": "hla.sh", "M1": 2.5, "M2": 8},
            ])
        )


    def test_average_can_be_calculated_for_multiple_classifiers_and_multiple_outer_indices(self):
        self.givenResultsTable(
            DataFrame([
                {"name" : "bla","OI" : 0,"II" : 0, "command" : "bla.sh","M1" : 1, "M2" : 3},
                {"name": "bla", "OI": 0, "II": 1, "command": "bla.sh", "M1": 10, "M2": 30},
                {"name": "bla", "OI": 0, "II": 2, "command": "bla.sh", "M1": 4, "M2": 13},
                {"name": "hla", "OI": 0, "II": 1, "command": "hla.sh", "M1": 1, "M2": 3},
                {"name": "hla", "OI": 0, "II": 2, "command": "hla.sh", "M1": 4, "M2": 13},
                {"name": "hla", "OI": 1, "II": 1, "command": "hla.sh", "M1": 2, "M2": 5},
                {"name": "hla", "OI": 1, "II": 2, "command": "hla.sh", "M1": 4, "M2": 6},
            ])
        )

        self.assertAverageIs(
            DataFrame([
                {"name": "bla", "OI": 0, "command": "bla.sh", "M1": 5, "M2": 15.33333},
                {"name": "hla", "OI": 0, "command": "hla.sh", "M1": 2.5, "M2": 8},
                {"name": "hla", "OI": 1, "command": "hla.sh", "M1": 3, "M2": 5.5},
            ])
        )

    def test_empty_df_empty_average(self):
        self.givenResultsTable(DataFrame([]))
        self.assertAverageIs(DataFrame([]))

        self.givenResultsTable(DataFrame([],columns=["name","command","M1","M2"]))
        self.assertAverageIs(DataFrame([],columns=["name","command","M1","M2"]))

    def givenResultsTable(self,table):
        self.table = table

    def assertAverageIs(self,expected):
        assert_frame_equal(
            self._sortDFCols(expected),
            self._sortDFCols(averageMetrics(self.table)),
            check_dtype=False
        )

    def _sortDFCols(self, df):
        return df.reindex(sorted(df.columns), axis=1)