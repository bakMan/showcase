import unittest
from lib.data.results import *
from pandas import DataFrame
from collections import *
from pandas.testing import *

class TestResultStore(unittest.TestCase):
    def test_selection_by_oindex_invokes_correct_command(self):
        self.givenRootDir("/path")

        self.getDataframeForOuterIndex(0)

        self.assertCommandWasInvoked("cat /path/0_*_*")

        self.givenRootDir("/path")

        self.getDataframeForOuterIndex(1)

        self.assertCommandWasInvoked("cat /path/1_*_*")

        self.givenRootDir("/")

        self.getDataframeForOuterIndex(1)

        self.assertCommandWasInvoked("cat //1_*_*")

        self.givenRootDir("/path/")

        self.getDataframeForOuterIndex(1)

        self.assertCommandWasInvoked("cat /path//1_*_*")

    def test_store_creates_named_data_frame_from_query_result(self):
        self.givenResultQueryResult(
            "Stupid Classifier	bash classifier.sh	0	0	0	1	1	3	0	0	0.6	0.2	0.2	0	0	0	0.6	0.9091	0	0",
            "Dumb Classifier	bash classifier_x.sh	0	0	0	2	3	4	0	0	1.6	2.2	3.2	0	1	0	0.2	0.2091	0	0"
        )

        self.assertDataframeOfArbitraryOuterIndexIs(
            DataFrame([
                OrderedDict(
                    {"name": "Stupid Classifier", "command": "bash classifier.sh", "OI": 0, "II": 0, "OC": 0,
                     "MC": 1, "UC": 1, "TP": 3, "UMC": 0, "UUC": 0, "TPR": 0.6, "MCR": 0.2, "UCR": 0.2, "OCR": 0,
                     "UMCR": 0, "UUCR": 0, "ACC": 0.6, "SCR": 0.9091,"MCC" : 0,"SCR_2" : 0}),
                OrderedDict(
                    {"name": "Dumb Classifier", "command": "bash classifier_x.sh", "OI": 0, "II": 0, "OC": 0,
                     "MC": 2, "UC": 3, "TP": 4, "UMC": 0, "UUC": 0, "TPR": 1.6, "MCR": 2.2, "UCR": 3.2, "OCR": 0,
                     "UMCR": 1, "UUCR": 0, "ACC": 0.2, "SCR": 0.2091,"MCC" : 0,"SCR_2" : 0}),
            ])
        )

    def test_it_can_handle_blank_lines(self):
        self.givenResultQueryResult(
            "Stupid Classifier	bash classifier.sh	0	0	0	1	1	3	0	0	0.6	0.2	0.2	0	0	0	0.6	0.9091	0	0",
            "",
            "",
            "Dumb Classifier	bash classifier_x.sh	0	0	0	2	3	4	0	0	1.6	2.2	3.2	0	1	0	0.2	0.2091	0	0",
            ""
        )

        self.assertDataframeOfArbitraryOuterIndexIs(
            DataFrame([
                OrderedDict(
                    {"name": "Stupid Classifier", "command": "bash classifier.sh", "OI": 0, "II": 0, "OC": 0,
                     "MC": 1, "UC": 1, "TP": 3, "UMC": 0, "UUC": 0, "TPR": 0.6, "MCR": 0.2, "UCR": 0.2, "OCR": 0,
                     "UMCR": 0, "UUCR": 0, "ACC": 0.6, "SCR": 0.9091,"MCC" : 0,"SCR_2" : 0}),
                OrderedDict(
                    {"name": "Dumb Classifier", "command": "bash classifier_x.sh", "OI": 0, "II": 0, "OC": 0,
                     "MC": 2, "UC": 3, "TP": 4, "UMC": 0, "UUC": 0, "TPR": 1.6, "MCR": 2.2, "UCR": 3.2, "OCR": 0,
                     "UMCR": 1, "UUCR": 0, "ACC": 0.2, "SCR": 0.2091,"MCC" : 0,"SCR_2" : 0}),
            ])
        )

    def test_store_creates_named_data_frame_from_result_with_single_line(self):
        self.givenResultQueryResult(
            "Stupid Classifier	bash classifier.sh	0	0	0	1	1	3	0	0	0.6	0.2	0.2	0	0	0	0.6	0.9091	0	0"
        )

        self.assertDataframeOfArbitraryOuterIndexIs(
            DataFrame([
                OrderedDict(
                    {"name": "Stupid Classifier", "command": "bash classifier.sh", "OI": 0, "II": 0, "OC": 0,
                     "MC": 1, "UC": 1, "TP": 3, "UMC": 0, "UUC": 0, "TPR": 0.6, "MCR": 0.2, "UCR": 0.2, "OCR": 0,
                     "UMCR": 0, "UUCR": 0, "ACC": 0.6, "SCR": 0.9091,"MCC" : 0,"SCR_2" : 0})
                ])
        )


    def test_empty_query_results_(self):
        self.givenResultQueryResult(
            ""
        )

        self.assertDataframeOfArbitraryOuterIndexIs(
            DataFrame()
        )

        self.givenResultQueryResult(
            "    "
        )

        self.assertDataframeOfArbitraryOuterIndexIs(
            DataFrame()
        )

        self.givenResultQueryResult(
            "    \t\t\t\n\n\n\n\t\t\t"
        )

        self.assertDataframeOfArbitraryOuterIndexIs(
            DataFrame()
        )


    def test_malformed_dataframe_results_in_error(self):
        self.givenResultQueryResult(
            "Stupid Classifier	bash classifier.sh	0	0	0	1	1	3	0	0	0.6	0.2	0.2	0	0	0	0.6	0.9091",
            "Dumb Classifier	bash classifier_x.sh	0	0	0	2	3	4	0	0	1.6	2.2	3.2	0	1	0	0.2"
        )

        with self.assertRaises(InvalidResultsFormat):
            self.getDataForArbitraryOuterIndex()

        self.givenResultQueryResult(
            "Stupid Classifier	bash classifier.sh	0	0	0	1	1	3	0	0	0.6	0.2	0.2	0	0	0	0.6",
            "Dumb Classifier	bash classifier_x.sh	0	0	0	2	3	4	0	0	1.6	2.2	3.2	0	1	0	0.2"
        )

        with self.assertRaises(InvalidResultsFormat):
            self.getDataForArbitraryOuterIndex()

        self.givenResultQueryResult(
            "Stupid Classifier	bash classifier.sh	0	a	0	1	1	3	0	0	0.6	0.2	0.2	0	0	0	0.6	0.9091",
            "Dumb Classifier	bash classifier_x.sh	0	0	0	2	3	4	0	0	1.6	2.2	3.2	0	1	0	0.2	0.2091"

        )

        with self.assertRaises(InvalidResultsFormat):
            self.getDataForArbitraryOuterIndex()


        self.givenResultQueryResult(
            "Stupid Classifier	bash classifier.sh	0	1	0	1	1	3	0	0	0.6	0.2	0.2	0	ahoj	0	0.6	0.9091",
            "Dumb Classifier	bash classifier_x.sh	0	0	0	2	3	4	0	0	1.6	2.2	3.2	vole	1	0	0.2	0.2091"

        )

        with self.assertRaises(InvalidResultsFormat):
            self.getDataForArbitraryOuterIndex()


        self.givenResultQueryResult(
            "Stupid Classifier	bash classifier.sh	0	1	0	1	1	3	0	0	0.6	0.2	0.2	0	ahoj	0	0.6	0.9091",
            "Dumb Classifier	bash classifier_x.sh	0	nan	0	2	3	4	0	0	1.6	2.2	3.2	vole	1	0	0.2	0.2091"

        )

        with self.assertRaises(InvalidResultsFormat):
            self.getDataForArbitraryOuterIndex()

    def setUp(self):
        self.root = None
        self.observed_command = None
        self.query_result = ""

    def givenRootDir(self,root):
        self.root = root

    def getDataframeForOuterIndex(self,oindex):
        return InnerResultsStore(self.root, filesystem=self, command_executor = self).resultsOfOuterFold(oindex)

    def assertCommandWasInvoked(self,expected_command):
        self.assertEqual(
            expected_command,
            self.observed_command
        )

    def executeCommand(self,observed_command):
        self.observed_command = observed_command
        return self.query_result

    def givenResultQueryResult(self,*lines):
        self.query_result = "\n".join(lines)

    def assertDataframeOfArbitraryOuterIndexIs(self,exp_df):
        obs_df = self.getDataForArbitraryOuterIndex()
        assert_frame_equal(
            exp_df,
            obs_df,
            check_dtype=False
        )

    def getDataForArbitraryOuterIndex(self):
        return self.getDataframeForOuterIndex(0)