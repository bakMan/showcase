from unittest import TestCase

from lib.data.cv import *
import unittest

import os.path

def normalizePath(path:str):
    return path.replace("/", os.path.sep)

class TestCVDataset(unittest.TestCase):
    def test_cv_dataset_builds_master_split_correctly(self):
        self.givenRoot("/bla")

        self.performLoading()

        self.assertMasterSplitCreatedWithSets(
            pth.join("/bla",TRAINING_SET_FILENAME),
            pth.join("/bla",TESTING_SET_FILENAME)
        )

    def test_cv_dataset_loads_subdatasets_from_root(self):
        self.givenRoot("/bla")

        self.performLoading()

        self.assertSubdataLoadedFrom("/bla")

    def test_on_iteration_cv_iterates_through_subdatasets(self):
        self.givenSubdatasets("one","two","three")

        self.performLoading()

        self.assertIterationResultsIn("one","two","three")

    def test_cv_dataset_is_indexable(self):
        self.givenSubdatasets("one","two","three")

        self.performLoading()

        self.assertItemAtIndexIs(0,"one")
        self.assertItemAtIndexIs(1,"two")
        self.assertItemAtIndexIs(2,"three")

        with self.assertRaises(IndexError):
            self.assertItemAtIndexIs(4,"bla")


    def setUp(self):
        self.root = os.path.sep
        self.master_split = None
        self.subdatasets = []

    def givenRoot(self,root):
        self.root = normalizePath(root)

    def givenSubdatasets(self,*subdatasets):
        self.subdatasets = list(subdatasets)

    def performLoading(self):
        self.cvdataset = CVDataset(self.root,
            split_factory = self.splitFactory,
            subdata_loader = self.loadSubdata
        )


    def splitFactory(self, training_set_path, testing_set_path):
        self.master_split = (training_set_path,testing_set_path)

    def loadSubdata(self,path):
        self.subdata_load_path = path
        return self.subdatasets

    def assertMasterSplitCreatedWithSets(self,exp_train,exp_test):
        self.assertEqual(
            self.master_split,
            (normalizePath(exp_train),normalizePath(exp_test))
        )

    def assertSubdataLoadedFrom(self,path):
        self.assertEqual(normalizePath(path),self.subdata_load_path)

    def assertIterationResultsIn(self,*exp_subdatasets):
        self.assertEqual(
            list(exp_subdatasets),
            list(self.cvdataset)
        )

    def assertItemAtIndexIs(self,index,item):
        self.assertEqual(
            self.cvdataset[index],
            item
        )


class LoaderTest(TestCase, metaclass=ABCMeta):
    def setUp(self):
        self.factory_arguments = set()
        self.path = os.path.sep
        self.listing = {}
        self.listed_path = None

    def givenPath(self,path):
        self.path = normalizePath(path)

    def givenListing(self,*listing):
        self.listing = {normalizePath(path):type for path,type in listing}

    def datasetFactory(self, path):
        self.factory_arguments.add(path)
        return "x"

    @abstractmethod
    def buildLoaderInstance(self, filesystem, datasetFactory):
        pass

    def performLoading(self):
        loader = self.buildLoaderInstance(self,self.datasetFactory)
        loader(self.path)

    def assertPathWasListed(self, path):
        self.assertEqual(
            self.listed_path,
            normalizePath(path)
        )

    def list(self,path):
        self.listed_path = path
        return set(self.listing.keys())

    def isDir(self,path):
        return self.listing.get(path,None) == "directory"


    def assertDatasetFactoryCalledFor(self,*paths):
        self.assertEqual(
            set(paths),
            self.factory_arguments
        )




class TestInnerCVLoader(LoaderTest):
    def assertDatasetFactoryCalledFor(self,*paths):
        super().assertDatasetFactoryCalledFor(
            *(normalizePath(path) for path in paths)
        )

    def test_loader_creates_cv_subdatasets(self):
        self.givenListing(
            ("/0","directory"),
            ("/1","directory"),
            ("/2","directory"),
            ("/3","directory"),
            ("/4","directory"),
        )

        self.performLoading()

        self.assertDatasetFactoryCalledFor("/0","/1","/2","/3","/4")

    def test_loader_lists_files_from_provided_path(self):
        self.givenPath("/bla")

        self.performLoading()

        self.assertPathWasListed("/bla")

    def test_nondirectories_are_ignored(self):
        self.givenListing(
            ("/0","directory"),
            ("/1","file"),
            ("/2","directory"),
            ("/3","file"),
            ("/4","directory"),
        )

        self.performLoading()

        self.assertDatasetFactoryCalledFor("/0","/2","/4")

    def test_empty_directory_means_no_datasets(self):
        self.givenListing()

        self.performLoading()

        self.assertDatasetFactoryCalledFor()

    def test_wrong_directory_names_are_ignored(self):
        self.givenListing(
            ("/0","directory"),
            ("/1_2","directory"),
            ("/2fsdfs","directory"),
            ("/fsddfsdfs","directory"),
            ("/4.4","directory"),
            ("/-4","directory"),
            ("/3/5/3/-4","directory"),
        )

        self.performLoading()

        self.assertDatasetFactoryCalledFor("/0")

    def buildLoaderInstance(self,filesystem,datasetFactory):
        return InnerCVDatasetLoader(filesystem=filesystem, cv_dataset_factory=datasetFactory)

class TestCVSplitsLoader(LoaderTest):
    def assertDatasetFactoryCalledFor(self,*paths):
        super().assertDatasetFactoryCalledFor(
            *( (normalizePath(train_path),normalizePath(test_path)) for train_path,test_path in paths)
        )

    def buildLoaderInstance(self,filesystem,datasetFactory):
        return CVSplitsLoader(filesystem=filesystem, split_factory=datasetFactory)

    def test_splits_loader_lists_correct_directory(self):
        self.givenPath("/bla")
        self.performLoading()
        self.assertPathWasListed("/bla/inner")

    def test_splits_loader_loads_single_dataset(self):
        self.givenListing(
            ("0_train.fasta","file"),
            ("0_test.fasta","file")
        )

        self.performLoading()

        self.assertDatasetFactoryCalledFor(
            ("/inner/0_train.fasta","/inner/0_test.fasta")
        )

    def test_multiple_datasets_can_be_loaded(self):

        self.givenListing(
            ("0_train.fasta","file"),
            ("0_test.fasta", "file"),
            ("1_train.fasta", "file"),
            ("1_test.fasta", "file"),
            ("2_train.fasta", "file"),
            ("2_test.fasta", "file")
        )
        self.performLoading()

        self.assertDatasetFactoryCalledFor(
            ("/inner/0_train.fasta","/inner/0_test.fasta"),
            ("/inner/1_train.fasta", "/inner/1_test.fasta"),
            ("/inner/2_train.fasta", "/inner/2_test.fasta")
        )


    def test_ordering_of_files_should_not_matter(self):

        self.givenListing(
            ("2_test.fasta", "file"),
            ("0_train.fasta","file"),
            ("1_train.fasta", "file"),
            ("0_test.fasta","file"),
            ("1_test.fasta", "file"),
            ("2_train.fasta", "file")
        )
        self.performLoading()

        self.assertDatasetFactoryCalledFor(
            ("/inner/0_train.fasta","/inner/0_test.fasta"),
            ("/inner/1_train.fasta", "/inner/1_test.fasta"),
            ("/inner/2_train.fasta", "/inner/2_test.fasta")
        )


    def test_directories_are_ignored(self):

        self.givenListing(
            ("0_train.fasta","file"),
            ("0_test.fasta", "file"),
            ("1_train.fasta", "file"),
            ("1_test.fasta", "file"),
            ("2_train.fasta", "file"),
            ("2_test.fasta", "file"),
            ("3_test.fasta", "directory"),
            ("3_test.fasta", "directory")
        )
        self.performLoading()

        self.assertDatasetFactoryCalledFor(
            ("/inner/0_train.fasta","/inner/0_test.fasta"),
            ("/inner/1_train.fasta", "/inner/1_test.fasta"),
            ("/inner/2_train.fasta", "/inner/2_test.fasta")
        )

    def test_files_with_malformed_names_are_ignored(self):

        self.givenListing(
            ("0_train.fasta","file"),
            ("0_test.fasta", "file"),
            ("1_train.fasta", "file"),
            ("1_test.fasta", "file"),
            ("2_train.fasta", "file"),
            ("2_test.fasta", "file"),
            ("200_train.fasta", "file"),
            ("200_test.fasta", "file"),
            ("ballbal","file"),
            ("034938402","file"),
            ("train.fasta", "file"),
            ("test.fasta", "file"),
            ("_train.fasta", "file"),
            ("_test.fasta", "file"),
            ("0002_train.fasta","file"),
            ("0002_test.fasta","file"),
            ("2a_train.fasta", "file"),
            ("2a_test.fasta", "file"),
            ("1x2a_train.fasta", "file"),
            ("1x2a_test.fasta", "file"),
            ("1_2_train.fasta", "file"),
            ("1_2_test.fasta", "file"),
            ("10__train.fasta", "file"),
            ("10__test.fasta", "file")
        )
        self.performLoading()

        self.assertDatasetFactoryCalledFor(
            ("/inner/0_train.fasta","/inner/0_test.fasta"),
            ("/inner/1_train.fasta", "/inner/1_test.fasta"),
            ("/inner/2_train.fasta", "/inner/2_test.fasta"),
            ("/inner/200_train.fasta", "/inner/200_test.fasta")
        )

    def test_if_one_if_one_from_pair_is_missing_dataset_is_ignored(self):

        self.givenListing(
            ("0_train.fasta","file"),
            ("1_test.fasta", "file"),
            ("2_train.fasta", "file"),
            ("2_test.fasta", "file")
        )
        self.performLoading()

        self.assertDatasetFactoryCalledFor(
            ("/inner/2_train.fasta", "/inner/2_test.fasta")
        )

    def exists(self, path):
        dir,file = pth.split(normalizePath(path))

        if dir != pth.join(self.path,INNER_CV_SUBDIRECTORY):
            return False

        return file in self.listing


if __name__ == '__main__':
    unittest.main()
