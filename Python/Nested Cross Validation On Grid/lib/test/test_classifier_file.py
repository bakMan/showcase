from lib.io.classifierfile import *
import unittest

class TestClassifierFileLoader(unittest.TestCase):
    def test_empty_file_means_no_classifier_programs(self):
        self.givenClassifierFile("")
        self.read()
        self.assertHasClassifiers()

        self.givenClassifierFile("              \t\t\t\n\n")
        self.read()
        self.assertHasClassifiers()

    def test_file_with_one_entry_can_be_parsed(self):
        self.givenClassifierFile("StupidClassifier\tbanana.sh -c")
        self.read()
        self.assertHasClassifiers(
            ("StupidClassifier","banana.sh -c")
        )

    def test_file_with_more_entries_can_be_parsed(self):
        self.givenClassifierFile(
            "StupidClassifier\tbanana.sh -c\n"
            "DumbClassifier\tbanana.sh -d"
        )

        self.read()
        self.assertHasClassifiers(
            ("StupidClassifier","banana.sh -c"),
            ("DumbClassifier","banana.sh -d")
        )


    def test_extra_whitespace_is_ignored(self):
        self.givenClassifierFile(
            "   StupidClassifier  \t    banana.sh -c   \n"
            "DumbClassifier\t  banana.sh -d "
        )

        self.read()
        self.assertHasClassifiers(
            ("StupidClassifier","banana.sh -c"),
            ("DumbClassifier","banana.sh -d")
        )

    def test_blank_lines_are_ignored(self):
        self.givenClassifierFile(
            "\n\n"
            "StupidClassifier\tbanana.sh -c\n"
            "DumbClassifier\tbanana.sh -d\n\n"
            "ExtraDumbClassifier\tbanana.sh -ed"
            "\n\n\n\n"
        )

        self.read()
        self.assertHasClassifiers(
            ("StupidClassifier","banana.sh -c"),
            ("DumbClassifier","banana.sh -d"),
            ("ExtraDumbClassifier", "banana.sh -ed")
        )

    def test_name_of_classifier_must_contain_only_characters_allowed_for_file_otherwise_it_is_ignored(self):
        self.givenClassifierFile(
            "StupidClassifier\tbanana.sh -c\n"
            "DumbClassifier..\tbanana.sh -d\n"
            "Elef?ant\tbanana.sh -d\n"
            "OK\tbanana.sh -c\n"            
            "Elef%ant\tbanana.sh -d\n"
            "Buddh*a\tbanana.sh -d\n"
            "Buddh/a\tbanana.sh -d\n"
            "Apple\tbanana.sh -c\n"
            "FI:T\tbanana.sh -c\n"
            "FI|T\tbanana.sh -c\n"
            "FI\"T\tbanana.sh -c\n"
            "FI>T\tbanana.sh -c\n"
            "FI<T\tbanana.sh -c\n"
            "FI.T\tbanana.sh -c\n"
            "FI    T\tbanana.sh -c\n"
            "FI_T\tbanana.sh -c\n"
            "Hell\tbanana.sh -c\n"
            "He-ll\tbanana.sh -c\n"
        )

        self.read()
        self.assertHasClassifiers(
            ("StupidClassifier", "banana.sh -c"),
            ("OK", "banana.sh -c"),
            ("Apple", "banana.sh -c"),
            ("Hell", "banana.sh -c"),
            ("He-ll", "banana.sh -c")
        )


    def test_more_columns_are_ignored(self):
        self.givenClassifierFile(
            "StupidClassifier\tbanana.sh -c\textra\tsuper\t\t\n"
            "DumbClassifier\tbanana.sh -d\thahaha"
        )

        self.read()
        self.assertHasClassifiers(
            ("StupidClassifier","banana.sh -c"),
            ("DumbClassifier","banana.sh -d")
        )


    def test_if_record_does_not_have_enough_columns_it_is_ignored(self):
        self.givenClassifierFile(
            "StupidClassifier\tbanana.sh -c\n"
            "NotOK\n"
            "DumbClassifier\tbanana.sh -d"
        )

        self.read()
        self.assertHasClassifiers(
            ("StupidClassifier","banana.sh -c"),
            ("DumbClassifier","banana.sh -d")
        )


    def test_if_record_has_empty_command_or_name_it_is_ignored(self):
        self.givenClassifierFile(
            "StupidClassifier\tbanana.sh -c\n"
            "NotOK\t\n"
            "DumbClassifier\tbanana.sh -d\n"
            "\tbanana.sh -d"
        )

        self.read()
        self.assertHasClassifiers(
            ("StupidClassifier","banana.sh -c"),
            ("DumbClassifier","banana.sh -d")
        )



    def setUp(self):
        self.path = ""
        self.content = ""
        self.classifiers = []

    def givenClassifierFile(self,content):
        self.content = content

    def read(self):
        self.classifiers = list(
            ClassifierFileReader(filesystem=self,classifier_program_factory=self.classifierFactory).read(self.path)
        )

    def assertHasClassifiers(self,*exp_classifiers):
        self.assertEqual(
            list(exp_classifiers),
            self.classifiers
        )

    def openForReading(self,file):
        yield from (self.content.split("\n"))

    def classifierFactory(self,name,command):
        return (name,command)

if __name__ == '__main__':
    unittest.main()