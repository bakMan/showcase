from lib.data.dataset import *
from lib.util.taxonomy import *
import unittest

class TestFileDataSplit(unittest.TestCase):
    def test_access_to_training_or_testing_test_triggers_reading_of_both(self):
        self.givenSets(
            training_set="training_set",
            testing_set="testing_set"
        )

        self.createFileDataSplit().training_set

        self.assertReaderRead("training_set")
        self.assertReaderRead("testing_set")


    def givenSets(self,training_set,testing_set):
        self.training_set = training_set
        self.testing_set  = testing_set

    def createFileDataSplit(self):
        return FileDatasetSplit(self.training_set,self.testing_set,self.reader)

    def setUp(self):
        self.read = set()
        self.sets = {}

    def reader(self,what):
        self.read.add(what)
        return self.sets.get(what,[])

    def assertReaderRead(self,what):
        self.assertTrue(what in self.read)

    def assertReaderHaveNotRead(self,what):
        self.assertTrue(what not in self.read)


class TestDatasetReader(unittest.TestCase):
    def test_reader_can_read_dataset(self):
        self.givenFileHasSequences(
            (
                "gi_343202481;tax=d:Bacteria,p:Actinobacteria,c:Actinobacteria,o:Streptomycetales,f:Streptomycetaceae,g:Streptomyces,s:Streptomyces_griseus;",
                "ACTG"

            ),
           (
               "gi_219846447;tax=d:Bacteria,p:Firmicutes,c:Clostridia,o:Clostridiales,f:Peptococcaceae,g:Desulfitobacterium,s:Desulfitobacterium_chlororespirans;",
               "GTGA"
           )
        )

        self.assertReadDatasetHasSequences(
            "ACTG",
            "GTGA"
        )

        self.assertReadDatasetHasTaxonomies(
            ["Bacteria","Actinobacteria","Actinobacteria","Streptomycetales","Streptomycetaceae","Streptomyces","Streptomyces_griseus"],
            ["Bacteria","Firmicutes","Clostridia","Clostridiales","Peptococcaceae","Desulfitobacterium","Desulfitobacterium_chlororespirans"]
        )

    def test_reader_can_read_dataset_with_one_sequence(self):
        self.givenFileHasSequences(
            (
                "gi_343202481;tax=d:Bacteria,p:Actinobacteria,c:Actinobacteria,o:Streptomycetales,f:Streptomycetaceae,g:Streptomyces,s:Streptomyces_griseus;",
                "ACTG"

            ),
        )

        self.assertReadDatasetHasSequences(
            "ACTG",
        )

        self.assertReadDatasetHasTaxonomies(
            ["Bacteria","Actinobacteria","Actinobacteria","Streptomycetales","Streptomycetaceae","Streptomyces","Streptomyces_griseus"],
        )

    def test_reader_can_read_dataset_with_zero_sequences(self):
        self.givenFileHasSequences()

        self.assertReadDatasetHasSequences()

        self.assertReadDatasetHasTaxonomies()

    def test_reader_ignores_sequence_with_malformed_id(self):
        self.givenFileHasSequences(
            (
                "gi_343202481;tax=d:Bacteria,p:Actinobacteria,c:Actinobacteria,o:Streptomycetales,f:Streptomycetaceae,g:Streptomyces,s:Streptomyces_griseus;",
                "ACTG"

            ),
           (
               "gi_219846447;d:Bacteria,p:Firmicutes,c:Clostridia,o:Clostridiales,f:Peptococcaceae,g:Desulfitobacterium,s:Desulfitobacterium_chlororespirans;",
               "GTGA"
           )
        )

        self.assertReadDatasetHasSequences(
            "ACTG",
        )

        self.assertReadDatasetHasTaxonomies(
            ["Bacteria","Actinobacteria","Actinobacteria","Streptomycetales","Streptomycetaceae","Streptomyces","Streptomyces_griseus"],
        )

    def givenFileHasSequences(self,*sequences):
        self.file_sequences = list(sequences)

    def assertReadDatasetHasSequences(self,*sequences):
        self.assertEqual(
            self._readDataset().sequences,
            list(sequences)
        )

    def assertReadDatasetHasTaxonomies(self,*taxonomies):
        self.assertEqual(
            self._readDataset().taxonomies,
            list(Taxonomy(taxonomy) if taxonomy is not None else None  for taxonomy in taxonomies)
        )

    def _readDataset(self):
        return DatasetReader(self.readFASTA)("")


    def readFASTA(self,_):
        return self.file_sequences
if __name__ == '__main__':
    unittest.main()
