import unittest
from lib.util.taxxi import *
from lib.util.taxonomy import *
class TestTaxxiParsing(unittest.TestCase):
    def test_parser_handles_empty_taxonomy(self):
        self.assertEqual(
            TAXXITaxonomyParser().parse(""),
            Taxonomy([])
        )

    def test_parser_handles_shorter_taxonomy(self):
        self.assertEqual(
            TAXXITaxonomyParser().parse("d:Bacteria,p:Actinobacteria,c:Actinobacteria"),
            Taxonomy(["Bacteria","Actinobacteria","Actinobacteria"])
        )

    def test_parser_handles_full_taxonomy(self):
        self.assertEqual(
            TAXXITaxonomyParser().parse("d:Bacteria,p:Actinobacteria,c:Actinobacteria,o:Streptomycetales"\
                                        ",f:Streptomycetaceae,g:Streptomyces,s:Streptomyces_bohaiensis"),
            Taxonomy(["Bacteria","Actinobacteria","Actinobacteria","Streptomycetales","Streptomycetaceae","Streptomyces","Streptomyces_bohaiensis"])
        )