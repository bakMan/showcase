from lib.dispatch.command_builder import *
from lib.evaluation.classifier_program import *
import unittest


class TestCommandBuilder(unittest.TestCase):
    def test_command_builder_builds_command_for_classifier_evaluation(self):
        self.givenResultPaths("/inner/results","/outer/results")
        self.givenRank(5)
        self.givenExecutablePath("/this/script.sh")
        self.givenRootPath("/dataset")
        self.givenClassifierProgram(ClassifierProgram("banana","banana.sh -xyz"))
        self.givenIndexes(2,3)
        self.givenInnerResultsTSV("inner_results.tsv")
        self.givenOuterResultsTSV("outer_results.tsv")

        self.buildCommandBuilder()

        self.assertClassifierEvaluationCommandIs(
            "/this/script.sh evaluate_classifier --name 'banana' --command 'banana.sh -xyz' --dataset-root '/dataset' --outer-index 2 --inner-index 3 --rank 5 > '/inner/results/2_3_banana'"
        )

    def test_if_rank_is_negative_or_noninteger_it_is_an_error(self):
        self.givenResultPaths("/inner/results","/outer/results")
        self.givenRank(-5)
        self.givenExecutablePath("/this/script.sh")
        self.givenRootPath("/dataset")
        self.givenClassifierProgram(ClassifierProgram("banana","banana.sh -xyz"))
        self.givenIndexes(2,3)
        self.givenInnerResultsTSV("inner_results.tsv")
        self.givenOuterResultsTSV("outer_results.tsv")

        with self.assertRaises(CommandBuilderError):
            self.buildCommandBuilder()

        self.givenResultPaths("/inner/results", "/outer/results")
        self.givenRank(None)
        self.givenExecutablePath("/this/script.sh")
        self.givenRootPath("/dataset")
        self.givenClassifierProgram(ClassifierProgram("banana", "banana.sh -xyz"))
        self.givenIndexes(2, 3)
        self.givenInnerResultsTSV("inner_results.tsv")
        self.givenOuterResultsTSV("outer_results.tsv")

        with self.assertRaises(CommandBuilderError):
            self.buildCommandBuilder()

        self.givenResultPaths("/inner/results", "/outer/results")
        self.givenRank(5)
        self.givenExecutablePath("/this/script.sh")
        self.givenRootPath("/dataset")
        self.givenClassifierProgram(ClassifierProgram("banana", "banana.sh -xyz"))
        self.givenIndexes(2, 3)
        self.givenInnerResultsTSV("inner_results.tsv")
        self.givenOuterResultsTSV("outer_results.tsv")

    def test_command_builder_builds_command_for_outer_fold_evaluation(self):
        self.givenResultPaths("/inner/results", "/outer/results")
        self.givenRank(5)
        self.givenExecutablePath("/this/script.sh")
        self.givenRootPath("/dataset")
        self.givenClassifierProgram(ClassifierProgram("banana", "banana.sh -xyz"))
        self.givenIndexes(2, 3)
        self.givenInnerResultsTSV("inner_results.tsv")
        self.givenOuterResultsTSV("outer_results.tsv")

        self.buildCommandBuilder()

        self.assertOuterEvaluationCommandIs(
            "/this/script.sh evaluate_outer_fold --results-path '/inner/results' --dataset-root '/dataset' --outer-index 2 --rank 5 --selection-metric 'SCR' > '/outer/results/2.tsv'"
        )


    def test_command_builder_builds_command_for_collector(self):
        self.givenRootPath("/dataset")
        self.givenRank(5)
        self.givenResultPaths("/inner/results", "/outer/results")
        self.givenExecutablePath("/this/script.sh")
        self.givenInnerResultsTSV("/xx/inner_results.tsv")
        self.givenOuterResultsTSV("/yy/outer_results.tsv")

        self.buildCommandBuilder()

        self.assertCollectorCommandIs(
            "/this/script.sh collect_results --results-path '/inner/results' --outer-results-dir '/outer/results' --inner-results-tsv '/xx/inner_results.tsv' --outer-results-tsv '/yy/outer_results.tsv' --keep-folders"
        )



    def test_command_builder_builds_command_for_outer_evaluation(self):
        #todo
        pass
    def __getattr__(self, item):
        if item.startswith("given"):
            element_name = item.replace("given","").lower()
            def f(*args):
                val = None
                if len(args) == 1:
                    val = args[0]
                else:
                    val = tuple(args)

                setattr(self,element_name,val)

            return f
        else:
            raise AttributeError("No such attribute {}".format(item))

    def buildCommandBuilder(self):
        self.builder = CommandBuilder(
            inner_results_path = self.resultpaths[0],
            outer_results_path = self.resultpaths[1],
            executable_path    = self.executablepath,
            dataset_path       = self.rootpath,
            rank               = self.rank,
            inner_results_tsv  = self.innerresultstsv,
            outer_results_tsv  = self.outerresultstsv
        )

    def assertClassifierEvaluationCommandIs(self,expected_cmd):
        self.classifier_evaluation_command = self.builder.buildClassifierEvaluationCommand(
            classifier_program = self.classifierprogram,
            outer_index = self.indexes[0],
            inner_index = self.indexes[1]
        )
        self.assertEqual(
            self.classifier_evaluation_command,
            expected_cmd
        )

    def assertOuterEvaluationCommandIs(self,expected_cmd):
        cmd = self.builder.buildOuterEvaluationCommand(outer_index=self.indexes[0])
        self.assertEqual(
            expected_cmd,
            cmd
        )

    def assertCollectorCommandIs(self,expected_command):
        self.assertEqual(
            self.builder.buildCollectorCommand(),
            expected_command
        )


if __name__ == '__main__':
    unittest.main()