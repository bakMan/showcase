import os.path as pth
class mockFile:
    def __init__(self,contents):
        self.read_contents_lines = contents.split("\n")
        self.written_contents = ""

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __iter__(self):
        yield from self.read_contents_lines

    def readline(self):
        if len(self.read_contents_lines) == 0:
            return ""
        else:
            return self.read_contents_lines.pop(0) + "\n"

    def write(self,string):
        self.written_contents = self.written_contents + string

    def getWrittenContents(self):
        return self.written_contents

    def read(self):
        return "\n".join(self.read_contents_lines) + "\n"

class baseFilesystem:
    def __init__(self):
        self.removed = []

    def remove(self,uri):
        self.removed.append(uri)

class mockFilesystem(baseFilesystem):

    ABSOLUTE_PATH_PREFIX = "abs"

    def __init__(self,filename_contents_map):
        super().__init__()
        self.filename_file_map = {filename:mockFile(contents) for filename,contents in filename_contents_map.items()}
        self.created_directories = []
        self.existing_directories = []
        self.removed = []

    def openForWriting(self, path):
        return self.filename_file_map[path]

    def openForReading(self, path):
        return self.openForWriting(path)

    def list(self,uri):
        return list(self.filename_file_map.keys())

    def getWrittenContentsOfFile(self,filename):
        return self.filename_file_map[filename].getWrittenContents()

    def mkDir(self,path):
        self.created_directories.append(path)

    def exists(self,path):
        return (path in self.filename_file_map or path in self.existing_directories or path in self.created_directories) \
               and not (path in self.removed)

    def setExistingDirectory(self,directory):
        self.existing_directories.append(directory)

    def setFile(self,filename,contents):
        self.filename_file_map[filename] = contents

    def isDir(self,path):
        return path in self.existing_directories or path in self.created_directories

    def abspath(self,path):
        return pth.join(self.ABSOLUTE_PATH_PREFIX,path) if not path.startswith(self.ABSOLUTE_PATH_PREFIX) else path

    def remove(self,uri):
        self.removed.append(uri)

class mockConstantFilesystem(baseFilesystem):
    def __init__(self,contents):
        super().__init__()
        self.file = mockFile(contents)
        self.removed = []

    def openForWriting(self, *args, **kwargs):
        return self.file

    def openForReading(self, *args, **kwargs):
        return self.openForWriting(*args,**kwargs)

    def list(self,*args,**kwargs):
        return []

    def getWrittenContents(self):
        return self.file.getWrittenContents()



class mockFilesystemThrowingError:
    def list(self,uri):
        raise ValueError("This error shall not pass !")