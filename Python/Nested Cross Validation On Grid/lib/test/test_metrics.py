from lib.evaluation.metrics import *
import unittest

class TestMetricsCalculator(unittest.TestCase):
    def test_it_can_calculate_metrics_on_one_rank_with_all_taxa_present(self):
        self.givenRank(0)

        self.givenTrainingSetTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Berdo"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Cerdo"],
            ["Berdo"],
        )

        self.givenActualTaxonomies(
            ["Ferdo"],
            ["Berdo"],
            ["Cerdo"],
            [],
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR",0.5)
        self.assertMetricEquals("ACC", 0.5)
        self.assertMetricEquals("UCR",0.25)
        self.assertMetricEquals("MCR",0.25)
        self.assertMetricEquals("OCR", 0)
        self.assertMetricEquals("UUCR", 0)
        self.assertMetricEquals("UMCR", 0)

    def test_it_can_calculate_metrics_on_one_rank_with_some_taxa_absent(self):
        self.givenRank(0)

        self.givenTrainingSetTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Berdo"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Cerdo"],
            ["Berdo"],
            ["Hughado"], #not in training
            ["Oldado"],  #not in training
        )

        self.givenActualTaxonomies(
            ["Ferdo"],
            ["Berdo"],
            ["Cerdo"],
            [],
            ["Berdo"], #OC
            [], #Correct
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR",0.5)
        self.assertMetricEquals("ACC", 0.4)
        self.assertMetricEquals("UCR",0.25)
        self.assertMetricEquals("MCR",0.25)
        self.assertMetricEquals("OCR",0.5)

    def test_if_all_taxa_is_absent_it_is_full_overclassification(self):
        self.givenRank(0)

        self.givenTrainingSetTaxonomies()

        self.givenExpectedTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Cerdo"],
            ["Berdo"],
            ["Hughado"], #not in training
            ["Oldado"],  #not in training
        )

        self.givenActualTaxonomies(
            ["Ferdo"],
            ["Berdo"],
            ["Cerdo"],
            ["Ekvador"],
            ["Berdo"], #OC
            ["Belebor"], #Correct
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR",0)
        self.assertMetricEquals("ACC", 0)
        self.assertMetricEquals("UCR",0)
        self.assertMetricEquals("MCR",0)
        self.assertMetricEquals("OCR",1)

    def test_all_unassigned_is_full_underclassification(self):
        self.givenRank(0)

        self.givenTrainingSetTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Berdo"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Cerdo"],
            ["Berdo"],
        )

        self.givenActualTaxonomies(
            [],
            [],
            [],
            [],
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR",0)
        self.assertMetricEquals("ACC", 0)
        self.assertMetricEquals("UCR",1)
        self.assertMetricEquals("MCR",0)
        self.assertMetricEquals("OCR",0)

    def test_it_can_calculate_metrics_different_rank(self):
        self.givenRank(1)

        self.givenTrainingSetTaxonomies(
            ["Ferdo","Mravec"],
            ["Cerdo","Prasa"],
            ["Berdo","Pes"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo","Mravec"],
            ["Cerdo","Nejaky"],
            ["Cerdo","Prasa"],
            ["Berdo","Hluchec"],
        )

        self.givenActualTaxonomies(
            ["Ferdo","Mravec"], #tp
            ["Cerdo","Prasa"], #oc
            ["Cerdo","Pes"], #mc
            ["Berdo"], #tu
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR",0.5)
        self.assertMetricEquals("ACC", 0.3333333)
        self.assertMetricEquals("UCR",0.0)
        self.assertMetricEquals("MCR",0.5)
        self.assertMetricEquals("OCR", 0.5)

        self.givenRank(0)

        self.calcMetrics()
        self.assertMetricEquals("TPR",1)
        self.assertMetricEquals("ACC", 1)
        self.assertMetricEquals("UCR",0)
        self.assertMetricEquals("MCR",0)
        self.assertMetricEquals("OCR", 0)


    def test_it_can_calculate_too_conservative_metric(self):
        self.givenRank(1)

        self.givenTrainingSetTaxonomies(
            ["Ferdo","Mravec"],
            ["Cerdo","Prasa"],
            ["Berdo","Pes"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo","Mravec"],
            ["Cerdo","Nejaky"],
            ["Cerdo","Prasa"],
            ["Berdo","Hluchec"],
            ["Erdo", "Hluchec"],
        )

        self.givenActualTaxonomies(
            ["Ferdo","Mravec"], #tp
            ["Cerdo","Prasa"], #oc
            ["Cerdo","Pes"], #mc
            ["Berdo"], #tu
            ["Cerdo"], #oc
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR",0.5)
        self.assertMetricEquals("ACC", 0.25)
        self.assertMetricEquals("UCR",0.0)
        self.assertMetricEquals("MCR",0.5)
        self.assertMetricEquals("OCR", 0.66666666666666)
        self.assertMetricEquals("UUCR", 0)

    def test_classifier_can_be_too_conservative(self):
        self.givenRank(2)

        self.givenTrainingSetTaxonomies(
            ["Ferdo","Mravec","Antalec"],
            ["Cerdo","Prasa","Paravec"],
            ["Berdo","Pes","Ukulelec"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo","Mravec","Korovec"],
            ["Cerdo","Prasa","Mufasa"],
            ["Berdo", "Pes", "Kopytovec"]
        )

        self.givenActualTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Berdo"],
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR", 0)
        self.assertMetricEquals("ACC", 0)
        self.assertMetricEquals("UCR", 0)
        self.assertMetricEquals("MCR", 0)
        self.assertMetricEquals("OCR", 0)
        self.assertMetricEquals("UUCR", 1)

    def test_underclassification_is_not_the_same_as_misclassification_on_different_rank(self):
        self.givenRank(2)

        self.givenTrainingSetTaxonomies(
            ["Ferdo","Mravec","Antalec"],
            ["Cerdo","Prasa","Paravec"],
            ["Berdo","Pes","Ukulelec"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo","Mravec","Korovec"],
            ["Cerdo","Prasa","Mufasa"],
            ["Berdo", "Pes", "Kopytovec"]
        )

        self.givenActualTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Requerdo"],
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR", 0)
        self.assertMetricEquals("ACC", 0)
        self.assertMetricEquals("UCR", 0)
        self.assertMetricEquals("MCR", 0)
        self.assertMetricEquals("OCR", 0)
        self.assertMetricEquals("UUCR", 0.6666666666666)
        self.assertMetricEquals("UMCR", 0.3333333333333)


        self.givenRank(2)

        self.givenTrainingSetTaxonomies(
            ["Ferdo","Mravec","Antalec"],
            ["Cerdo","Prasa","Paravec"],
            ["Berdo","Pes","Ukulelec"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo","Mravec","Antalec"],
            ["Cerdo","Prasa","Paravec"],
            ["Berdo", "Pes", "Ukulelec"]
        )

        self.givenActualTaxonomies(
            ["Ferdo"],
            ["Requerdo"],
            ["Berdo"],
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR", 0)
        self.assertMetricEquals("ACC", 0)
        self.assertMetricEquals("UCR", 0.66666666666)
        self.assertMetricEquals("MCR", 0.33333333333)
        self.assertMetricEquals("OCR", 0)
        self.assertMetricEquals("UUCR", 0)
        self.assertMetricEquals("UMCR", 0)


    def test_with_no_taxonomies_in_training_set_everything_is_oc(self):
        self.givenRank(0)

        self.givenTrainingSetTaxonomies()

        self.givenExpectedTaxonomies(
            ["Ferdo"],
            ["Cerdo"],
            ["Cerdo"],
            ["Berdo"],
        )

        self.givenActualTaxonomies(
            ["Ferdo"],
            ["Berdo"],
            ["Cerdo"],
            ["Berdo"]
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR", 0)
        self.assertMetricEquals("ACC", 0)
        self.assertMetricEquals("UCR", 0)
        self.assertMetricEquals("MCR", 0)
        self.assertMetricEquals("OCR", 1)
        self.assertMetricEquals("UUCR", 0)
        self.assertMetricEquals("UMCR", 0)


    def test_it_can_calculate_overall_error(self):
        self.givenRank(1)

        self.givenTrainingSetTaxonomies(
            ["Ferdo","Serdo"],
            ["Cerdo","Merdo"],
            ["Berdo","Kerdo"],
            ["Helado","Cortado"],
        )

        self.givenExpectedTaxonomies(
            ["Ferdo","Kerdo"], #not in training
            ["Cerdo","Urdo"],  #not in training
            ["Berdo", "Kerdo"],
            ["Helado", "Cortado"],
            ["Hughado","Bughado"], #not in training
            ["Helado","Cortado"],
            ["Helado","Abado"] #not in training
        )

        self.givenActualTaxonomies(
            ["Ferdo"], #tu
            ["Berdo"], #umc
            ["Cerdo"], #mc
            ["Helado","Cortado"], #tp
            [], #tu
            ["Helado"], #uc
            [], #uuc
        )

        self.calcMetrics()
        self.assertMetricEquals("TPR",0.333333333)
        self.assertMetricEquals("ACC", 0.33333333)
        self.assertMetricEquals("UCR",0.333333333)
        self.assertMetricEquals("MCR",0.333333333)
        self.assertMetricEquals("OCR",0)
        self.assertMetricEquals("UUCR", 0.25)
        self.assertMetricEquals("UMCR", 0.25)
        self.assertMetricEquals("SCR",harmonic_mean([1 - e for e in [0.333333333,0.333333333,0,0.25,0.25]]))

    def givenRank(self,rank):
        self.rank = rank

    def givenTrainingSetTaxonomies(self,*lists):
        self.training_set_taxonomies = set(
            self._mkTaxonomies(lists)
        )

    def givenExpectedTaxonomies(self,*lists):
        self.expected_taxonomies = self._mkTaxonomies(lists)

    def givenActualTaxonomies(self,*lists):
        self.actual_taxonomies = self._mkTaxonomies(lists)

    def _mkTaxonomies(self,lists):
        return [
            Taxonomy(list) for list in lists
        ]

    def calcMetrics(self):
        self.metrics = MetricCalculator(self.rank,self.training_set_taxonomies).calculate(self.expected_taxonomies,self.actual_taxonomies)

    def assertMetricEquals(self,metric,val):
        self.assertAlmostEqual(
            self.metrics[metric],
            val
        )

if __name__ == '__main__':
    unittest.main()