import unittest

from lib.dispatch.local_executor import *

class TestLocalExecutor(unittest.TestCase):
    def test_executor_runs_jobs_on_flush_even_if_batch_was_not_populated(self):

        self.executeCommand("job1")

        self.assertSeenCommands()

        self.flushExecutor()

        self.assertSeenCommands("job1")

    def test_if_batch_is_full_it_is_run_before_adding_another_job(self):

        self.givenBatchSize(2)

        self.executeCommand("job1")

        self.assertSeenCommands()

        self.executeCommand("job2")

        self.assertSeenCommands()

        self.executeCommand("job3")

        self.assertSeenCommands("job1","job2")

    def test_executor_jobs_with_unsatisfied_dependencies_are_not_run(self):
        self.givenBatchSize(2)

        self.executeCommandAfter("job1",[1000])
        self.executeCommandAfter("job2", [1000])
        self.executeCommandAfter("job3", [1000])

        self.assertSeenCommands()


    def test_job_with_dependencies_is_executed_when_its_dependencies_become_satisfied(self):
        self.givenBatchSize(2)

        self.executeCommandAfter("job1",[3])
        self.executeCommandAfter("job2",[4])
        self.executeCommand("job3")
        self.executeCommand("job4")
        self.executeCommand("job5")
        self.flushExecutor()

        self.assertSeenCommands("job3","job4","job1","job2","job5")

    def test_job_execution_is_correct_with_multiple_dependencies(self):
        self.givenBatchSize(2)

        self.executeCommandAfter("job1",[3,4])
        self.executeCommandAfter("job2",[3,5])
        self.executeCommand("job3")
        self.executeCommand("job4")
        self.executeCommand("job5")
        self.flushExecutor()

        self.assertSeenCommands("job3","job4","job1","job5","job2")


    def test_flush_runs_all_jobs_even_if_they_have_nested_dependencies(self):
        self.givenBatchSize(4)

        self.executeCommand("job1")
        self.executeCommandAfter("job2",[1])
        self.executeCommandAfter("job3",[2])
        self.executeCommandAfter("job4",[3])
        self.flushExecutor()

        self.assertSeenCommands("job1","job2","job3","job4")

    def test_dependencies_work_with_jobids_outputted_by_executor(self):
        self.givenBatchSize(4)

        id1 = self.executeCommand("job1")
        id2 = self.executeCommandAfter("job2", [id1])
        id3 = self.executeCommandAfter("job3", [id2])
        self.executeCommandAfter("job4", [id3])
        self.flushExecutor()

        self.assertSeenCommands("job1", "job2", "job3", "job4")


    def test_unrunnable_job_does_NOT_cause_executor_to_loop_infinitely(self):
        self.givenBatchSize(2)

        self.executeCommandAfter("job1",[1000])
        self.flushExecutor()

        self.assertSeenCommands()

    def test_job_with_empty_dependendency_list_is_job_with_no_dependencies(self):
        self.givenBatchSize(2)

        self.executeCommandAfter("job1",[])
        self.flushExecutor()

        self.assertSeenCommands("job1")


    def test_batch_size_one_means_delayed_execution_by_one_job(self):
        self.givenBatchSize(1)

        self.executeCommand("job1")
        self.executeCommand("job2")

        self.assertSeenCommands("job1")


    def test_batch_size_less_than_one_is_error(self):
        self.givenBatchSize(0)

        with self.assertRaises(ExecutionError):
            self.getInstance()

        self.givenBatchSize(-1)

        with self.assertRaises(ExecutionError):
            self.getInstance()

        self.givenBatchSize(1)

        self.getInstance()

    def setUp(self):
        self.batch_size = 4
        self.seen_commands = []
        self.instance = None

    def getInstance(self):
        if self.instance is None:
            self.instance = LocalExecutor(
                shell_runner = self,
                job_batch_size = self.batch_size
            )
        return self.instance

    def executeCommand(self,cmd):
        return self.getInstance().execute(cmd)

    def executeCommandAfter(self,command,deps):
        return self.getInstance().executeAfter(command,deps)

    def flushExecutor(self):
        self.instance.flush()

    def assertCommandInSeen(self, expected_cmd):
        self.assertTrue(
            expected_cmd in self.seen_commands
        )

    def assertSeenCommands(self,*commands):
        self.assertEqual(
            list(commands),
            self.seen_commands
        )


    def givenBatchSize(self,size):
        self.batch_size = size

    #shell runner
    def runCommands(self,cmds):
        self.seen_commands = self.seen_commands + cmds