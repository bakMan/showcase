from lib.dispatch.executor import *
import re
import tempfile
import subprocess
import logging

class PBSCommandBuilder:

    WALLTIME_PATTERN = "[0-9]?[0-9]:[0-9]?[0-9]:[0-9]?[0-9]"
    SPACE_PATTERN = "[0-9]+(mb|gb)"

    def __init__(self):

        self.memory = "8gb"
        self.select = 1
        self.ncpus = 1
        self.walltime = "2:00:00"
        self.scratch = None
        self.cluster = None
        self.dependencies = []
        self.additional = None

    def setMemory(self,memory):
        self.__checkIsValidMemorySpaceExpression(memory)
        self.memory = memory

    def __checkIsValidMemorySpaceExpression(self, memory_expression):

        #None means no scratch
        if memory_expression is None:
            self.scratch = None
            return

        if re.match(self.SPACE_PATTERN, memory_expression) is None:
            raise ExecutionError("Wrong memory size description: '{}'. Format should be '{}'.".format(memory_expression,self.SPACE_PATTERN))

    def setNodes(self, select : int):
        self.select = select

    def setNcpus(self,ncpus : int):
        self.ncpus = ncpus

    def setAdditional(self,x):
        self.additional = x

    def setWalltime(self,walltime):

        if re.match(self.WALLTIME_PATTERN,walltime) is None:
            raise ExecutionError("Malformed syntax of walltime string '{}'. The string should have format '{}'".format(walltime,self.WALLTIME_PATTERN))

        self.walltime = walltime

    def setDependencies(self,deps):
        self.dependencies = deps

    def setScratchSize(self,scratch_size):
        self.__checkIsValidMemorySpaceExpression(scratch_size)
        self.scratch = scratch_size

    def setCluster(self,cluster):
        self.cluster = cluster

    def createQsubCommand(self):

        return "qsub -l select={}:ncpus={}:mem={}{}{}{} -l walltime={} {}".format(
            str(self.select),
            str(self.ncpus),
            self.memory,
            self.__getScratchString(),
            self.__getClusterString(),
            self.__getAdditionalString(),
            self.walltime,
            self._getDependencyString()
        )

    def __getScratchString(self):
        if self.scratch is None:
            return ""
        else:
            return ":scratch_local={}".format(self.scratch)

    def __getAdditionalString(self):
        if self.additional is None:
            return ""
        else:
            return ":{}".format(self.additional)

    def __getClusterString(self):
        if self.cluster is None:
            return ""
        else:
            return ":cluster={}".format(self.cluster)

    def _getDependencyString(self):
        if len(self.dependencies) == 0:
            return ""
        else:
            dep_jobs_string = ":".join(self.dependencies)
            return "-W depend=afterany:" + dep_jobs_string

class PBSExecutor(Executor):
    def __init__(self,pbs_command_builder = PBSCommandBuilder()):
        self.pbs_command_builder = pbs_command_builder

    def execute(self,command):
        return self.executeAfter(command,[])

    def executeAfter(self,command,jobids):
        self.pbs_command_builder.setDependencies(jobids)
        return self._scheduleCommandOnPBS(command)

    def _scheduleCommandOnPBS(self,command):
        script_file = self._writeCommandToFile(command)
        pbs_commandline = "cat {} | {}".format(script_file,self.pbs_command_builder.createQsubCommand())
        logging.info("RUNNING: " + pbs_commandline)
        res = subprocess.run(pbs_commandline,shell=True,stdout=subprocess.PIPE)
        return res.stdout.decode("utf-8").strip()

    def _writeCommandToFile(self,command):
        file = tempfile.mktemp()
        with open(file,"w") as f:
            print(command,file=f)
        return file

    def flush(self):
        return