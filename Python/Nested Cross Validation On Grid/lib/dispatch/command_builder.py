from lib.evaluation.classifier_program import *
from lib.io.filesystem import *

class CommandBuilderError(Exception):
    pass

class CommandBuilder:

    EVALUATE_CLASSIFIER_TEMPLATE = "{executable_path} evaluate_classifier --name '{classifier_name}' --command '{classifier_command}' --dataset-root '{dataset_path}' --outer-index {outer_index} --inner-index {inner_index} --rank {rank} > '{inner_results_path}/{outer_index}_{inner_index}_{classifier_name}'"
    EVALUATE_OUTER_TEMPLATE      = "{executable_path} evaluate_outer_fold --results-path '{inner_results_path}' --dataset-root '{dataset_path}' --outer-index {outer_index} --rank {rank} --selection-metric 'SCR' > '{outer_results_path}/{outer_index}.tsv'"
    COLLECTOR_TEMPLATE           = "{executable_path} collect_results --results-path '{inner_results_path}' --outer-results-dir '{outer_results_path}' --inner-results-tsv '{inner_results_tsv}' --outer-results-tsv '{outer_results_tsv}' --keep-folders"

    def __init__(self,inner_results_path,outer_results_path,executable_path,dataset_path,rank,inner_results_tsv,outer_results_tsv,filesystem=Filesystem()):
        self.inner_results_path = inner_results_path
        self.outer_results_path = outer_results_path
        self.executable_path    = executable_path
        self.dataset_path       = dataset_path
        self.rank               = rank
        self.filesystem         = filesystem
        self.inner_results_tsv  = inner_results_tsv
        self.outer_results_tsv  = outer_results_tsv

        self.checkValidityOfAttributes()

    def checkValidityOfAttributes(self):
        if type(self.rank) != int:
            raise CommandBuilderError("Classifier rank can be only of type integer.")

        if self.rank < 0:
            raise CommandBuilderError("Rank value must be above zero.")


    def buildClassifierEvaluationCommand(self, classifier_program : ClassifierProgram, outer_index, inner_index):
        return self.EVALUATE_CLASSIFIER_TEMPLATE.format(
            executable_path    = self.filesystem.abspath(self.executable_path),
            classifier_name    = classifier_program.name,
            classifier_command = classifier_program.command,
            dataset_path = self.filesystem.abspath(self.dataset_path),
            outer_index = outer_index,
            inner_index = inner_index,
            rank = self.rank,
            inner_results_path = self.filesystem.abspath(self.inner_results_path)
        )

    def buildOuterEvaluationCommand(self,outer_index):
        return self.EVALUATE_OUTER_TEMPLATE.format(
            executable_path=self.filesystem.abspath(self.executable_path),
            dataset_path=self.filesystem.abspath(self.dataset_path),
            inner_results_path=self.filesystem.abspath(self.inner_results_path),
            outer_results_path=self.filesystem.abspath(self.outer_results_path),
            rank=self.rank,
            outer_index = outer_index
        )

    def buildCollectorCommand(self):
        return self.COLLECTOR_TEMPLATE.format(
            executable_path=self.filesystem.abspath(self.executable_path),
            inner_results_path=self.filesystem.abspath(self.inner_results_path),
            outer_results_path=self.filesystem.abspath(self.outer_results_path),
            inner_results_tsv = self.filesystem.abspath(self.inner_results_tsv),
            outer_results_tsv = self.filesystem.abspath(self.outer_results_tsv)
        )