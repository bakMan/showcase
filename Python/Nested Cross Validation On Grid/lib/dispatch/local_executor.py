import multiprocessing as mp
from lib.dispatch.executor import *
import subprocess
import logging

class ParallelBashRunner:
    def __init__(self,ncpus=mp.cpu_count()):
        self.ncpus = ncpus

    def runCommands(self,commands):
        pool = mp.Pool(self.ncpus)
        pool.map(self._runJob, commands)

    def _runJob(self,command):
        subprocess.run(command,shell=True)

class LocalExecutor(Executor):
    def __init__(self,job_batch_size=mp.cpu_count(),shell_runner=ParallelBashRunner()):

        if job_batch_size <= 0:
            raise ExecutionError("Invalid job batch size '{}', it must be natural number.".format(job_batch_size))

        self.job_queue = []
        self.job_dependency_map = {}
        self.already_run_jobs = set()

        self.job_batch_size = job_batch_size
        self.shell_runner = shell_runner

        self.jobid_counter = 1

    def execute(self,command):
        self._executeIfEnoughJobs()
        return self._registerJob(command)

    def _registerJob(self, command):
        new_jobid = self._generateJobid()
        self.job_queue.append((new_jobid, command))
        return new_jobid

    def _generateJobid(self):
        jobid = self.jobid_counter
        self.jobid_counter += 1
        return jobid

    def executeAfter(self,command,jobids):
        self._executeIfEnoughJobs()
        new_jobid = self._registerJob(command)
        self.job_dependency_map[new_jobid] = jobids
        return new_jobid

    def _executeIfEnoughJobs(self):
        runnable_jobs = self._getJobIDsOfRunnableJobs()
        if len(runnable_jobs) >= self.job_batch_size:
            self._runJobs(runnable_jobs)

    def _getJobIDsOfRunnableJobs(self):
        return [
            jobid for jobid,_ in self.job_queue if self._hasSatisfiedDependencies(jobid)
        ]

    def _hasSatisfiedDependencies(self,jobid):
        dependencies = self.job_dependency_map.get(jobid,[])
        return all(depid in self.already_run_jobs for depid in dependencies)

    def flush(self):
        while True:
            runnable_jobs = self._getJobIDsOfRunnableJobs()

            if len(runnable_jobs) == 0:
                break

            self._runJobs(runnable_jobs)

        if len(self.job_queue) != 0:
            jobs_not_executed = set(str(jobid) for jobid,_ in self.job_queue)
            jobs_not_executed_str = ",".join(jobs_not_executed)
            logging.warning("Jobs '{}' were not executed due to unsatisfiable dependencies.".format(jobs_not_executed_str))

    def _runJobs(self,jobids):
        commands = self._getCommandsForJobids(jobids)
        self.shell_runner.runCommands(commands)
        self._removeJobs(jobids)

    def _getCommandsForJobids(self,jobids):
        return [
            command for jobid, command in self.job_queue if jobid in jobids
        ]

    def _removeJobs(self,jobids):
        self.already_run_jobs.update(jobids)
        self.job_queue = [
            (jobid,command) for jobid,command in self.job_queue if jobid not in jobids
        ]
