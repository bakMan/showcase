from lib.dispatch.command_builder import *
from lib.dispatch.executor import *
from lib.evaluation.classifier_program import *
from lib.data.cv import *

class Dispatcher:
    def __init__(self,command_builder : CommandBuilder,executor : Executor):
        self.command_builder = command_builder
        self.executor = executor

    def dispatch(self,classifier_programs : [ClassifierProgram],dataset : CVDataset):
        # classifier programs cannot be lazy at it is iterated repeatedly
        outer_jobids = []
        classifier_programs = list(classifier_programs)
        for outer_index,inner_cv in enumerate(dataset):
            inner_jobids = self._dispatchClassifierJobs(classifier_programs,outer_index,inner_cv)
            outer_jobid = self._dispatchOuterJob(inner_jobids,outer_index)
            outer_jobids.append(outer_jobid)

        self._dispatchCollectorJob(outer_jobids)
        self.executor.flush()

    def _dispatchClassifierJobs(self,classifier_programs,outer_index,inner_cv_dataset):
        jobs = []
        for inner_index,_ in enumerate(inner_cv_dataset):
            for program in classifier_programs:
                command = self.command_builder.buildClassifierEvaluationCommand(program,outer_index,inner_index)
                jobid = self.executor.execute(command)
                jobs.append(jobid)

        return jobs

    def _dispatchOuterJob(self,jobs_to_wait_for,outer_index):
        command = self.command_builder.buildOuterEvaluationCommand(outer_index)
        return self.executor.executeAfter(command,jobs_to_wait_for)

    def _dispatchCollectorJob(self,jobs_to_wait_for):
        command = self.command_builder.buildCollectorCommand()
        self.executor.executeAfter(command,jobs_to_wait_for)


class OuterOnlyDispatcher(Dispatcher):
    #ignore inner job dispatch
    def _dispatchClassifierJobs(self,classifier_programs,outer_index,inner_cv_dataset):
        return []