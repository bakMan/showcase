class ExecutionError(Exception):
    pass

class Executor:
    def execute(self,command):
        raise NotImplementedError()

    def executeAfter(self,command,jobids):
        raise NotImplementedError()

    def flush(self):
        raise NotImplementedError()