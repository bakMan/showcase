from lib.evaluation.classifier_program import ClassifierProgram
from lib.evaluation.classifier_program_evaluator import evaluateClassifierProgram
from lib.data.cv import openNestedCVDataset
from lib.util.definitions import METRICS_ORDERING
from lib.evaluation.evaluate import selectAverageTopClassifier
from lib.data.results import InnerResultsStore
from lib.util.util import error
from lib.io.classifierfile import *
from lib.dispatch.dispatcher import *
from lib.dispatch.local_executor import *
from lib.dispatch.pbs_executor import *
from lib.data.results import *
import shutil

import os

SCIPT_PATH_VAR = "NESTED_CV_EVAL_SCRIPT"

class Workflow:

    def run(self,options):
        raise NotImplementedError()

    def checkIfRunnableWith(self,options):
        raise NotImplementedError()

    def checkOptionIsSet(self,option: str, options):
        if getattr(options, option) is None:
            error("Option {} missing, check -h for details.".format(option))

    def getOrderedMetricValues(self, metrics):
        metrics_in_order = self.getOrderedMetricNames(metrics)
        rounded_metrics = [round(val, 4) for val in metrics_in_order]
        return rounded_metrics

    def getOrderedMetricNames(self, metrics):
        return [metrics[metric_name] for metric_name in METRICS_ORDERING]


class EvaluateClassifierWorkflow(Workflow):
    def checkIfRunnableWith(self,options):
        self.checkOptionIsSet("name", options)
        self.checkOptionIsSet("command", options)
        self.checkOptionIsSet("dataset_root", options)
        self.checkOptionIsSet("oindex", options)
        self.checkOptionIsSet("iindex", options)

    def run(self,options):
        dataset = self._getEvaluationDataset(options)
        program = self._buildClassifierProgram(options)
        results,metrics = evaluateClassifierProgram(program,dataset,options.rank)
        self._saveDebug(results,options)
        self.printMetrics(program,metrics,options)

    def _buildClassifierProgram(self, options):
        return ClassifierProgram(options.name, options.command)

    def _getEvaluationDataset(self, options):
        nested_cv_dataset = openNestedCVDataset(options.dataset_root)
        target_dataset = nested_cv_dataset[options.oindex][options.iindex]
        return target_dataset

    def printMetrics(self,program,metrics,options):
        rounded_metrics = self.getOrderedMetricValues(metrics)
        row_data = [program.name,program.command,str(options.oindex),str(options.iindex)] + list(map(str,rounded_metrics))
        print("\t".join(row_data))

    def _saveDebug(self,results,options):
        try:
            if not options.debug_path is None:
                self.__writeResults(options, results)

        except Exception as e:
            logging.warning("Unable to store debugging info. Error: {}".format(str(e)))

    def __writeResults(self, options, results):
        if not os.path.exists(options.debug_path):
            os.mkdir(options.debug_path)

        outfile_name = "{}_{}_{}.out".format(options.oindex, options.iindex, options.name)

        out_str = "\n".join(map(str, results))

        with open(pth.join(options.debug_path, outfile_name), "w") as f:
            print(out_str, file=f)


class EvaluateOuterFoldWorkflow(Workflow):
    def checkIfRunnableWith(self,options):
        self.checkOptionIsSet("dataset_root", options)
        self.checkOptionIsSet("oindex", options)
        self.checkOptionIsSet("selection_metric", options)

    def run(self,options):
        top_clasisifier = self._getWinnerOfThisFold(options)
        target_dataset = self._getEvaluationDataset(options)
        results,metrics = evaluateClassifierProgram(top_clasisifier, target_dataset, options.rank)
        self.printMetrics(top_clasisifier, metrics, options)

    def _getEvaluationDataset(self, options):
        nested_cv_dataset = openNestedCVDataset(options.dataset_root)
        target_dataset = nested_cv_dataset[options.oindex].master_split
        return target_dataset

    def _getWinnerOfThisFold(self, options):
        result_store = InnerResultsStore(options.results_path)
        results_of_this_fold = result_store.resultsOfOuterFold(options.oindex)
        top_clasisifier = selectAverageTopClassifier(results_of_this_fold, options.selection_metric)
        return top_clasisifier

    def printMetrics(self,program,metrics,options):
        rounded_metrics = self.getOrderedMetricValues(metrics)
        row_data = [program.name,program.command,str(options.oindex)] + list(map(str,rounded_metrics))
        print("\t".join(row_data))


class CollectResultsWorkflow(Workflow):
    def run(self, options):
        inner_results = self._getInnerResultsDataFrame(options)
        outer_results = self._getOuterResultsDataFrame(options)

        self._outputTSV(inner_results,options.inner_results_tsv)
        self._outputTSV(outer_results,options.outer_results_tsv)

        self._tryToDeleteFoldersIfRequested(options)

    def _getInnerResultsDataFrame(self,options):
        inner_results_store = InnerResultsStore(options.results_path)
        return inner_results_store.getAllResults()

    def _getOuterResultsDataFrame(self, options):
        outer_results_store = OuterResultsStore(options.outer_results_dir)
        return outer_results_store.getAllResults()

    def _tryToDeleteFoldersIfRequested(self,options):
        try:
            if options.keep_folders is None:
                shutil.rmtree(options.outer_results_dir)
                shutil.rmtree(options.results_path)

        except Exception as e:
            pass

    def checkIfRunnableWith(self, options):
        pass

    def _outputTSV(self,df,path):
        df.to_csv(path,sep="\t",index=False,float_format="%.4f")

class RunCVWorkflow(Workflow):

    def run(self,options):
        classifiers = readClassifierFile(options.classifier_file)
        dataset     = openNestedCVDataset(options.dataset_root)
        job_dispatcher  = self._buildDispatcher(options)
        job_dispatcher.dispatch(classifiers,dataset)

    def _buildDispatcher(self,options):
        command_builder = self._buildCommandBuilder(options)
        executor        = self._buildExecutor(options)
        if options.only_outer:
            return OuterOnlyDispatcher(command_builder, executor)
        else:
            return Dispatcher(command_builder,executor)

    def _buildCommandBuilder(self,options):
        executable_path = os.environ[SCIPT_PATH_VAR]
        self._createDirIfNotExists(options.inner_results_dir,options.force)
        self._createDirIfNotExists(options.outer_results_dir,options.force)
        return CommandBuilder(
            inner_results_path=options.inner_results_dir,
            outer_results_path=options.outer_results_dir,
            executable_path=executable_path,
            dataset_path=options.dataset_root,
            rank=options.rank,
            inner_results_tsv=options.inner_results_tsv,
            outer_results_tsv=options.outer_results_tsv
        )

    def _createDirIfNotExists(self,dir,force=False):
        if pth.exists(dir):
            if force == True:
                return
            error("Path '{}' already exists. Start in different path, or use --force argument.".format(dir))
        else:
            os.mkdir(dir)

    def _buildExecutor(self,options):
        if options.execute_locally:
            return LocalExecutor()
        else:
            pbs_command_builder = self._buildPBSCommandBuilder(options)
            return PBSExecutor(pbs_command_builder)

    def _buildPBSCommandBuilder(self,options):
        builder = PBSCommandBuilder()
        builder.setMemory(options.memory)
        builder.setNcpus(options.ncpus)
        builder.setNodes(1)
        builder.setScratchSize(options.scratch)
        builder.setCluster(options.cluster)
        builder.setWalltime(options.walltime)
        builder.setAdditional(options.additional)
        return builder

    def checkIfRunnableWith(self,options):
        self.checkOptionIsSet("dataset_root",options)
        self.checkOptionIsSet("classifier_file",options)

        if not pth.exists(options.dataset_root):
            error("Path to dataset '{}' is not valid.".format(options.dataset_root))

        if not pth.isdir(options.dataset_root):
            error("Path to dataset '{}' is not directory.".format(options.dataset_root))

        if not pth.exists(options.classifier_file):
            error("Path to classifier file '{}' is not valid.".format(options.classifier_file))

        if not pth.isfile(options.classifier_file):
            error("Path to classifier file '{}' is not regular file.".format(options.classifier_file))

        if not SCIPT_PATH_VAR in os.environ:
            raise error("Environmental variable '{}' is not set. It must be set and point to this script.".format(SCIPT_PATH_VAR))