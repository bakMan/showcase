from pandas import DataFrame
from lib.evaluation.classifier_program import *

ERR_MISSING_METRIC = "Metric '{}' missing from result table"
ERR_MISSING_COLUMN = "Result table does not contain compulsory column '{}'"
ERR_TABLE_EMPTY    = "Result table cannot be empty"


IMPOSSIBLE_SCORE = -10000000

class TopClassifierSelector:
    def __init__(self,metric):
        self.metric = metric

    def select(self,table : DataFrame) -> ClassifierProgram:
        self.__checkTable(table)

        table = table.fillna(IMPOSSIBLE_SCORE)
        index_of_max = table.loc[:,self.metric].idxmax()
        name         = table.loc[:,HEADER_NAME][index_of_max]
        command      = table.loc[:, HEADER_COMMAND][index_of_max]

        return ClassifierProgram(name,command)

    def __checkTable(self, table):
        if table.empty:
            raise ValueError(ERR_TABLE_EMPTY)

        if self.metric not in table.columns:
            raise ValueError(ERR_MISSING_METRIC.format(self.metric))

        if HEADER_NAME not in table.columns:
            raise ValueError(ERR_MISSING_COLUMN.format(HEADER_NAME))

        if HEADER_COMMAND not in table.columns:
            raise ValueError(ERR_MISSING_COLUMN.format(HEADER_COMMAND))

def averageMetrics(df):
    if df.empty:
        return df

    averages   = df.groupby([HEADER_NAME, HEADER_COMMAND, HEADER_OUTER_INDEX]).mean().reset_index()
    without_ii = averages.drop(HEADER_INNER_INDEX, axis=1)
    return without_ii

def selectAverageTopClassifier(df,metric):
    average_df = averageMetrics(df)
    return TopClassifierSelector(metric).select(average_df)