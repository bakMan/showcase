from lib.util.taxonomy import *
from statistics import *
from sklearn.metrics import *


class MetricCalculator:
    def __init__(self,rank,training_set_taxonomies):
        self.rank = rank
        self.training_set_taxonomies = training_set_taxonomies
        self.classifiable_taxa_on_rank = self.__mkSetOfClassifiableTaxa(training_set_taxonomies)
        self._initialize()

    def _initialize(self):
        self.oc = 0
        self.uc = 0
        self.mc = 0
        self.tp = 0
        self.tu = 0
        self.uuc = 0
        self.umc = 0

        self.classifiable   = 0
        self.unclassifiable = 0

        self.classifiable_expected = []
        self.classifiable_observed = []

    def __mkSetOfClassifiableTaxa(self, training_set_taxonomies):
        return set(
            taxonomy[self.rank] for taxonomy in training_set_taxonomies
            if taxonomy[self.rank] != Taxonomy.UNASSIGNED\
        )

    def calculate(self,expected_taxonomies,actual_taxonomies):
        self.__calculateCounts(expected_taxonomies,actual_taxonomies)
        metrics = self.__calculateMetrics()
        self._initialize()
        return metrics

    def __calculateCounts(self,expected_taxonomies,actual_taxonomies):
        for (expected,actual) in zip(expected_taxonomies,actual_taxonomies):
            expected_up_to_rank = expected[:self.rank + 1]
            actual_up_to_rank   = actual[:self.rank  + 1]
            self.__makeCount(expected_up_to_rank,actual_up_to_rank)

    def __makeCount(self,expected,actual):
        if self._shouldHaveBeenClassified(expected):
            self._countClassifiable(actual, expected)
        else:
            self._countUnclassifiable(actual,expected)

    def _countClassifiable(self, actual, expected):
        self.classifiable += 1

        self.classifiable_expected.append(expected[self.rank])
        self.classifiable_observed.append(actual[self.rank])

        if actual == expected:
            self.tp += 1
        elif actual.isParentOf(expected):
            self.uc += 1
        else:
            self.mc += 1


    def _countUnclassifiable(self,actual : Taxonomy, expected : Taxonomy):
        self.unclassifiable += 1

        closest_taxonomy = self.__getClosestClassifiableTaxonomyTo(expected)

        if closest_taxonomy == actual:
            self.tu += 1
        elif actual.isParentOf(closest_taxonomy):
            self.uuc += 1
        elif closest_taxonomy.isParentOf(actual):
            self.oc += 1
        else:
            self.umc += 1


    def __getClosestClassifiableTaxonomyTo(self,taxonomy):
        if len(self.training_set_taxonomies) == 0:
            return Taxonomy([])

        training_set_taxonomies = list(self.training_set_taxonomies)
        distances = [training_taxonomy.nCommonTaxa(taxonomy) for training_taxonomy in training_set_taxonomies]
        max_idx = distances.index(max(distances))
        closest_taxonomy = training_set_taxonomies[max_idx]
        return closest_taxonomy.getCommonTaxonomy(taxonomy)

    def _shouldHaveBeenClassified(self,expected):
        return expected == self.__getClosestClassifiableTaxonomyTo(expected)

    def __calculateMetrics(self):
        ucr  = self._safeDiv(self.uc, self.classifiable)
        mcr  = self._safeDiv(self.mc, self.classifiable)
        ocr  = self._safeDiv(self.oc, self.unclassifiable)
        uucr = self._safeDiv(self.uuc, self.unclassifiable)
        umcr = self._safeDiv(self.umc, self.unclassifiable)
        mcc  = matthews_corrcoef(self.classifiable_expected,self.classifiable_observed)

        return {
            "TPR"  : self._safeDiv(self.tp, self.classifiable),
            "MCC"  : mcc,
            "UCR"  : ucr,
            "MCR"  : mcr,
            "OCR"  : ocr,
            "UUCR" : uucr,
            "UMCR" : umcr,
            "ACC" : self._safeDiv(self.tp, self.classifiable + self.oc),
            "TP"  : self.tp,
            "UC"  : self.uc,
            "MC"  : self.mc,
            "OC"  : self.oc,
            "UUC" : self.uuc,
            "UMC" : self.umc,
            "SCR" : harmonic_mean([1 - e for e in [ucr,mcr,ocr,uucr,umcr]]),
            "SCR_2" : harmonic_mean([1-(ocr + uucr + umcr),mcc])
        }

    def _safeDiv(self,a,b):
        if b == 0:
            return 0
        return a/b