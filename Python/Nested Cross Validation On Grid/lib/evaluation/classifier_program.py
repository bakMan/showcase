from lib.data.dataset import *
from lib.util.command_executor import *
from lib.util.definitions import *
from lib.util.taxxi import *
from lib.util.taxonomy import *
import logging

class ClassifierProgram:
    def __init__(self,name,command):
        self.name    = name
        self.command = command

        self._checkValNotEmpty("Name", self.name)
        self._checkValNotEmpty("Command", self.command)

    def _checkValNotEmpty(self, val_name, val):
        if val is None or val == "":
            raise ValueError(val_name + " of classifier program cannot be null")

    def makeCommandline(self,train,test,rank=5):
        self._checkValNotEmpty("Training set",train)
        self._checkValNotEmpty("Testing set",test)

        command_without_whitespace = self.command.strip()
        train = self._deleteQuotes(train)
        test  = self._deleteQuotes(test)
        return  command_without_whitespace + " --rank {} --train \"{}\" --test \"{}\"".format(rank,train,test)

    def _deleteQuotes(self, string):
        return string.replace("\"", "").replace("'", "")

    def __eq__(self, other):
        if type(self) != type(other):
            return False

        return self.name == other.name and self.command == other.command


class ClassifierProgramRunner:
    def __init__(self,command_executor = executeCommand,taxonomy_parser = TAXXITaxonomyParser(),rank=DEFAULT_RANK):
        self.command_executor = command_executor
        self.taxonomy_parser  = taxonomy_parser
        self.rank = rank

    def runClassifier(self,program : ClassifierProgram,dataset_split : FileDatasetSplit):
        program_output = self._executeClassifier(dataset_split, program)
        return self._parseOutput(program_output)

    def _executeClassifier(self, dataset_split, program : ClassifierProgram):
        command = program.makeCommandline(dataset_split.training_set_path, dataset_split.testing_set_path,rank = self.rank)
        return self.command_executor(command)

    def _parseOutput(self,output : str):
        lines = output.split("\n")

        #last blank is ignored
        if(lines[-1] == ""):
            lines = lines[:-1]

        return [
            self._parseOutputLine(line) for line in lines
        ]

    def _parseOutputLine(self, line):
        try:
            return self.taxonomy_parser.parse(line)
        except Exception as e:
            logging.warning("Unable to parse taxonomoy '{}'. Error: {}".format(line,str(e)))
            return WrongTaxonomy()


def runClassifier(program : ClassifierProgram,dataset_split : FileDatasetSplit,rank=DEFAULT_RANK):
    return ClassifierProgramRunner(rank=rank).runClassifier(program,dataset_split)