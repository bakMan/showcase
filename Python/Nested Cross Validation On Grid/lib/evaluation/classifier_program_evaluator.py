from lib.evaluation.classifier_program import *
from lib.evaluation.metrics import *
from lib.util.definitions import *

class EvaluationError(Exception):
    pass

def evaluateClassifierProgram(program : ClassifierProgram,data_split,rank = DEFAULT_RANK):
    expected_taxonomies = data_split.testing_set.taxonomies
    training_taxonomies = data_split.training_set.taxonomies
    classifier_results = tryToRunClassifierProgram(data_split, program, rank)

    if len(classifier_results) != len(expected_taxonomies):
        raise EvaluationError("Classifier returned different number of taxonomies ({}) "
                              "than expected ({}).".format(len(classifier_results),len(expected_taxonomies)))

    metric_calculator   = MetricCalculator(rank,training_taxonomies)
    return (classifier_results,metric_calculator.calculate(expected_taxonomies,classifier_results))


def tryToRunClassifierProgram(data_split, program, rank):
    try:
        classifier_results = runClassifier(program, data_split, rank=rank)
        return classifier_results
    except Exception as e:
        raise EvaluationError("Unable to evaluate classifier program '{}' ({}). "
                              "Error: {}".format(program.name,program.command,str(e)))

