#!/usr/bin/env bash

for t in acceptance_tests/*;
do
    pushd .
    cd $t
    ./runtest.sh
    popd
done