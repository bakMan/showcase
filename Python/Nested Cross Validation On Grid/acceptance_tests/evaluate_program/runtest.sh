#!/usr/bin/env bash

RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[0;32m'

python3 ../../nested_cv.py evaluate_classifier --name "Stupid Classifier" --command 'bash classifier.sh' --dataset-root dataset --inner-index 0 --outer-index 0 --rank 6 > observed_metrics.txt

diff observed_metrics.txt expected_metrics.txt

if [ $? -eq 0 ];
then
    echo -e "$GREEN OK $NC"
else
    echo -e "$RED FAIL $NC"
fi