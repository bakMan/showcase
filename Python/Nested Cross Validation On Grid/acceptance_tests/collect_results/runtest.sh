#!/usr/bin/env bash

RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[0;32m'

python3 ../../nested_cv.py collect_results --results-path results --outer-results-dir outer_results --inner-results-tsv inner_results.tsv --outer-results-tsv outer_results.tsv --keep-folders

EVAL_RES=$?

diff inner_results.tsv expected_inner_results.tsv
diff outer_results.tsv expected_outer_results.tsv

DIFF_RES=$?

if [ $DIFF_RES -eq 0 ] && [ $EVAL_RES -eq 0 ];
then
    echo -e "$GREEN OK $NC"
else
    echo -e "$RED FAIL $NC"
fi