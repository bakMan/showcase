#!/usr/bin/env bash

RED='\033[0;31m'
NC='\033[0m' # No Color
GREEN='\033[0;32m'

python3 ../../nested_cv.py evaluate_outer_fold --results-path results --dataset-root dataset --outer-index 0 --rank 6 --selection-metric "SCR" > observed_metrics.txt

EVAL_RES=$?

diff observed_metrics.txt expected_metrics.txt

DIFF_RES=$?

if [ $DIFF_RES -eq 0 ] && [ $EVAL_RES -eq 0 ];
then
    echo -e "$GREEN OK $NC"
else
    echo -e "$RED FAIL $NC"
fi