#! /usr/bin/env python3
from optparse import OptionParser
from lib.util.util import error
from lib.workflows import *
from lib.util.definitions import *
import logging
import sys

COLLECT_RESULTS = "collect_results"

RUN_CV = "run_cv"

EVALUATE_OUTER_FOLD = "evaluate_outer_fold"

EVALUATE_CLASSIFIER = "evaluate_classifier"

N_DECIMALS = 4

ERR_NO_WORKFLOW = "Program requires one positional argument - workflow to run. See --list option output to seee availible workflows"
ERR_UNKNOWN_WORKFLOW = "Unknown command {}"



def main():
    logging.basicConfig(level=logging.INFO)
    workflow_name, options = parseCommandline()
    workflow = buildWorkflow(workflow_name)
    workflow.checkIfRunnableWith(options)
    workflow.run(options)


def parseCommandline():
    parser = OptionParser()
    parser.add_option("", "--name", dest="name",
                      help="Name of classifier under evaluation")
    parser.add_option("", "--command", dest="command",
                      help="Classifier command to run. It must accept --train --test and --rank argument. See documentation for details.")

    parser.add_option("", "--dataset-root", dest="dataset_root",
                      help="Path to nested CV dataset root folder.")

    parser.add_option("", "--results-path", dest="results_path",
                      help="Path to directory with evaluation results.")

    parser.add_option("", "--debug-path", dest="debug_path",
                      help="Path to directory to store debugging info in.",default="debug")

    parser.add_option("", "--outer-index", dest="oindex",
                      help="Index of outer CV fold.",type="int")

    parser.add_option("", "--inner-index", dest="iindex",
                      help="Index of inner CV fold.", type="int")

    parser.add_option("", "--rank", dest="rank",
                      help="Taxonomic rank to evaluate on.", type="int",default=DEFAULT_RANK)

    parser.add_option("", "--selection-metric", dest="selection_metric",
                      help="Abberviation of metric to use in outer fold winning classifier selection",default=DEFAULT_SELECTION_METRIC)


    parser.add_option("", "--local", dest="execute_locally",action="store_true",
                      help="Should execute locally and not send jobs to PBS ?",default=False)

    parser.add_option("", "--inner-results-dir", dest="inner_results_dir",
                      help="Directory to store results of inner CV loops in.", default="./inner_results")

    parser.add_option("", "--outer-results-dir", dest="outer_results_dir",
                      help="Directory to store results of outer CV loops in.", default="./outer_results")

    parser.add_option("", "--classifier-file", dest="classifier_file",
                      help="Path to classifier file.")

    parser.add_option("", "--memory", dest="memory",default="8gb",
                      help="Memory to request for each classifier job. Only applicable if run on grid. Syntax of this parameter is [0-9]+(gb|mb).")

    parser.add_option("", "--ncpus", dest="ncpus",type="int",default=1,
                      help="Number of cpus to request for each classifier job. Only applicable if run on grid. Local execution ignores this parameters and takes all avilible CPUs.")

    parser.add_option("", "--walltime", dest="walltime",default="2:00:00",
                      help="Time to request on grid for each classifier job. Only applicable if run on grid.")

    parser.add_option("", "--scratch", dest="scratch", default=None,
                      help="If set, a given amount of scratch space will be requested for each classifier job. Scratch is a fast temporary storage beneficial if a job does frequent"
                           " disk I/O. Syntax of this parameter is [0-9]+(gb|mb). The path to scratch will be availible through $SCRATCHDIR environmental variable. Only applicable if run on grid.")

    parser.add_option("", "--cluster", dest="cluster", default=None,
                      help="Cluster restriction for jobs. If nothing is specified, all clusters will be used.")

    parser.add_option("", "--additional", dest="additional", default=None,
                      help="Additional PBS resource properties.")

    parser.add_option("", "--inner-results-tsv", dest="inner_results_tsv",
                      help="File in which all the results of all inner folds should be stored.", default="./inner_results.tsv")

    parser.add_option("", "--outer-results-tsv", dest="outer_results_tsv",
                      help="File in which results of the outer folds should be stored.",
                      default="./outer_results.tsv")


    parser.add_option("", "--keep-folders", dest="keep_folders",action="store_true",
                      help="If set, folders with partial results (rows of inner and outer result tsv) should be kept. Debugging option.")

    parser.add_option("", "--list", dest="list",action="store_true",
                      help="If set, list possible workflows and exit.")

    parser.add_option("","--only-outer",dest="only_outer",action="store_true",help="If set, only outer fold jobs are dispatched.")

    parser.add_option("", "--force", dest="force", action="store_true",
                      help="Ignore existence of inner and outer results folders.")

    (options, args) = parser.parse_args()

    if options.list is not None:
        listWorkflowsAndExit()

    if len(args) != 1:
        error(ERR_NO_WORKFLOW)

    return (args[0],options)

def buildWorkflow(workflow_name):
    if workflow_name == EVALUATE_CLASSIFIER:
        return EvaluateClassifierWorkflow()
    elif workflow_name == EVALUATE_OUTER_FOLD:
        return EvaluateOuterFoldWorkflow()
    elif workflow_name == RUN_CV:
        return RunCVWorkflow()
    elif workflow_name == COLLECT_RESULTS:
        return CollectResultsWorkflow()
    else:
        error(ERR_UNKNOWN_WORKFLOW.format(workflow_name))


def listWorkflowsAndExit():
    str = """
    {} - Evaluate single classifier on a given dataset (specified by dataset root path, inner and outer index).
    {} - Given results of a inner fold, determine winning classifier and evaluate it on outer fold data.
    {} - Collect inner and outer fold evaluation results and produce inner_results.tsv and outer_results.tsv.
    
    {} - You probably want this. Evaluate classifiers specified in classifierfile on a given nested CV dataset.
    """

    print(
        str.format(EVALUATE_CLASSIFIER,EVALUATE_OUTER_FOLD,COLLECT_RESULTS,RUN_CV),
        file = sys.stderr
    )
    exit(0)

if __name__ == '__main__':
    main()