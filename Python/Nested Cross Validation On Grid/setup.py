from setuptools import *

setup(
    name='Nested CV Tool',
    version='',
    packages=find_packages(),
    url='',
    license='',
    author='Stanislav Smatana',
    author_email='ismatana@fit.vutbr.cz',
    description='',
    scripts = ["nested_cv.py"]
)
