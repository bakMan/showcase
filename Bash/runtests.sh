#!/bin/bash

#PROJEKT IOS c. 1
#runtest.sh - prostredie pre regresne testovanie
#Stanislav Smatana, xsmata01@stud.fit.vutbr.cz 2012


export LC_ALL=C

#konstanty
HELP="Usage: $0 [-vtrsc] TEST_DIR [REGEX]

    -v  validate tree
    -t  run tests
    -r  report results
    -s  synchronize expected results
    -c  clear generated files

    It is mandatory to supply at least one option."

EXIT_SUCCESS=0
EXIT_ERROR=1
EXIT_FAILURE=2

#globalna premenna s errorkodom programu - na zaciatok tam davame 0 = sucess
ERRORCODE="$EXIT_SUCCESS"

#funkcia ktora do globalnej premennej ERRORCODE ulozi errorcode
function error()
{
   if [ "$ERRORCODE" -lt "$1" ]
   then
      ERRORCODE="$1"
   fi
}

#CHYBOVE HLASKY

ERR_TSTDIR="fatalna chyba: Nepodarilo sa otvorit korenovy adresar:"
WARN_CD="varovanie: Nepodarilo sa otvorit priecinok:"
WARN_SYMLINKS="varovanie: Skript nasiel symbolicke odkazy v priecinku:"
WARN_FILES="varovanie: Najdene nepovolene subory v priecinku ktory ma obsahovat\
 adresare:"
WARN_CMD="varovanie: V danom priecinku neexistuje cmd-given alebo nemate
 opravnenie ho spustat:"
WARN_STDIN="varovanie: Stdin-given v danom priecinku nie je mozne citat:"
WARN_EXPCAPDELTA="varovanie: Subor \
nie je pristupny pre zapis \
alebo citanie:"
WARN_BADFILES="varovanie: Najdene nepovolene subory v priecinku:"
WARN_NUM="varovanie: Subor neobsahuje iba cislo:"
WARN_HARDLINKS="varovanie: Priecinok obsahuje pevne odkazy:"
WARN_EXPECTED="varovanie: V priecinku nie su vsetky subory expected alebo ich \
nemozno citat - testovanie moze byt skreslene:"
WARN_CAPTURED="varovanie: V priecinku sa nenachadzaju niektore subory captured \
alebo ich nemozno citat - spustili ste testovanie?:"
ERR_SYNC_STAT="varovanie: Nepodarilo sa synchronizovat status-captured v:"
ERR_SYNC_STDOUT="varovanie: Nepodarilo sa synchronizovat stdout-captured v:" 
ERR_SYNC_STDERR="varovanie: Nepodarilo sa synchronizovat stderr-captured v:"

#Funkcia nacita do premennej DIRS priecinky z test dir podla filtra
#params - $0 testdir $1 regex
function NacitajTestDir()
{

   #To ci sa podarilo otvorit testdir netestujem 
   #test prebieha uz v hlavnom programe pred volanim
   #tejto funkcie
   DIRS=`find "$1" -type d 2>/dev/null  | grep -E "$2"| sort`

}

#Funkcia ktora overi ci je v danom priecinku cmd given a ci mame pravo ho
#spustat
function isCmdGiven()
{
   if [ -e cmd-given -a -x cmd-given ]
   then
      return 0
   else
      return 1
   fi
}

#Funkcia ktora skontroluje ci je v priecinku stdin-given a ci je readable
function isStdinGiven()
{
   if [ -e stdin-given ]
   then
      if [ ! -r stdin-given ]
      then
         return 1
      fi
   fi

   return 0
}

#Funkcia ktora zisti ci v danom priecinku nie su uplne nepovolene subory
function isNotOthers()
{
GREPSTR="(cmd-given|stdin-given|stderr-expected|stderr-captured|stderr-delta|stdout-expected|\
stdout-captured|stdout-delta|status-expected|status-captured|status-delta)$"

# 1 pretoze v ls -l vystupuje aj riadok "celkom: x"
  if [ `ls -l 2> /dev/null | grep -E -v $GREPSTR  | wc -l 2>/dev/null ` -ne 1 ]
  then
     return 1
  fi

  return 0
}

#funkcia overi ci vsetky suboroy exp,cap,delta su zapisovatele ak existuju
function isExpCapDelta()
{

   #Povolena konstrukcia ?
   for file in {stdout,stderr,status}-{expected,captured,delta}
   do
      if [ -e "$file" ]
      then
         if [ ! -w "$file" ]
         then
           ERRFILE="$file"
           return 1;
         fi
      fi
   done

   return 0
}

#funkcia zisti ci je v danom subore len cislo
#ak subory neexistuju nepovazuje to za chybu
function isOnlyNumber()
{
   #subor neexistuje ? 
   if [ ! -e "$1" ]
   then
      return 0
   fi

   if [ `cat "$1" 2> /dev/null | grep -E '^[0-9]+$' | wc -l` -eq 1 ]
   then
      return 0
   else
      return 1
   fi
}

#Funkcia ktora zisti ci sa v danom priecinku nachadzaju hardlinks
function isHardLinks()
{
   
   FILES=`ls -l| grep '^[^d]' | sed '1d' | awk '{print $2}'`
  
  if [ "$FILES" == "" ]
  then
     return 0;
  fi

   while read line
   do
      if [ "$line" -ne 1 ]
      then
        return 1 
      fi
   done << _EOF_
$FILES
_EOF_

   return 0

}

#Funkcia na validovanie testovacieho stromu
function Validuj()
{
   #priecinok kde sa budeme vzdy vracat   
   TESTDIR=`pwd`

   while read dir
   do
      #vezmeme priecinok zo zoznamu a skusime ci sa tam da dostat
      #EXIT CODE
      cd "$dir" 2> /dev/null || { echo "$WARN_CD $dir" &1>2 ; error
      "$EXIT_ERROR" ; continue; }

      #spocitame symlinks - tie su error vzdy
      if [ "`find . -maxdepth 1 -type l 2> /dev/null | wc -l`" -ne 0 ]
      then
         echo "$WARN_SYMLINKS $dir" 1>&2
         error "$EXIT_ERROR"
      fi

      #kontrola hardlinkov - mozu byt vsade
      isHardLinks || { echo "$WARN_HARDLINKS $dir" 1>&2; error "$EXIT_ERROR"; }


      #zistime ci obsahuje priecinky
      # gt 1 pretoze kazdy priecinok obsahuje minimalne .
      DIR_CNT=`find . -maxdepth 1 -type d 2> /dev/null | wc -l`

      if [ "$DIR_CNT" -gt 1 ]
      then
         #priecinok ktory obsahuje priecinky nesmie obsahovat nic ine
         if [ "$DIR_CNT" -ne `find . -maxdepth 1 | wc -l` ]
         then
            echo "$WARN_FILES $dir" 1>&2
            error "$EXIT_ERROR"
         fi
      else
         #test cmd given
         isCmdGiven || { echo "$WARN_CMD $dir" 1>&2 ; error "$EXIT_ERROR"; }
         isStdinGiven || { echo "$WARN_STDIN $dir" 1>&2 ; error "$EXIT_ERROR"; } 
         isExpCapDelta || { echo "$WARN_EXPCAPDELTA $ERRFILE $dir" 1>&2 ; error "$EXIT_ERROR"; }
         isNotOthers || { echo "$WARN_BADFILES $dir" 1>&2 ; error "$EXIT_ERROR"; }
         isOnlyNumber "status-expected" || { echo "$WARN_NUM status-expected v $dir" 1>&2 ; error "$EXIT_ERROR"; }
         isOnlyNumber "status-captured" || { echo "$WARN_NUM status-captured v $dir" 1>&2 ; error "$EXIT_ERROR"; }
      fi
      
      #ak sa behom spustania zmazal priecinok testdir - ide o fatalne zlyhanie
      cd "$TESTDIR" ||  { echo "$ERR_TESTDIR" 1>&2; exit "$EXIT_FAILURE"; }
   done << _EOF_
$1
_EOF_

}

#Funkcia urobi diff medzi ocakavanym a skutocnym vysledkom
#vysledok vypisuje na dany file descriptor $1
#param 2 je string reprezentujuci nazov testu vo vypise
function report()
{
   #chybajuci expected subor sposobi varovanie - ale report sa vykona
   #if [ ! -r status-expected -o ! -r stdout-expected -o ! -r stderr-expected ]
   #vypnute testovanie na stderr
   if [ ! -r status-expected -o ! -r stdout-expected ]
   then
      echo "$WARN_EXPECTED ./$2" 1>&2
      error "$EXIT_ERROR"
   fi

   #chybajuci captured zamedzi reportu - je to hruba chyba indikujuca zlyhanie
   if [ ! -r status-captured -o ! -r stdout-captured -o ! -r stderr-captured ]
   then
      echo "$WARN_CAPTURED ./$2"
      error "$EXIT_ERROR"
      return 1
   fi

   
   # co s chybajucimi captured ?!
   diff -up status-expected status-captured > status-delta 2> /dev/null
   diff -up stdout-expected stdout-captured > stdout-delta 2> /dev/null

   #vypnute testovanie na stderr kvoli cudzim testom
   #diff -up stderr-expected stderr-captured > stderr-delta 2> /dev/null
   echo "" > stderr-delta

   OK_COL=""
   FAIL_COL=""

   if [ -t "$1" ]
   then
      OK_COL='\e[1;32m'
      FAIL_COL='\e[1;31m'
   fi

   if [ -n "`cat status-delta stdout-delta stderr-delta`" ]
   then
      echo -e "$2:$FAIL_COL FAILED\e[00m" 1>&"$1"
      error "$EXIT_ERROR"
   else
      echo -e "$2:$OK_COL OK\e[00m" 1>&"$1"
   fi
   
}

#funkcia test v strome vykona testy a reportuje vysledky na stderr
function Testuj()
{
   TESTDIR=`pwd`

   while read dir
   do
      cd "$dir" 2> /dev/null || { echo "$WARN_CD $dir" &1>2 ; error
      "$EXIT_ERROR" ; continue; }
     
      if [ -e cmd-given ]
      then
         if [ -e stdin-given ] 
         then
            ulimit -t 15; ./cmd-given 1> stdout-captured 2> stderr-captured < stdin-given
         else
            ulimit -t 15; ./cmd-given 1> stdout-captured 2> stderr-captured < /dev/null
         fi
         
         echo "$?" > status-captured

         report 2 "`echo $dir | cut -c 3-`"
      fi

      cd "$TESTDIR" ||  { echo "$ERR_TESTDIR" 1>&2 ; exit "$EXIT_FAILURE"; }
   done << _EOF_
$1
_EOF_

}

#funkcia Reportuj vypise vysledky testovania na stdout 
function Reportuj()
{
  TESTDIR=`pwd`

#HERE document je dirty workaround na zabranenie shellu v pusteni subshellu
   while read dir
   do
      cd "$dir" 2> /dev/null || { echo "$WARN_CD $dir" &1>2 ; error
      "$EXIT_ERROR" ; continue; }

      if [ -e cmd-given ]
      then
         report 1 "`echo $dir|cut -c 3-`"
      fi
      
      cd "$TESTDIR" ||  { echo "$ERR_TESTDIR" 1>&2 ; exit "$EXIT_FAILURE"; }
   done << _EOF_ 
$1
_EOF_


}

function Synchronizuj()
{
   TESTDIR=`pwd`

   while read dir
   do
      cd "$dir" 2> /dev/null || { echo "$WARN_CD $dir" &1>2 ; error
      "$EXIT_ERROR" ; continue; }
   
      if [ -e cmd-given ]
      then
         mv status-captured status-expected 2> /dev/null || { error "$EXIT_FAILURE"; echo "$ERR_SYNC_STAT $dir"; }
         mv stdout-captured stdout-expected 2> /dev/null || { error "$EXIT_FAILURE"; echo "$ERR_SYNC_STDOUT $dir"; }
         mv stderr-captured stderr-expected 2> /dev/null || { error "$EXIT_FAILURE"; echo "$ERR_SYNC_STDERR $dir"; }
      fi
       
      cd "$TESTDIR" ||  { echo "$ERR_TESTDIR" 1>&2 ; exit "$EXIT_FAILURE"; }
   done << _EOF_
$1
_EOF_

}

function Cisti()
{

   TESTDIR=`pwd`

   while read dir
   do
      cd "$dir" 2> /dev/null || { echo "$WARN_CD $dir" &1>2 ; error
      "$EXIT_ERROR" ; continue; }
  
      rm status-captured 2> /dev/null 
      rm status-delta 2> /dev/null 
      rm stdout-captured 2> /dev/null 
      rm stdout-delta 2> /dev/null 
      rm stderr-captured 2> /dev/null 
      rm stderr-delta 2> /dev/null 
       
      cd "$TESTDIR" ||  { echo "$ERR_TESTDIR" 1>&2; exit "$EXIT_FAILURE"; }
   done << _EOF_
$1
_EOF_
}

#DEBUG
#set -- -vtrsc ./tree/tests 

#riadiace kody

VALIDATE=0
TEST=0
REPORT=0
SYNC=0
CLEAR=0

while getopts :vtrsc param
do
   case "$param" in
      v) VALIDATE=1 ;;
      t) TEST=1 ;;
      r) REPORT=1 ;; 
      s) SYNC=1 ;;
      c) CLEAR=1 ;;
      ?) echo "$HELP" 1>&2; exit $EXIT_FAILURE ;;
   esac
done

# ak sa ziadne prikazy nespracovali - OPTIND bude stale 1
if [ "$OPTIND" -eq 1 ]
then
   echo "$HELP" 1>&2
   exit $EXIT_FAILURE
fi

((OPTIND--))

shift $OPTIND

# po spracovani prikazov nam nesmu zostat viac nez 2 a menej nez 1 params - inak chyba
if [ "$#" -gt 2 -o "$#" -lt 1 ]
then
   echo "$HELP" 1>&2
   exit $EXIT_FAILURE
fi

#skusime ci mozme spravit cd do testdir - ak nie = fatalne zlyhanie
cd "$1" 2> /dev/null || { echo "$ERR_TSTDIR $1" 1>&2 ; exit "$EXIT_FAILURE"; }

#Nacitame vsetky priecinky z TESTDIR do premennej DIRS
NacitajTestDir './' "$2"

#Vykonavanie jednotlivych akcii skriptu

#VALIDATE
if [ "$VALIDATE" -eq 1 ]
then
  Validuj "$DIRS"
fi

if [ "$TEST" -eq 1 ]
then
   Testuj "$DIRS"
fi

if [ "$REPORT" -eq 1 ]
then
   Reportuj "$DIRS"
fi

if [ "$SYNC" -eq 1 ]
then
   Synchronizuj "$DIRS"
fi

if [ "$CLEAR" -eq 1 ]
then
   Cisti "$DIRS"
fi

#errorcode je v globalnej premennej ERRORCODE, nastavovanej pomocou funkcie
#error 
exit "$ERRORCODE"
