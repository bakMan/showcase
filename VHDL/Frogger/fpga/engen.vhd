-- Enable generator
-- 
-- Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
-- 14.6.2012
--
-- Generator povolovacieho signalu.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity engen is

generic (
   -- hodnota pri ktorej sa ma aktivovat signal EN
   MAXVALUE : integer
   );
port (
   CLK : in std_logic;

   -- signal na aktivovanie/deaktivovanie jednotky
   ENABLE : in std_logic;

   -- generovany povolovaci signal
   EN  : out std_logic
   );

end entity;

architecture main of engen is

   signal cnt : integer range 0 to MAXVALUE := 0;

begin

   process (CLK)
   begin
      if (CLK'event) and (CLK = '1') then
         EN <= '0';
         if (ENABLE = '1') then
            if (cnt = MAXVALUE) then
               cnt <= 0;
               EN <= '1';
            else
               cnt <= cnt + 1;
            end if;
         end if;
      end if;
   end process;

end main;

