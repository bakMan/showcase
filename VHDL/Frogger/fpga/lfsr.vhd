-- LINEAR FEEDBACK SHIFT REGISTER
-- 
-- Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
-- 14.6.2012
--
-- Generovanie nahodnych cisel na 8 bitoch
-- pricom seed generatora je zadavany ako 
-- genericky parameter.

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
use IEEE.STD_LOGIC_arith.ALL;

entity lfsr is
   generic (SEED: std_logic_vector(7 downto 0):= (others => '1'));
   port (RST,EN, CLK : in std_logic; 
      Y : buffer std_logic_vector(7 downto 0));
end lfsr;


architecture RTL of lfsr is

   constant Width: positive := 8;
   signal Taps: std_logic_vector(Width-1 downto 0);

begin

	LFSR: process (CLK)

	-- internal registers and signals
	variable LFSR_Reg: std_logic_vector(Width-1 downto 0) := SEED;
	variable Feedback: std_logic;
	
	begin

		Taps <= "10001110";

		if RST='0' then

			LFSR_Reg := (others=>'1');

		elsif rising_edge(CLK) then

         -- nahodna hodnota je generovana neustale, ale vystup musi byt
         -- stabilny, preto ku kopirovaniu dojde len pri signale EN
         if EN = '1' then
            Y <= LFSR_Reg;	-- parallel data out
         end if;
						
			Feedback := LFSR_Reg(Width-1); 			

			for N in Width-1 downto 1 loop

				if (Taps(N-1)='1') then

					LFSR_Reg(N) := LFSR_Reg(N-1) xor Feedback;

				else

					LFSR_Reg(N) := LFSR_Reg(N-1);

				end if;

			end loop;

			LFSR_Reg(0) := Feedback;

		end if;

		
	end process;

end RTL;

