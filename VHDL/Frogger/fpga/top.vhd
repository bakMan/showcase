-- TOPLEVEL ENTITA HRY FROGGER
-- 
-- Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
-- 14.6.2012

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.vga_controller_cfg.all;


architecture beh of tlv_pc_ifc is
   --definicia stavov FSM radica hry
   type t_state is (INIT,PLAY,LOSE,WIN);
   signal present_state, next_state : t_state;

   -- informacia pre FSM ze auta su nacitane
   signal load  : std_logic;
   signal loaded  : std_logic;
   signal loaded2  : std_logic;

   -- signaly urcujuce poziciu zaby
   signal frog_x  : std_logic_vector(11 downto 0);
   signal frog_y  : std_logic_vector(11 downto 0);
   
   --signal ktory urcuje ze zaba prehrala
   signal frog_dead,frog_dead2  :  std_logic;
   
   -- rgb iduce z jednotky aut na ceste
   signal car_r,car2_r  : std_logic_vector(2 downto 0);
   signal car_g,car2_g  : std_logic_vector(2 downto 0);
   signal car_b,car2_b  : std_logic_vector(2 downto 0);

   -- signal na aktivovanie/deaktivovanie grafickeho vystupu
   -- z jednotiek aut
   signal car_en  : std_logic;
   

   -- signal oznamujuci ze jednotky car chcu poskytnut rgb
   signal car_vga_hit,car2_vga_hit  : std_logic;
   
   -- signal oznamujuci ze zaba vyhrala hru
   signal frog_win  : std_logic;
   
   -- signal vracajuci zabu na pociatocnu poziciu
   signal frog_rst  : std_logic;

   -- signaly zabieho registra rgb zaby + oznamenie ze je frogreg
   -- pripraveny poskytnut rgb
   signal frog_vga_hit  : std_logic;
   
   signal frog_r : std_logic_vector(2 downto 0);
   signal frog_g : std_logic_vector(2 downto 0);
   signal frog_b : std_logic_vector(2 downto 0);

   -- signal na aktivovanie/deaktivovanie grafickeho vystupu
   -- jednotky zaby
   signal frog_en  : std_logic;

   -- signal ktorym radic hry vybera pozadie
   signal background_sel  : std_logic_vector(1 downto 0);

   -- signaly ktore rgb nesu z generatora pozadia hry
   -- druh vykreslovania pozadia sa vyberie pomocou
   -- background_sel
   signal backg_r  : std_logic_vector(2 downto 0);
   signal backg_g  : std_logic_vector(2 downto 0);
   signal backg_b  : std_logic_vector(2 downto 0);
   
   -- signaly iduce z VGA selectora do radica jednotky vga
   signal vga_r  : std_logic_vector(2 downto 0);
   signal vga_g  : std_logic_vector(2 downto 0);
   signal vga_b  : std_logic_vector(2 downto 0);

   -- signaly z vga ktore urcuju suradnice ziadaneho pixelu
   signal vga_x  : std_logic_vector(11 downto 0);
   signal vga_y  : std_logic_vector(11 downto 0);

  
   -- nastavenia grafickeho rezimu
   signal isettings: std_logic_vector(60 downto 0);

   --signal z kontrolera klavesnice
   signal key  : std_logic_vector(15 downto 0);
   signal key_vld : std_logic;

   -- romky obsahujuce napis LOSE a WIN
   type screen is array(0 to 14) of std_logic_vector(0 to 19);
   constant win_screen : screen := (
   "00000000000000000000",
   "00000000000000000000",
   "00000000000000000000",
   "01010101011000000010",
   "01010101010100000010",
   "01010101010010000010",
   "01010101010001000010",
   "01010101010000100010",
   "01010101010000010010",
   "01010101010000001010",
   "01111101010000000110",
   "00000000000000000000",
   "00000000000000000000",
   "00000000000000000000",
   "00000000000000000000"
   );
   
   constant lose_screen : screen := (
      "00000000000000000000",
      "00000000000000000000",
      "01000111100111101110",
      "01001000010100001000",
      "01001000010100001000",
      "01001000010100001000",
      "01001000010111101110",
      "01001000010000101000",
      "01001000010000101000",
      "01001000010000101000",
      "01001000010000101000",
      "01001000010000101000",
      "01110111101111101110",
      "00000000000000000000",
      "00000000000000000000"
      );
   
   
   
begin

   SetMode(r640x480x60, isettings);

   ----------------------------------------------------
   -- ZAPOJENIE KOMPONENT
   ----------------------------------------------------

   -- CONTROLLER VGA
      vga_controller: entity work.vga_controller(arch_vga_controller)
      generic map (REQ_DELAY => 4) 
      port map (
         CLK    => CLK,
         RST    => RESET,
         ENABLE => '1',
         MODE   => isettings,

         DATA_RED    => vga_r,
         DATA_GREEN  => vga_g,
         DATA_BLUE   => vga_b,
         ADDR_COLUMN => vga_x,
         ADDR_ROW    => vga_y,

         VGA_RED   => RED_V,
         VGA_BLUE  => BLUE_V,
         VGA_GREEN => GREEN_V,
         VGA_HSYNC => HSYNC_V,
         VGA_VSYNC => VSYNC_V

      );

      -- JEDNOTKA ZABY
      frog: entity work.frog
      port map(
         CLK=> CLK,
         RST=> frog_rst,
         FROG_EN => frog_en,
         r => frog_r,
         g => frog_g,
         b => frog_b,
         x => vga_x,
         y => vga_y,
         vga_hit => frog_vga_hit,
         frog_win => frog_win,
         frog_x => frog_x,
         frog_y => frog_y,
         key => key,
         key_vld => key_vld
               );
     
      -- AUTA NA POZICII [0,32] (LAVY HORNY ROH OBJEKTU)
      car: entity work.car_unit
      port map(
         CLK => CLK,
         EN => car_en,
         LOAD => load,
         LOADED => loaded,
         r => car_r,
         g => car_g,
         b => car_b,
         x => vga_x,
         y => vga_y,
         pos_x => X"000",
         pos_y => X"020",
         frog_x => frog_x,
         frog_y => frog_y,
         frog_dead => frog_dead,
         vga_hit => car_vga_hit
        );

      -- AUTA POZICIA [0,0x100]
      car2: entity work.car_unit
      port map(
         CLK => CLK,
         EN => car_en,
         LOAD => load,
         LOADED => open,
         r => car2_r,
         g => car2_g,
         b => car2_b,
         x => vga_x,
         y => vga_y,
         pos_x => X"000",
         pos_y => X"100",
         frog_x => frog_x,
         frog_y => frog_y,
         frog_dead => frog_dead2,
         vga_hit => car2_vga_hit
        );

      -- CONTROLLER KLAVESNICE
      kbc_u : entity work.keyboard_controller(arch_keyboard)
      port map(
         -- Hodiny, Reset
         CLK        => SMCLK,
         RST        => RESET,

         -- Stisknute klavesy
         DATA_OUT   => key,
         DATA_VLD => key_vld,
            
         -- Signaly klavesnice 
         KB_KIN     => KIN,
         KB_KOUT    => KOUT
      );


      ----------------------------------------------------------------
      -- PROCESY GENERATORA HERNEHO POZADIA A SELECTORA RGB VYSTUPU  
      ----------------------------------------------------------------
                     
      -- GENERATOR POZADIA HRY
      process(vga_x,vga_y)
      begin
         -- tri zelen pasiky - jeden v strede, dolu a hore
         if (vga_y >=X"000" and vga_y < X"020") or  (vga_y >= X"0E0" and vga_y < X"100") or (vga_y >= X"1BF" and vga_y < X"1E0") then
            backg_g <= "011";
            backg_b <= "001";
         else
            -- ostatok ma farbu cesty
            backg_r <= "001";
            backg_g <= "001";
            backg_b <= "001";
         end if;

      end process;

      -- VGA SELECTOR
      --logika vyberu vga vystupu
      -- prioritny koder, priorita je nasledujuca
      -- 1. Zaba
      -- 2. Auta
      -- 3. Pozadie
      vga_selector : process is
      begin
         vga_g <= "000";
         vga_b <= "000";
         vga_r <= "000";

         if (frog_vga_hit = '1') then
           -- vystup z zaby
           vga_r <= frog_r;
           vga_g <= frog_g;
           vga_b <= frog_b;
         elsif(car_vga_hit = '1') then
           -- vystup z jednotky aut 1
           vga_r <= car_r;
           vga_g <= car_g;
           vga_b <= car_b;
         elsif(car2_vga_hit = '1') then
           -- vystup z jednotky aut 2
           vga_r <= car2_r;
           vga_g <= car2_g;
           vga_b <= car2_b;
         else
            -- ak sa ziadna jednotka k vykreslovaniu nehlasi, vykresli sa pozadie
            -- pozadie sa vybera na zaklade signalu background_sel
            -- pozadia win screen a lose screen su ulozene v romkach kde jeden bit 
            -- predstavuje policko s rozmerom 32x32 pixelov
            if background_sel = "01" then
               -- win screen
               if win_screen(conv_integer(vga_y(11 downto 5)))(conv_integer(vga_x(11 downto 5))) = '1' then
                  vga_b <= "111";
               end if;
            elsif background_sel = "10" then
               -- lose screen
               if lose_screen(conv_integer(vga_y(11 downto 5)))(conv_integer(vga_x(11 downto 5))) = '1' then
                  vga_r <= "111";
               end if;
            else
               -- ak nejde ani o obrazovku vyhry ani prehry
               -- zapojime vystup generatora herneho pozadia
              vga_r <= backg_r;
              vga_g <= backg_g;
              vga_b <= backg_b;
           end if;
        end if;
      end process;

      -------------------------------------------------------------
      -- FSM RADICA CELEJ HRY
      -------------------------------------------------------------
      sync_logic : process(RESET, CLK)
      begin
         if (RESET = '1') then
            present_state <= INIT;
         elsif (CLK'event AND CLK = '1') then
            present_state <= next_state;
         end if;
      end process sync_logic;
      
      -- LOGIKA DALSIEHO STAVU
      next_state_logic : process(present_state,loaded)
      begin
         case (present_state) is
            when INIT=>
               next_state <= INIT;
              
               -- ak jednotky aut vygenerovali auta ideme do stavu play
               if (loaded = '1') or (loaded2 = '1') then
                  next_state <= PLAY;
               end if;

            when PLAY =>
               next_state <= PLAY;

               -- ak zabu zrazilo auto => stav lose
               -- ak sa dostala na y suradnicu 0 => stav win
               if frog_win = '1' then
                  next_state <= WIN;
               elsif frog_dead = '1' or frog_dead2 = '1' then
                  next_state <= LOSE;
               end if;

            when LOSE=>
               next_state <= LOSE;

               -- ked hrac stlaci klavesu A => pokracujeme na stav INIT (restart hry)
               if key(12) = '1'  then
                  next_state <= INIT;
               end if;

            when WIN=>
               next_state <= WIN;

               -- ked hrac stlaci klavesu A => pokracujeme na stav INIT (restart hry)
               if key(12) = '1'  then
                  next_state <= INIT;
               end if;

            when others=>
               next_state <= INIT;
               
         end case;
      end process next_state_logic;
      
      -- LOGIKA VYSTUPU FSM
      output_logic : process(present_state)
      begin
         load <= '0';
         frog_rst <= '0';
         frog_en <= '0';
         car_en <= '0';
         background_sel <= "00";

         case (present_state) is
            when INIT=>
               -- vrat zabu na zaciatocnu poziciu a daj jednotkam aut povel
               -- na vygenerovanie aut
               load <= '1';
               frog_rst <= '1';

            when PLAY=>
               -- povoleny graficky vystup jednotky zaby a auta, pozadie hry z generatora pozadia
               car_en <= '1';
               frog_en <= '1';

            when LOSE=>
               --  vyberieme pozadie z romky LOSE, graf. vystup zaby a aut je zakazany
               background_sel <= "10";

            when WIN=>
               --  vyberieme pozadie z romky WIN, graf. vystup zaby a aut je zakazany
               background_sel <= "01";
         end case;      

      end process output_logic;
     

end beh;
