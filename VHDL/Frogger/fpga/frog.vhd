-- FROG 
-- 
-- Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
-- 14.6.2012
--
-- Entita starajuca sa o pohyb a vykreslovanie zaby.

library ieee;  
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity frog is
  port (
      CLK : in std_logic;
      RST : in std_logic;

      -- signal ktory umoznuje vypnut graficky vystup jednotky frog
      frog_en : in std_logic;

      -- rgb data
      r : out std_logic_vector(2 downto 0);
      g : out std_logic_vector(2 downto 0);
      b : out std_logic_vector(2 downto 0);
      
      --signalizacia ze ziadane VGA suradnice lezia v oblasti zaby
      vga_hit : out std_logic;


      -- vstup z klavesnice
      key : in std_logic_vector(15 downto 0);

      -- validacny input klavesnice
      key_vld : in std_logic;

      -- indikator ze zaba sa nachadza na poslednej linii - t.j. vyhrala
      frog_win : out std_logic;
     
      -- aktualna pozicia zaby
      signal frog_x  : buffer std_logic_vector(11 downto 0);
      signal frog_y  : buffer std_logic_vector(11 downto 0);

      --vstup z VGA sucasne ziadaneho pixelu
      signal x : in std_logic_vector(11 downto 0);
      signal y : in std_logic_vector(11 downto 0)
  );
end entity frog;

architecture beh of frog is

   --konstanty zaby
   constant FROG_INIT_X : std_logic_vector(11 downto 0):= X"140"; 
   constant FROG_INIT_Y : std_logic_vector(11 downto 0):= X"1C0";
   constant FROG_SIZE   : std_logic_vector(11 downto 0):= X"020"; -- 32 pixelov
   constant SCREEN_MAX_X : std_logic_vector(11 downto 0):= X"280";

   -- bitmapa zaby
   type frog_rom is array (0 to 31) of std_logic_vector(31 downto 0);
   constant frog_bmp : frog_rom := (
   "00110011000001111100000010011000",
   "00010010000011111110000010010010",
   "10010010000011111110000010010011",
   "11011010000011111110000011110110",
   "01111010000111111111000011110100",
   "00111110000111111111000011111100",
   "00111110000111111111000011111000",
   "00011110000111111111000011111000",
   "00011110000111111111000001110000",
   "00001110000111111111000001110000",
   "00001110011111111111110001100000",
   "00000111111111111111111011100000",
   "00000011111111111111111111000000",
   "00000011111111111111001110000000",
   "00000111000111111111100010000000",
   "00000011100111111111100010000000",
   "00000011100111111111100110000000",
   "00000011110111111111101110000000",
   "00000011111111111111111110000000",
   "00000001111111111111111110000000",
   "00000001111111111111111110000000",
   "00000001111111111111111100000000",
   "00000000111111111111111100000000",
   "00000000111111111111111100000000",
   "00000000111111111111111100000000",
   "00000000111111111111111100000000",
   "00000000111111111111111000000000",
   "00000000111111111111111000000000",
   "00000000111111111111111000000000",
   "00000000001111111111110000000000",
   "00000000000111111111110000000000",
   "00000000000000000000000000000000"

   );
   
begin

   -- POHYB ZABY
   -- ziskavanie vstupu z klavesnice tak aby nedochadzalo k jej prilisnej citlivosti
   -- a zaroven aby controller mohol pracovat na frekvencii SMCLK
   process(clk, rst) is
      variable in_access : std_logic := '0';
   begin
     if (rst = '1') then
      
       -- v pripade resetu jednotka vrati zabu na zaciatocnu poziciu
       frog_x <= FROG_INIT_X; 
       frog_y <= FROG_INIT_Y; 

     elsif (rising_edge(clk)) then
         if in_access='0' then
            if key_vld='1' then 
               in_access:='1';

               if key(9)='1' and frog_x /= X"260" then  -- 6
                  frog_x <= frog_x+FROG_SIZE;
               elsif key(1)='1' and frog_x /= X"000" then  -- 4
                  frog_x <= frog_x-FROG_SIZE;
               elsif key(4)='1' and frog_y /= X"000" then  -- 2
                  frog_y <= frog_y-FROG_SIZE;
               elsif key(6)='1' and frog_y /= X"1C0" then  -- 8
                  frog_y <= frog_y+FROG_SIZE;
               end if;

            end if;
         else
            if key_vld ='0' then 
               in_access:='0';
            end if;
         end if;
     end if;
   end process;


   -- LOGIKA GRAFICKEHO VYSTUPU
   out_logic : process is
   begin
     r <= "000";   
     g <= "000";   
     b <= "000";   
     vga_hit <= '0'; 
     
     if frog_en = '1' then

        -- su pozadovane suradnice v medziach objektu ? 
        if ( x >= frog_x ) and ( x < frog_x + FROG_SIZE) then
           if (y>= frog_y and y < frog_y + FROG_SIZE ) then

              -- ak su v ramci objektu, je na suradnici ktoru pozaduju v bitmape zaby 1 ?
              if(frog_bmp(conv_integer(y - frog_y))(conv_integer(x - frog_x)) = '1') then

                 -- zabu kreslime na zeleno + prihlasime sa o vykreslovanie
                  g <= "111";   
                  vga_hit <= '1';

              end if;

           end if;
        end if;
     end if;

   end process out_logic;
     
   -- INDIKACIA VYHRY
   -- ak je uz y suradnica zaby uplne hore - hrac vyhral
   frog_win <= '1' when frog_y = X"000" else '0';
   
end beh;

