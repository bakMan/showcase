
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:51:59 06/09/2012 
-- Design Name: 
-- Module Name:    tile_memory - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity car_unit is
  generic(
    NUM_ROWS : integer := 6;
    TILE_SIZE : integer := 32;
    TILES_IN_ROW : integer := 20
    );
  port (
    clk : std_logic;
    en : std_logic;
    
    -- load urcuje ze jednotka ma nacitat novy level
    load : in std_logic;
    -- signalom loaded jednotka oznamuje ze level je nacitany
    loaded : out std_logic;

    -- signal urcujuci poziciu celeho objektu
    pos_x : in std_logic_vector(11 downto 0);
    pos_y : in std_logic_vector(11 downto 0);

    -- suradnice z VGA controllera
    x : in std_logic_vector(11 downto 0);
    y : in std_logic_vector(11 downto 0);

    -- suradnice kde lezi zaba
    frog_x : in std_logic_vector(11 downto 0);
    frog_y : in std_logic_vector(11 downto 0);

    -- signal oznamujuci ze zaba je mrtva
    frog_dead : out std_logic;

    -- r g b vysielane jednotkou
    r : out std_logic_vector(2 downto 0);
    g : out std_logic_vector(2 downto 0);
    b : out std_logic_vector(2 downto 0);

    -- signal ze na danych suradniciach toto zariadenie ma rgb 
    vga_hit : out std_logic
    
    
  );
end entity car_unit;

architecture beh of car_unit is
   -- vektor obsahuje povolovace pre jednotlive posuvne registre
   signal en_vector  : std_logic_vector( NUM_ROWS - 1  downto 0);

   -- na zaklade hodnoty v speeds_vectore sa vyberie ktory citac
   -- bude povolovat dany posuvny register
   signal speeds_vector  : std_logic_vector(7 downto 0);

   -- vektor obsahujuci nahodne vygenerovany vstup do registrov aut
   signal input_vector  : std_logic_vector(7 downto 0);

   -- tento vector bude urcovat smer posunu v registre (pohybu aut)
   signal dirs_vector  : std_logic_vector(7 downto 0);

   -- pole rotacnych registrov
   type car_arr is array (0 to NUM_ROWS - 1) of std_logic_vector(TILES_IN_ROW - 1 downto 0);
   signal car_regs : car_arr;

   -- dva povolovacie signaly prvy s frekvenciou 0.5 hz pre pomale auta
   -- druhy s frekvenciou 0.25 hz pre rychle auta
   signal en_05hz,en_025hz  : std_logic;
   
   --pomocne signaly
   -- relativne pozicie suradnic vga v ramci objektu
   signal rel_x,rel_y : std_logic_vector(11 downto 0);

   -- relativne pozicie suradnic zaby v ramci objektu
   signal frog_rel_x,frog_rel_y : std_logic_vector(11 downto 0);

   -- cislo policka na ktore aktualne vga chce
   signal tile_x,tile_y : std_logic_vector(6 downto 0);

   --cislo policka na ktorom je aktualne zaba
   signal frog_tile_x,frog_tile_y : std_logic_vector(6 downto 0);

   -- displacement v ramci policka (pre indexaciu rom textur)
   signal disp_x,disp_y : std_logic_vector(4 downto 0);

   --romky textur auta a motorky
   type tex_rom is array (0 to 31) of std_logic_vector(31 downto 0);
   constant car : tex_rom :=(
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000011111100000000111111000000",
      "00000011111100000000111111000000",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00111111111111111111111111111100",
      "00000011111100000000111111000000",
      "00000011111100000000111111000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000"
   ); 
      
   
   constant bike_left : tex_rom :=(
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000100000000000000000",
      "00000000000001000000000000000000",
      "00000000010010000000000000000000",
      "00000000010100000000111100000000",
      "00000111111000000001111110000000",
      "00001111111111111111111110000000",
      "00001111111111111111111110000000",
      "00001111111111111111111110000000",
      "00000111111000000001111110000000",
      "00000000010100000000111100000000",
      "00000000010010000000000000000000",
      "00000000000001000000000000000000",
      "00000000000000100000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000"
      );

   constant bike_right : tex_rom :=(
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000100000000000000",
      "00000000000000000010000000000000",
      "00000000000000000001001000000000",
      "00000000111100000000101000000000",
      "00000001111110000000011111100000",
      "00000001111111111111111111110000",
      "00000001111111111111111111110000",
      "00000001111111111111111111110000",
      "00000001111110000000011111100000",
      "00000000111100000000101000000000",
      "00000000000000000001001000000000",
      "00000000000000000010000000000000",
      "00000000000000000100000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000",
      "00000000000000000000000000000000"
      );

   
begin
   rel_x <= x - pos_x;
   rel_y <= y - pos_y;

   frog_rel_x <= frog_x - pos_x;
   frog_rel_y <= frog_y - pos_y;

   tile_x <= rel_x(11 downto 5);
   tile_y <= rel_y(11 downto 5);

   frog_tile_x <= frog_rel_x(11 downto 5);
   frog_tile_y <= frog_rel_y(11 downto 5);

   disp_x <= rel_x(4 downto 0);
   disp_y <= rel_y(4 downto 0);
   
   -- instancovanie 3x lfsr
   -- aktivne su pri loade
   dirs_lfsr: entity work.lfsr(RTL)
   generic map( SEED => "01010101")
   port map(
      CLK => CLK,
      RST => '1',
      EN => LOAD,
      Y => dirs_vector
      );
  
   speeds_lfsr: entity work.lfsr(RTL)
   port map(
      CLK => CLK,
      RST => '1',
      EN => LOAD,
      Y => speeds_vector
      );

   input_lfsr: entity work.lfsr(RTL)
   port map(
      CLK => CLK,
      RST => '1',
      EN => '1',
      Y => input_vector
      );

   -- 2x povolovacie signaly
   engen_05hz: entity work.engen
   generic map(MAXVALUE => 25000000)
   port map(
         CLK => CLK,
         ENABLE => '1',
         EN => en_05hz
      );

   engen_025hz: entity work.engen
   generic map(MAXVALUE => 12500000)
   port map(
         CLK => CLK,
         ENABLE => '1',
         EN => en_025hz
         );

   -- indikator nacitavania registrov 
   -- aktivuje sa so signalom LOAD
   -- a ked nadobudne hodnotu TILES_IN_ROW 
   -- vysle signal loaded radicu hry
   -- proces posuvnych registrov
   -- ak je LOAD 1 - zlava nasuvame z input registra
   -- ak je LOAD 0 - rotujeme bud vlavo alebo vpravo podla dir
   loaded_indicator: entity work.engen
   generic map(MAXVALUE => TILES_IN_ROW - 1)
   port map(
         CLK => CLK,
         ENABLE => LOAD,
         EN => LOADED
         );

   process(CLK)
   begin
      if CLK'event and CLK='1' then
         for I in 0 to NUM_ROWS - 1 loop
            if LOAD = '1' then
               car_regs(I) <= input_vector(I) & car_regs(I)(TILES_IN_ROW -1 downto 1);
               --car_regs(I) <= '1' & car_regs(I)(TILES_IN_ROW -1 downto 1);
            else
               if en_vector(I) = '1' then
                  if (dirs_vector(I) = '0') then
                     car_regs(I) <= car_regs(I)(0) & car_regs(I)(TILES_IN_ROW -1 downto 1);
                  else
                     car_regs(I) <= car_regs(I)(TILES_IN_ROW -2 downto 0) & car_regs(I)(TILES_IN_ROW -1);
                  end if;
               end if;
            end if;
         end loop;
      end if;
   end process;

   --logika priradovanie enable posuvnym registrom
   --ak je vygenerovana speed 0 - pouzijeme 05hz engen
   --ak je vygenerovana speed 1 - pouzijeme 025hz engen
   enable_logic: for I in 0 to NUM_ROWS - 1 generate
      en_vector(I) <= en_05hz when speeds_vector(I) = '0' else en_025hz;
   end generate;

   -- logika grafickeho vystupu
   -- a) musime byt v hraniciach celeho containera
   rgb_logic : process is
   begin
      vga_hit <= '0';
      r <= "000";
      g <= "000";
      b <= "000";
      if EN = '1' then

         if ( x >= pos_x) and (x < pos_x + TILE_SIZE*TILES_IN_ROW) then
            if ( y >= pos_y) and (y < pos_y + TILE_SIZE*NUM_ROWS) then
               -- b) na zodpovedajucom tile v registroch musi byt 1
               -- odcitanim suradnic celeho objektu ziskam relativne
               -- umiestnenie vnutri. Vektor od 11 do 5 urcuje kolkokrat
               -- sa v nom nachadza cislo TILE_SIZE. (t.j. cislo tileu)
               if  car_regs(conv_integer(tile_y))(conv_integer(tile_x)) = '1' then
                  -- ak je to rychle - je to motorka 
                  -- ak je to pomale je to auto
                  if speeds_vector(conv_integer(tile_y)) = '0' then
                     if car(conv_integer(disp_y))(conv_integer(disp_x)) = '1' then
                        if dirs_vector(conv_integer(tile_y)) = '0' then
                           vga_hit <= '1';
                           b <= "111";
                        else
                           vga_hit <= '1';
                           b <= "111";
                           r <= "111";
                        end if;
                     end if;
                  else
                     if dirs_vector(conv_integer(tile_y)) = '0' then
                        if bike_right(conv_integer(disp_y))(conv_integer(disp_x)) = '1' then
                           vga_hit <= '1';
                           r <= "111";
                        end if;
                     else
                        if bike_left(conv_integer(disp_y))(conv_integer(disp_x)) = '1' then
                           vga_hit <= '1';
                           g <= "111";
                        end if;
                     end if;
                  end if;
               end if; 
            end if;
         end if;
      end if;
   end process;

   -- indikacia smrti zaby
   frog_dead_indicator: process(frog_x,frog_y)
   begin
      frog_dead <= '0';
      if EN ='1' then
         if ( frog_x >= pos_x) and (frog_x < pos_x + TILE_SIZE*TILES_IN_ROW) then

            if ( frog_y >= pos_y) and (frog_y < pos_y + TILE_SIZE*NUM_ROWS) then

               if car_regs(conv_integer(frog_tile_y))(conv_integer(frog_tile_x)) = '1' then
                  frog_dead <= '1';
               end if;

            end if;

         end if;
      end if;
   end process;
end beh;


