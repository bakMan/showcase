#include <fitkitlib.h>
#include <string.h>


// Vypis uzivatelskej napovedy 
void print_user_help(void){
   term_send_str_crlf("Ovladanie:");
   term_send_str_crlf("4 - vlavo");
   term_send_str_crlf("6 - vpravo");
   term_send_str_crlf("2 - hore");
   term_send_str_crlf("8 - dole");
   term_send_str_crlf("A - nova hra pri obrazovke vyhry/prehry");
}

// Ziadne uzivatelske prikazy okrem napovedy nie su 
unsigned char decode_user_cmd(char *cmd_ucase, char *cmd)
{
   return CMD_UNKNOWN;
}

void fpga_initialized(){}

int main(void)
{
   initialize_hardware();

   set_led_d6(1);                       // LED D6
   set_led_d5(1);                       // LED D5
	while(1)
	   terminal_idle();                   // obsluha terminalu
}
