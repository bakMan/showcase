/*
* Subor:   proj4retazec.c
* Datum:   2011/12/17
* Autor:   Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
* Projekt: Ceske radenie
* Popis:   Hlavicka obsahujuca deklaracie funkcii pracujucich s ceskymi retazcami
*/
#include <wchar.h>
#include <stdio.h>
#include <stdbool.h>

//zakladna velkost alokovaneho priestoru pre retazce
#define VELKOST_ALOKACIE 40

//navratove chybove kody
enum navraty
{
   E_OK,
   E_MALLOC,
   E_SUBOR,
   E_ZLY_ZNAK,
};

//polozka zoznamu
typedef struct tpolozka
{
   wchar_t *retazec;
   struct tpolozka *dalsi;
   
} TPolozka;

//jednosmerne viazany zoznam
typedef struct tzoznam
{
   TPolozka *hlava;
   TPolozka *chvost;
   
} TZoznam;

//ukazatel na vseobecnu porovnavajucu funkciu dvoch retazcov
typedef bool (*leqFunkcia)(wchar_t *,wchar_t *);

/**
 * Funkcia na nacitanie riadku z daneho subotu
 * @param fp Subor z ktoreho sa ma riadok nacitat
 * @param retazec 
 * @return Chybovy kod ak pri nacitavani doslo k chybe
 * @return WEOF ak funkcia narazila na koniec suboru 
 */
int nacitajRiadok(FILE *fp,wchar_t **retazec);

/**
 * Funkcia na nacitanie riadkov textoveho suboru do jednosmerne viazaneho
 * zoznamu.
 * @param subor Nazov suboru z ktoreho sa ma nacitavat
 * @param zoz Ukazatel na zoznam do ktoreho sa nacitaju riadky suboru
 * @return Chybovy kod
 */
int nacitajSubor(const char *subor,TZoznam *zoz);

/**
 * Funkcia na zapis zoznamu riadkov do suboru.
 * @param subor Nazov suboru do ktoreho zapisujeme
 * @param zoz Zoznam riadkov na zapisanie
 * @return Chybovy kod
 */
 int zapisSubor(const char *subor,TZoznam *zoz);
 
/**
 * Funkcia na inicializaciu jednosmerne viazaneho zoznamu sirokych znakov
 * @param zoz Ukazatel na zoznam na inicializaciu
 */
void zoznamInit(TZoznam *zoz);

/**
 * Funkcia prida polozku na koniec zoznamu
 * @param zoz Ukazatel na zoznam
 * @param retazec Retazec ktory chceme pridat do zoznamu
 * @return true ak sa operacia podarila
 * @return false ak operacia zlyhala
 */
bool append(TZoznam *zoz, wchar_t *retazec);

/**
 * Funkcia zmaze prvy prvok v zozname a zaroven vrati jeho hodnotu
 * @param zoz Ukazatel na zoznam
 */
wchar_t *pop(TZoznam *zoz);

/**
 * Funkcia zmaze cely zoznam
 * @param zoz Ukazatel na zoznam
 */
void znicZoznam(TZoznam *zoz);

/**
 * Funkcia vykona selection sort nad zoznamom podla porovnavacej funkcie
 * @param zoz Zoznam nad ktorym treba vykonat zoradenie
 * @param jeMensiRovny Porovnavacia funkcia
 */
void selectSort(TZoznam *zoz,leqFunkcia jeMensiRovny);

/**
 * Funkcia vrati normalizovanu hodnotu ceskeho znaku na ktory ukazuje
 * *ukazatel, zaroven *ukazatel inkrementuje
 * @param ukazatel Ukazatel na ukazatel na siroky znak
 * @return Normalizovana hodnota sirokeho znaku pre cestionu
 */
unsigned char normZnak(wchar_t **ukazatel);

/**
 * Porovnavacia funkcia pre ceske retazce
 * @param a,b Retazce na porovnanie
 * @return true Ak je retazec a abecedne pred b
 * @return false Ak je retazec a abecedne za b
 */
bool leqCeska(wchar_t *a, wchar_t *b);
