/*
* Subor:   proj4params.c
* Datum:   2011/12/17
* Autor:   Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
* Projekt: Ceske radenie
* Popis:   Hlavicka obsahujuca definiciu univerzalnej overovacej a 
*          nacitacej funkcie urcenej na extrahovanie a overenie parametrov
*          main.
*/
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "proj4params.h"

/**
 * Funkcia overVzor overi ci dane pole stringov zodpoveda vzoru a zaroven
 * z dat na zaklade vzoru extrahuje hodnoty daneho typu, pricom ich ulozi
 * do variabilnych premennych.
 * 
 * Priklad vzoru: "--arcsin","%d","%s",NULL
 * 
 * 1. String, ktory nezacina % sa berie ako literal ktory musi byt presne
 * rovnaky v datach, inak su automaticky povazovane za nevyhovujuce vzoru.
 * 
 * 2. %d znamena ze dalsou polozkou v poli ma byt text reprezentujuci
 * cele cislo. Samotny matching a nacitanie robi funkcia scanf, preto 
 * odporucam pozriet jej manualove stranky. Nacitana celociselna hodnota
 * by sa ulozila do premennej na ktoru odkazuje prvy variabilny parameter
 * (t.j. volitelne parametre su pointre).
 * 
 * 3. %s - znamena retazec - t.j. ku kladnemu vyhodnoteniu dojde vzdy 
 * pokial retazec v poli existuje (aj ked je prazdny). Pointer na retazec 
 * sa priradi do premennej na ktoru ukazuje dalsi variabilny parameter.
 * Ten musi mat teda datovy typ char **.
 * 
 * @param vzor Pole stringov ktore reprezentuju vzor ukoncene pointrom s hodnotou null.
 * @param data Pole dat na ktorom sa bude rozpoznavanie odohravat (tiez ukoncene pointrom s hodnotou null).
 * @param ...  Zodpovedajuci pocet ukazatelov na premenne do ktorych sa maju zapisat rozpoznane hodnoty.
 * @return ARG_MATCH Ak data zodpovedaju vzoru.
 * @return ARG_ZLY_POCET Ak data obsahuju viac alebo menej poloziek ako povoluje vzor
 * @return ARG_ZLY_VSTUP Ak data obsahuju spravny pocet poloziek, ale nezodpovedaju vzoru.
 * 
 * @warning Nespravne pouzitie moze sposobit pad programu !
 * @warning Funkcia nekontroluje ci uzivatel zadal dostatok volitelnych premennych !
 * @warning Data aj vzor musia byt ukoncene hodnotou NULL !!!
 * 
 */
int overVzor(char **vzor, char **data,...) { 
   va_list pointre; 
   va_start(pointre,data);
   
   int i=0;
   void *pdata=NULL;
   
   // Budeme nacitavat zo vzoroveho pola pokial sme neprisli na koniec vzoru
   while (vzor[i] != NULL)
   {
      // ak data koncia skor ako vzor - uzivatel zadal malo argumentov
      if (data[i] == NULL)
      {
         va_end(pointre);
         return ARG_ZLY_POCET;
      }
      
      // ak je na zaciatku stringu vo vzore % - ma to specialny vyznam
      if ( vzor[i][0] == '%' )
      {
         // ak obsahuje % - budeme nieco nacitavat do volitelnych parametrov
         // musime si nacitat prvy pointer z va_list 
         pdata = va_arg(pointre,void *);
         
         if( vzor[i][1] == 's')
         {
            // ak je to %s - tak jednoducho **char dereferencujeme a priradime 
            // adresu pola kde je slovo ktore chceme nacitat - je zbytocne volat scanf
            *((char **)pdata) = data[i];
         }
         else
         {
            // ak to nie je %s - posunieme pointer na argument sscanfu
            // a podla odpovedajucej polozky vo vzore nacitame
            if( sscanf(data[i],vzor[i],pdata) != 1)
            {// ak sa nepodarilo nacitat - koncime s chybou
               va_end(pointre);
               return ARG_ZLY_VSTUP;
            }
         }
         
      }
      else
      { // ak na zaciatko nie je % povazujeme to za retazcovy vzor ktory sa ma presne zhodovat
        // sa ma presne zhodovat s datami na danom mieste
         if ( strcmp(vzor[i],data[i]) != 0)
         {
            va_end(pointre);
            return ARG_ZLY_VSTUP;
         }
         
      }
      
      i++;
   }
   
   va_end(pointre);
   // ak data pokracuju a vzor uz skoncil - mame zly pocet parametrov
   if (data[i] != NULL)
      return ARG_ZLY_POCET;
   
   // inak sa data a vzor zhoduju a ich nacitanie prebehlo uspesne
   return ARG_MATCH;
   
}
