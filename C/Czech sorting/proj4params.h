/*
* Subor:   proj4params.c
* Datum:   2011/12/17
* Autor:   Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
* Projekt: Ceske radenie
* Popis:   Hlavicka obsahujuca definiciu univerzalnej overovacej a 
*          nacitacej funkcie urcenej na extrahovanie a overenie parametrov
*          main.
*/
/**
 * Navratove hodnoty funkcie na overenie vzoru
 */
enum paramNavraty
{
  ARG_MATCH=0,
  ARG_ZLY_POCET=4,
  ARG_ZLY_VSTUP=5, 
   
};

/**
 * Funkcia overVzor overi ci dane pole stringov zodpoveda vzoru a zaroven
 * z dat na zaklade vzoru extrahuje hodnoty daneho typu, pricom ich ulozi
 * do variabilnych premennych.
 * 
 * Priklad vzoru: "--arcsin","%d","%s",NULL
 * 
 * 1. String, ktory nezacina % sa berie ako literal ktory musi byt presne
 * rovnaky v datach, inak su automaticky povazovane za nevyhovujuce vzoru.
 * 
 * 2. %d znamena ze dalsou polozkou v poli ma byt text reprezentujuci
 * cele cislo. Samotny matching a nacitanie robi funkcia scanf, preto 
 * odporucam pozriet jej manualove stranky. Nacitana celociselna hodnota
 * by sa ulozila do premennej na ktoru odkazuje prvy variabilny parameter
 * (t.j. volitelne parametre su pointre).
 * 
 * 3. %s - znamena retazec - t.j. ku kladnemu vyhodnoteniu dojde vzdy 
 * pokial retazec v poli existuje (aj ked je prazdny). Pointer na retazec 
 * sa priradi do premennej na ktoru ukazuje dalsi variabilny parameter.
 * Ten musi mat teda datovy typ char **.
 * 
 * @param vzor Pole stringov ktore reprezentuju vzor ukoncene pointrom s hodnotou null.
 * @param data Pole dat na ktorom sa bude rozpoznavanie odohravat (tiez ukoncene pointrom s hodnotou null).
 * @param ...  Zodpovedajuci pocet ukazatelov na premenne do ktorych sa maju zapisat rozpoznane hodnoty.
 * @return ARG_MATCH Ak data zodpovedaju vzoru.
 * @return ARG_ZLY_POCET Ak data obsahuju viac alebo menej poloziek ako povoluje vzor
 * @return ARG_ZLY_VSTUP Ak data obsahuju spravny pocet poloziek, ale nezodpovedaju vzoru.
 * 
 * @warning Nespravne pouzitie moze sposobit pad programu !
 * @warning Funkcia nekontroluje ci uzivatel zadal dostatok volitelnych premennych !
 * @warning Data aj vzor musia byt ukoncene hodnotou NULL !!!
 * 
 */
int overVzor(char **vzor, char **data, ...);
