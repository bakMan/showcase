/*
* Subor:   proj4.c
* Datum:   2011/12/17
* Autor:   Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
* Projekt: Ceske radenie
* Popis:   Hlavny zdrojovy subor.
*/
#include<stdio.h>
#include<stdlib.h>
#include<locale.h>
#include<assert.h>

#include "proj4retazec.h"
#include "proj4params.h"

enum akcie
{
   AK_HELP,
   AK_RADENIE,
};

char *hlasky[]=
{
  [E_MALLOC] = "Nepodarilo sa alokovat pamatovy priestor",
  [E_SUBOR]  = "Pozadovany subor sa nepodarilo otvorit",
  [ARG_ZLY_POCET] = "Zadali ste nespravny pocet argumentov",
  [ARG_ZLY_VSTUP] = "Zadali ste nespravne argumenty", 
  [E_ZLY_ZNAK] = "V subore sa nachadza nepodporovany znak",
};

char *help=
"Program proj4\n"
"Autor: Stanislav Smatana, xsmata01@stud.fit.vutbr.cz\n"
"Datum: 17.12.2011\n"
"Program sluzi na radenie retazcov v subore podla pravidiel ceskej abecedy\n"
"Pouzitie: proj2 -h\n"
"          proj2 [--loc LOC] VSTUP VYSTUP\n"
"Popis parametrov:\n"
"  -h                   Vypise tuto napovedu\n"
"  --loc LOC            Nastavi danu lokalizaciu LOC.\n"
"  VSTUP                Vstupny subor s nezoradenymi datami.\n"
"  VYSTUP               Vystupny subor pre ulozenie zoradenych dat.\n\n"
"\nPriklad pouzitia:\n"
"proj2 --loc cs_CZ.UTF-8 vstup.txt vystup.txt\n"
"Vstup: 10 100 1000\n";

//struktura na spracovanie parametrov
typedef struct tparam
{
   char *in_subor;
   char *out_subor;
   char *locale;
   int chyba;
   int akcia;
   
} TParam;


/**
 * Funkcia na chybovy vypis
 * @param err Chybovy kod
 */
void chyba(int err);

/**
 * Funkcia na spracovanie parametrov
 * @param argv Pole argumentov funkcie main
 * @return Struktura TParam obsahujuca udaje zo spracovania parametrov.
 */
TParam spracujParametre(char **argv);

int main(int argc, char **argv)
{
   //spracovanie parametrov
   TParam param;
   param = spracujParametre(argv);
   
   if(param.chyba != ARG_MATCH)
   {
      chyba(param.chyba);
      param.akcia = AK_HELP;
   }
   
   if(param.akcia == AK_HELP)
   {
      printf("%s",help);
      return EXIT_SUCCESS;
   }
   
   /*
    * V pripade ze uzivatel nezadal locale bude inicializovane na NULL.
    * Defaultne locale je vsak "" nie NULL.
    */
   if ( setlocale(LC_CTYPE, (param.locale == NULL) ? "" : param.locale) == NULL)
   {
      fprintf(stderr,"Varovanie: Zadali ste znakovu sadu ktora sa v systeme nenachadza, pouzijem defaultnu\n");
      setlocale(LC_CTYPE, "");
   }
   
   //deklarujeme si jednosmerne viazany zoznam pre riadky suboru
   TZoznam zoz;
   int navrat;

   //nacitame data zo suboru
   if ( (navrat = nacitajSubor(param.in_subor,&zoz)) != E_OK)
   {
      chyba(navrat);
      return EXIT_FAILURE;
   }
   
   //zoradime data podla ceskej abecedy
   selectSort(&zoz,leqCeska);
   
   //zapiseme vysledok do suboru
   if ( (navrat = zapisSubor(param.out_subor,&zoz)) != E_OK)
   {
      znicZoznam(&zoz);
      chyba(navrat);
      return EXIT_FAILURE;
   }   
   
   //ak je vsetko v poriadku koncime 
   //pozn.: nemusime zoznam dealokovat .. dialo sa to rovno pri zapise do suboru
   return EXIT_SUCCESS;
}

void chyba(int err)
{
   fprintf(stderr,"Chyba: %s\n",hlasky[err]);
}


TParam spracujParametre(char *argv[])
{
   TParam params;
   params.locale=NULL;
   
   
   //pole vzorov
  char *vzor_loc[]={"--loc","%s","%s","%s",NULL};
  char *vzor_nloc[]={"%s","%s",NULL};
  char *vzor_help[]={"-h",NULL};
   
   argv++;
   
   params.akcia = AK_HELP;
   if( (params.chyba =  overVzor(vzor_help, argv)) == ARG_MATCH)
      return params;
      
   //overime nase dva vzory parametrov
   params.akcia=AK_RADENIE;
   if( (params.chyba =  overVzor(vzor_loc, argv, &params.locale,&params.in_subor,&params.out_subor)) == ARG_MATCH)
      return params;
      
   params.chyba =  overVzor(vzor_nloc, argv, &params.in_subor,&params.out_subor);
   
   return params;
   
}

