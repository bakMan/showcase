/*
* Subor:   proj4retazec.c
* Datum:   2011/12/17
* Autor:   Stanislav Smatana, xsmata01@stud.fit.vutbr.cz
* Projekt: Ceske radenie
* Popis:   Zdrojovy subor obsahujuci definicie funkcii pracujucich s ceskymi retazcami
*/
#include<stdio.h>
#include<stdlib.h>
#include<wchar.h>
#include<errno.h>

#include "proj4retazec.h"

/*
 * CESKE ZNAKOVE TABULKY
 */
 #define VAHA_CH 10
 //vahy znakov pre radenie
unsigned char vahy_znakov[]=
{
   [L'a'] = 1,
   [L'á'] = 1,
   [L'A'] = 1,
   [L'Á'] = 1,
   [L'b'] = 2,
   [L'B'] = 2,
   [L'c'] = 3,
   [L'C'] = 3,
   [L'č'] = 4,
   [L'Č'] = 4,
   [L'd'] = 5,
   [L'D'] = 5,
   [L'ď'] = 5,
   [L'Ď'] = 5,
   [L'e'] = 6,
   [L'E'] = 6,
   [L'é'] = 6,
   [L'É'] = 6,
   [L'ě'] = 6,
   [L'Ě'] = 6,
   [L'f'] = 7,
   [L'F'] = 7,
   [L'g'] = 8,
   [L'G'] = 8,
   [L'h'] = 9,
   [L'H'] = 9,
   [L'i'] = 11,
   [L'I'] = 11,
   [L'í'] = 11,
   [L'Í'] = 11,
   [L'j'] = 12,
   [L'J'] = 12,
   [L'k'] = 13,
   [L'K'] = 13,
   [L'l'] = 14,
   [L'L'] = 14,
   [L'm'] = 15,
   [L'M'] = 15,
   [L'n'] = 16,
   [L'N'] = 16,
   [L'ň'] = 16,
   [L'Ň'] = 16,
   [L'o'] = 17,
   [L'O'] = 17,
   [L'ó'] = 17,
   [L'Ó'] = 17,
   [L'p'] = 18,
   [L'P'] = 18,
   [L'q'] = 19,
   [L'Q'] = 19,
   [L'r'] = 20,
   [L'R'] = 20,
   [L'ř'] = 21,
   [L'Ř'] = 21,
   [L's'] = 22,
   [L'S'] = 22,
   [L'š'] = 23,
   [L'Š'] = 23,
   [L't'] = 24,
   [L'T'] = 24,
   [L'ť'] = 24,
   [L'Ť'] = 24,
   [L'u'] = 25,
   [L'U'] = 25,
   [L'ú'] = 25,
   [L'Ú'] = 25,
   [L'ů'] = 25,
   [L'Ů'] = 25,
   [L'v'] = 26,
   [L'V'] = 26,
   [L'w'] = 27,
   [L'W'] = 27,
   [L'x'] = 28,
   [L'X'] = 28,
   [L'y'] = 29,
   [L'Y'] = 29,
   [L'ý'] = 29,
   [L'Ý'] = 29,
   [L'z'] = 30,
   [L'Z'] = 30,
   [L'ž'] = 31,
   [L'Ž'] = 31,
    
};

//vzajomne vahy znakov s diakritikou
//vsetky ostatne MAJU HODNOTU 0 !!!
unsigned char vahy_diakritiky[]=
{
   [L'á'] = 50,
   [L'Á'] = 50,
   [L'ď'] = 50,
   [L'Ď'] = 50,
   [L'é'] = 50,
   [L'É'] = 50,
   [L'ě'] = 100,
   [L'Ě'] = 100,
   [L'í'] = 50,
   [L'Í'] = 50,
   [L'ň'] = 50,
   [L'Ň'] = 50,
   [L'ó'] = 50,
   [L'Ó'] = 50,
   [L'ť'] = 50,
   [L'Ť'] = 50,
   [L'ú'] = 50,
   [L'Ú'] = 50,
   [L'ů'] = 100,
   [L'Ů'] = 100,
   [L'ý'] = 50,
   [L'Ý'] = 50,
   
   
};

/**
 * Funkcia na nacitanie riadku z daneho subotu
 * @param fp Subor z ktoreho sa ma riadok nacitat
 * @param retazec 
 * @return Chybovy kod ak pri nacitavani doslo k chybe
 * @return WEOF ak funkcia narazila na koniec suboru 
 */
int nacitajRiadok(FILE *fp,wchar_t **retazec)
{
      
   
   // najprv alokujeme pamatovy priestor
   if((*retazec = (wchar_t *) malloc(sizeof(wchar_t) * VELKOST_ALOKACIE)) == NULL)
      return E_MALLOC;
      
   wint_t znak;
   int velkost_buff = VELKOST_ALOKACIE; // zakladna velkost alokacie
   wchar_t *novy_buff=NULL;
    
   int i=0;
   while ((znak = fgetwc(fp)) != L'\n' && znak != WEOF)
   {
      if (znak > L'ž')
      {
         free(*retazec);
         return E_ZLY_ZNAK;
      }
         
      //kontrolujeme ci nam este staci buffer
      if(i == velkost_buff - 1)
      {
         //zdvojnasobime buffer
         velkost_buff *= 2;
         if((novy_buff = realloc(*retazec, sizeof(wchar_t) * velkost_buff)) == NULL)
         {
            free(*retazec);
            return E_MALLOC;
         }
         else
            *retazec = novy_buff;
      }
      //ulozime znak do retazca
      (*retazec)[i] =  (wchar_t) znak;
      i++;
   }
   
   //WEOF vznika aj pokial je nacitany znak chybny, aj pokial subor konci !
   if (znak == WEOF)
   {
      if(errno == EILSEQ)
      { //pripad chybneho znaku
         free(*retazec); 
         return E_ZLY_ZNAK;
      }//pripad konca suboru
      else if(!i)
      {
         free(*retazec);
         return WEOF;
      }
   }
   
   //retazec musi byt ukonceny \0
   
   (*retazec)[i] =  L'\0';
   
   //vraciame retazec
   return E_OK;
   
}

/**
 * Funkcia na nacitanie riadkov textoveho suboru do jednosmerne viazaneho
 * zoznamu.
 * @param subor Nazov suboru z ktoreho sa ma nacitavat
 * @param zoz Ukazatel na zoznam do ktoreho sa nacitaju riadky suboru
 * @return Chybovy kod
 */
int nacitajSubor(const char *subor,TZoznam *zoz)
{
   FILE *fp=NULL;
   if((fp = fopen(subor,"r")) == NULL)
      return E_SUBOR;
   
   zoznamInit(zoz);
   
   int navrat=E_OK;
   wchar_t *riadok=NULL;
   
   while((navrat = nacitajRiadok(fp,&riadok)) == E_OK)
   {
      if(!append(zoz,riadok))
      {
         znicZoznam(zoz);
         fclose(fp);
         return E_MALLOC;
      }
      
   }
   
   if (navrat == E_MALLOC || navrat == E_ZLY_ZNAK)
   {
      znicZoznam(zoz);
      fclose(fp);
      return navrat;
   }
   
   fclose(fp);
   return E_OK;
}

/**
 * Funkcia na zapis zoznamu riadkov do suboru.
 * @param subor Nazov suboru do ktoreho zapisujeme
 * @param zoz Zoznam riadkov na zapisanie
 * @return Chybovy kod
 */
int zapisSubor(const char *subor, TZoznam *zoz)
{
   FILE *fp=NULL;
   if((fp = fopen(subor,"w")) == NULL)
      return E_SUBOR;
      
   wchar_t *riadok=NULL;
   while(zoz->hlava != NULL)
   {
      riadok = pop(zoz);
      fwprintf(fp,L"%ls\n",riadok);
      free(riadok);
   }
   
   fclose(fp);
   return E_OK;
   
}

/**
 * Funkcia na inicializaciu jednosmerne viazaneho zoznamu sirokych znakov
 * @param zoz Ukazatel na zoznam na inicializaciu
 */
void zoznamInit(TZoznam *zoz)
{
   zoz->hlava = NULL;
   zoz->chvost = NULL;
}

/**
 * Funkcia prida polozku na koniec zoznamu
 * @param zoz Ukazatel na zoznam
 * @param retazec Retazec ktory chceme pridat do zoznamu
 * @return true ak sa operacia podarila
 * @return false ak operacia zlyhala
 */
bool append(TZoznam *zoz, wchar_t *retazec)
{
   TPolozka *nova=NULL;
   
   if ((nova = malloc(sizeof(TPolozka))) == NULL)
      return false;
   
   nova->dalsi=NULL;
   nova->retazec = retazec;
   
   //ak bola hlava zoznamu NULL - t.j. mali sme prazdny zoznam
   //bude aj ona ukazovat na tento prvok - t.j. mame prvok s jenou polozkou
   if(zoz->hlava == NULL)
      zoz->hlava = nova;
   
   //ak zoznam nie je prazdny musime existujucemu chvostu dat novu dalsiu
   //a zaroven predosla bude nastavena na predchadzajuci chvost
   if(zoz->chvost != NULL)
      zoz->chvost->dalsi=nova;
   
   zoz->chvost=nova;
         
   return true;
}

/**
 * Funkcia zmaze prvy prvok v zozname a zaroven vrati jeho hodnotu
 * @param zoz Ukazatel na zoznam
 */
wchar_t *pop(TZoznam *zoz)
{
   wchar_t *retazec=NULL;
   
   // musime kontrolovat ci nie je zoznam prazdny - inak SEGFAULT
   if (zoz->hlava == NULL)
      return NULL;
      
   TPolozka *tmp=NULL;
   //odlozime si novu hlavu zoznamu
   tmp = zoz->hlava->dalsi;
   //zachivame retazec
   retazec = zoz->hlava->retazec;
   //mozme polozku uvolnit
   free(zoz->hlava);
   
   zoz->hlava = tmp;
   
   return retazec;
}

/**
 * Funkcia zmaze cely zoznam
 * @param zoz Ukazatel na zoznam
 */
void znicZoznam(TZoznam *zoz)
{
   while(zoz->hlava != NULL)
   {
      TPolozka *tmp=NULL;
      tmp=zoz->hlava;
      
      //hlavu posunieme dalej
      zoz->hlava = zoz->hlava->dalsi;
   
      //prvy zmazeme
      free(tmp->retazec);
      free(tmp);
   }
   
}


/**
 * Funkcia vykona selection sort nad zoznamom podla porovnavacej funkcie
 * @param zoz Zoznam nad ktorym treba vykonat zoradenie
 * @param jeMensiRovny Porovnavacia funkcia
 */
void selectSort(TZoznam *zoz,leqFunkcia jeMensiRovny)
{
   /*
    * Zarazka bude ukazovat odkial zacina nezoradena cast pola
    */
   TPolozka *zarazka = zoz->hlava; 
   TPolozka *aktualna=NULL;
   TPolozka *minimalna=NULL;
   
   // V pripade ze za zarazkou uz nie je nic - nemusime dalej radit
   while(zarazka !=NULL && zarazka->dalsi != NULL)
   {
      aktualna = zarazka->dalsi;
      minimalna = zarazka;
      
      //prechadzame od zarazky na koniec zoznamu a hladame najmensi prvok
      while(aktualna != NULL)
      {
         //to v akom vztahu prvky su zistujeme pomocou funkcie, ktora nam
         //bola dana ako parameter
         if(jeMensiRovny(aktualna->retazec,minimalna->retazec))
            minimalna=aktualna;
         
         aktualna=aktualna->dalsi;
      }
      
      //ak je najmensi prvok nasa zarazka ziadne vymeny robit nemusime
      if(minimalna != zarazka)
      {  //vymenime polozkam DATA - keby sme mali vymienat polozky je to humus
         wchar_t *tmp=zarazka->retazec;
         zarazka->retazec=minimalna->retazec;
         minimalna->retazec=tmp;
         
      }
      
      
      zarazka=zarazka->dalsi;
   }
   
}

/**
 * Funkcia vrati normalizovanu hodnotu ceskeho znaku na ktory ukazuje
 * *ukazatel, zaroven *ukazatel inkrementuje
 * @param ukazatel Ukazatel na ukazatel na siroky znak
 * @return Normalizovana hodnota sirokeho znaku pre cestionu
 */
unsigned char normZnak(wchar_t **ukazatel)
{
   /*
    * Zistime ci je na danom mieste znak CH, t.j. postupnost znakov 'C' a 'H'.
    * V nasom poli s vahami ma C aj c rovnaku vahu, takisto H aj h ma rovnaku vahu.
    * Preto mozme nimi zaindexovat pole a zistit ci sa rovnaju vahe maleho c a h.
    * ak ano zvysime ukazatel o dva aby ukazoval na dalsi znak na nacitanie.
    */
   if ( vahy_znakov[*(*ukazatel)] == vahy_znakov[L'c'] && vahy_znakov[*((*ukazatel) + 1)] == vahy_znakov[L'h'])
   {
      *ukazatel += 2;
      return VAHA_CH;
   }
   
   //vratime vahu a posunieme ukazatel o jeden prvok
   return vahy_znakov[(*((*ukazatel)++))];
}

/**
 * Relacia usporiadania pre dva ceske znaky.
 * @param a,b Siroky cesky retazec
 * @return Pravda ak je retazec a mensi (t.j. abecedne pred) alebo rovny retazu b
 */
bool leqCeska(wchar_t *a,wchar_t *b)
{
   //funkcia normZnak hybe s pointrami - preto si ich musime zachovat
   wchar_t *a2 = a;
   wchar_t *b2 = b;
   int vysledok = 0;
   
   /*
    * Do vysledku si vzdy ukladame rozdiel dvoch normalizovanych hodnot znakov.
    * Pokial je ich rozdiel nulovy, t.j. su rovnake - iterujeme dalej.
    * Ochrana pretecenia je zabezpecena tym ze ak by bol jeden zo znakov nulovy
    * a jeden nenulovy podmienka nebude platit a cyklus konci. Problem nastava
    * ak su oba nulove. Preto musime este testovat jeden ci nahodou nie je nulovy
    * ak je tak takisto je nutne skoncit.
    */
   while (!(vysledok = (normZnak(&a2) - normZnak(&b2))) && *b2);
   
   /*
    * Ak su po porovnavani bez diakritiky riadky stale rovnake - porovnavame s diakritikou.
    * Mozme si dovolit rovnaky princip pretoze vahy pre '\0' budu 0.
    */
   if (vysledok == 0)
      while (!(vysledok = (vahy_diakritiky[*a] - vahy_diakritiky[*b])) && *b) a++,b++;
   
   //odcitavame a - b, t.j. ak je a mensie - vysledok je mensi nez 0
   if (vysledok <= 0)
      return true;
   else
      return false;
   
}

