/*
 * Implementacia klasickeho synchronizacneho problemu Reader-Writer
 * Stanislav Smatana, xsmata01, 1.5.2012
 * 
 * sh_mem - Funkcie pre pracu so zdielanou pamatou
 */

#include <assert.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>

#include "sh_mem.h"
#include "error.h"


/**
 * Funkcia pre inicializaciu struktury zdielanej pamate t_shared
 * @param mem Ukazatel na strukturu zdielanej pamate
 */
void initPamat(t_shared *mem)
{
   mem->fd=0;
   mem->data=NULL;
   mem->meno=NULL;
}

/**
 * Funkcia pre vytvorenie zdielanej pamate ako pola integerov
 * @param pocet Pocet integerov v zdielanej pamati
 * @param shared Ukazatel na strukturu zdielanej pamati
 * @param meno Meno virtualnej pamati
 * @return E_OK ak vsetko prebehlo v poriadku
 * @return E_SHMEM ak sa nepodarilo vytvorit zdielanu pamat
 * @note Parameter pocet nie je velkost v bajtoch ale nasobkoch int
 */
int vytvorPamatInt(t_shared *shared,char *meno,int pocet)
{
   
   //pointre nastavime najprv na NULL - inak by znicPamat
   //mohol vyvolat segfault
   shared->meno = meno;
   shared->data = NULL;
   
   if ( (shared->fd=shm_open(shared->meno, O_CREAT|O_RDWR, 0600)) < 0)
      return E_SHMEM;
   
   if (ftruncate(shared->fd, sizeof(int) * pocet))
   {
      znicPamat(shared);
      return E_SHMEM;
   }
   
   if ( (shared->data = mmap(NULL, sizeof(int) * pocet, PROT_READ | PROT_WRITE,  MAP_SHARED, shared->fd,0)) == MAP_FAILED)
   {
      znicPamat(shared);
      return E_SHMEM;
   }
   
   return E_OK;
}

/**
 * Funkcia pre vytvorenie zdielanej pamate ako pola integerov
 * @param pocet Pocet integerov v zdielanej pamati
 * @param shared Ukazatel na strukturu zdielanej pamati
 * @param meno Meno virtualnej pamati
 * @return E_OK ak vsetko prebehlo v poriadku
 * @return E_SHMEM ak sa nepodarilo vytvorit zdielanu pamat
 * @note Parameter pocet nie je velkost v bajtoch ale nasobkoch int
 */
int vytvorMutexy(t_shared *shared,char *meno,int pocet)
{
   
   //meno pomenovanej zdielanej pamate
   shared->meno = meno;
   
   if ( (shared->fd=shm_open(shared->meno, O_CREAT|O_RDWR, 0600)) < 0)
      return E_SHMEM;
   
   if (ftruncate(shared->fd, sizeof(sem_t) * pocet))
   {
      znicPamat(shared);
      return E_SHMEM;
   }
   
   if ( (shared->data = mmap(NULL, sizeof(sem_t) * pocet, PROT_READ | PROT_WRITE,  MAP_SHARED, shared->fd,0)) == MAP_FAILED)
   {
      znicPamat(shared);
      return E_SHMEM;
   }
   
   //vytvaranie nepomenovanych POSIX semaforov v zdielanej pamati
   for(int i=0;i<pocet;i++)
      if(sem_init(&((sem_t *) shared->data)[i], 1, 1))
      {
         znicPamat(shared);
         return E_SEMAPHORE;
      }
   
   return E_OK;
}

/**
 * Funkcia na dealokaciu zdielanej pamate
 * @param shared Ukazatel na strukturu zdielanej pamati
 */
void znicPamat(t_shared *shared)
{
   close(shared->fd);
   shm_unlink(shared->meno);
   
   shared->meno=NULL;
   shared->data=NULL;
}
