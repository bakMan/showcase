/*
 * Implementacia klasickeho synchronizacneho problemu Reader-Writer
 * Stanislav Smatana, xsmata01, 1.5.2012
 */

#include <stdlib.h>
#include <stdio.h> 
#include <semaphore.h>
#include <signal.h>

#include "parent.h"
#include "sh_mem.h"
#include "error.h"
#include "child.h"


char *helpstring=
{"Pouzitie: readerWriter W R C SW SR OUT\n"
"\nW   : pocet pisarov"
"\nR   : pocet citatelov"
"\nC   : pocet cyklov"
"\nSW  : casovy interval pre pisarov"
"\nSR  : casovy interval pre citatelov"
"\nOUT : vystupny subor, '-' pre stdout\n\n"};


int main(int argc, char **argv)
{
   TParam params;
   t_shared semafory,data;
   int ret=0;
   
   initParam(&params);
   initPamat(&semafory);
   initPamat(&data);
   
   // vytvorenie pola 6 binarnych semaforov a pola 4 int v zdielanom
   // priestore
   if ((ret = spracujParametre(argc,argv,&params)) != E_OK)
   {
      error(ret);
      exit(1);
   }
   
   if (params.akcia == AK_HELP)
   {
      printf("%s",helpstring);
      exit(0);
   }
   
   if((ret = vytvorMutexy(&semafory,"/xsmata01.mutexes",7)) != E_OK)
   {
      error(ret);
      exit(2);
   }
   
   if((ret = vytvorPamatInt(&data,"/xsmata01.data",4)) != E_OK)
   {
      error(ret);
      exit(2);
   }
   
   
   //SHM je MAKRO pre pristup do zdielanej pamate! 
   //inicializacia zdielanej pamate
   //pre uplny vyznam makra SHM vid. sh_mem.h
   SHM(data,CNT)=1;
   SHM(data,DATA)=-1;
   SHM(data,READ_CNT)=0;
   SHM(data,WRITE_CNT)=0;
   
   
   //vytvorenie deti
   TChildren deti;
   if ((ret = vytvorDeti(&params,&deti,semafory,data)) != E_OK)
   {
      zabiDeti(&deti);
      freeDeti(&deti);
      znicPamat(&semafory);
      znicPamat(&data);
      fclose(params.out);
      error(ret);
      exit(2);
   }
   
   
   //cakanie na writerov
   cakajNaWriterov(&deti);
   
   //zapis nuly
   sem_wait(SEM(semafory,WRITE_SEM));
      SHM(data,DATA)=0;
   sem_post(SEM(semafory,WRITE_SEM));
   
   //cakanie na readerov
   cakajNaReaderov(&deti);
   
   //exit

   freeDeti(&deti);
   znicPamat(&semafory);
   znicPamat(&data);
   fclose(params.out);
   exit(0);
}


