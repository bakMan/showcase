/*
 * Implementacia klasickeho synchronizacneho problemu Reader-Writer
 * Stanislav Smatana, xsmata01, 1.5.2012
 * 
 * child - Hlavne funkcie detskych procesov pisara a citatela
 */
#include "sh_mem.h"

/**
 * Funkcia reprezentujuca main writera
 * @param id Id pisara
 * @param wtime Vrchna hranica casoveho intervalu pre generoanie spanku
*  @param semafory Struktura zdielanej pamate so semaformi
 * @param data Struktura zdielanej pamate s datami
 * @param cycles Pocet zapisovacich cyklov
 * 
 */
void writerMain(int id, int wtime,FILE *out, t_shared semafory, t_shared data, int cycles);


/**
 * Funkcia reprezentujuca main readera
 * @param id Id pisara
 * @param wtime Vrchna hranica casoveho intervalu pre generoanie spanku
 * @param semafory Struktura zdielanej pamate so semaformi
 * @param data Struktura zdielanej pamate s datami
 * 
 */
void readerMain(int id, int wtime,FILE *out,t_shared semafory, t_shared data);


/**
 * Funkcia vypise do suboru out spravu msg pricom respektuje semafory
 * a zvysuje citac akcii o 1
 */
void childMsg(int id,FILE *out, t_shared semafory, t_shared data,int msg);
