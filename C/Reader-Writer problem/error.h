//error.h - enumeracia chybovych kodov pre cely program
/**
 * Chybove kody
 */
enum errCodes
{
   E_OK,
   E_ARGC,
   E_ARGV,
   E_FILE,
   E_SHMEM,
   E_SEMAPHORE,
   E_MALLOC,
   E_FORK,
};
