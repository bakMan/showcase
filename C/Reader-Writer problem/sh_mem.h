/*
 * Implementacia klasickeho synchronizacneho problemu Reader-Writer
 * Stanislav Smatana, xsmata01, 1.5.2012
 * 
 * sh_mem - Funkcie pre pracu so zdielanou pamatou
 */

#ifndef SHMEM_H
#define SHMEM_H

//makra pre pretypovanie a pohodlnejsi pristup k datam v t_shared
// pre pamat integerov
#define SHM(mem,index)   ((int *) mem.data)[index]
//pre semafory
#define SEM(mem,index)   &((sem_t *) mem.data)[index]

//enumeracie pre prehladnu indexaciu v poli semaforov a dat
enum sem
{
   OUT_SEM,
   READCNT_SEM,
   WRITECNT_SEM,
   READ_SEM_A,
   READ_SEM_B,
   WRITE_SEM,
   CNT_SEM
};

enum data
{
   CNT,
   DATA,
   READ_CNT,
   WRITE_CNT,
};
   
   

/**
 * Obecna struktura pre zdielanu pamat
 * fd - file descriptor POSIX objektu zdielanej pamaty
 * data - pointer na memory mapped shared memory object
 */
typedef struct t_shared
{
   int fd;
   void *data;
   char *meno;
} t_shared;


/**
 * Funkcia pre inicializaciu struktury zdielanej pamate t_shared
 * @param mem Ukazatel na strukturu zdielanej pamate
 */
void initPamat(t_shared *mem);

/**
 * Funkcia pre vytvorenie mutexov v zdielanej pamati
 * @param shared Ukazatel na strukturu zdielanej pamati
 * @param pocet Pocet mutexov na vytvorenie
 * @param meno Meno virtualnej pamati
 * @return E_OK ak vsetko prebehlo v poriadku
 * @return E_SHMEM ak sa nepodarilo vytvorit zdielanu pamat
 * @return E_SEMAPHORE ak sa nepodarilo vytvorit semafory
 */
int vytvorMutexy(t_shared *shared,char *meno,int pocet);

/**
 * Funkcia pre vytvorenie zdielanej pamate ako pola integerov
 * @param pocet Pocet integerov v zdielanej pamati
 * @param shared Ukazatel na strukturu zdielanej pamati
 * @param meno Meno virtualnej pamati
 * @return E_OK ak vsetko prebehlo v poriadku
 * @return E_SHMEM ak sa nepodarilo vytvorit zdielanu pamat
 * @note Parameter pocet nie je velkost v bajtoch ale nasobkoch int
 */
int vytvorPamatInt(t_shared *shared,char *meno,int pocet);

/**
 * Funkcia na dealokaciu zdielanej pamate
 * @param shared Ukazatel na strukturu zdielanej pamati
 */
void znicPamat(t_shared *shared);

#endif
