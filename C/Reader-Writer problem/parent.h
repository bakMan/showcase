/*
 * Implementacia klasickeho synchronizacneho problemu Reader-Writer
 * Stanislav Smatana, xsmata01, 1.5.2012
 * 
 * parent - Funkcie rodicovskeho procesu
 */

#include "sh_mem.h"

/**
 * Akcie ktore program moze vykonavat
 * 1. vypisat napovedu
 * 2. spustit simulaciu
 */
enum Akcie
{
   AK_HELP,
   AK_RUN
};


/**
 * Struktura pre ulozenie parametrov programu
 */
typedef struct param
{
   int akcia;
   int writers;
   int readers;
   int cycles;
   int wtime;
   int rtime;
   FILE *out;
} TParam;
   
/**
 * Struktura obsahujuca pole pid readrov a writerov
 * a pocet uspesne vytvorenych deti
 */
typedef struct tchildren
{
   int *readers;
   int *writers;
   int num_writers;
   int num_readers;
}TChildren;


/**
 * Funkcia na vypisanie chybovej hlasky a ukoncenie programu s chybou
 * @param errnum Index do pola chybovych hlasok errors
 */
void error(int errnum);

/**
 * Funkcia pre inicializaciu struktury TParam
 * @param params Ukazatel na strukturu TParam
 */
void initParam(TParam *params);


/**
 * Funkcia na spracovanie parametrov programu a ich ulozenie do
 * struktury TParam
 * @param params Ukazatel na strukturu TParam
 * @return E_ARGC Ak uzivatel zadal zly pocet parametrov
 * @return E_ARGV Ak uzivatel zadal 1 alebo viac nespravnych parametrov
 * @return E_FILE Ak sa nepodarilo otvorit uzivatelom zadany subor OUT
 * @return E_OK Ak vsetko prebehlo v poriadku
 */
int spracujParametre(int argc,char **argv,TParam *params);

/**
 * Funkcia na vytvorenie deti
 * @param params Ukazatel na strukturu parametrov programu
 * @param children Ukazatel na prazdnu strukturu TChildren
 * @param semafory Struktura zdielanej pamate so semaformi
 * @param data Struktura zdielanej pamate s datami
 * @return E_OK ak je vsetko v poriadku
 * @return E_FORK ak sa nepodarilo vytvorit nejake dieta
 */
int vytvorDeti(TParam *params, TChildren *children, t_shared semafory, t_shared data);

/**
 * Funkcia odosle vsetkym detom signal SIGTERM
 * @param deti Ukazatel na strukturu TCHildren
 */
void zabiDeti(TChildren *deti);

/**
 * Funkcia na uvolnenie alokovanych dat v strukture TChildren
 * @param deti ukazatel na strukturu TChildren
 */
void freeDeti(TChildren *deti);

/**
 * Funkcia cakania na ukoncenie vsetkych writerov
 * @param children Ukazatel na strukturu obsahujucu pid deti
 */
void cakajNaWriterov(TChildren *children);

/**
 * Funkcia cakania na ukoncenie vsetkych writerov
 * @param children Ukazatel na strukturu obsahujucu pid deti
 */
void cakajNaReaderov(TChildren *children);

