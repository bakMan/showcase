/*
 * Implementacia klasickeho synchronizacneho problemu Reader-Writer
 * Stanislav Smatana, xsmata01, 1.5.2012
 * 
 * child - Hlavne funkcie detskych procesov pisara a citatela
 */

#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

#include "child.h"
#include "sh_mem.h"

enum msgs
{
   W_NEW_VALUE,
   W_READY,
   W_WRITES,
   W_WRITTEN,
   R_READY,
   R_READS,
   R_READ,
};


char *msgs[]=
{  "%d: writer: %d: new value\n",
   "%d: writer: %d: ready\n",
   "%d: writer: %d: writes a value\n",
   "%d: writer: %d: written\n",
   "%d: reader: %d: ready\n",
   "%d: reader: %d: reads a value\n",
   "%d: reader: %d: read: %d\n",
};


/**
 * Funkcia reprezentujuca main writera
 * @param id Id pisara
 * @param wtime Vrchna hranica casoveho intervalu pre generoanie spanku
*  @param semafory Struktura zdielanej pamate so semaformi
 * @param data Struktura zdielanej pamate s datami
 * @param cycles Pocet zapisovacich cyklov
 * 
 */
void writerMain(int id, int wtime,FILE *out, t_shared semafory, t_shared data, int cycles)
{
   for(int i=0; i < cycles; i++)
   {
      // new value
      childMsg(id,out,semafory,data,W_NEW_VALUE);
      
      //uspanie *1000 aby sme zmenili mikrosekundy na milisekundy
      srand(time(0));
      
      // pozor delenie 0 by vyvolalo SIGFPE
      //generovanie nahodneho cisla pomocou hornych bitov - su najnahodnejsie
      // vid. man 3 rand
      if(wtime !=0)
         usleep(0 + (int)(wtime*1000 * (rand() / (RAND_MAX + 1.0))));
      
      //ready
      childMsg(id,out,semafory,data,W_READY);
      
      //zvysime pocet writerov aktualne nachadzajucich sa v kritickej
      //sekcii
      sem_wait(SEM(semafory,WRITECNT_SEM));
         SHM(data,WRITE_CNT)=SHM(data,WRITE_CNT) +1;
      
         if( SHM(data,WRITE_CNT) == 1)
            sem_wait(SEM(semafory,READ_SEM_B));
      sem_post(SEM(semafory,WRITECNT_SEM));
      
      //vstup do kritickej sekcie
      sem_wait(SEM(semafory,WRITE_SEM));
         childMsg(id,out,semafory,data,W_WRITES);
         SHM(data,DATA)=id;
         childMsg(id,out,semafory,data,W_WRITTEN);
      sem_post(SEM(semafory,WRITE_SEM));
      
      //dekrementacia poctu writerov v kritickej sekcii poprip. uvolnenie 
      //readrov
      sem_wait(SEM(semafory,WRITECNT_SEM));
         SHM(data,WRITE_CNT)=SHM(data,WRITE_CNT) - 1;
      
         if( SHM(data,WRITE_CNT) == 0)
            sem_post(SEM(semafory,READ_SEM_B));
         
      sem_post(SEM(semafory,WRITECNT_SEM));
   }
}

/**
 * Funkcia reprezentujuca main readera
 * @param id Id pisara
 * @param wtime Vrchna hranica casoveho intervalu pre generoanie spanku
 * @param semafory Struktura zdielanej pamate so semaformi
 * @param data Struktura zdielanej pamate s datami
 * 
 */
void readerMain(int id, int rtime,FILE *out, t_shared semafory, t_shared data)
{
   int hodnota=-1;
   
   do
   {
      childMsg(id,out,semafory,data,R_READY);
      
      //zvysenie poctu citatelov v kritickej sekcii
      sem_wait(SEM(semafory,READ_SEM_A));
         sem_wait(SEM(semafory,READ_SEM_B));
            sem_wait(SEM(semafory,READCNT_SEM));
         
               SHM(data,READ_CNT) = SHM(data,READ_CNT) + 1;
               if (SHM(data,READ_CNT) == 1)
         
                  sem_wait(SEM(semafory,WRITE_SEM));
            sem_post(SEM(semafory,READCNT_SEM));
         sem_post(SEM(semafory,READ_SEM_B));
      sem_post(SEM(semafory,READ_SEM_A));
      
      childMsg(id,out,semafory,data,R_READS);
      hodnota=SHM(data,DATA);
      
      //vypis precitanej hodnoty je trosku iny 
      sem_wait(SEM(semafory,CNT_SEM));
         sem_wait(SEM(semafory,OUT_SEM));
         
            fprintf(out,msgs[R_READ],SHM(data,CNT),id,hodnota);
            fflush(out);
         
         sem_post(SEM(semafory,OUT_SEM));
      
         SHM(data,CNT) = SHM(data,CNT) + 1;
      
      sem_post(SEM(semafory,CNT_SEM));
      
      sem_wait(SEM(semafory,READCNT_SEM));
         
         SHM(data,READ_CNT) = SHM(data,READ_CNT) - 1;
         if (SHM(data,READ_CNT) == 0)
            sem_post(SEM(semafory,WRITE_SEM));
      
      sem_post(SEM(semafory,READCNT_SEM));
      
      //pozor delenie 0 by vyvolalo SIGFPE
      //generovanie nahodneho cisla pomocou hornych bitov - su najnahodnejsie
      // vid. man 3 rand
      if(rtime != 0)
         usleep(0 + (int)(rtime*1000 * (rand() / (RAND_MAX + 1.0))));
      
   }while(hodnota != 0);
}

/**
 * Funkcia vypise do suboru out spravu msg pricom respektuje semafory
 * a zvysuje citac akcii o 1
 */
void childMsg(int id,FILE *out, t_shared semafory, t_shared data,int msg)
{
   sem_wait(SEM(semafory,CNT_SEM));
      sem_wait(SEM(semafory,OUT_SEM));
         fprintf(out,msgs[msg],SHM(data,CNT),id);
         fflush(out);
      sem_post(SEM(semafory,OUT_SEM));
   
      SHM(data,CNT) = SHM(data,CNT) + 1;
   sem_post(SEM(semafory,CNT_SEM));
    
}
