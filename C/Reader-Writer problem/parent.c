/*
 * Implementacia klasickeho synchronizacneho problemu Reader-Writer
 * Stanislav Smatana, xsmata01, 1.5.2012
 * 
 * parent - Funkcie rodicovskeho procesu
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

#include "error.h"
#include "parent.h"
#include "child.h"


/**
 * Chybove hlasky
 */
char *errors[]=
{
   [E_OK] = NULL,
   [E_ARGC] = "Zadali ste nespravny pocet argumentov (info param. -h)",
   [E_ARGV] = "Zadali ste nespravne parametre (info param. -h) ",
   [E_FILE] = "Nepodarilo sa otvorit vystupny subor",
   [E_SHMEM] ="Nepodatilo sa otvorit zdielanu pamat",
   [E_SEMAPHORE] = "Nepodarilo sa vytvorit semafory",
   [E_MALLOC] = "Nepodarilo sa alokovat miesto v pamati",
   [E_FORK] = "Nepodarilo sa vytvorit vsetky detske procesy",
};

/**
 * Funkcia na vypisanie chybovej hlasky a ukoncenie programu s chybou
 * @param errnum Index do pola chybovych hlasok errors
 */
void error(int errnum)
{
   fprintf(stderr,"Chyba: %s\n",errors[errnum]);
   
}

/**
 * Funkcia pre inicializaciu struktury TParam
 * @param params Ukazatel na strukturu TParam
 */
void initParam(TParam *params)
{
   params->akcia=-1;
   params->writers=0;
   params->readers=0;
   params->cycles=0;
   params->wtime=0;
   params->rtime=0;
   params->out=NULL;
}

/**
 * Funkcia na spracovanie parametrov programu a ich ulozenie do
 * struktury TParam
 * @param params Ukazatel na strukturu TParam
 * @return E_ARGC Ak uzivatel zadal zly pocet parametrov
 * @return E_ARGV Ak uzivatel zadal 1 alebo viac nespravnych parametrov
 * @return E_FILE Ak sa nepodarilo otvorit uzivatelom zadany subor OUT
 * @return E_OK Ak vsetko prebehlo v poriadku
 */
int spracujParametre(int argc,char **argv,TParam *params)
{
   
   //ak mame iba jeden argument - moze to byt iba help
   if ( argc == 2)
   {
      if ( strcmp(argv[1],"-h") == 0)
      {
         params->akcia=AK_HELP;
         return E_OK;
      }
      else
         return E_ARGV;
   }
   
   //ak nebol argc 2 a je iny ako 7 je to chyba
   
   if (argc != 7)
      return E_ARGC;
   
   //mame 7 argumentov
   //prvych 5 je ciselnych - pomozem si polom
   int tmp[5];
   
   // note argv[i + 1] pretoze argv[0] je nazov programu
   for(int i=0; i < 5; i++)
      if(sscanf(argv[i + 1],"%d",&tmp[i]) != 1)
         return E_ARGV;
   
   //posledny argument je outfile ak '-' stdout inak ho skusime otvorit
   
   if(strcmp(argv[6], "-") == 0)
   {
      params->out=stdout;
   }
   else
   {
      if ( (params->out = fopen(argv[6],"w")) == NULL)
         return E_FILE;
   }
   
   // spatne z pola do struktury
   params->writers=tmp[0];
   params->readers=tmp[1];
   params->cycles=tmp[2];
   params->wtime=tmp[3];
   params->rtime=tmp[4];
   
   return E_OK;
}

/**
 * Funkcia na vytvorenie deti
 * @param params Ukazatel na strukturu parametrov programu
 * @param children Ukazatel na prazdnu strukturu TChildren
 * @param semafory Struktura zdielanej pamate so semaformi
 * @param data Struktura zdielanej pamate s datami
 * @return E_OK ak je vsetko v poriadku
 * @return E_FORK ak sa nepodarilo vytvorit nejake dieta
 */
int vytvorDeti(TParam *params, TChildren *children, t_shared semafory, t_shared data)
{
   // pocet vytvorenych deti je najprv nula
   children->num_readers=0;
   children->num_writers=0;
   
   //alokujeme polia pre readrov a writerow
   
   if( (children->readers = (int *) malloc(sizeof(int) * params->readers)) == NULL )
      return E_MALLOC;
   
   //tu uz su readri allocovany - free !!!
   if ( (children->writers = (int *) malloc(sizeof(int) * params->writers)) == NULL )
   {
      free(children->readers);
      return E_MALLOC;
   }
   
   int pid;
   int sum = params->writers + params->readers;
   
   for(int i=0;i < sum;i++)
   {
      if((pid = fork()) == 0)
      { //dieta zacina bud writerMain alebo readerMain
         
         // DOLEZITE - CISLA DETI ZACINAJU OD 1 !!! preto i + 1
         if( i < params->writers)
            writerMain(i + 1, params->wtime, params->out, semafory, data, params->cycles);
         else
            readerMain(i - params->writers + 1, params->wtime, params->out, semafory, data);
      
         //uzavriet pamat, vystupny subor je vhodne aj u dietata
         fclose(params->out);
         close(semafory.fd);
         close(data.fd);
         freeDeti(children);
         exit(0);
         
      }
      else if(pid > 0)
      { // parent si pid dietata zapise do pola
         if (i < params->writers)
         {
            children->writers[i]=pid;
            children->num_writers++;
         }
         else
         {
            //musime indexovat zase od nuly, odcitame pocet writerov
            children->readers[i - params->writers ]=pid;
            children->num_readers++;
         }
      }
      else
      {  //nepodareny fork
         return E_FORK;
      }
   }
   
   return E_OK;
}

/**
 * Funkcia odosle vsetkym detom signal SIGTERM
 * @param deti Ukazatel na strukturu TCHildren
 */
void zabiDeti(TChildren *deti)
{
 
   for(int i=0; i < deti->num_writers; i++)
      kill(deti->writers[i],SIGTERM);
      
   for(int i=0; i < deti->num_readers; i++)
      kill(deti->readers[i],SIGTERM);
}

/**
 * Funkcia na uvolnenie alokovanych dat v strukture TChildren
 * @param deti ukazatel na strukturu TChildren
 */
void freeDeti(TChildren *deti)
{
   free(deti->readers);
   free(deti->writers);
   
   deti->readers=NULL;
   deti->writers=NULL;
}

/**
 * Funkcia cakania na ukoncenie vsetkych writerov
 * @param params Ukazatel na strukturu parametrov programu
 * @param children Ukazatel na strukturu obsahujucu pid deti
 */
void cakajNaWriterov(TChildren *children)
{
   for(int i=0; i < children->num_writers;i++)
      waitpid(children->writers[i],NULL,0);
}
/**
 * Funkcia cakania na ukoncenie vsetkych writerov
 * @param params Ukazatel na strukturu parametrov programu
 * @param children Ukazatel na strukturu obsahujucu pid deti
 */
void cakajNaReaderov(TChildren *children)
{
   for(int i=0; i < children->num_readers;i++)
      waitpid(children->readers[i],NULL,0);
}
