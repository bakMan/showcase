/**
 * @file error.c
 * @brief Module for error printing.
 * @author Dagmar Prokopova (xproko26)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "error.h"

/** Function with variable number of arguments for error printing.
 *
 * @param[in] fmt string containing formating signs
 */
void printError(const char* fmt, ...)
{
    va_list arguments;
    va_start (arguments, fmt);
    fprintf(stderr, "ERROR: ");
    vfprintf(stderr, fmt, arguments);
    fprintf(stderr, "\n");
    va_end(arguments);
}
