/**
 * @FILE generic.c
 * @AUTHOR Jan Remes (xremes00@stud.fit.vutbr.cz)
 * @BRIEF Provides stuff necessary for generic L-value assignment
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "global.h"
#include "structures.h"
#include "memory_interpret.h"
#include "interpret.h"




/* ************************************************************************ *
 ~ ~~~~~~~~~ Function assignGeneric (this one does the trick) ~~~~~~~~~~~~~ ~
 * ************************************************************************ */

/**
  * assignGeneric - assigns a valaue into a generic L-value
  *
  * The L-value must be specified as a correct expression containing:
  * <variable> [accessElement] ... [accessElement] <accessElement|subVar>
  * 
  * @PARAM[in] place Expression denoting where to assign
  * @PARAM[in] expression What to assign
  * @PARAM[in,out] VARTABLE VAriable table to evaluate expressions and to be changed
  */
void assignGeneric(tExpr *place, tExpr *expression, variable * VARTABLE)
{
    bool isSubVar;
    bool valid = false;
    long final_left, final_right;


    if(place->type == T_INTFUNC && place->data.index == FUNC_SUBVAR)
    {
        isSubVar = true;

        // We have guaranteed at least one parameter. Responsible:
        // syntax analyzer (Karel Benes)
        variable temp = evalExpr(place->params->next, VARTABLE);

        if(temp.type == VAR_NUMERIC)
            final_left = (long) temp.value.numeric;
        else
        {
            fprintf(stderr, "Left-side indexing with non-numeric value\n");
            exit(TYPE_INCOMPATIBILITY);
        }

        if(place->params->next->next != NULL)
        {
            valid = true;
            temp = evalExpr(place->params->next->next, VARTABLE);

            if(temp.type == VAR_NUMERIC)
                final_right = (long) temp.value.numeric;
            else
            {
                fprintf(stderr, "Left-side indexing with non-numeric value\n");
                exit(TYPE_INCOMPATIBILITY);
            }
        }
       
    }
    else if(place->type == T_INTFUNC && place->data.index == FUNC_ACCESS)
    {
        isSubVar = false;
        if(place->params->next != NULL)
        {
            variable temp = evalExpr(place->params->next, VARTABLE);

            if(temp.type != VAR_NUMERIC)
            {
                fprintf(stderr, "Left-side indexing with non-numeric value\n");
                exit(TYPE_INCOMPATIBILITY);
            }
            else
                final_left = (long) temp.value.numeric;
        }
        else
        {
            fprintf(stderr, "Left-side index missing\n");
            exit(MISC_RUNTIME_ERROR);
        }
    }
    else
    {
        fprintf(stderr, "Left-side indexing: Innermost must be subarray (substring) or element selection");
        exit(TYPE_INCOMPATIBILITY);
    }

    /** Now going down through the tree and storing indexes */
    variable temp;

    while((place = place->params)->type == T_INTFUNC && place->data.index == FUNC_ACCESS)
    {
        if(place->params->next == NULL)
        {
            // should contain the index for accessElement, therefore it's an error
            fprintf(stderr, "Left-side indexing: Missing index\n");
            exit(MISC_RUNTIME_ERROR);
        }

        temp = evalExpr(place->params->next, VARTABLE);

        if(temp.type == VAR_NUMERIC)
            num_stack_push(temp.value.numeric);
        else
        {
            fprintf(stderr, "Left-side indexing with non-numeric value\n");
            exit(MISC_RUNTIME_ERROR);
        }
    }

    variable * replaced;

    if(place->type == T_VARIABLE)
    {
        replaced = &(VARTABLE[place->data.index]);
        replaced->no_touch = true;
    }
    else
    {
        fprintf(stderr, "Refusing to lef-side index something, which is not a variable\n");
        exit(TYPE_INCOMPATIBILITY);
    }

    unsigned long index;

    while(!NS_is_empty())
    {
        if(replaced->type == VAR_ARRAY)
        {
            index = num_stack_pop();

            if(index < replaced->value.array.size)
                replaced = &(replaced->value.array.data[index]);
            else
            {
                fprintf(stderr, "Left-side index beyond the array's size\n");
                exit(MISC_RUNTIME_ERROR);
            }
        }
        else
        {
            fprintf(stderr, "Left-side indexing: Indexed element is not an array\n");
        }
    }
    variable assigned = evalExpr(expression, VARTABLE);

    if(!isSubVar)
    {
        // assigning into array element
        if(replaced->type == VAR_ARRAY)
        {
            if(final_left < (signed)replaced->value.array.size)
            {
                replaced->value.array.data[final_left] = copyVariable(assigned, LT_STACK);
                if(assigned.no_touch == false)
                    destroyVariable(&assigned);
            }
            else
            {   
                fprintf(stderr, "Left-side index beyond the array's size\n");
                exit(MISC_RUNTIME_ERROR);
            }
        }
        else
        {
            fprintf(stderr, "Only array elements may be accessed\n");
            exit(TYPE_INCOMPATIBILITY);
        }
    }
    else
    {
        /* We have a subVar here. Procedure:
           1.) Check and fill parameters (final_right)
           2.) Check whether any replacing will ocur at all
           3.) Check array borders
           4.) Shift and reallocate
           5.) Fill in
         */

        long input_len;
        long place_len;
        int difference;

        if(replaced->type != assigned.type)
        {
            fprintf(stderr, "Will only assign array in subarray or string in substring\n");
            exit(TYPE_INCOMPATIBILITY);
        }
        
        if(replaced->type == VAR_STRING)
        {
            input_len = assigned.value.string.length - 1;
            
            if(!valid)
                // The maximal allowed is the position of '\0', since final_right 
                // is the index right BEHIND the last replaced
                final_right = replaced->value.string.length - 1;

            place_len = final_right - final_left;
            difference = input_len - place_len;

            if(final_left < 0 || final_right <= final_left || final_right >= (signed)replaced->value.string.length)
            {
                // invalid indexing, no replacing will happen
                if(assigned.no_touch == false)
                    destroyVariable(&assigned);
                return;
            }

            if(difference > 0)
            {
                // I expect errors here
                void * tmp = replaced->value.string.data;
                replaced->value.string.data = ifj_malloc(replaced->value.string.length + difference, LT_STACK);
                memcpy(replaced->value.string.data, tmp, replaced->value.string.length);

                for(long i = replaced->value.string.length - 1; i >= final_right; --i)
                {
                    replaced->value.string.data[difference + i] = replaced->value.string.data[i];
                }
            }
            else if(difference < 0)
            {
                for(long i = final_right; i < (signed)replaced->value.string.length;++i)
                    replaced->value.string.data[i + difference] = replaced->value.string.data[i];

                void * tmp = replaced->value.string.data;
                replaced->value.string.data = ifj_malloc(replaced->value.string.length + difference, LT_STACK);
                memcpy(replaced->value.string.data, tmp, replaced->value.string.length);
            }

            replaced->value.string.length += difference;

            for(long i = 0; i < input_len; ++i)
                replaced->value.string.data[final_left + i] = assigned.value.string.data[i];

            if(assigned.no_touch == false)
                destroyVariable(&assigned);

            return;
        }
        else if(replaced->type == VAR_ARRAY)
        {
            input_len = assigned.value.array.size;
            
            if(!valid)
                final_right = replaced->value.array.size;

            place_len = final_right - final_left;
            difference = input_len - place_len;

            if(final_left < 0 || final_right <= final_left || final_right >= (signed)replaced->value.array.size)
            {
                // invalid indexing, no replacing will happen
                if(assigned.no_touch == false)
                    destroyVariable(&assigned);
                return;
            }

            if(difference > 0)
            {
                void * tmp = replaced->value.array.data;
                replaced->value.array.data = ifj_malloc(sizeof(variable)*(replaced->value.string.length + difference), LT_STACK);
                memcpy(replaced->value.array.data, tmp, sizeof(variable)*(replaced->value.string.length));

                for(long i = replaced->value.array.size - 1; i >= final_right; --i)
                    replaced->value.string.data[i + difference] = replaced->value.string.data[i];
            }
            else if(difference < 0)
            {
                for(long i = final_right; i < (signed)replaced->value.array.size; ++i)
                    replaced->value.array.data[i + difference] = replaced->value.array.data[i];

                void * tmp = replaced->value.array.data;
                replaced->value.array.data = ifj_malloc((replaced->value.array.size + difference) * sizeof(variable), LT_STACK);
                memcpy(replaced->value.array.data, tmp, (replaced->value.array.size + difference) * sizeof(variable));
            }

            replaced->value.array.size += difference;

            for(long i = 0; i < input_len; ++i)
                replaced->value.array.data[final_left + i] = copyVariable(assigned.value.array.data[i], LT_STACK);

            if(assigned.no_touch == false)
                destroyVariable(&assigned);

            return;
        }
    }
}
