/**
 * @FILE interpret.c
 * @AUTHOR Jan Remes (xremes00@stud.fit.vutbr.cz)
 * @BRIEF Interpret executive code
 */

/* ************************ *
 ~ ~~~[ INCLUDES ]~~~~~~~~~ ~
 * ************************ */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/* Required for pow() function */
#include <math.h>

#include "global.h"
#include "structures.h"
#include "interpret.h"
#include "functions.h"
#include "variable.h"

#include "memory.h"
#include "memory_interpret.h"

//! This is nasty and I know it, but I won't create a header file for a single function
void assignGeneric(tExpr *place, tExpr *expression, variable * VARTABLE);

/* ************************ *
 ~ ~~~[ MACROS ]~~~~~~~~~~~ ~
 * ************************ */

/* Internal macros */
#define evalError(EXP, ...) /*do { fprintf(stderr, "%s%s",__func__," in interpret.c: "); \
    fprintf(stderr, EXP, __VA_ARGS__); fprintf(stderr, "\n"); exit(ERR_42); } while(0) */


#ifdef DEBUG
#define debugFile stderr
#define printPlain(TEXT, ...) fprintf(debugFile, TEXT, __VA_ARGS__)
#define printDebug(TEXT) fprintf(debugFile, "-- %s in interpret.c: %s\n", __func__, TEXT)
#else
#define debugFile
#define printPlain(TEXT, ...) 
#define printDebug(TEXT)
#endif

/* ************************************************** *
 ~ ~~~[ STATIC FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************** */
static inline void retVar(variable * moved)
{
    if(moved->type == VAR_STRING)
        moved->value.string.data = func_ret(moved->value.string.data, moved->value.string.length);
    else if(moved->type == VAR_ARRAY)
    {
        moved->value.array.data = func_ret(moved->value.array.data, moved->value.array.size * sizeof(variable));
        for(unsigned long i = 0; i < moved->value.array.size; ++i)
            retVar(&(moved->value.array.data[i]));
    }
}

static inline void movVar(variable * moved)
{
    void * tmp;

    if(moved->type == VAR_STRING)
    {
        tmp = moved->value.string.data;
        moved->value.string.data = ifj_malloc(moved->value.string.length, LT_STACK);
        memcpy(moved->value.string.data, tmp, moved->value.string.length);
    }
    else if(moved->type == VAR_ARRAY)
    {
        tmp = moved->value.array.data;
        moved->value.array.data = ifj_malloc(moved->value.array.size * sizeof(variable), LT_STACK);
        memcpy(moved->value.array.data, tmp, moved->value.array.size * sizeof(variable));
        for(unsigned long i = 0; i < moved->value.array.size; ++i)
            movVar(&(moved->value.array.data[i]));
    }
}

/* ************************ *
 ~ ~~~[ FUNCTIONS ]~~~~~~~~ ~
 * ************************ */

/** Executes the given function with parameters described by second argument
 * @RETURN variable Function always returns a variable
 * @PARAM function The function to be executed
 * @PARAM params The function's parameters
 */
variable executeFunction(tFunc * function, tExpr * params, variable * OLDVARTABLE)
{
    /* memory preparations */
    func_call();
    /* Create this function instance's variable table */
    variable * VARTABLE = createVartable(function->vartable_size);
    insertParams(VARTABLE, function->paramnumber, params, OLDVARTABLE);

    /* Set the first command and the default return value */
    tCommand * current_command = function->commands;

    variable result = {
        .type = VAR_NIL,
    };

    /* Go through commands and execute them */
    while(current_command != NULL)
    {
        switch(current_command->type)
        {

            case COMM_RETURN:
                /* Make sure the variable data persists */
                result = evalExpr(current_command->expression, VARTABLE);
                retVar(&result);
                expr_fin();
                func_end();
                return result;

            case COMM_IFELS:
                if(isTrue(evalExpr(current_command->expression, VARTABLE)))
                    current_command = current_command->next;
                else
                    current_command = current_command->alternative;
                expr_fin();
                break;

            case COMM_PASS: //!< kompas, lol
                current_command = current_command->next;
                break;

            case COMM_ASSIGNMENT:
                if(current_command->lvalue != NULL)
                    assignGeneric(current_command->lvalue,current_command->expression,VARTABLE);
                else
                {
                    result = evalExpr(current_command->expression, VARTABLE);
                    movVar(&result);
                    expr_fin();
                    VARTABLE[current_command->index] = result;
                    VARTABLE[current_command->index].no_touch = true;
                }

                current_command = current_command->next;
                break;

            default:
                fprintf(stderr, "executeFunction in interpret.c: Undefined \
                        command type\n");
                exit(INTERNAL_ERROR);
        }
    }
    
    func_end();
    return result;
}

/** Returns the value of an expression
 *
 * Evaluates the expression tree and returns its value.
 * Calls itself recursively for solving the children nodes in the tree
 * @RETURN variable The value of the expression
 * @PARAM expression The expression tree
 * @PARAM VARTABLE Current variable table
 */
variable evalExpr(tExpr * expression, variable * VARTABLE)
{
    if(expression == NULL)
    {
        fprintf(stderr, "evalExpr in interpret.c: Runtime error, NULL \
                expression received\n");
        exit(INTERNAL_ERROR);
    }

    /* Temporary variables */
    variable left;
    variable right;
    variable result;


    /* The core of this function. It behaves accordingly to the type of node */
    switch(expression->type)
    {
        /* Return the variable itself */
        case T_VARIABLE:
            /** @WARNING Does not check VARTABLE size */
            result = VARTABLE[expression->data.index];

            if(result.type == VAR_UNDEFINED)
            {
                fprintf(stderr, "Undefined variable may not be used in expression\n");
                exit(MISC_RUNTIME_ERROR);
            }

            /* Make sure no one messes the variable contents */
            if(result.type == VAR_STRING || result.type == VAR_ARRAY)
                result.no_touch = true;
            else
                result.no_touch = false;
            return result;

        /* Return just the value of the literal */
        case T_LITERAL:
            result = expression->data.value;

            /* Even literal values should not be changed (remember something about recursion?) */
            if(result.type == VAR_STRING || result.type == VAR_ARRAY)
                result.no_touch = true;
            else
                result.no_touch = false;
            return result;

        /* Add or concatenate. Be aware of the strings and their 'clenness' */
        case T_PLUS:
            /* find the value of children nodes */
            left = evalExpr(expression->left_child, VARTABLE);
            right = evalExpr(expression->right_child, VARTABLE);

            switch(left.type)
            {
                /* Just add the two numbers or invoke an error */
                case VAR_NUMERIC:
                    if(right.type == VAR_NUMERIC)
                    {
                        left.value.numeric += right.value.numeric;
                        return left;
                    }
                    else
                    {
                        fprintf(stderr, "Incompatible non-numeric type for addition\n");
                        evalError("Wrong data type (%d) for addition (right child)", right.type);
                        exit(TYPE_INCOMPATIBILITY);
                    }

                /* Concatenating. Be careful about strings! */
                case VAR_STRING:
                    /* 'string' + 'string' means concatenating */
                    if(right.type == VAR_STRING)
                    {
                        result.type = VAR_STRING;
                        result.value.string.length = left.value.string.length + right.value.string.length - 1;

                        /* allocate memory for the new string and insert proper data to it*/
                        result.value.string.data = ifj_malloc(result.value.string.length, ST_STACK);
                        strcpy(result.value.string.data, left.value.string.data);

                        /* copy data from the right string to the new one */
                        strcat(result.value.string.data, right.value.string.data);

                        result.no_touch = false;
                        return result;
                    }

                    else if(right.type == VAR_NUMERIC)
                    {
                        result.type = VAR_STRING;
                        result.value.string.length = left.value.string.length + DOUBLEMAXCHARS;
                        result.value.string.data = ifj_malloc(result.value.string.length, ST_STACK);
                        strcpy(result.value.string.data, left.value.string.data);

                        /* Copy the 'numeric' behind the string */
                        snprintf(&(result.value.string.data[left.value.string.length - 1]), DOUBLEMAXCHARS + 1, "%g", right.value.numeric);

                        result.no_touch = false;
                        return result;
                    }

                    else if(right.type == VAR_BOOL)
                    {
                        result.type = VAR_STRING;
                        /* The magic numbers - lenOf("true"), lenOf("false") */
                        result.value.string.length = left.value.string.length + (right.value.boolean ? 4 : 5);

                        result.value.string.data = ifj_malloc(result.value.string.length, ST_STACK);
                        strcpy(result.value.string.data, left.value.string.data);

                        if(right.value.boolean)
                            strcat(result.value.string.data, "true");
                        else
                            strcat(result.value.string.data, "false");

                        result.no_touch = false;
                        return result;
                    }

                    else if(right.type == VAR_NIL)
                    {
                        result.type = VAR_STRING;
                        result.no_touch = false;

                        /* The magic number - lenOf("Nil") */
                        result.value.string.length = left.value.string.length + 3;
                        result.value.string.data = ifj_malloc(result.value.string.length, ST_STACK);
                        strcpy(result.value.string.data, left.value.string.data);

                        strcat(result.value.string.data, "Nil");
                        return result;
                    }
                    else
                    {
                        fprintf(stderr, "Incompatible types for concatenating");
                        evalError("Error 11 - incompatible type %d to be concatenated with string", right.type);
                        exit(TYPE_INCOMPATIBILITY);
                    }
                
                case VAR_ARRAY:

                    if(right.type == VAR_ARRAY)
                    {
                        if(left.no_touch)
                            result = copyVariable(left, ST_STACK);
                        else
                            result = left;

                        result.value.array.size += right.value.array.size;

                        void * tmp = result.value.array.data;
                        result.value.array.data = ifj_malloc(sizeof(variable) * result.value.array.size, ST_STACK);
                        memcpy(result.value.array.data, tmp, sizeof(variable) * result.value.array.size);

                        for(unsigned long i = left.value.array.size; i < result.value.array.size; ++i)
                            result.value.array.data[i] = copyVariable(right.value.array.data[i - left.value.array.size], ST_STACK);

                        return result;
                    }
                    else
                    {
                        fprintf(stderr, "Will not concatenate array with non-array\n");
                        exit(TYPE_INCOMPATIBILITY);
                    }   
                
                default:
                    fprintf(stderr, "Incomatible type to be '+'-ed\n");
                    evalError("Unexpected type %d on the left side of '+'", left.type);
                    exit(TYPE_INCOMPATIBILITY);
            }
            return result;

        case T_INTFUNC:
            return (FUNCTABLE[expression->data.index])(expression->params, VARTABLE);

        case T_MINUS:
            /* Means only subtraction */
            left = evalExpr(expression->left_child, VARTABLE);
	        right = evalExpr(expression->right_child, VARTABLE);

    	    if(left.type == VAR_NUMERIC && right.type == VAR_NUMERIC)
            {
                result.type = VAR_NUMERIC;
                result.value.numeric = left.value.numeric - right.value.numeric;
                result.no_touch = false;
                return result;
            }
            else
            {
                fprintf(stderr, "Incompatible types for subtraction\n");
                evalError("T_MINUS: Received arguments of wrong types (%d, %d)", left.type, right.type);
                exit(TYPE_INCOMPATIBILITY);
            }
        case T_ASTERISK:
            left = evalExpr(expression->left_child, VARTABLE);
            right = evalExpr(expression->right_child, VARTABLE);

            /* May be multiplication or string power */
            
            if(left.type == VAR_NUMERIC)
            {

                if(right.type == VAR_NUMERIC)
                {
                    result.type = VAR_NUMERIC;
                    result.value.numeric = left.value.numeric * right.value.numeric;
                    result.no_touch = false;
                    return result;
                }
                else
                {
                    fprintf(stderr, "Incompatible type to be multiplied with\n");
                    evalError("T_ASTERISK: Unexpected type %d after 'numeric' '*'", right.type);
                    exit(TYPE_INCOMPATIBILITY);
                }
            }
            /* Must be string power */
            else if(left.type == VAR_STRING && right.type == VAR_NUMERIC)
            {
                int exponent = (int) right.value.numeric;

                /* Means resulting string is empty */
                if(exponent == 0)
                {
                    result.value.string.data = ifj_malloc(1, ST_STACK);
                    result.value.string.length = 1;
                    result.value.string.data[0] = '\0';
                }
                else
                {
                    result.value.string.length = strlen(left.value.string.data) * exponent + 1;

                    result.value.string.data = ifj_malloc(result.value.string.length, ST_STACK);
                    memcpy(result.value.string.data, left.value.string.data, left.value.string.length);

                    if(exponent > 1)
                    {
                        char * tmpstring = ifj_malloc(left.value.string.length, ST_STACK);
                        strcpy(tmpstring, result.value.string.data);

                        for(int i = 1; i < exponent; ++i)
                        {
                            /* Repaired to strcpy(). The strcat() function is to smart 
                             * @EDIT Rewriting again to strcat(). Can be rewritten when optimizing */
                            strcat(result.value.string.data, tmpstring);
                        }
                    }
                }

                result.type = VAR_STRING;
                result.no_touch = false;
                return result;
            }
            else
            {
                fprintf(stderr, "Incompatible non-numeric type in string power\n");
                evalError("T_ASTERISK: Unexpected types %d, %d operands", left.type, right.type);
                exit(TYPE_INCOMPATIBILITY);
            }
          

        case T_SLASH:
                    left = evalExpr(expression->left_child, VARTABLE);
                    right = evalExpr(expression->right_child, VARTABLE);

                    if(left.type == VAR_NUMERIC && right.type == VAR_NUMERIC)
                    {
                        if(right.value.numeric == 0.0)
                        {
                            /** Error 10 throwing */
                            fprintf(stderr, "You can't divide by zero\n");
                            exit(ZERO_DIVISION);
                        }
                        left.value.numeric /= right.value.numeric;
                        return left;
                    }
                    else
                    {
                        fprintf(stderr, "Incompatible types for division\n");
                        evalError("T_SLASH: Unexpected %d, %d types operands", left.type, right.type);
                        exit(TYPE_INCOMPATIBILITY);
                    }


        case T_POWER:

                    left = evalExpr(expression->left_child, VARTABLE);
                    right = evalExpr(expression->right_child, VARTABLE);

                    if(left.type == VAR_NUMERIC && right.type == VAR_NUMERIC)
                    {
                        left.value.numeric = pow(left.value.numeric, right.value.numeric);
                        return left;
                    }

                    else
                    {
                        fprintf(stderr, "Incompatible non-numeric types for numeric power\n");
                        evalError("Non-numeric arguments (types %d, %d) for power", left.type, right.type);
                        exit(TYPE_INCOMPATIBILITY);
                    }

        case T_EQ:
        case T_NE:
        case T_LT:
        case T_GT:
        case T_LE: 
        case T_GE:
                    /* All comparison operations expect all variables to be defined */
                    left = evalExpr(expression->left_child, VARTABLE);
                    right = evalExpr(expression->right_child, VARTABLE);

                    result.type = VAR_BOOL;
                    if(left.type != right.type)
                    {
                        if(expression->type == T_EQ)
                            result.value.boolean = false;
                        else if(expression->type == T_NE)
                            result.value.boolean = true;
                        else
                        {
                            fprintf(stderr, "Type incompatibility: Comparing two different types\n");
                            exit(TYPE_INCOMPATIBILITY);
                        }
                    }
                    else
                    {
                        switch(expression->type)
                        {
                            case T_EQ:
                                result.value.boolean = (cmpVariables(left,right) == 0);
                                break;
                            case T_NE:
                                result.value.boolean = (cmpVariables(left, right) != 0);
                                break;
                            case T_LT:
                                result.value.boolean = (cmpVariables(left, right) < 0);
                                break;
                            case T_GT:
                                result.value.boolean = (cmpVariables(left, right) > 0);
                                break;
                            case T_LE:
                                result.value.boolean = (cmpVariables(left, right) <= 0);
                                break;
                            case T_GE:
                                result.value.boolean = (cmpVariables(left, right) >= 0);
                                break;
                            default:
                                /* This is done to avoid warnings, we never get here */
                                ;
                        }
                    }
                    return result;
        
        case T_AND:
                    /* All logical operations expect variables to be always defined */
                    left = evalExpr(expression->left_child, VARTABLE);
                    right = evalExpr(expression->right_child, VARTABLE);

                    result.type = VAR_BOOL;
                    if(isTrue(left) && isTrue(right))
                        result.value.boolean = true;
                    else
                        result.value.boolean = false;

                    return result;
        case T_OR:
                    left = evalExpr(expression->left_child, VARTABLE);
                    right = evalExpr(expression->right_child, VARTABLE);

                    result.type = VAR_BOOL;
                    if(isTrue(left) || isTrue(right))
                        result.value.boolean = true;
                    else
                        result.value.boolean = false;

                    return result;
                    
        case T_NOT:
                    left = evalExpr(expression->left_child, VARTABLE);

                    result.type = VAR_BOOL;

                    result.value.boolean = !isTrue(left);

                    return result;

        case T_IN:
        case T_NOTIN:
                    left = evalExpr(expression->left_child, VARTABLE);
                    right = evalExpr(expression->right_child, VARTABLE);

                    result.type = VAR_BOOL;
                    result.value.boolean = isIn(left, right);

                    if(expression->type == T_NOTIN)
                        result.value.boolean = !result.value.boolean;

                    return result;
             
        case T_FUNCCALL:
                    return executeFunction(expression->data.function, expression->params, VARTABLE);

        case T_UMINUS:
                    result = evalExpr(expression->left_child, VARTABLE);
                    if(result.type == VAR_NUMERIC)
                    {
                        result.value.numeric = -result.value.numeric;
                        return result;
                    }
                    else
                    {
                        fprintf(stderr, "Incompatible non-numeric type for unary minus\n");
                        evalError("Unexpected type %d at T_UMINUS", result.type);
                        exit(TYPE_INCOMPATIBILITY);
                    }

        default:
            fprintf(stderr, "evalExpr in interpret.c: Operation %d not supported yet\n", expression->type);
            exit(INTERNAL_ERROR);
    }
}

#undef debugFile
#undef printPlain
#undef printDebug
