/**
 * @FILE functions.h
 * @AUTHOR Jan Remes (xremes00@stud.fit.vutbr.cz)
 * @BRIEF Header for internal functions file
 */

#ifndef _FUNCTIONS_H_INCLUDED_
#define _FUNCTIONS_H_INCLUDED_

#define _INTERPRET_EXPERIMENTAL_
#define ARRAYSIZE 5
#define evalError(EXP, ...)

#include "global.h"
#include "structures.h"

//extern variable (* FUNCTABLE[])(tExpr * expression, variable * VARTABLE);

/** Prints variable according to specs.
 *
 * Contains experimental support for printing arrays
 * @RETURN nil The built-in function 'print' returns nil
 * @PARAM argument List of variables to be printed
 */
variable ifj12_print(tExpr * expression, variable * VARTABLE);

/** Reads and stores a user input
 *
 * Reads one line from 'stdin' and stores it as a variable of 'string' type
 * Both parameters are present only to comply with the other functions
 * @PARAM[in] expression NULL here, no data required
 * @PARAM[in] VARTABLE Unused in this function
 * @RETURN variable A 'string' variable containing the input
 */
variable ifj12_input(tExpr * expression, variable * VARTABLE);

/**
 * ifj12_numeric - Convertion to numeric variables
 * Converts numeric and string variables only
 * @PARAM[in] expression The expression (variable) to be converted
 * @PARAM[in] VARTABLE The variable table required for evaluating expressions
 * @RETURN numeric A 'numeric' variable
 * @NOTE May throw a "12" error due to incompatible types
 * @QUESTION Does it have to cnvert something? Can the string be empty?
 */
variable ifj12_numeric(tExpr * expression, variable * VARTABLE);

/**
 * ifj12_typeof - The built-in function typeOf()
 *
 * Returns the type of a variable. The types are specified in the 
 * project specifications
 * @PARAM[in] param The expression, which type is to be returned
 * @PARAM[in] VARTABLE The variable table required to evaluate the expression
 * @RETURN numeric The type of the parameter
 */
variable ifj12_typeof(tExpr * param, variable * VARTABLE);

/**
 * ifj12_len - The built-in function  len()
 *
 * Returns the length of a string variable. Returns 0.0 for other types
 * @PARAM[in] param The string, which length is to be computed
 * @PARAM[in] VARTABLE VAriable table required for evaluating expressions
 * RETURN length The length of the given string
 */
variable ifj12_len(tExpr * param, variable * VARTABLE);

/**
 * ifj12_find - The built-in function find()
 *
 * Searches for the first occurence of the given substring in a string and
 * returns its position (indexed from zero). For the detailed behavior, see
 * project specifications
 * @PARAM[in] params The function's parameters (string, substring)
 * @PARAM[in] VARTABLE The variable table required for evaluating expressions
 * @RETURN The position of searched substring
 * @NOTE May throw the '11' error
 */
variable ifj12_find(tExpr * params, variable * VARTABLE);

/**
 * ifj12_sort - The built-in function sort()
 *
 * Returns sorted string - the sequence of characters at input sorted in
 * non-falling order by their ASCII codes. Uses function 'sort' from 'ial.c'
 * to do the trick. The sorted string is a new string (deep copy).
 * @PARAM[in] param The string to be sorted
 * @PARAM[in] VARTABLE The variable table required to evaluate expressions
 * @RETURN The sorted string
 * @NOTE May throw Error 11
 */
variable ifj12_sort(tExpr * expression, variable * VARTABLE);

/** Create an array
 *
 * @RETURN variable The resulting variable with type VAR_ARRAY
 * @PARAM[in] expression The expression to be converted into an array
 * @PARAM[in] VARTABLE Current variable table
 */
variable arrayInit(tExpr * expression, variable * VARTABLE);

/**
 * subVar - Creates a subvariable from variable
 * Returns a part of a 'string' or 'array' variable. Should not be used as
 * an array indexing function when accessing single index only
 * @WARNING Must be called with at least three parameters (non-NULL)
 */
variable subVar(tExpr * params, variable * VARTABLE);

/**
 * accessElement - The internal function for acessing an array item
 *
 * This is a INTERNAL, not a BUILT-IN function, meaning that the programmer
 * should not be able to call this function. It is available for the syntax
 * analyzer to synthesise things like 'item = array[3.0]'
 *
 * For accessing elements in multi-dimensional arrays, this function must be
 * called recursively for each level. THIS MAY BE ACTUALLY REWRITTEN to provide
 * non-recursive way to access anything in any level, but I feel better having
 * this simple and clear.
 *
 * @PARAM[in] params The tExpr tree describing the access command
 * @PARAM[in] VARTABLE The variable table for evaluating expressions
 * @RETURN variable The value of the desired element (may be actually another array)
 * @NOTE Doesn't work for strings (since we do not have a 'char' type, why the heck would you want this?)
 * @NOTE Returns whole array if second argument (meant pseudo 'user' argument, not VARTABLE) is missing
 */
variable accessElement(tExpr * params, variable * VARTABLE);

/**
 * isIn - Determines, whether one variable is IN another, as defined by Falcon Survival Guide,
 * and therefore, by project task.
 *
 * Returns false in case searched variable is not a string or an array
 * In the other cases it searches the string or the array for the item and if found, returns true
 * @RETURN bool Whether or not was the item found in searched variable
 * @PARAM[in] item The item being searched
 * @PARAM[in] searched The variable being searched in
 */
bool isIn(variable item, variable searched);

#endif // _FUNCTIONS_H_INCLUDED_
