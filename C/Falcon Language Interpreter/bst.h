/**
 * @file bst.h
 * @author Stanislav Smatana
 * @brief Interface to binary search tree implementation.
 */
#ifndef _BST_H
#define _BST_H

#include <stdbool.h>


/** Binary search tree node structure. */
typedef struct tbstnode tBSTNode;
struct tbstnode
{
   // Searching key - string.
   char *key;
   // Useful user data.
   void *data;
   // Pointers to left and right subtree
   tBSTNode *lptr,*rptr;

} ;

/** Definition of pointer to function for bstApplyFunc */
typedef void (*tApplyFunc)(void *);

/** Function to create and initialize tree's root node.
 *
 * @return New tree containing only root node.
 * @return NULL if operation failed.
 */
tBSTNode *bstCreateTree();

/** Function to dispose binary search tree.
 *
 * @param tree_root Pointer to tBSTNode tree node structure
 *                  to act as a tree root node.
 */
void bstTreeDestroy(tBSTNode *root_node);


/** Function for looking up node with given key.
 *
 * @param tree_root Pointer to tree root node.
 * @param key String to use as a search key.
 *
 * @return Pointer to pointer to node's useful data.
 * @return NULL if node was not found.
 *
 * @warning This function returns pointer to pointer and allows
 *          manipulation of node's internal data pointer. Be careful.
 */
void **bstLookUp(tBSTNode *tree_root,const char *key);

/** Function for inserting new data into binary search tree. 
 *
 * @param tree_root Pointer to tree root node.
 * @param key String to use as a key for new node.
 * @param data Pointer to data to be inserted into node.
 *
 * @return TRUE if insertion was successful.
 * @return FALSE if insertion failed (due to failed malloc).
 */
bool bstInsert(tBSTNode *tree_root, char *key, void *data);

/** Function for calling given function for every node in tree.
 *
 * This function goes through whole tree and for every node calls 
 * given function func with pointer to node's data as argument.
 * For more info on type of the func, please refer to tApplyFunc 
 * declaration.
 *
 * @param tree_root Pointer to tree root node.
 * @param func Pointer to function of type tApplyFunc to be called on every
 *             node during tree traversal.
 */
void bstApplyFunc(tBSTNode *tree_root, tApplyFunc func);

/** Function dumps given bst tree to file in graphviz dot format.
 *
 * @param tree_root Pointer to tree root node.
 * @param output_file Name of file to dump graph into. If NULL - stdout will be
 *                    used.
 *
 * @return TURE on success.
 */
bool bstGraphicDump(tBSTNode *tree_root, const char *output_file);

#endif
