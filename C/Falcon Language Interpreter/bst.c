/**
 * @file bst.h
 * @author Stanislav Smatana
 * @brief Interface to binary search tree implementation.
 *
 * @warning Tree was not properly tested.
 *
 * @todo Prerob bstTreeDestroy a bstApplyFunc na nerekurzivnu formu bez nutnosti
 *        pouzitia zasobnika. Da sa to ?
 *
 * @todo Osetri situaciu pri insertovani prvku s rovnakym klucom.
 * @todo Uprav prasacky for v bstInsert.
 * 
 * @todo Preloz sakra komentare 
 * @todo !!! Pri ifj_free cistim aj data ? 
 */

#include "bst.h"
#include "memory.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/** Function to create and initialize tree's root node.
 *
 * @return New tree containing only root node.
 * @return NULL if operation failed.
 */
tBSTNode *bstCreateTree()
{
   tBSTNode *root_node;

   if( (root_node  = (tBSTNode *) ifj_malloc(sizeof(tBSTNode),BST_INSERT)) == NULL)
      return NULL; 

   root_node->lptr=NULL;
   root_node->rptr=NULL;
   root_node->key=NULL;
   root_node->data=NULL;

   return root_node;
}

/** Function for inserting new data into binary search tree. 
 *
 * @param tree_root Pointer to tree root node.
 * @param key String to use as a key for new node.
 * @param data Pointer to data to be inserted into node.
 *
 * @return TRUE if insertion was successful.
 * @return FALSE if insertion failed (due to failed ifj_malloc).
 */
bool bstInsert(tBSTNode *tree_root, char *key, void *data)
{
   if(tree_root->key == NULL)
   {
      tree_root->key=key;
      tree_root->data=data;
   }
   else
   {
      // dostaneme sa na koniec
      // for posuva hodnotu ukazatela tmp vzdy bud na ADRESU ukazatela
      // lptr alebo rptr podla vysledku strcmp
      // cyklus sa ukonci pokial je hodnota na *tmp NULL
      // tmp vtedy obsahuje ADRESU ukazatela ktory bude ukazovat na novy prvok
      tBSTNode **tmp;
      for(tmp = &tree_root; *tmp != NULL; tmp = strcmp(key, (*tmp)->key) < 0 ? &((*tmp)->lptr) : &((*tmp)->rptr) ); 
      
      if( (*tmp = (tBSTNode *) ifj_malloc(sizeof(tBSTNode),BST_INSERT)) == NULL)
         return false;
      else
      {
        (*tmp)->data=data;
        (*tmp)->key=key;
        (*tmp)->lptr=NULL;
        (*tmp)->rptr=NULL;
      }


   }

   return true;
}

/** Function for looking up node with given key.
 *
 * @param tree_root Pointer to tree root node.
 * @param key String to use as a search key.
 *
 * @return Pointer to pointer to node's useful data.
 * @return NULL if node was not found.
 *
 * @warning This function returns pointer to pointer and allows
 *          manipulation of node's internal data pointer. Be careful.
 */
void **bstLookUp(tBSTNode *tree_root,const char *key)
{
   // prazdny strom
   if (tree_root->key == NULL)
      return NULL;

   //cyklus prehladava strom pokial sa hladany kluc nerovna sucasnemu
   //v kazdom kroku posunie podla hodnoty porovnania tree_root bud na lptr 
   //alebo rptr -> ak po posunuti dostaneme NULL znamena to ze skoncil
   //a element nenasiel
   int tmp;

   while( (tmp = strcmp(key,tree_root->key)) != 0 )
      if ( (tree_root = tmp < 0 ? tree_root->lptr : tree_root->rptr) == NULL)
         return NULL;

   return &(tree_root->data);
}

/** Function to dispose binary search tree.
 *
 * @param tree_root Pointer to tBSTNode tree node structure
 *                  to act as a tree root node.
 */
void bstTreeDestroy(tBSTNode *root_node)
{
   tBSTNode *lptr,*rptr;
   lptr = root_node->lptr;
   rptr = root_node->rptr;

   //aj key bude uvolneny
   //poradie je zvolene aby nevznikali mrtve ramena rekurzie 
   if (lptr != NULL) 
      bstTreeDestroy(lptr);

   if (rptr != NULL) 
      bstTreeDestroy(rptr);

}

/** Function for calling given function for every node in tree.
 *
 * This function goes through whole tree and for every node calls 
 * given function func with pointer to node's data as argument.
 * For more info on type of the func, please refer to tApplyFunc 
 * declaration.
 *
 * @param tree_root Pointer to tree root node.
 * @param func Pointer to function of type tApplyFunc to be called on every
 *             node during tree traversal.
 */
void bstApplyFunc(tBSTNode *tree_root, tApplyFunc func)
{
   if(tree_root->key == NULL)
      return;

   func(tree_root->data);

   if (tree_root->lptr != NULL) 
      bstApplyFunc(tree_root->lptr,func);

   if (tree_root->rptr != NULL) 
      bstApplyFunc(tree_root->rptr,func);
}

// helper
void bstDumpTreeToFile(tBSTNode *tree_root, FILE *fp)
{

   if( tree_root != NULL)
   {
      //ako node id pouzijeme adresu objektu
      //label bude key - ide o situaciu ak by 
      //bol nazov dvoch nodes rovnaky
      //v tomto pripade to nehrozi - ale kvoli buducemu
      //vyuzitiu
      if(tree_root->key != NULL)
         fprintf(fp,"%ld [label=\"%s\"];\n",(unsigned long) tree_root,tree_root->key);

      //zapiseme relaciu k lavemu a pravemu dietatu ak nie su NULL
      //a rekurzivne zostupujeme
      //najprv po lavych stranach, potom po pravych - aby sme zachovali strany
      if(tree_root->lptr != NULL)
      {
         fprintf(fp,"%ld -> %ld;\n",(unsigned long) tree_root, (unsigned long) tree_root->lptr);
         bstDumpTreeToFile(tree_root->lptr, fp);
         
      }
      
      if(tree_root->rptr != NULL)
      {
         fprintf(fp,"%ld -> %ld;\n",(unsigned long) tree_root, (unsigned long) tree_root->rptr);
         bstDumpTreeToFile(tree_root->rptr, fp);
         
      }


   }

}

/** Function dumps given bst tree to file in graphviz dot format.
 *
 * @param tree_root Pointer to tree root node.
 * @param output_file Name of file to dump graph into. If NULL - stdout will be
 *                    used.
 *
 * @return TURE on success.
 */
bool bstGraphicDump(tBSTNode *tree_root, const char *output_file)
{
   FILE *fp;

   if(output_file == NULL)
      fp=stdout;
   else
   {
      if( (fp = fopen(output_file,"w")) == NULL)
         return false;
   }

   //zapiseme hlavicku suboru
   fprintf(fp,"digraph G{\n");

   //telo bude tree dump
   bstDumpTreeToFile(tree_root,fp);

   fprintf(fp,"}\n");

   fclose(fp);
   return true;

}
