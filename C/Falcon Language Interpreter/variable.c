/**
 * @FILE vartable.c
 * @AUTHOR Jan Remes (xremes00@stud.fit.vutbr.cz)
 * @BRIEF Contains functions for the variable table
 * @VERSION 0.3
 */

#include <stdlib.h>
#include <string.h>

#include "global.h"
#include "structures.h"
#include "memory_interpret.h"
#include "variable.h"
#include "interpret.h"


/**
 * Creates a variable table and returns pointer to it
 * @RETURN Pointer to the allocated variable table
 * @PARAM vartable_size Size of the variable table
 */
variable * createVartable(unsigned vartable_size)
{
    variable * result = ifj_malloc(vartable_size * sizeof(variable), LT_STACK);
    for(unsigned i = 0; i < vartable_size; ++i)
        result[i].type = VAR_UNDEFINED;

    return result;
}

/**
 * Fills the proper field of variable table with parameter values
 * @RETURN void
 * @PARAM vartable The variable table to fill
 * @PARAM paramnumber The number of function's formal parameters
 * @PARAM param The tExpr list representing parameters
 * @NOTE Could return, whether there were enough parameters, but I do not consider
 * this necessary
 */
void insertParams(variable * vartable, unsigned paramnumber, tExpr * param, variable * oldvartable)
{
    /** Fill no more then 'paramnumber' items */
    for(unsigned i = 0; i < paramnumber; ++i)
    {
        /** Check for existing parameter specified */
        if(param != NULL)
        {
            /** If there is one, insert its value and proceed o the next one */
            vartable[i] = copyVariable(evalExpr(param, oldvartable), LT_STACK);
            param = param->next;
        }
        else
            /** Deal with non-specified parameters */
            vartable[i].type = VAR_NIL;
    }
}

/** Creates a copy of a variable
 *
 * For undefined, nil, boolean or numeric, this doesn't do anything
 * For other variables, a deep copy is created and returned
 * @PARAM[in] original Original variable to be copied
 * @RETURN variable The variable copy
 */
variable copyVariable(variable original, TE_alloc_type type)
{
    variable result;

    switch(original.type)
    {
        case VAR_STRING:
            result.type = VAR_STRING;
            /* Anybody can modify it and it must be freed at some point */
            result.no_touch = false;
            result.value.string.length = original.value.string.length;
            result.value.string.data = ifj_malloc(original.value.string.length, type);
            strcpy(result.value.string.data, original.value.string.data);
            return result;
        case VAR_ARRAY:
            result.type = VAR_ARRAY;
            /* Can be modified and must be freed */
            result.no_touch = original.no_touch;
            result.value.array.size = original.value.array.size;

            result.value.array.data = ifj_malloc(result.value.array.size * sizeof(variable), type);

            /* Copy all the array contents */
            for(unsigned i = 0; i < original.value.array.size; ++i)
                result.value.array.data[i] = copyVariable(original.value.array.data[i], type);

            return result;

        /* For other data types, just return what was given */
        default:
            return original;
    }
}
/** Returns the 'truthness' of variable
 *
 * Returns, whetherthe variable evaluates as 'true' or 'false'.
 * Specifications may be found in project definition.
 * Invokes an error with variable of type differing from specifications
 * @RETURN bool The variable's 'truthness' value
 * @PARAM predicate The variable, which is evaluated
 */
bool isTrue(variable predicate)
{
    switch(predicate.type)
    {   
        case VAR_NIL:
            return false;
        case VAR_BOOL:
            return predicate.value.boolean;
        case VAR_NUMERIC:
            if(predicate.value.numeric == 0.0)
                return false;
            else return true;
        case VAR_STRING:
            /* Returns false for empty string */
            if(predicate.value.string.length == 0)
               return false;
            else return true;
        default:
            fprintf(stderr, "Incompatible type is checked for truthness\n");
            //fprintf(stderr, "isTrue in interpret.c: Expected 'nil', 'bool', 'numeric' or 'string' type\n");
            exit(TYPE_INCOMPATIBILITY);
    }
}


/** Compares the variables and determines, which one is 'bigger'
 *
 * Only compares variables of the same type, should be checked before use
 * @PARAM[in] left Variable to be compared
 * @PARAM[in] right Variable to be compared
 * @RETURN int The number specifying, which variable is 'bigger'
 * Result == 0 ==> The variables equal
 * Result  < 0 ==> The left variable is greater than the right
 * Result  > 0 ==> The right variable is greater than the left
 */
int cmpVariables(variable left, variable right)
{
    if(left.type == right.type)
    {
        int result; // for VAR_ARRAY

        switch(left.type)
        {
            case VAR_NIL:
                return 0;

            case VAR_BOOL:
                if(left.value.boolean == right.value.boolean)
                    return 0;
                else if(left.value.boolean == false)
                    return -1;
                else return 1;

            case VAR_NUMERIC:
                if(left.value.numeric == right.value.numeric)
                    return 0;
                else if(left.value.numeric < right.value.numeric)
                    return -1;
                else return 1;

            case VAR_STRING:
                return strcmp(left.value.string.data, right.value.string.data);

            case VAR_ARRAY:    
                /* Go through array and recursively compare each element */
                for(unsigned i = 0; i < left.value.array.size && i < right.value.array.size; ++i)
                {
                    if((result = cmpVariables(left.value.array.data[i], right.value.array.data[i])) != 0)
                        return result;
                }
                /* If arrays differ in size, they are not equal */
                return right.value.array.size - left.value.array.size;
            default:
                fprintf(stderr, "Incompatible type for comparison\n");
                exit(TYPE_INCOMPATIBILITY);
        }
    }
    else
    {
        /** @WARNING Not sure if gusta, see forum */
        fprintf(stderr, "Refusing to compare operands of different types\n");
        exit(TYPE_INCOMPATIBILITY);
    }
}
 
/** Destroy given variable
 * 
 * Deallocates (frees) necessary elements of variable
 * Calls itself recursively for destroying arrays
 * @PARAM current Variable to be destroyed
 */
void destroyVariable(variable *current)
{
    if(current == NULL)
        return;

    if(current->type == VAR_STRING)
    {
//        ifj_free(current->value.string.data);
        current->value.string.data = NULL;
    }

    else if(current->type == VAR_ARRAY)
    {
        for(unsigned i = 0; i < current->value.array.size; ++i)
            destroyVariable(&(current->value.array.data[i]));
        if(current->value.array.size > 0)
        {
  //          ifj_free(current->value.array.data);
        }
            current->value.array.data = NULL;

    }
}
