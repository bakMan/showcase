#!/usr/bin/python

import subprocess
from optparse import OptionParser
import json

OK = 0
BAD_STDOUT = 1
BAD_RETURN_CODE = 2
BAD_STDERR = 3

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

def run_test(test,options):
    step_number = 0
    step_count = len(test['steps'])
    for step in test['steps']:
        step_number+=1
        if options.verbose:
            print bcolors.OKBLUE + 'going to process step "%s"' %\
 (step['do']) + bcolors.ENDC

        called_one = subprocess.Popen(step['do'],shell=True,stdout=subprocess.PIPE,\
            stderr=subprocess.PIPE)

        response =  called_one.communicate()
        output = response[0]
        stderr = response[1]
        return_code = called_one.returncode

        summary = [OK,'']

        try:
            if output != step['expect']['stdout']:
                summary[1] = 'Expected "%s" but recieved "%s" on stdout' \
                    %(step['expect']['stdout'],output)
                summary[0] = BAD_STDOUT
        except KeyError:
            pass

        try:
            if return_code != step['expect']['return_code']:
                summary[1] = 'Expected %d but recieved %d as return code' \
                % (step['expect']['return_code'],return_code)
                summary[0] = BAD_RETURN_CODE
        except KeyError:
            pass

        if summary[0] != OK:
            if options.detailed:
                print bcolors.FAIL+'%s: failed \n\tat step "%s"\n\t%s' %\
                (test['name'],step['do'],summary[1]),bcolors.ENDC
            else:
                print bcolors.FAIL+test['name'], ': failed',bcolors.ENDC
            return (False, step_number, step_count)
    # end of the for loop

    print bcolors.OKGREEN+test['name'], ': OK', bcolors.ENDC
    return (True, step_number, step_count)

def load_test(filename):
    """
    Returns the test dictionary from file filename.
    """

    try:
        fp = open(filename)
    except IOError:
        print bcolors.WARNING+ 'Failed to open file %s' % (filename),\
 bcolors.ENDC
        return None
    test = json.load(fp)

    if not isinstance(test,dict):
        print bcolors.WARNING+ 'File %s is corrupted (the top level object has to\
 be a dictionary)' % (filename), bcolors.ENDC
        return None

    if 'name' not in test:
        print bcolors.WARNING+ 'Test in file %s is corrupted (missing\
 "name")' % (filename), bcolors.ENDC
        return None

    if 'steps' not in test:
        print bcolors.WARNING+ 'Test in file %s is corrupted (missing\
 "steps")' % (filename), bcolors.ENDC
        return None

    if not isinstance(test['steps'],list):
        print bcolors.WARNING+ 'Test in file %s is corrupted ("steps" is\
 not a list)' % (filename), bcolors.ENDC
        return None

    if len(test['steps']) < 1:
        print bcolors.WARNING+ 'Test in file %s is corrupted (must consist\
 of positive number of steps)' % (filename), bcolors.ENDC
        return None

    for step in test['steps']:
        if not isinstance(step,dict):
            print bcolors.WARNING+ 'Test in file %s is corrupted (every\
 step must be a dictionary)' % (filename), bcolors.ENDC
            return None

        if not 'do' in step:
            print bcolors.WARNING+ 'Test in file %s is corrupted (every\
 step must have the "do" key)' % (filename), bcolors.ENDC
            return None


    return test

def get_test_list(filename):
    try:
        fp = open(filename)
    except IOError:
        print bcolors.WARNING+ 'Failed to open file %s' % (filename),\
                bcolors.ENDC
        return None;

    test_list = fp.readlines()
    fp.close()

    for i in range(len(test_list)):
        test_list[i] = test_list[i][:-1]

    return test_list
    

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("-v","--verbose", dest="verbose",
        help="Verbose mode. Useful mainly for script debugging process.",
        action="store_true",default=False)
    parser.add_option("-d","--detailed", dest="detailed",
        help="Detailed info on why the test failed.",
        action="store_true",default=False)
    parser.add_option("-m","--monochromatic", dest="monochromatic",
        help="Turns colors off.",
        action="store_true", default=False)
    parser.add_option("-s","--summary", dest="summary",
        help="Prints summary of the whole test set.",
        action="store_true", default=False)
    parser.add_option("-f","--file", dest="tests_file",
        help="Runs also tests in the given file")
    (options, args) = parser.parse_args()

    if options.monochromatic:
        bcolors.HEADER = ''
        bcolors.OKBLUE = ''
        bcolors.OKGREEN = ''
        bcolors.WARNING = ''
        bcolors.FAIL = ''
        bcolors.ENDC = ''

    steps_count = 0
    steps_succesful = 0
    tests_count = 0
    tests_succesful = 0


    if options.tests_file:
        args.extend(get_test_list(options.tests_file))


    for filename in args:
        test = load_test(filename)
        if test != None:
            tests_count += 1
            result = run_test(test,options)
            steps_count += result[2]

            if result[0] == True:
                tests_succesful += 1
                steps_succesful += result[1]
            else:
                steps_succesful += result[1] - 1
#            fp = open('a.out','w')
#            json.dump(test,fp,sort_keys=True, indent=4)

    if options.summary:
        print '%d %% tests succesful (%d out of %d)' %\
            (100.0*tests_succesful/tests_count,tests_succesful, tests_count)
        print '%d %% steps succesful (%d out of %d)' %\
            (100.0*steps_succesful/steps_count,steps_succesful, steps_count)
