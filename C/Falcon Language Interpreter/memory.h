/**
 * @file      memory.h
 * @author    David Kaspar (xkaspa34) aka Dee'Kej
 * @version   1.0
 * @brief     Header file containing common interface for all modules of IFJ12
 *            interpret.
 */


/* ************************************************************************** *
 * ***[ START OF MEMORY.H ]************************************************** *
 * ************************************************************************** */

/* Safety mechanism against multi-including of this header file. */
#ifndef MEMORY_H
#define MEMORY_H


/* ************************************************************************** *
 ~ ~~~[ HEADER FILES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

#include <stddef.h>             /* Because of use of size_t data type. */


/* ************************************************************************** *
 ~ ~~~[ CONSTANTS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * These constants are outside of memory.c so they can be easily changed and
 * might be used by other header files.
 */
#define SCANNER_BUFFER_SIZE   16U

#ifndef NDEBUG
#define DEF_SEG_SIZE          256U * 1024U        /* Debugging only. */
#else
#define DEF_SEG_SIZE          128U * 1024U        /* MMAP_THRESHOLD */
#endif

/*
 * NOTE: In case OPTIMIZER is implemented, then OPTIMIZER_ON has to be defined
 * via #define (or -NDEBUG during compilation). Also, be advised that
 * OPTIMIZER_ON has to be also defined for memory_main.h header file!!!
 */


/* ************************************************************************** *
 ~ ~~~[ MODULE DATA TYPES DECLARATIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/**
 * Enumerate representing type of allocation being made at the moment.
 */
typedef enum alloc_type {
  BUFFER,                 /* Buffer used by SCANNER. */
  LITERAL,                /* Literals of interpreted code. */
  IDENTIFIER,             /* Identifiers of interpreted code. */

  PERSISTENT,             /* Other functional code of program. */

  PARSER_PERS,            /* Data which survives PARSER for OPTIMIZER. */
  PARSER_TEMP,            /* Temporary data for PARSER. */
  PARSER_STACK,           /* Stack for PARSER for TPushdownItem. */

  BST_INSERT,             /* Helpful alias for BST_INSERT function. */

#ifdef OPTIMIZER_ON
  OPT,                    /* Data of OPTIMIZER. */
#endif

  LT_STACK,               /* Long-term stack used by INTERPRET. */
  ST_STACK,               /* Short-term stack used by INTERPRET. */
  NUM_STACK,              /* Stack for storing numerical values by INTERPRET. */
} TE_alloc_type;


/* ************************************************************************** *
 ~ ~~~[ MODULE FUNCTIONAL PROTOTYPES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

void *ifj_malloc(size_t size, TE_alloc_type type);

/* ************************************************************************** *
 ~ ~~~[ SECTION FOR INTEGRATION TESTING ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * This should ease up process of memory management integration, because
 * ifj_free() is not supported anymore. It helps find out forgotten uses of this
 * function.
 */
#ifndef NDEBUG

#include <stdio.h>

#define ifj_free(ptr)                                                          \
  fprintf(stderr, "ERROR! Memory freeing is not allowed!!! ifj_free() call:\n" \
                  "at line: %d\nin file: %s\nPlease, remove it!\n"             \
                  , __LINE__, __FILE__)                                       
  

#endif


/* ************************************************************************** *
 * ***[ END OF MEMORY.H ]**************************************************** *
 * ************************************************************************** */

#endif

