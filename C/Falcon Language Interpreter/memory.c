/**
 * @file      memory.c
 * @author    David Kaspar (xkaspa34) aka Dee'Kej
 * @version   0.9
 * @brief     Memory management module for IFJ12 interpret.
 *
 * @detailed This module contains definitions for functions accessible via
 * memory.h file and other sub header files. It also contains other data types
 * and auxiliary functions definitions.
 *
 * @note This module should provide interface for programmer so he should not
 * worry about if the allocation have failed or the allocated memory will be
 * correctly freed.
 * 
 * @note It is advised to define NDEBUG during/before compilation to avoid
 * excess testing done by assert() macros. This might slightly increase
 * performance and can't lead to inaccessible memory because of abort() function
 * used by asert() macro in case of any failure.
 *
 * @note There is some conditional translation for this module in case OPTIMIZER
 * enabled. In that case OPTIMIZER_ON has to be defined for the module to work
 * properly.
 */


/* ************************************************************************** *
 * ***[ START OF MEMORY.C ]************************************************** *
 * ************************************************************************** */


/* ************************************************************************** *
 ~ ~~~[ HEADER FILES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/* Our own header files. */
#include "error.h"
#include "parser.h"

#include "memory.h"

#include "memory_main.h"
#include "memory_scanner.h"
#include "memory_parser.h"
#include "memory_interpret.h"

#ifdef OPTIMIZER_ON
#include "memory_optimizer.h"
#endif


/* ************************************************************************** *
 ~ ~~~[ MACRO DEFINITIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/* Macro for testing of successful allocation to avoid redundant code. */
#define alloc_test(ptr)                                                        \
  if (ptr == NULL) {                                                           \
    printError("%s", strerror(ENOMEM));                                        \
    exit(INTERNAL_ERROR);                                                      \
  }


/* ************************************************************************** *
 ~ ~~~[ DATA TYPES DEFINITIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Structure for storing data of our own STACK, which is defined below. In case
 * the stack will grow up a lot, then it will form double-linked list of stack
 * segments.
 */
typedef struct stack_segment {
  struct stack_segment *p_next;     /* Pointer to next segment. */
  struct stack_segment *p_prev;     /* Pointer to previous segment. */

  char *p_eos;                      /* Pointer to end of actual segment. */
  char data[];                      /* Start of useful data. */
} TS_stack_seg;


/*
 * Structure for representing our own STACK. The whole concept is very similar
 * to stack used in assembly language, it has been only adjusted for use within
 * our program.
 */
typedef struct stack {
  TS_stack_seg *p_1st;              /* Pointer to start of stack. */
  TS_stack_seg *p_act;              /* Pointer to actual stack segment. */
  
  char *EBP;                        /* Base pointer - same as in assembler. */
  char *ESP;                        /* Stack pointer - same as in assembler. */

#ifndef NDEBUG
  char *p_last_alloc;               /* For debugging purposes only. */
#endif
} TS_stack;


/*
 * Default structure used for memory management. In case of no free memory in
 * segment is available, then new segment is allocated. It is then added to the
 * linked list of the used segments.
 */
typedef struct segment {
  struct segment *p_next;           /* Next segment. */

  bool filled;                      /* Any free space left? */

  char *p_free;                     /* Pointer to free space of segment. */
  char *p_eos;                      /* Pointer to the end of segment. */

  char data[];                      /* Useful segment data. */
} TS_segment;


/*
 * Main structure used for memory management of whole compiler/interpret. Every
 * memory used should be accessible through here, so it can be easily manageable
 * and freed.
 */
typedef struct memory_data {

  /* Program persistent data. */
  TS_segment *p_persist_1st;        /* First segment. */
  TS_segment *p_persist_act;        /* Active segment. */


  /* Literals. */
  TS_segment *p_lit_1st;            /* First segment. */
  TS_segment *p_lit_act;            /* Active segment. */
  TS_segment *p_lit_act_prev;       /* Previously active segment. */

  /* Identifiers. */
  TS_segment *p_IDs_1st;            /* First segment. */
  TS_segment *p_IDs_act;            /* Active segment. */
  TS_segment *p_IDs_act_prev;       /* Previously active segment. */


  /* Parser data, which survives for optimizer. */
  TS_segment *p_parser_pers_1st;    /* First segment. */
  TS_segment *p_parser_pers_act;    /* Active segment. */
  
  /* Parser temporary data. */
  TS_segment *p_parser_temp_1st;    /* First segment. */
  TS_segment *p_parser_temp_act;    /* Active segment. */

  /* Parser pseudo-stack data. */
  TS_segment *p_parser_stack_1st;   /* First segment. */
  TS_segment *p_parser_stack_act;   /* Active segment. */


#ifdef OPTIMIZER_ON
  /* Optimizer runtime data. */
  TS_segment *p_optimizer_1st;      /* First segment. */
  TS_segment *p_optimizer_act;      /* Active segment. */
#endif


  /* Interpret pseudo-stack data, for numerical values. */
  TS_segment *p_num_stack_1st;      /* First segment. */
  TS_segment *p_num_stack_act;      /* Active segment. */

  /* Interpret required stacks. */
  TS_stack *p_lt_stack;             /* Long-term stack. */
  TS_stack *p_st_stack;             /* Short-term stack. */


  void *p_buffer;                   /* Pointer to buffer used by SCANNER. */
  bool in_function;                 /* Flag for BST_INSERT malloc. */

  
#ifndef NDEBUG
  /* For debugging purposes only. */
  char *p_lit_last_alloc;           /* Pointer to last literal allocated. */
  char *p_IDs_last_alloc;           /* Pointer to last identifier allocated. */
#endif

} TS_memory;

static TS_memory *p_mem = NULL;     /* Global pointer for this module. */


#ifndef NDEBUG
/* For debugging purposes only. */
static unsigned long alloc_count = 0;   /* Internal counter of allocations. */
static unsigned long realloc_count = 0; /* Internal counter of reallocations. */
#endif


/* ************************************************************************** *
 ~ ~~~[ CONSTANTS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Constants representing default value of each private memory part (segments).
 * It is advised not to exceed size of 128 KB as defined by MMAP_THRESHOLD,
 * otherwise it would lead to use of private anonymous mapping of mmap() by
 * standard malloc() function. See 'man 3 malloc' for more information.
 */
static const size_t SEG_SIZE = DEF_SEG_SIZE - sizeof(TS_segment);
static const size_t STACK_SEG_SIZE = DEF_SEG_SIZE - sizeof(TS_stack_seg);


/* Value representing size (in bytes) of smallest data structure used. */
static const size_t SD_SIZE = sizeof(tFunc);

/* Value representing size (in bytes) of biggest data structure used. */
static const size_t BD_SIZE = sizeof(TPushdownItem);

/* Constant used for allocating with buffer-like approach in SCANNER. */
static const size_t STR_BUF_SIZE = SCANNER_BUFFER_SIZE / sizeof(char);


/* ************************************************************************** *
 ~ ~~~[ FUNCTIONAL PROTOTYPES OF LOCAL FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

static inline void parser_stage_setup(void);
static inline void interpret_stage_setup(void);

#ifdef OPTIMIZER_ON
static inline void optimizer_stage_setup(void);
#endif

static inline void *segment_malloc(size_t size, TE_alloc_type type,
                                   TS_segment **pp_active);
static inline void *buffer_malloc(size_t size);
static inline void *string_malloc(size_t size, TE_alloc_type type,
                                  TS_segment **pp_active,
                                  TS_segment **pp_active_prev);
static inline void *stack_malloc(size_t size, TS_stack *p_stack);

static inline void *buffer_realloc(void *ptr, size_t size);
static inline void *string_realloc(void *ptr, size_t size, TE_alloc_type type,
                                   TS_segment **pp_act,
                                   TS_segment **pp_act_prev, TS_segment *p_1st);
static inline void *stack_realloc(void *ptr, size_t size, TS_stack *p_stack);

static inline TS_segment *new_segment(TE_alloc_type type);
static inline TS_stack_seg *new_stack_seg(void);

static inline void substack_new(TS_stack *p_stack);
static inline void substack_restore(TS_stack *p_stack);
static inline void stack_update_act(TS_stack *p_stack);

static inline void pointers_swap(char **ptr_A, char **ptr_B);

static inline void segments_free(TS_segment *p_seg);
void memory_free(void);


/* ************************************************************************** *
 ~ ~~~[ PRIMARY FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/**
 * Prepares compiler/interpret memory according to stage it is actually active.
 * 
 * This is just simple wrapper function for calling more specialized functions.
 *
 * @param stage is enumerate representing actual active runtime stage.
 */
void memory_setup(TE_stage stage)
{{{
  switch (stage) {
    case PARSER :
      parser_stage_setup();
      return;

#ifdef OPTIMIZER_ON
    case OPTIMIZER :
      optimizer_stage_setup();
      return;
#endif

    case INTERPRET :
      interpret_stage_setup();
      return;
    
    default :
      printError("Unknown runtime stage!");
      exit(ERR_42);
  }
}}}


/**
 * Allocates memory in proper memory part and returns pointer to this available
 * memory.
 *
 * In fact, this is just a wrapper function for calling more specialized
 * functions. It is not guaranteed that this function will call malloc()
 * function in the end, but it is guaranteed that pointer returned by this
 * function can be used for storing data of requested size.
 *
 * @param size is size of memory to be allocated in bytes (size_t).
 * @param type is enumerate representing what kind of allocation is being made.
 * @returns pointer to available memory of given size.
 */
void *ifj_malloc(size_t size, TE_alloc_type type)
{{{
  assert(p_mem != NULL);    /* Allocation without initialized memory? */

  switch (type) {
    case BUFFER :
      return buffer_malloc(size);

    case LITERAL :
      return string_malloc(size, type, &p_mem->p_lit_act,
                           &p_mem->p_lit_act_prev);
    
    case IDENTIFIER :
      return string_malloc(size, type, &p_mem->p_IDs_act,
                           &p_mem->p_IDs_act_prev);

    case PERSISTENT :
      return segment_malloc(size, type, &p_mem->p_persist_act);

    
    /*
     * In case of BST_INSERT type of allocation, it is determined where it
     * should be allocated by value of in_function flag.
     */
    case BST_INSERT :
      if (p_mem->in_function == true) {

        /* Re-use of PARSER_TEMP - it is common allocation of BST_INSERT. */
        case PARSER_TEMP :
          return segment_malloc(size, PARSER_TEMP, &p_mem->p_parser_temp_act);
      }
      /*
       * else is not used here because of the feature C languages 'switch' has.
       * In case in_function flag is set to false, above case is skipped because
       * of the condition and run continues below, where it uses PARSER_PERS for
       * allocation. This substitutes the redundancy of executive code. This
       * means the ORDER in which cases here appear is IMPORTANT!
       */

    case PARSER_PERS :
      return segment_malloc(size, PARSER_PERS, &p_mem->p_parser_pers_act);

    case PARSER_STACK :
      /* Making sure of pushed object, otherwise tpdi_pop() wouldn't work. */
      assert(size == sizeof(TPushdownItem));

      return segment_malloc(size, type, &p_mem->p_parser_stack_act);

#ifdef OPTIMIZER_ON
    case OPT :
      return segment_malloc(size, type, &p_mem->p_optimizer_act);
#endif


    case NUM_STACK :
      /* Making sure of pushed object, otherwise num_stack_pop() won't work. */
      assert(size == sizeof(long));

      return segment_malloc(size, type, &p_mem->p_num_stack_act);

    case LT_STACK :
      return stack_malloc(size, p_mem->p_lt_stack);

    case ST_STACK :
      return stack_malloc(size, p_mem->p_st_stack);


    /* This should not happen! */
    default :
      printError("Unkown allocation type!");
      exit(ERR_42);
  }
}}}


/**
 * Reallocates memory of given pointer and returns pointer to newly available
 * memory.
 *
 * Reallocation is provided only for some types of previous allocation. This
 * function shouldn't be used in other cases, because reallocation in some
 * memory structures is not supported.
 *
 * @param ptr is pointer of previous allocation.
 * @param size is newly requested size of reallocation in bytes.
 * @returns pointer to newly reallocated memory of given size.
 */
void *ifj_realloc(void *ptr, size_t size, TE_alloc_type type)
{{{
  assert(p_mem != NULL);    /* Reallocation without initialized memory? */

  switch (type) {
    case BUFFER : 
      return buffer_realloc(ptr, size);

    case LITERAL :
      return string_realloc(ptr, size, type, &p_mem->p_lit_act,
                            &p_mem->p_lit_act_prev, p_mem->p_lit_1st);
    
    case IDENTIFIER :
      return string_realloc(ptr, size, type, &p_mem->p_IDs_act,
                            &p_mem->p_IDs_act_prev, p_mem->p_IDs_1st);

    case LT_STACK :
      return stack_realloc(ptr, size, p_mem->p_lt_stack);

    case ST_STACK :
      return stack_realloc(ptr, size, p_mem->p_st_stack);

    default :
      printError("Only SCANNER & INTERPRET are allowed to use ifj_realloc() "
                 "function!");
      exit(ERR_42);
  }
}}}


/* ************************************************************************** *
 ~ ~~~[ PARSER-SPECIFIC FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/**
 * Simple function which set flag representing that parsing is about to begin
 * processing inside function.
 */
void set_IF_flag(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  p_mem->in_function = true;

  return;
}}}


/**
 * Clears the flag previously set by the set_IF_flag() function.
 */
void clear_IF_flag(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  p_mem->in_function = false;

  return;
}}}


/**
 * Recycles the PARSER_TEMP segments by stashing theirs content.
 */
void stash_temp(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  /* Valid pointers? */
  assert(p_mem->p_parser_temp_1st != NULL);
  assert(p_mem->p_parser_temp_act != NULL);

  
  /* Reassigning to start from beginning. */
  p_mem->p_parser_temp_act = p_mem->p_parser_temp_1st;

  /* Stashing every created segment so far. */
  while (p_mem->p_parser_temp_act != NULL) {
    p_mem->p_parser_temp_act->p_free = p_mem->p_parser_temp_act->data;
    p_mem->p_parser_temp_act = p_mem->p_parser_temp_act->p_next;
  }
  
  /* Reseting pointer to active segment. */
  p_mem->p_parser_temp_act = p_mem->p_parser_temp_1st;

  return;
}}}


/**
 * Pops one TPushdownItem structure from PARSER_STACK segment and returns it.
 * 
 * This function doesn't test if it was used before anything was pushed to
 * PARSER_STACK. In that case it is highly probable that the program would end
 * up in almost endless loop and eventually will cause Segmentation fault.  
 *
 * @returns TPushdownItem structure, which has been previously stored here.
 */
TPushdownItem tpdi_pop(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  /* Valid pointers? */
  assert(p_mem->p_parser_stack_1st != NULL);
  assert(p_mem->p_parser_stack_act != NULL);
  
  /* Pop on empty stack? */
  assert(p_mem->p_parser_stack_act->p_free != p_mem->p_parser_stack_1st->data);


  /* Auxiliary pointer to active segment. */
  TS_segment *p_seg = p_mem->p_parser_stack_act;

  /* Auxiliary pointer to last item pushed to stack. */
  TPushdownItem *p_item = (TPushdownItem *) p_seg->p_free - 1;

  
  /* Have we popped from empty segment? */
  if ((char *) p_item < p_seg->data) {

    /*
     * We have popped from empty segment. Reseting auxiliary pointer and
     * finding previous segment.
     */
    p_seg = p_mem->p_parser_stack_1st;

    while (p_seg->p_next != p_mem->p_parser_stack_act) {
      p_seg = p_seg->p_next;
    }
  
    /* Updating pointer to active segment. */
    p_mem->p_parser_stack_act = p_seg;
    
    /* Re-acquiring pointer to TPushdownItem and updating segment. */
    p_item = (TPushdownItem *) p_seg->p_free - 1;
    p_seg->filled = false;
  }


  /* Updating pointer to free memory space. */
  p_seg->p_free = (char *) p_item;

  return *p_item;                       /* Returning the whole structure. */
}}}


/* ************************************************************************** *
 ~ ~~~[ INTERPRET-SPECIFIC FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/**
 * Pops one long int from NUM_STACK segment and returns it.
 * 
 * This function doesn't test if it was used before anything was pushed to
 * NUM_STACK. In that case it is highly probable that the program would end
 * up in almost endless loop and eventually will cause Segmentation fault.  
 *
 * @returns long int, which has been previously stored to NUM_STACK.
 */
long int num_stack_pop(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  /* Valid pointers? */
  assert(p_mem->p_num_stack_1st != NULL);
  assert(p_mem->p_num_stack_act != NULL);

  /* Pop on empty stack? */
  assert(p_mem->p_num_stack_act->p_free != p_mem->p_num_stack_1st->data);


  /* Auxiliary pointer to active segment. */
  TS_segment *p_seg = p_mem->p_num_stack_act;

  /* Auxiliary pointer to last item pushed to stack. */
  long *p_item = (long *) p_seg->p_free - 1;


  /* Have we popped from empty segment? */
  if ((char *) p_item < p_seg->data) {

    /*
     * We have popped from empty segment. Reseting auxiliary pointer and
     * finding previous segment.
     */
    p_seg = p_mem->p_num_stack_1st;

    while (p_seg->p_next != p_mem->p_num_stack_act) {
      p_seg = p_seg->p_next;
    }
  
    /* Updating pointer to active segment. */
    p_mem->p_num_stack_act = p_seg;
    
    /* Re-acquiring pointer to long and updating segment. */
    p_item = (long *) p_seg->p_free - 1;
    p_seg->filled = false;
  }


  /* Updating pointer to free memory space. */
  p_seg->p_free = (char *) p_item;

  return *p_item;                       /* Returning the whole structure. */
}}}


/**
 * Simple function for testing if NUM_STACK is empty or not.
 *
 * @returns 'true' if NUM_STACK is empty, otherwise 'false'.
 */
bool NS_is_empty(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  if (p_mem->p_num_stack_act->p_free == p_mem->p_num_stack_1st->data)
    return true;
  else
    return false;
}}}


/**
 * Wrapper function used by INTERPRET. Informs memory management, that new
 * 'substacks' are required.
 */
void func_call(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  substack_new(p_mem->p_lt_stack);      /* New 'stack' for long-term stack. */
  substack_new(p_mem->p_st_stack);      /* New 'stack' for short-term stack. */

  return;
}}}


/**
 * Copies memory from src, of given size, to ESP of ST_STACK. ESP is then
 * updated to point exactly behind end of copied memory block.
 * 
 * In case src is NULL or size is equal to zero, then no copying is done. This
 * functions uses memcpy() for copying, because INTERPRET is supposed to make
 * sure that source & destination will not overlap.
 *
 * @param src is pointer to source from which the function will copy.
 * @param size is size is bytes of memory block to be copied.
 * @returns pointer to memory where copied memory is located.
 */
void *func_ret(void *src, size_t size)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  TS_stack *p_stack = p_mem->p_st_stack;
  
  /* ST_STACK properly initialized? */
  assert(p_stack != NULL);
  assert(p_stack->p_act != NULL);
  assert(p_stack->ESP != NULL);

  /* Anything to copy? */
  if (src == NULL || size == 0x0) {
    return (void *) p_stack->ESP;
  }

  char *p_new = p_stack->ESP + size;    /* Pointer to end of copying. */

  /* Are we still within our memory segment? */
  if (p_new > p_stack->p_act->p_eos) {

    /* Is there no segment left? */
    if (p_stack->p_act->p_next == NULL) {
      TS_stack_seg *p_seg = new_stack_seg();      /* Acquiring new segment. */

      /* Listing in the new segment. */
      p_stack->p_act->p_next = p_seg;
      p_seg->p_prev = p_stack->p_act;

      p_stack->p_act = p_seg;
    }
    else {
      p_stack->p_act = p_stack->p_act->p_next;
    }

    p_new = p_stack->p_act->data;                 /* Pointer to be returned. */
    memcpy((void *) p_new, src, size);            /* Copying memory. */
    p_stack->ESP = p_new + size;                  /* New value of ESP. */
  }
  else {
    memcpy((void *) p_stack->ESP, src, size);     /* Copying memory. */
    
    /*
     * Swapping pointers values since p_new points to rest of free space and ESP
     * to memory address to be returned.
     */
    pointers_swap(&p_new, &p_stack->ESP);
  }
  
  return (void *) p_new;            /* Pointer to start of copied space. */
}}}


/**
 * Restores ESP & EBP from both, ST_STACK and LT_STACK, and updates theirs p_act
 * pointer.
 */
void func_end(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  substack_restore(p_mem->p_st_stack);
  substack_restore(p_mem->p_lt_stack);

  return;
}}}


/**
 * Stashes content of actual 'substack' of ST_STACK.
 */
void expr_fin(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

  /* Properly initialized ST_STACK? */
  assert(p_mem->p_st_stack != NULL);
  assert(p_mem->p_st_stack->EBP != NULL);

  p_mem->p_st_stack->ESP = p_mem->p_st_stack->EBP;    /* Reseting ESP. */
  stack_update_act(p_mem->p_st_stack);                /* Updating p_act. */

  return;
}}}


/* ************************************************************************** *
 ~ ~~~[ STAGE-SETUP FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Initializes memory management before PARSER is used for the first time.
 */
static inline void parser_stage_setup(void)
{{{
  assert(p_mem == NULL);                        /* Double call? */

  /* Registering memory free to be used at any exit of program. */
  atexit(memory_free);

  /* Allocating space for memory info storing. */
  p_mem = (TS_memory *) malloc(sizeof(TS_memory));

  /* Successful allocation? */
  alloc_test(p_mem);
  
  /* Segments allocation. */
  p_mem->p_lit_1st = new_segment(LITERAL);
  p_mem->p_IDs_1st = new_segment(IDENTIFIER);
  p_mem->p_persist_1st = new_segment(PERSISTENT);
  p_mem->p_parser_pers_1st = new_segment(PARSER_PERS);
  p_mem->p_parser_temp_1st = new_segment(PARSER_TEMP);
  p_mem->p_parser_stack_1st = new_segment(PARSER_STACK);

  /* Initialization of pointers of active elements. */
  p_mem->p_lit_act = p_mem->p_lit_1st;
  p_mem->p_IDs_act = p_mem->p_IDs_1st;
  p_mem->p_persist_act = p_mem->p_persist_1st;
  p_mem->p_parser_pers_act = p_mem->p_parser_pers_1st;
  p_mem->p_parser_temp_act = p_mem->p_parser_temp_1st;
  p_mem->p_parser_stack_act = p_mem->p_parser_stack_1st;

  /* Initialization, because no allocations of these has been done yet. */
  p_mem->p_lit_act_prev = NULL;
  p_mem->p_IDs_act_prev = NULL;
  p_mem->p_buffer = NULL;

  /* We didn't started parsing yet -> no parsing of function has been done. */
  p_mem->in_function = false;

  /* Initialization of INTERPRET memory segments. */
  p_mem->p_num_stack_1st = NULL;
  p_mem->p_num_stack_act = NULL;

  p_mem->p_lt_stack = NULL;
  p_mem->p_st_stack = NULL;

#ifdef OPTIMIZER_ON
  /* Segment allocation & initialization for OPTIMIZER if it used. */
  p_mem->p_optimizer_1st = new_segment(OPT);
  p_mem->p_optimizer_act = p_mem->p_optimizer_1st;
#endif

  // // // // // // // // // // // // // // // // // // // // // // // // //
  
#ifndef NDEBUG

  /* For debugging purposes only. */
  p_mem->p_lit_last_alloc = NULL;
  p_mem->p_IDs_last_alloc = NULL;
#endif

  return;
}}}


/*
 * Frees no more necessary memory segments and buffer (if it was allocated),
 * because OPTIMIZER won't need those. This function is used only when OPTIMIZER
 * is used.
 */
#ifdef OPTIMIZER_ON
static inline void optimizer_stage_setup(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */
  
  /* Freeing buffer, if it was used. */
  if (p_mem->p_buffer != NULL) {
    free((void *) p_mem->p_buffer);
    p_mem->p_buffer = NULL;
  }

  /* Memory segments freeing. */
  segments_free(p_mem->p_IDs_1st);
  segments_free(p_mem->p_parser_temp_1st);
  segments_free(p_mem->p_parser_stack_1st);

  /* Freed memory segment pointers updating to prevent double free. */
  p_mem->p_IDs_1st = NULL;
  p_mem->p_IDs_act = NULL;
  p_mem->p_IDs_act_prev = NULL;
  p_mem->p_parser_temp_1st = NULL;
  p_mem->p_parser_temp_act = NULL;
  p_mem->p_parser_stack_1st = NULL;
  p_mem->p_parser_stack_act = NULL;

  // // // // // // // // // // // // // // // // // // // // // // // // //
  
#ifndef NDEBUG
  /* For debugging purposes only. */
  p_mem->p_lit_last_alloc = NULL;
  p_mem->p_IDs_last_alloc = NULL;
#endif

  return;
}}}
#endif


/*
 * Frees no more necessary memory segments and allocates memory required for
 * run of INTERPRET. 
 */
static inline void interpret_stage_setup(void)
{{{
  assert(p_mem != NULL);                /* Corrupted memory management? */

#ifndef OPTIMIZER_ON
  /* Freeing buffer, if it was used. */
  if (p_mem->p_buffer != NULL) {
    free((void *) p_mem->p_buffer);
    p_mem->p_buffer = NULL;
  }

  /* Memory segments freeing. */
  segments_free(p_mem->p_IDs_1st);
  segments_free(p_mem->p_parser_temp_1st);
  segments_free(p_mem->p_parser_stack_1st);
#else
  segments_free(p_mem->p_optimizer_1st);
#endif
  segments_free(p_mem->p_parser_pers_1st);

  /* Freed memory segment pointers updating to prevent double free. */
#ifndef OPTIMIZER_ON
  p_mem->p_IDs_1st = NULL;
  p_mem->p_IDs_act = NULL;
  p_mem->p_IDs_act_prev = NULL;
  p_mem->p_parser_temp_1st = NULL;
  p_mem->p_parser_temp_act = NULL;
  p_mem->p_parser_stack_1st = NULL;
  p_mem->p_parser_stack_act = NULL;
#ifndef NDEBUG
  /* For debugging purposes only. */
  p_mem->p_lit_last_alloc = NULL;
  p_mem->p_IDs_last_alloc = NULL;
#endif
#else
  p_mem->p_optimizer_1st = NULL;
  p_mem->p_optimizer_act = NULL;
#endif
  p_mem->p_parser_pers_1st = NULL;
  p_mem->p_parser_pers_act = NULL;


  /* Creating NUM_STACK and pointer to active segment initialization.. */
  p_mem->p_num_stack_1st = new_segment(NUM_STACK);
  p_mem->p_num_stack_act = p_mem->p_num_stack_1st;


  /*
   * Allocating memory for both stacks with one allocation since they both have
   * same life expectancy.
   */
  p_mem->p_lt_stack = (TS_stack *) malloc(sizeof(TS_stack) * 2);
  
  alloc_test(p_mem->p_lt_stack);       /* Successful allocation? */

  /* Assigning memory space from previous allocation. */
  p_mem->p_st_stack = p_mem->p_lt_stack + 1;

  p_mem->p_lt_stack->p_1st = new_stack_seg();
  p_mem->p_st_stack->p_1st = new_stack_seg();

  p_mem->p_lt_stack->p_act = p_mem->p_lt_stack->p_1st;
  p_mem->p_st_stack->p_act = p_mem->p_st_stack->p_1st;

  p_mem->p_lt_stack->p_1st->p_prev = NULL;
  p_mem->p_st_stack->p_1st->p_prev = NULL;

  p_mem->p_lt_stack->EBP = p_mem->p_lt_stack->p_1st->data;
  p_mem->p_st_stack->EBP = p_mem->p_st_stack->p_1st->data;

  p_mem->p_lt_stack->ESP = p_mem->p_lt_stack->EBP;
  p_mem->p_st_stack->ESP = p_mem->p_st_stack->EBP;

  // // // // // // // // // // // // // // // // // // // // // // // // //

#ifndef NDEBUG
  /* For debugging purposes only. */
  p_mem->p_lt_stack->p_last_alloc = NULL;
  p_mem->p_st_stack->p_last_alloc = NULL;
#endif

  return;
}}}


/* ************************************************************************** *
 ~ ~~~[ ALLOCATION FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Assigns memory of given size from program's private memory segment and
 * returns pointer to start of the free block.
 *
 * NOTE: Function is written with presumption, that given size of requested
 *       will not exceed SEG_SIZE, otherwise it might lead to private memory
 *       corruption! This condition is only tested when debugging with use of
 *       assert macros.
 */
static inline void *segment_malloc(size_t size, TE_alloc_type type,
                                   TS_segment **pp_act)
{{{
  if (size == 0x0) {
    return NULL;                                /* Same as malloc(0); */
  }

  assert(size <= SEG_SIZE);                     /* Too big allocation? */
  
  /* Wrong use of function? */
#ifdef OPTIMIZER_ON
  assert(type == PERSISTENT || type == PARSER_PERS || type == PARSER_TEMP ||
         type == PARSER_STACK || type == OPT || type == NUM_STACK);
#else
  assert(type == PERSISTENT || type == PARSER_PERS || type == PARSER_TEMP ||
         type == PARSER_STACK || type == NUM_STACK);
#endif

  assert(pp_act != NULL && *pp_act != NULL);    /* Valid pointers? */

  // // // // // // // // // // // // // // // // // // // // // // // // //

  TS_segment *p_seg = *pp_act;      /* Auxiliary pointer for easier access. */

  /* All memory segments used? */
  if (p_seg->filled == true) {
    p_seg = new_segment(type);
    (*pp_act)->p_next = p_seg;
  }


  /* Auxiliary pointer pointing to end of new allocation. */
  char *p_new = p_seg->p_free + size;


  /* Are we still within our memory segment? */
  if (p_new > p_seg->p_eos) {

    /* 
     * Should be actual segment set as full? (Condition in case size has
     * different size than SD_SIZE.)
     */
    if ((p_new + SD_SIZE) > p_seg->p_eos) {
      p_seg->filled = true;
    }

    TS_segment *p_seg_prev;         /* Auxiliary pointer. */

    /* Finding new available segment. */
    do {
      p_seg_prev = p_seg;
      p_seg = p_seg->p_next;
    } while (p_seg != NULL && (p_seg->filled == true
                               || (p_seg->p_free + size) > p_seg->p_eos));

    
    /* No segment with enough memory found, create new segment.  */
    if (p_seg == NULL || (p_seg->p_free + size) > p_seg->p_eos) {
      TS_segment *p_new_seg = new_segment(type);
      p_seg_prev->p_next = p_new_seg;
      p_seg = p_new_seg;
    }

    /* New assigning for use below. */
    p_new = p_seg->p_free + size;
  }


  /* Would there be enough space left after allocation? */
  if ((p_new + SD_SIZE) > p_seg->p_eos) {
    p_seg->filled = true;
  }

  /*
   * Segment acquired, swapping pointers values since p_new points to rest of
   * free space and p_free to memory address to be returned.
   */
  pointers_swap(&p_new, &p_seg->p_free);


  /* Active segment pointer update required? */
  if (p_seg != *pp_act || p_seg->filled == true) {
    while ((*pp_act)->p_next != NULL && (*pp_act)->filled == true) {
      *pp_act = (*pp_act)->p_next;
    }
  }
  
  return (void *) p_new;            /* Pointer to available memory space. */
}}}


/*
 * Allocates memory for buffer for numbers of type double, used by SCANNER.
 */
static inline void *buffer_malloc(size_t size)
{{{
  if (size == 0x0) {
    return NULL;                      /* Same as malloc(0); */
  }

  assert(p_mem != NULL);              /* Corrupted memory management? */
  assert(size <= SEG_SIZE);           /* Too big size of buffer? */
  assert(p_mem->p_buffer == NULL);    /* Double call of function? */

  p_mem->p_buffer = malloc(size);

  alloc_test(p_mem->p_buffer);        /* Successful allocation? */

#ifndef NDEBUG
  /* For debugging purposes only. */
  alloc_count++;
#endif

  return p_mem->p_buffer;
}}}


/*
 * Assigns memory of given size from program's private memory segment and
 * returns pointer to start of that free block.
 *
 * NOTE: Function is written with presumption, that given size of requested
 *       will not exceed SEG_SIZE, otherwise it might lead to private memory
 *       corruption! To be more specific, it is expected that every string
 *       allocation will have size of multiplication of STR_BUF_SIZE.
 */
static inline void *string_malloc(size_t size, TE_alloc_type type,
                                  TS_segment **pp_act,
                                  TS_segment **pp_act_prev)
{{{
  if (size == 0x0) {
    return NULL;                              /* Same as malloc(0); */
  }

  assert(size <= SEG_SIZE);                   /* Too big allocation? */
  assert(type == LITERAL || type == IDENTIFIER);  /* Wrong use of function? */
  assert(pp_act != NULL && *pp_act != NULL);  /* Valid pointers? */


  TS_segment *p_seg = *pp_act;        /* Auxiliary pointer for easier access. */

  /* All memory segments used? */
  if (p_seg->filled == true) {
    p_seg = new_segment(type);
    (*pp_act)->p_next = p_seg;
  }


  /* Finding the end of string of previous allocation to save memory space. */
  while (*(p_seg->p_free++) != '\0')
    ;

  /* Auxiliary pointer pointing to end of new allocation. */
  char *p_new = p_seg->p_free + size;


  /* Are we still within our memory segment? */
  if (p_new > p_seg->p_eos) {
    
    /* 
     * Should be actual segment set as full? (Condition in case size has
     * different size than STR_BUF_SIZE.)
     */
    if ((p_new + STR_BUF_SIZE) > p_seg->p_eos) {
      p_seg->filled = true;
    }


    TS_segment *p_seg_prev;         /* Auxiliary pointer. */

    /* Finding new available segment. */
    do {
      p_seg_prev = p_seg;
      p_seg = p_seg->p_next;
    } while (p_seg != NULL && (p_seg->filled == true
                               || (p_seg->p_free + size) > p_seg->p_eos));

    /* No segment with enough memory found, create new segment.  */
    if (p_seg == NULL) {
      TS_segment *p_new_seg = new_segment(type);
      p_seg_prev->p_next = p_new_seg;
      p_seg = p_new_seg;
      p_seg->p_free++;              /* Skipping '\0' character at start. */
    }


    /* New assigning for use below. */
    p_new = p_seg->p_free + size;
  }
  
  /* Assigning actual segment as last used for potential reallocation. */
  *pp_act_prev = p_seg;

  /* Would there be enough space left after allocation? */
  if ((p_new + STR_BUF_SIZE) > p_seg->p_eos) {
    p_seg->filled = true;
  }

  
  /* Active segment pointer update required? */
  if (p_seg != *pp_act || p_seg->filled == true) {
    while ((*pp_act)->p_next != NULL && (*pp_act)->filled == true) {
      *pp_act = (*pp_act)->p_next;
    }
  }
  
  // // // // // // // // // // // // // // // // // // // // // // // // //

#ifndef NDEBUG

  /* For debugging purposes only. */
  switch (type) {
    case LITERAL :
      p_mem->p_lit_last_alloc = p_seg->p_free;
      break;

    case IDENTIFIER :
      p_mem->p_IDs_last_alloc = p_seg->p_free;
      break;

    default :
      printError("What the f*ck happened?! Wild pointer???");
      exit(ERR_42);
  }
#endif

  // // // // // // // // // // // // // // // // // // // // // // // // //

  return (void *) p_seg->p_free;    /* Pointer to available memory space. */
}}}


/*
 * Assigns memory of given size from given stack and returns pointer to start of
 * the that free block.
 *
 * NOTE: Function is written with presumption, that given size of requested
 *       will not exceed SEG_SIZE, otherwise it might lead to private memory
 *       corruption! This condition is only tested when debugging with use of
 *       assert macros.
 */
static inline void *stack_malloc(size_t size, TS_stack *p_stack)
{{{
  if (size == 0x0) {
    return NULL;                            /* Same as malloc(0); */
  }

  assert(size <= STACK_SEG_SIZE);           /* Too big allocation? */

  /* Properly initialized stack? */
  assert(p_stack != NULL);
  assert(p_stack->p_act != NULL);
  assert(p_stack->ESP != NULL);

  // // // // // // // // // // // // // // // // // // // // // // // // //

  /* Auxiliary pointer pointing to end of new allocation. */
  char *p_new = p_stack->ESP + size;

  /* Are we still within our memory segment? */
  if (p_new > p_stack->p_act->p_eos) {

    /* Is there no segment left? */
    if (p_stack->p_act->p_next == NULL) {
      TS_stack_seg *p_seg = new_stack_seg();  /* Acquiring new segment. */

      /* Listing in the new segment. */
      p_stack->p_act->p_next = p_seg;
      p_seg->p_prev = p_stack->p_act;
      
      p_stack->p_act = p_seg;

      p_new = p_seg->data;
    }
    else {
      p_stack->p_act = p_stack->p_act->p_next;
      p_new = p_stack->p_act->data;
    }
    
    /* Shifting ESP to end of allocation. */
    p_stack->ESP = p_new + size;
  }
  else {
    /*
     * Swapping pointers values since p_new points to rest of free space and ESP
     * to memory address to be returned.
     */
    pointers_swap(&p_new, &p_stack->ESP);
  }

#ifndef NDEBUG
  p_stack->p_last_alloc = p_new;            /* Debugging purposes only. */
#endif

  return (void *) p_new;            /* Pointer to available memory space. */
}}}


/* ************************************************************************** *
 ~ ~~~[ REALLOCATION FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Reallocates buffer used by SCANNER to given size.
 */
static inline void *buffer_realloc(void *ptr, size_t size)
{{{
  assert(p_mem != NULL);            /* Corrupted memory management? */
  assert(ptr != NULL);              /* Use ifj_malloc() instead! */
  assert(ptr == p_mem->p_buffer);   /* Reallocation of buffer? */
  assert(size != 0x0);              /* There's no need for free() by others. */
  assert(size <= SEG_SIZE);         /* Too big reallocation? */

  void *p_new = realloc(ptr, size); 

  alloc_test(p_new);                /* Successful reallocation? */
  p_mem->p_buffer = p_new;          /* Backup of new pointer. */

#ifndef NDEBUG
  /* For debugging purposes only. */
  realloc_count++;
#endif

  return p_new;
}}}


/*
 * Reallocates memory used for storing string used by SCANNER to given size.
 * Returns pointer to new memory space, where previous content of allocation is
 * copied in case of need. Given void *ptr is actually used just for debugging
 * since memory management also stores the last 'position' of allocation.
 */
static inline void *string_realloc(void *ptr __attribute__((unused)),
                                   size_t size, TE_alloc_type type,
                                   TS_segment **pp_act,
                                   TS_segment **pp_act_prev, TS_segment *p_1st)
{{{
  assert(ptr != NULL);                /* Use ifj_malloc() instead! */
  assert(size != 0x0);                /* There's no need for free by others. */
  assert(size <= SEG_SIZE);           /* Too big reallocation? */

#ifndef NDEBUG
  assert(p_mem != NULL);              /* Corrupted memory management? */
  
  /* Testing if realloc was requested on last returned pointer. */
  switch (type) {
    case LITERAL :
      assert(ptr = p_mem->p_lit_last_alloc);
      break;

    case IDENTIFIER :
      assert(ptr = p_mem->p_IDs_last_alloc);
      break;

    default :
      printError("Wrong use of string_realloc() function!");
      exit(ERR_42);
  }
#endif

  /* Valid pointers? */
  assert(p_1st != NULL);              
  assert(pp_act != NULL && *pp_act != NULL);
  assert(pp_act_prev != NULL && *pp_act_prev != NULL);

  // // // // // // // // // // // // // // // // // // // // // // // // // 

  TS_segment *p_seg = *pp_act_prev;   /* Auxiliary ptr for easier access. */
  char *p_new = p_seg->p_free + size; /* Pointer to end of new reallocation. */


  /* Are we still within our memory segment? */
  if (p_new > p_seg->p_eos) {
    TS_segment *p_seg_prev;           /* Auxiliary pointer */

    /* Finding new available segment. */
    do {
      p_seg_prev = p_seg;
      p_seg = p_seg->p_next;
    } while (p_seg != NULL && (p_seg->filled == true
                               || (p_seg->p_free + size) > p_seg->p_eos));

    /* No segment with enough memory found, create new segment.  */
    if (p_seg == NULL) {
      TS_segment *p_new_seg = new_segment(type);
      p_seg_prev->p_next = p_new_seg;
      p_seg = p_new_seg;
    }

    
    /* Finding end of string of previous allocation/segment creation. */
    while (*(p_seg->p_free++) != '\0')
      ;
    
    /* Copying previous allocated memory to new segment. */
    memcpy((void *) p_seg->p_free, (void *) (*pp_act_prev)->p_free, 
           (size_t) ((*pp_act_prev)->p_eos - (*pp_act_prev)->p_free));
    
    /* Recycling previously used segment. */
    (*pp_act_prev)->filled = false;
    (*pp_act_prev)->p_free--;

    /* New assigning for use below. */
    p_new = p_seg->p_free + size;
  }



  /* Assigning actual segment as last used for potential reallocation again. */
  *pp_act_prev = p_seg;


  /* Would there be enough space left after reallocation? */
  if ((p_new + STR_BUF_SIZE) > p_seg->p_eos) {
    p_seg->filled = true;
  }
  

  /* Active segment pointer update required? */
  if (*pp_act != *pp_act_prev || p_seg->filled == true) {
    *pp_act = p_1st;                  /* Starting from first segment. */

    while ((*pp_act)->p_next != NULL && (*pp_act)->filled == true) {
      *pp_act = (*pp_act)->p_next;
    }
  }

  // // // // // // // // // // // // // // // // // // // // // // // // //

#ifndef NDEBUG

  /* For debugging purposes only. */
  switch (type) {
    case LITERAL :
      p_mem->p_lit_last_alloc = p_seg->p_free;
      break;

    case IDENTIFIER :
      p_mem->p_IDs_last_alloc = p_seg->p_free;
      break;

    default :
      printError("What the f*ck happened?! Wild pointer???");
      exit(ERR_42);
  }
#endif

  // // // // // // // // // // // // // // // // // // // // // // // // //

  return (void *) p_seg->p_free;
}}}


/*
 * Reallocates memory used for INTERPRET stacks. Returns pointer to new memory
 * space, where previous content of allocation is copied in case of need.
 */
static inline void *stack_realloc(void *ptr, size_t size, TS_stack *p_stack)
{{{
  assert(ptr != NULL);                /* Use ifj_malloc() instead! */
  assert(size != 0x0);                /* There's no need for free by others. */
  assert(size <= STACK_SEG_SIZE);     /* Too big reallocation? */

  /* Properly initialized stack? */
  assert(p_stack != NULL);
  assert(p_stack->p_act != NULL);
  assert((char *) ptr == p_stack->p_last_alloc);  /* Proper reallocation? */

  // // // // // // // // // // // // // // // // // // // // // // // // //
  
  char *p_new = (char *) ptr + size;  /* Pointer to end of new reallocation. */
  
  /* Are we still within our memory segment? */
  if (p_new > p_stack->p_act->p_eos) {
    TS_stack_seg *p_seg;              /* Auxiliary pointer. */
    
    /* Is there no segment left? */
    if (p_stack->p_act->p_next == NULL) {
      p_seg = new_stack_seg();        /* New segment acquiring. */

      /* Listing in the new segment. */
      p_stack->p_act->p_next = p_seg;
      p_seg->p_prev = p_stack->p_act;
    }
    else {
      p_seg = p_stack->p_act->p_next;
    }
    
    /* Copying previously stored data. */
    memcpy((void *) p_seg->data, (void *) ptr,
           (size_t) (p_stack->p_act->p_eos - (char *) ptr));
    
    /* Updating active segment + recycling ptr + setting new ESP. */
    p_stack->p_act = p_seg;
    ptr = (void *) p_seg->data;
    p_stack->ESP = (char*) ptr + size;
  }
  else {
    /* Shifting ESP to end of reallocation. */
    p_stack->ESP = p_new;
  }
  

#ifndef NDEBUG
  p_stack->p_last_alloc = (char *) ptr;   /* Debugging purposes only. */
#endif

  return ptr;                         /* Recycled pointer. */
}}}


/* ************************************************************************** *
 ~ ~~~[ SEGMENTS ALLOCATION FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Auxiliary function for allocation of new memory segment and initializing it
 * by given segment type.
 */
static inline TS_segment *new_segment(TE_alloc_type type)
{{{
  /* Segment allocation. */
  TS_segment *p_new = (TS_segment *) malloc(sizeof(TS_segment) + SEG_SIZE);
  
  /* Successful allocation? */
  alloc_test(p_new);

#ifndef NDEBUG
  /* For debugging purposes only. */
  alloc_count++;
#endif
  
  /* Common initialization for every type of segment. */
  p_new->p_next = NULL;
  p_new->filled = false;
  p_new->p_free = p_new->data;
  p_new->p_eos = p_new->data + SEG_SIZE;
  

  /* Individual initializations. */
  switch (type) {

    /* Nothing to do. */
    case IDENTIFIER :
    case PERSISTENT :
    case PARSER_PERS :
    case PARSER_TEMP :
    case PARSER_STACK :
    case NUM_STACK :
#ifdef OPTIMIZER_ON
    case OPT :
#endif
      break;
  
    /* Literals specific initialization. */
    case LITERAL :
      p_new->data[0] = '\0';
      break;

    /* Something went wrong - this shouldn't happen! */
    default :
      printError("Unsupported segment creation!");
      exit(ERR_42);
      break;
  }

  return p_new;
}}}

/*
 * Auxiliary function for allocation of new memory segment for stack and
 * initializing it.
 */
static inline TS_stack_seg *new_stack_seg(void)
{{{
  /* Segment allocation. */
  TS_stack_seg *p_new = (TS_stack_seg *) malloc(sizeof(TS_stack_seg) +
                                                STACK_SEG_SIZE);
 
#ifndef NDEBUG
  /* For debugging purposes only. */
  alloc_count++;
#endif

  /* Successful allocation? */
  alloc_test(p_new);
  
  /* Initialization. p_prev must be initialized outside this function. */
  p_new->p_eos = p_new->data + STACK_SEG_SIZE;
  p_new->p_next = NULL;

  return p_new;
}}}


/* ************************************************************************** *
 ~ ~~~[ AUXILIARY FUNCTIONS FOR INTERPRET STACKS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Creates new 'stack' of STACK segment. EBP is stored as first, ESP is stored
 * right behind EBP.
 */
static inline void substack_new(TS_stack *p_stack)
{{{
  /* Not initialized stack? */
  assert(p_stack != NULL);
  assert(p_stack->p_1st != NULL && p_stack->p_act != NULL);
  assert(p_stack->EBP != NULL && p_stack->ESP != NULL);

  /* Enough space to store 2 char pointers? */
  if ((p_stack->ESP + 2 * sizeof(char *)) > p_stack->p_act->p_eos) {

    /*
     * Just an auxiliary pointer to avoid GCC warnings about 'dereferencing
     * type-punned pointer will break strict-aliasing rules'.
     */
    char *p_data;
    
    /* Is there no segment left? */
    if (p_stack->p_act->p_next == NULL) {

      /* Acquiring new stack segment. */
      TS_stack_seg *p_seg = new_stack_seg();
      
      /* Listing new segment to double-linked list. */
      p_stack->p_act->p_next = p_seg;
      p_seg->p_prev = p_stack->p_act;
      
      /* Setting new activity. */
      p_stack->p_act = p_seg;

      /* Using auxiliary pointer to avoid warning. */
      p_data = p_seg->data;
    }
    else {
      /* Setting p_act pointer to following segment. */
      p_stack->p_act = p_stack->p_act->p_next;
      
      /* Using auxiliary pointer to avoid warning. */
      p_data = p_stack->p_act->data;
    }

    /* Storing EBP & ESP. */
    *(char **) p_data = p_stack->EBP;
    *((char **) p_data + 1) = p_stack->ESP;
    
    /* Setting new ESP. */
    p_stack->ESP = p_data + 2 * sizeof(char *);
  }
  else {
    /* Storing EBP & ESP. */
    *(char **) p_stack->ESP = p_stack->EBP;
    *((char **) p_stack->ESP + 1) = p_stack->ESP;
    
    /* Setting new ESP. */
    p_stack->ESP = p_stack->ESP + 2 * sizeof(char *);
  }
  
  /* Setting new EBP. */
  p_stack->EBP = p_stack->ESP;

  return;
}}}


/*
 * Creates a new 'substack' on given STACK. EBP & ESP is backed up in the STACK
 * data.
 */
static inline void substack_restore(TS_stack *p_stack)
{{{
  /* Is STACK properly initialized? */
  assert(p_stack != NULL);
  assert(p_stack->p_act != NULL);
  assert(p_stack->EBP != NULL);

  /* Restoring from empty stack? */
  assert(p_stack->EBP != p_stack->p_1st->data);

  /* Restoring ESP & EBP of given STACK. */
  p_stack->ESP = *((char **) p_stack->EBP - 1);
  p_stack->EBP = *((char **) p_stack->EBP - 2);

  /* Updating p_act pointer of given STACK. */
  stack_update_act(p_stack);

  return;
}}}


/*
 * Auxiliary function for updating p_act pointer of stack to point to proper
 * memory segment. This function should be used every time the substack has been
 * restored or stashed. It is expected that ESP pointer will never get 'ahead'
 * of p_act pointer (in scope of segments).
 */
static inline void stack_update_act(TS_stack *p_stack)
{{{
  /* Not initialized stack? */
  assert(p_stack != NULL);
  assert(p_stack->p_act != NULL);
  assert(p_stack->ESP != NULL);
  
  /* Updating p_act until we're not pointing to same segment as ESP. */
  while (p_stack->ESP < p_stack->p_act->data &&
         p_stack->ESP > p_stack->p_act->p_eos) {
    p_stack->p_act = p_stack->p_act->p_prev;
  }

  return;
}}}


/* ************************************************************************** *
 ~ ~~~[ MEMORY FREEING FUNCTIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Auxiliary function for freeing all segments of one TS_segment memory class.
 */
static inline void segments_free(TS_segment *p_seg)
{{{
  TS_segment *p_seg_del;          /* Pointer to segment to be deleted. */

  while (p_seg != NULL) {
    p_seg_del = p_seg;
    p_seg = p_seg->p_next;
    free((void *) p_seg_del);
  }

  return;
}}}


/*
 * Auxiliary function for freeing all segment of one TS_stack_seg memory class.
 */
static inline void stack_segs_free(TS_stack_seg *p_seg)
{{{
  TS_stack_seg *p_seg_del;        /*  Pointer to segment to be deleted. */

  while (p_seg != NULL) {
    p_seg_del = p_seg;
    p_seg = p_seg->p_next;
    free((void *) p_seg_del);
  }

  return;
}}}


/**
 * Frees all memory used during compiling/interpretation of program.
 *
 * This function is automatically registered in PARSER stage setup for use when
 * program exits, no matter the reason, by using atexit() function. This is
 * safety mechanism to avoid memory leaks and to avoid excessive code in other
 * modules of compiler/interpret connected to memory management.
 */
void memory_free(void)
{{{
  /* Multi-call of memory_free? */
  if (p_mem == NULL) {
    return;
  }

  
  /* Buffer freeing. */
  if (p_mem->p_buffer != NULL) {
    free(p_mem->p_buffer);
  }

  
  /* Freeing of all segments of every memory class. */
  segments_free(p_mem->p_persist_1st);
  segments_free(p_mem->p_lit_1st);
  segments_free(p_mem->p_IDs_1st);
  segments_free(p_mem->p_parser_pers_1st);
  segments_free(p_mem->p_parser_temp_1st);
  segments_free(p_mem->p_parser_stack_1st);
  segments_free(p_mem->p_num_stack_1st);

#ifdef OPTIMIZER_ON
  segments_free(p_mem->p_optimizer_1st);
#endif

  
  /* Freeing both stacks if they were allocated. */
  if (p_mem->p_lt_stack != NULL) {
    stack_segs_free(p_mem->p_lt_stack->p_1st);
    stack_segs_free(p_mem->p_st_stack->p_1st);

    /* Freeing both structures holding INTERPRET stacks. */
    free((void *) p_mem->p_lt_stack);
  }


  /* Freeing structure holding 'info' about memory management. */
  free((void *) p_mem);
  p_mem = NULL;


#ifndef NDEBUG
  /* Displaying number of system (re)allocations count. */
  fprintf(stderr, "MEMORY MANAGER:\nallocations: %lu\nreallocations: %lu\n",
          alloc_count, realloc_count);
#endif

  return;
}}}


/* ************************************************************************** *
 ~ ~~~[ SELF-TESTING PART ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/*
 * Helpful function for quick swapping of 2 pointers using XOR binary operation
 * and typecasting.
 */
static inline void pointers_swap(char **ptr_A, char **ptr_B)
{{{
  *ptr_A = (char *) ((uintptr_t) *ptr_A ^ (uintptr_t) *ptr_B);
  *ptr_B = (char *) ((uintptr_t) *ptr_A ^ (uintptr_t) *ptr_B);
  *ptr_A = (char *) ((uintptr_t) *ptr_A ^ (uintptr_t) *ptr_B);

  return;
}}}


/* ************************************************************************** *
 ~ ~~~[ SELF-TESTING PART ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

#ifdef MEMORY_DEBUG

#include <stdio.h>

int main(void)
{{{
  atexit(memory_free);

  parser_stage_setup();

  /* Overlapping allocations? */
  unsigned long *ptr, *ptr_backup;

  ptr = segment_malloc(10 * sizeof(unsigned long), PERSISTENT,
                       &p_mem->p_persist_act);

  ptr_backup = ptr;

  for (unsigned long i = 0; i < 10; i++) {
    *ptr++ = i;
  }

  ptr = segment_malloc(10 * sizeof(unsigned long), PERSISTENT,
                       &p_mem->p_persist_act);

  for (unsigned long i = 10; i < 20; i++) {
    *ptr++ = i;
  }

  ptr = segment_malloc(10 * sizeof(unsigned long), PERSISTENT,
                       &p_mem->p_persist_act);

  for (unsigned long i = 20; i < 30; i++) {
    *ptr++ = i;
  }

  ptr = ptr_backup;

  /* Printing content of arrays - numbers 0 to 29 should be printed. */
  for (unsigned long i = 0; i < 30; ptr++, i++) {
    printf("%lu\n", *ptr);
  }

  /* String allocations testing. */
  char *ptr2;

  ptr2 = string_malloc(STR_BUF_SIZE, LITERAL, &(p_mem->p_lit_act),
                       &(p_mem->p_lit_act_prev));
  strcpy(ptr2, "Hello,");

  ptr2 = string_malloc(STR_BUF_SIZE, LITERAL, &(p_mem->p_lit_act),
                       &(p_mem->p_lit_act_prev));
  strcpy(ptr2, "World!");

  ptr2 = string_malloc(STR_BUF_SIZE, LITERAL, &(p_mem->p_lit_act),
                       &(p_mem->p_lit_act_prev));
  strcpy(ptr2, "How");

  ptr2 = string_malloc(STR_BUF_SIZE, LITERAL, &(p_mem->p_lit_act),
                       &(p_mem->p_lit_act_prev));
  strcpy(ptr2, "are");

  ptr2 = string_malloc(STR_BUF_SIZE, LITERAL, &(p_mem->p_lit_act),
                       &(p_mem->p_lit_act_prev));
  strcpy(ptr2, "you?");
  
  /* Reallocation testing -> decrease SEG_SIZE and use DDD for this. */
  char *str = "A: Very very long long string... B: You don't say?!";

  size_t size = STR_BUF_SIZE;

  ptr2 = string_malloc(size, LITERAL, &(p_mem->p_lit_act),
                       &(p_mem->p_lit_act_prev));

  for (unsigned i = 0; i < (strlen(str) + 1); i++) {
    if (i == size) {
      size += size;
      ptr2 = string_realloc(ptr2, size, LITERAL, &p_mem->p_lit_act,
                            &p_mem->p_lit_act_prev, p_mem->p_lit_1st);
    }

    ptr2[i] = str[i];
  }
    
  ptr2 = p_mem->p_lit_1st->data;

  printf("\n");

  ptr2 = p_mem->p_lit_1st->data;


  /* Printing content of literal segment. */
  for (unsigned i = 0; i < 7; i++) {
    printf("%s \t-> ", ptr2);
    
    do {
      printf("%c (%3d)| ", *ptr2, (int) *ptr2);
    } while (*ptr2++ != '\0');

    printf("\n");
  }


  /* Testing PARSER_STACK segment and its functions. Use DDD!!! */
  TPushdownItem *ptr3, item __attribute__((unused));

  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_STACK);
  
  ptr3->next = NULL;
  ptr3->type = NONTERMINAL;

  item = tpdi_pop();


  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_STACK);
  ptr3->next = NULL;
  ptr3->type = TERMINAL;

  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_STACK);
  ptr3->next = NULL;
  ptr3->type = NONTERMINAL;

  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_STACK);
  ptr3->next = NULL;
  ptr3->type = BOTTOM;

  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_STACK);
  ptr3->next = NULL;
  ptr3->type = NONTERMINAL;

  item = tpdi_pop();
  item = tpdi_pop();
  item = tpdi_pop();
  item = tpdi_pop();


  /* Testing PARSER_TEMP and it's stashing. Use DDD!!! */
  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_TEMP);
  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_TEMP);
  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_TEMP);
  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_TEMP);
  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_TEMP);
  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_TEMP);
  ptr3 = ifj_malloc(sizeof(TPushdownItem), PARSER_TEMP);

  stash_temp();

  ptr3 = ifj_malloc(sizeof(long), PARSER_PERS);
  ptr3 = ifj_malloc(sizeof(long), BST_INSERT);
  set_IF_flag();
  ptr3 = ifj_malloc(sizeof(long), BST_INSERT);
  clear_IF_flag();
  ptr3 = ifj_malloc(sizeof(long), BST_INSERT);

  /* Testing INTERPRET functions. */
  interpret_stage_setup();

  ptr3 = ifj_malloc(sizeof(long), ST_STACK);
  ptr3 = ifj_malloc(sizeof(long), LT_STACK);

  func_call();
  func_call();
  func_call();

  func_end();
  func_end();
  func_end();

  func_call();
  func_call();
  func_call();

  expr_fin();
  expr_fin();
  ptr3 = func_ret(ptr3, sizeof(long));

  return EXIT_SUCCESS;
}}}

#endif


/* ************************************************************************** *
 * ***[ END OF MEMORY.C ]**************************************************** *
 * ************************************************************************** */

