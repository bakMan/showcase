/**
 * @file interpret.h
 * @brief IFJ12 interpret header
 * @author Jan Remes
 * @version 0.4
 */

#ifndef INTERPRET_H_INCLUDED
#define INTERPRET_H_INCLUDED

/** @NOTE The maximal length of '%g' converted string is 14
 * optional '-'
 * one number before decimal point
 * decimal point
 * six numbers after (default,read man)
 * 'e'
 * optional '-'
 * at most 3 numbers (maximal is 308, try it)
 */
#define DOUBLEMAXCHARS 14

#include "global.h"
#include "structures.h"
#include "variable.h"

extern variable (* FUNCTABLE[])(tExpr * expression, variable * VARTABLE);

/** WILL NEED FOLLOWING FUNCTIONS
 *  OK createVartable   (interpret/vartable.h)
 *  OK insertParams     (interpret/vartable.h)
 *  OK copyVariable     (interpret/interpret.h)
 * TST evalExpr         (interpret/interpret.h)
 * BAD destroyVartable  (interpret/vartable.h)
 *  OK isTrue           (interpret/interpret.h)
 */


/** Executes the given function with parameters described by second argument
 * @RETURN variable Function always returns a variable
 * @PARAM function The function to be executed
 * @PARAM params The function's parameters
 */
variable executeFunction(tFunc * function, tExpr * params, variable * OLDVARTABLE);

/** Returns the value of an expression
 *
 * Evaluates the expression tree and returns its value.
 * Calls itself recursively for solving the children nodes in the tree
 * @RETURN variable The value of the expression
 * @PARAM expression The expression tree
 * @PARAM VARTABLE Current variable table
 */
variable evalExpr(tExpr * expression, variable * VARTABLE);

#endif // INTERPRET_H_INCLUDED
