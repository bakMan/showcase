/**
 * @file      memory_parser.h
 * @author    David Kaspar (xkaspa34) aka Dee'Kej
 * @version   1.0
 * @brief     Header file for PARSER module of memory management for IFJ12
 *            interpret.
 *
 * @detailed  This header file contains specific interface for PARSER only.
 *            Interface common for every module is defined in memory.h file.
 */


/* ************************************************************************** *
 * ***[ START OF MEMORY_PARSER.H ]******************************************* *
 * ************************************************************************** */

/* Safety mechanism against multi-including of this header file. */
#ifndef MEMORY_PARSER_H
#define MEMORY_PARSER_H


/* ************************************************************************** *
 ~ ~~~[ HEADER FILES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

#include "memory.h"                           /* Common interface. */

#include "parser.h"


/* ************************************************************************** *
 ~ ~~~[ MACRO DEFINITIONS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

/* Aliasing tpdi_pop() function, so it doesn't have to be renamed in PARSER. */
#define pop(stack) tpdi_pop(), *(stack) = (*stack)->next

/* ************************************************************************** *
 ~ ~~~[ MODULE FUNCTIONAL PROTOTYPES ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ~
 * ************************************************************************** */

void set_IF_flag(void);         /* Sets in_function flag. */
void clear_IF_flag(void);       /* Clear in_function flag. */
void stash_temp(void);          /* Stashes content of PARSER_TEMP. */

TPushdownItem tpdi_pop(void);   /* Pops one TPushdownItem from PARSER_STACK. */


/* ************************************************************************** *
 * ***[ END OF MEMORY_PARSER.H ]********************************************* *
 * ************************************************************************** */

#endif

