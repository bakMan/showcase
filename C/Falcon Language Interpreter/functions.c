/**
 * @FILE functions.c
 * @AUTHOR Jan Remes (xremes00@stud.fit.vutbr.cz)
 * @BRIEF Contains IFJ12 internal functions
 * @VERSION 0.2
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "global.h"
#include "interpret.h"
#include "structures.h"
#include "functions.h"
#include "variable.h"
#include "ial.h"

#define DEFAULTSTRINGSIZE 25


/** Global variable FUNCTABLE
 *
 * Array of pointers to built-in functions
 * Used in interpret
 */
variable (* FUNCTABLE [])(tExpr * expression, variable * VARTABLE) =
{
    [FUNC_PRINT]    = &ifj12_print,
    [FUNC_INPUT]    = &ifj12_input,
    [FUNC_NUMERIC]  = &ifj12_numeric,
    [FUNC_TYPEOF]   = &ifj12_typeof,
    [FUNC_LEN]      = &ifj12_len,
    [FUNC_FIND]     = &ifj12_find,
    [FUNC_SORT]     = &ifj12_sort,
    [FUNC_ARRAYINIT]    = &arrayInit,
    [FUNC_SUBVAR]   = &subVar,
    [FUNC_ACCESS]   = &accessElement,
};



/** Prints variable according to specs.
 *
 * Contains experimental support for printing arrays
 * @RESULT nil
 * @PARAM argument Variable to be printed
 */

void printVariable(variable argument)
{
    switch(argument.type)
    {
        case VAR_NIL:
            printf("Nil");
            break;
        case VAR_BOOL:
            argument.value.boolean ? printf("true") : printf("false");
            break;
        case VAR_NUMERIC:
            printf("%g", argument.value.numeric);
            break;

        case VAR_STRING:
            printf("%s", argument.value.string.data);
            break;

#ifdef _INTERPRET_EXPERIMENTAL_
        case VAR_ARRAY:
            putchar('[');
            if(argument.value.array.size != 0)
            {
                /* Haha, this was done even with empty array */
                printVariable(argument.value.array.data[0]);
            }
            for(unsigned i = 1; i < argument.value.array.size ;++i)
            {
                printf(",");
                printVariable(argument.value.array.data[i]);
            }

            putchar(']');
            break;
#endif // _INTERPRET_EXPERIMENTAL_

        default:
            fprintf(stderr, "ifj12_print in functions.c: Unexpected type %d for printing\n", argument.type);
            exit(INTERNAL_ERROR);
    }
    return;
}

/** Print list of expressions
 *
 * Uses internal function printVariable
 * @RESULT variable Nil variable (as described in specs.
 * @PARAM expression Expression list to be printed
 * @PARAM VARTABLE Current variable table
 */
variable ifj12_print(tExpr * expression, variable * VARTABLE)
{
    if(expression == NULL)
    {
        fprintf(stderr, "Built-in function print() requires one parameter\n");
        exit(MISC_RUNTIME_ERROR);
    }


    variable result = {.type = VAR_NIL,};
    variable printed;

    while(expression != NULL)
    {

        printed = evalExpr(expression, VARTABLE);
        printVariable(printed);
        if(printed.no_touch == false)
            destroyVariable(&printed);

        expression = expression->next;
    }
    return result;
}



/** Reads and stores a user input
 *
 * Reads one line from 'stdin' and stores it as a variable of 'string' type
 * Both parameters are present only to comply with the other functions
 * @PARAM[in] expression NULL here, no data required
 * @PARAM[in] VARTABLE Unused in this function
 * @RETURN variable A 'string' variable containing the input
 * @WARNING Throws warnings about unused expressions, nothing to worry about
 */
variable ifj12_input(tExpr * expression, variable * VARTABLE)
{
    variable result = {.type = VAR_STRING, .no_touch = false};

    unsigned linelength = 0;
    result.value.string.length = DEFAULTSTRINGSIZE;
    result.value.string.data = ifj_malloc(result.value.string.length, ST_STACK);

    int znak;

    while(((znak = fgetc(stdin)) != '\n') && znak != EOF)
    {
        result.value.string.data[linelength++] = znak;
        if(linelength >= result.value.string.length - 2)
        {
            result.value.string.length *= 2;
            result.value.string.data = ifj_realloc(result.value.string.data, result.value.string.length, ST_STACK);
        }
    }

    result.value.string.data[linelength] = '\0';

    return result;
}

/**
 * ifj12_numeric - Convertion to numeric variables
 * Converts numeric and string variables only
 * @PARAM[in] expression The expression (variable) to be converted
 * @PARAM[in] VARTABLE The variable table required for evaluating expressions
 * @RETURN numeric A 'numeric' variable
 * @NOTE May throw a "12" error due to incompatible types
 * @QUESTION Does it have to cnvert something? Can the string be empty?
 */
variable ifj12_numeric(tExpr * expression, variable * VARTABLE)
{
    if(expression == NULL)
    {
        fprintf(stderr, "Built-in function numeric() requires one parameter\n");
        exit(MISC_RUNTIME_ERROR);
    }
    variable converted, result;

    result.type = VAR_NUMERIC;

    converted = evalExpr(expression, VARTABLE);

    if(converted.type == VAR_NUMERIC)
    {
        result.value.numeric = converted.value.numeric;
        return result;
    }
    else if(converted.type == VAR_STRING)
    {
        bool valid = false;
        int num_part = -1;
        int index = -1;

        while(++index < (signed)converted.value.string.length && converted.value.string.data[index] != '\0')
        {
            switch(num_part)
            {
                case -1:
                    valid = false;
                    if(isspace(converted.value.string.data[index]))
                        ;
                    else if(isdigit(converted.value.string.data[index]))
                        num_part = 1;
                    else goto done;
                    break;
                case 1:
                    // Cela cast cisla
                    valid = false;
                    if(isdigit(converted.value.string.data[index]))
                        ;
                    else if(converted.value.string.data[index] == '.')
                        num_part = 2;
                    else if(converted.value.string.data[index] == 'e')
                        num_part = 4;
                    else goto done;
                    break;
                case 2:
                    // Zacatek desetinne casti, musi se nacist aspon jedno cislo
                    valid = false;
                    if(isdigit(converted.value.string.data[index]))
                    {
                        valid = true;
                        num_part = 3;
                    }
                    else goto done;
                    break;
                case 3:
                    // desetinna cast
                    valid = true;
                    if(isdigit(converted.value.string.data[index]))
                        ;
                    else if(converted.value.string.data[index] == 'e')
                        num_part = 4;
                    else
                        goto done;
                    break;
                case 4:
                    // zacatek exponentu
                    valid = false;
                    if(converted.value.string.data[index] == '+' || converted.value.string.data[index] == '-')
                        num_part = 5;
                    else if(isdigit(converted.value.string.data[index]))
                    {
                        valid = true;
                        num_part = 6;
                    }
                    else goto done;
                    break;
                case 5:
                    // exponent - bylo znamenko, ted jeste neco nacist
                    valid = false;
                    if(isdigit(converted.value.string.data[index]))
                    {
                        valid = true;
                        num_part = 6;
                    }
                    else goto done;
                    break;
                case 6:
                    valid = true;
                    goto done;
                default:
                    goto done;
            }
        }
done:
        if(valid)
        {
            char * endptr;

            errno = 0;

            result.value.numeric = strtod(converted.value.string.data, &endptr);

            if(endptr == converted.value.string.data)
            {
                /* No conversion performed, see 'man strtod' */
                fprintf(stderr, "Could not convert the string to numeric - invalid string\n");
                exit(NUMERIC_ERROR);   
            }
            else if(errno != 0)
            {
                fprintf(stderr, "Could not convert string to numeric - the number exceeds the type's range\n");
                dbgError("Range error, result is %g", result.value.numeric);
                exit(NUMERIC_ERROR);
            }

            return result;
        }
        else
        {
            fprintf(stderr, "numeric(): This is not a valid IFJ12 numeric literal string\n");
            exit(NUMERIC_ERROR);
        }


    }
    else 
    {
        fprintf(stderr, "Built-in function numeric() supports string and numeric operands only\n");
        dbgError("Unexpected type %d", converted.type);
        exit(NUMERIC_ERROR);
    }
}

/**
 * ifj12_typeof - The built-in function typeOf()
 *
 * Returns the type of a variable. The types are specified in the
 * project specifications
 * @PARAM[in] param The expression, which type is to be returned
 * @PARAM[in] VARTABLE The variable table required to evaluate the expression
 * @RETURN numeric The type of the parameter
 */
variable ifj12_typeof(tExpr * param, variable * VARTABLE)
{
    if(param == NULL)
    {
        fprintf(stderr, "Built-in function typeOf() requires one parameter\n");
        exit(MISC_RUNTIME_ERROR);
    }
    variable identified = evalExpr(param, VARTABLE);

    variable result = {.type = VAR_NUMERIC};

    switch(identified.type)
    {
        case VAR_UNDEFINED:
        case VAR_UNKNOWN:
            dbgError("%s","Da fuq?!! Undefined or unknown variable here?");
            exit(INTERNAL_ERROR);
            break;
        case VAR_NIL:
            result.value.numeric = 0.0;
            break;
        case VAR_BOOL:
            result.value.numeric = 1.0;
            break;
        case VAR_NUMERIC:
            result.value.numeric = 3.0;
            break;
        case VAR_FUNCNAME:
            result.value.numeric = 6.0;
            break;
        case VAR_STRING:
            result.value.numeric = 8.0;
            if(identified.no_touch == false)
                destroyVariable(&identified);
            break;
        case VAR_ARRAY:
            result.value.numeric = 9.0;
            if(identified.no_touch == false)
                destroyVariable(&identified);
            break;
    }

    return result;
}

/**
 * ifj12_len - The built-in function  len()
 *
 * Returns the length of a string variable. Returns 0.0 for other types
 * @PARAM[in] param The string, which length is to be computed
 * @PARAM[in] VARTABLE VAriable table required for evaluating expressions
 * RETURN length The length of the given string
 */
variable ifj12_len(tExpr * param, variable * VARTABLE)
{
    if(param == NULL)
    {
        fprintf(stderr, "Built-in function len() requires one parameter\n");
        exit(MISC_RUNTIME_ERROR);
    }
    variable string = evalExpr(param, VARTABLE);
    variable result = {.type = VAR_NUMERIC};

    if(string.type == VAR_STRING)
        result.value.numeric = (double) strlen(string.value.string.data);
    else if(string.type == VAR_ARRAY)
        result.value.numeric = (double) string.value.array.size;
    else
        result.value.numeric = 0.0;

    if(string.no_touch == false)
        destroyVariable(&string);

    return result;
}

/**
 * ifj12_find - The built-in function find()
 *
 * Searches for the first occurence of the given substring in a string and
 * returns its position (indexed from zero). For the detailed behavior, see
 * project specifications
 * @PARAM[in] params The function's parameters (string, substring)
 * @PARAM[in] VARTABLE The variable table required for evaluating expressions
 * @RETURN The position of searched substring
 * @NOTE May throw the '11' error
 */
variable ifj12_find(tExpr * params, variable * VARTABLE)
{
    if(params == NULL || params->next == NULL)
    {
        fprintf(stderr, "Built-in function find() requires two parameters\n");
        exit(MISC_RUNTIME_ERROR);
    }
    variable string = evalExpr(params, VARTABLE);
    variable substring = evalExpr(params->next, VARTABLE);

    if(string.type != VAR_STRING || substring.type != VAR_STRING)
    {
        /* Throw Error 11, see specifications */
        fprintf(stderr, "The built-in function 'find' can only have string parameters\n");
        dbgError("Unexpected %d, %d as types for 'find'", string.type, substring.type);
        exit(TYPE_INCOMPATIBILITY);
    }

    variable result = {.type = VAR_NUMERIC};

    result.value.numeric = (double)substringPosition(string.value.string.data, substring.value.string.data);

    if(string.no_touch == false)
        destroyVariable(&string);
    if(substring.no_touch == false)
        destroyVariable(&substring);

    return result;
}

/**
 * ifj12_sort - The built-in function sort()
 *
 * Returns sorted string - the sequence of characters at input sorted in
 * non-falling order by their ASCII codes. Uses function 'sort' from 'ial.c'
 * to do the trick. The sorted string is a new string (deep copy).
 * @PARAM[in] param The string to be sorted
 * @PARAM[in] VARTABLE The variable table required to evaluate expressions
 * @RETURN The sorted string
 * @NOTE May throw Error 11
 */
variable ifj12_sort(tExpr * param, variable * VARTABLE)
{
    if(param == NULL)
    {
        fprintf(stderr, "Built-in function sort() requires one parameter\n");
        exit(MISC_RUNTIME_ERROR);
    }
    variable string = copyVariable(evalExpr(param, VARTABLE), ST_STACK);

    if(string.type != VAR_STRING)
    {
        fprintf(stderr, "The built-in function 'sort' can only have string parameter");
        dbgError("Unexpected %d as a parameter, string expected", string.type);
        exit(TYPE_INCOMPATIBILITY);
    }

    /* The function 'sort' from ial.c */
    sort(string.value.string.data, strlen(string.value.string.data));

    return string;
}


/** Create an array
 *
 * @RETURN variable The resulting variable with type VAR_ARRAY
 * @PARAM[in] expression The expression to be converted into an array
 * @PARAM[in] VARTABLE Current variable table
 */
variable arrayInit(tExpr * expression, variable * VARTABLE)
{
    variable result = {.type = VAR_ARRAY, .no_touch = false};

    result.value.array.size = ARRAYSIZE;
    result.value.array.data = ifj_malloc(result.value.array.size * sizeof(variable), ST_STACK);

    unsigned index = 0;

    while(expression != NULL)
    {
        if(index == result.value.array.size)
        {
            result.value.array.size *= 2;
            result.value.array.data = ifj_realloc(result.value.array.data, result.value.array.size * sizeof(variable), ST_STACK);
        }

        result.value.array.data[index] = copyVariable(evalExpr(expression, VARTABLE), ST_STACK);
        result.value.array.data[index].no_touch = true;
        expression = expression->next;
        index += 1;
    }

    /* This is size, not the last index! Value correct! */
    result.value.array.size = index;

    return result;
}

/**
 * subVar - Creates a subvariable from variable
 * Returns a part of a 'string' or 'array' variable. Should not be used as
 * an array indexing function when accessing single index only
 * @WARNING Must be called with at least three parameters (non-NULL)
 */
variable subVar(tExpr * params, variable * VARTABLE)
{
    variable whole = evalExpr(params, VARTABLE);
    variable first, second;
    long left, right;

    if(whole.type == VAR_STRING || whole.type == VAR_ARRAY)
    {
        /* Valid, let's evaluate indices */
        params = params->next;
        if(params != NULL)
        {
            first = evalExpr(params, VARTABLE);
            params = params->next;
        }
        else
        {
            first.type = VAR_NUMERIC;
            first.value.numeric = 0.0;
        }

        if(params != NULL)
            second = evalExpr(params, VARTABLE);
        else
        {
            if(whole.type == VAR_STRING)
            {
                second.type = VAR_NUMERIC;
                second.value.numeric = whole.value.string.length;
            }
            else
            {
                second.type = VAR_NUMERIC;
                second.value.numeric = whole.value.array.size;
            }
        }

        /* Check whether they are numbers */
        if(first.type != VAR_NUMERIC || second.type != VAR_NUMERIC)
        {
            fprintf(stderr, "Incompatible (non-numeric) types for indexing\n");
            dbgError("Unexpected types %d,%d as indices", first.type, second.type);
            exit(TYPE_INCOMPATIBILITY);
        }

        left = (long) first.value.numeric;
        right = (long) second.value.numeric;

        /* Selecting subvariable */
        variable result;

        if(whole.type == VAR_STRING)
        {
            result.type = VAR_STRING;
            result.no_touch = false;

            if(left < 0 || left >= right || right > (signed)whole.value.string.length)
            {
                result.value.string.length = 0;
                result.value.string.data = ifj_malloc(1, ST_STACK);
                result.value.string.data[0] = '\0';
            }
            else
            {
                result.value.string.length = right - left + 1;
                result.value.string.data = ifj_malloc(result.value.string.length, ST_STACK);
                strncpy(result.value.string.data, whole.value.string.data + left, right - left);
                result.value.string.data[result.value.string.length - 1] = '\0';
            }
        }
        else
        {
            result.type = VAR_ARRAY;
            result.no_touch = false;

            if(left < 0 || left >= right || right > (signed)whole.value.array.size)
            {
                result.value.array.size = 0;
                result.value.array.data = NULL;
            }
            else
            {
                result.value.array.size = right - left;
                result.value.array.data = ifj_malloc(result.value.array.size * \
                                                            sizeof(variable), ST_STACK);
                for(unsigned i = 0; i < result.value.array.size; ++i)
                {
                    result.value.array.data[i] = copyVariable(whole.value.array.data[left + i], ST_STACK);
                }
            }

        }
        if(whole.no_touch == false)
            destroyVariable(&whole);
        return result;
    }
    else
    {
        fprintf(stderr, "Incompatible type to be indexed, expected string or array\n");
        dbgError("Unexpected type %d as first operand", whole.type);
        exit(TYPE_INCOMPATIBILITY);
    }
}

/**
 * accessElement - The internal function for acessing an array item
 *
 * This is a INTERNAL, not a BUILT-IN function, meaning that the programmer
 * should not be able to call this function. It is available for the syntax
 * analyzer to synthesise things like 'item = array[3.0]'
 *
 * For accessing elements in multi-dimensional arrays, this function must be
 * called recursively for each level. THIS MAY BE ACTUALLY REWRITTEN to provide
 * non-recursive way to access anything in any level, but I feel better having
 * this simple and clear.
 *
 * @PARAM[in] params The tExpr tree describing the access command
 * @PARAM[in] VARTABLE The variable table for evaluating expressions
 * @RETURN variable The value of the desired element (may be actually another array)
 * @NOTE Doesn't work for strings (since we do not have a 'char' type, why the heck would you want this?)
 * @NOTE Returns whole array if second argument (meant pseudo 'user' argument, not VARTABLE) is missing
 */
variable accessElement(tExpr * params, variable * VARTABLE)
{
    variable array = evalExpr(params, VARTABLE);

    if(array.type != VAR_ARRAY)
    {
        fprintf(stderr, "Incompatible type: You can only access array elements\n");
        exit(TYPE_INCOMPATIBILITY);
    }

    params = params->next;

    if(params == NULL)
    {
        if(array.no_touch)
            return copyVariable(array, ST_STACK);
        else
            return array;
    }
    else
    {
        variable index = evalExpr(params, VARTABLE);

        if(index.type != VAR_NUMERIC)
        {
            fprintf(stderr, "Incompatible types: Non-numeric indexing\n");
            exit(TYPE_INCOMPATIBILITY);
        }

        if(index.value.numeric >= array.value.array.size)
        {
            fprintf(stderr, "Can't access element beyond the array size\n");
            exit(MISC_RUNTIME_ERROR);
        }

        variable result = copyVariable(array.value.array.data[(int)index.value.numeric], ST_STACK);

        if(array.no_touch == false)
        destroyVariable(&array);

        return result;
    }
}

/**
 * isIn - Determines, whether one variable is IN another, as defined by Falcon Survival Guide,
 * and therefore, by project task.
 *
 * Returns false in case searched variable is not a string or an array
 * In the other cases it searches the string or the array for the item and if found, returns true
 * @RETURN bool Whether or not was the item found in searched variable
 * @PARAM[in] item The item being searched
 * @PARAM[in] searched The variable being searched in
 */
bool isIn(variable item, variable searched)
{
    bool result;
    switch(searched.type)
    {
        case VAR_NIL:
        case VAR_BOOL:
        case VAR_NUMERIC:
            result = false;
            break;
        case VAR_STRING:
            if(item.type == VAR_STRING)
                result = substringPosition(searched.value.string.data, item.value.string.data) != -1.0;
            else
               result = false;
            break;
        case VAR_ARRAY:
            result = false;
            for(unsigned long i = 0; i < searched.value.array.size; ++i)
            {
                if(item.type == searched.value.array.data[i].type)
                {
                    if(cmpVariables(searched.value.array.data[i], item) == 0)
                    {
                        result = true;
                        break; // for
                    }
                }
            }
            break; // switch
        default:
            fprintf(stderr, "Unexpected type for IN, NOTIN\n");
            exit(INTERNAL_ERROR);
    }
    if(item.no_touch == false)
        destroyVariable(&item);
    if(searched.no_touch == false)
        destroyVariable(&searched);
    return result;
}
