/**
 * @file parser.h
 * @brief Syntactic parser interface.
 * @author Karel Benes
 * @version 0.42
 */

#ifndef _PARSER_H_
#define _PARSER_H_

#include "bst.h"
#include "global.h"
#include "scanner.h"
#include "structures.h"

/** Pushdown item category.
 */
typedef enum{
    TERMINAL,   //<  token
    NONTERMINAL,//<  already built node
    BOTTOM,     //<  indicates bottom of the stack
}TPushdownItemType;

/** Token class. To be used in final code to make it clearer.
 */
typedef enum{
    ILLEGAL,    // all keywords unaccepted in expressions
    OPERAND,    // identifiers and literals
    ARIT_BINOP, // +,-,*,/,**
    REL_BINOP,  // ==, !=, <=, >=, <, >, in, notin
    LOG_BINOP,  // and, or, not,
    PAR,        // (,),[,]
    MISC        // comma, colon, EOL and @
}TExprTermClass;

/** This is what precedence parser pushes on its stack.
 */
typedef struct t_pushdown_item{
    struct t_pushdown_item *next; /**< Stack creating pointer */
    TPushdownItemType type; /**< Simply the terminal-nonterminal determination.*/
    union{
        TToken term; /**<Terminal item. */
        tExpr *node; /**<Nonterminal, (sub)tree */
    }content; /**< The pushdown item itself */
}TPushdownItem;

int parseSource(void);

/** Loop list implementation.
 */
typedef struct t_loop TLoop;
struct t_loop{
    TLoop *next; /**< list creating pointer */
    tCommand *first_line; /**< in 'while' loop points to the ifels command,
    in 'loop' loops points to the first command after the 'loop EOL' line */
    tCommand *last_line; /**< NULL for 'while' loops, for 'loop' loops points
    to the ifels command at the end */
};

/* Synthetised Loop List head, defined in parser module */
extern TLoop *sll_head;

/* Function table root, defined in parser module */
extern tBSTNode *functable;

#endif
