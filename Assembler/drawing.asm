; Tic tac toe
; 
; autor: Stanislav Smatana
; mail:  xsmata01@stud.fit.vutbr.cz
; 
; drawing.asm: Funkcie spojene s vykreslovanim hernej plochy.

bits 32
%include 'include\win32.inc'
%include 'include\general.inc'
%include 'include\defs.asm'

dllimport CreateCompatibleDC, gdi32.dll 
dllimport SelectObject, gdi32.dll 
dllimport BitBlt, gdi32.dll
dllimport DeleteDC, gdi32.dll
dllimport BeginPaint, user32.dll
dllimport EndPaint, user32.dll
dllimport GetStockObject, gdi32.dll
dllimport TextOut, gdi32.dll, TextOutA 
dllimport SetWindowPos, user32.dll


extern game_matrix, hWnd,hDC,hCompatibleDC, hCell_bmp, hCello_bmp, hCellx_bmp, matrixGet, matrixSet,num_x_cells,num_y_cells
global drawMatrix, windowResize

[section .data use32 class=data]
ps:
  istruc PAINTSTRUCT            
  iend


[section .code use32 class=code]

function drawMatrix
begin
   
   invoke BeginPaint, [hWnd], ps      
   mov [hDC], eax

   ; Zobrazeni pozadi.      
   invoke CreateCompatibleDC, [hDC]                           ; Vytvorime memory DC kompatibilni se specifickym device.
   mov [hCompatibleDC], eax


   ;cyklus vykreslovania hracej plochy
   mov ecx,0
   for0:
      cmp ecx,NUM_X
      je end_for0

      mov ebx,0
      for1:
         cmp ebx,NUM_Y
         je end_for1
        
         ;pozor matica je v [riadky,stlpce]
         call matrixGet, game_matrix, ecx, ebx
         push ecx ;SelectObject kazi ecx
         cmp eax, CELL_BLANK 
         jne x_cell
            invoke SelectObject, [hCompatibleDC], [hCell_bmp]  ; Vybereme bitmapu pozadia           
            jmp endif
         x_cell:
            cmp eax, CELL_X
            jne o_cell
            invoke SelectObject, [hCompatibleDC], [hCellx_bmp]  ; Vybereme bitmapu pozadia           
            jmp endif
         o_cell:
            invoke SelectObject, [hCompatibleDC], [hCello_bmp]  ; Vybereme bitmapu pozadia           
         endif:
         pop ecx
         
         mov eax,CELL_SIZE
         mul ebx
         mov esi,eax
         mov eax,CELL_SIZE
         mul ecx

         ; BitBlt kazi ecx :(
         push ecx
         invoke BitBlt, [hDC], esi, eax, CELL_SIZE, CELL_SIZE, [hCompatibleDC], 0, 0, 0xCC0020               ; Nakopirujeme bity z pametoveho DC do aktualniho DC
         pop ecx
         
         inc ebx
         jmp for1

      end_for1:
      inc ecx
      jmp for0
   end_for0:

     

   invoke DeleteDC, [hCompatibleDC]
   invoke EndPaint, [hWnd], ps

   return
end

;procedura vypocita a nastavi velkost okna na zaklade poctu x a y policok
function windowResize
begin
   push eax
   push ebx
   push ecx

   mov eax, NUM_X
   mov ebx, CELL_SIZE
   mul ebx
   mov ecx,eax ;x-size
   mov eax,NUM_Y
   mul ebx ; y - size, treba korigovat (zrejme kvoli menu)
   add eax, Y_OFFSET

   ;move window odosle spravu WM_PAINT
   invoke SetWindowPos, [hWnd],NULL, 0,0, ecx,eax, SWP_NOMOVE

   pop ecx
   pop ebx
   pop eax
   return
end
