; Tic tac toe
; 
; autor: Stanislav Smatana
; mail:  xsmata01@stud.fit.vutbr.cz
; 
; game.asm: Funkcie spojene s hernou logikou.

bits 32
%include 'include\win32.inc'
%include 'include\general.inc'
%include 'include\defs.asm'


global matrixGet, matrixSet, isWinner
extern num_x_cells,num_y_cells, win_cnt

[section .data use32 class=data]

[section .code use32 class=code]

; Funkcia vrati hodnotu policka ulozeneho v bitovom poli.
;
; @param p_matrix Adresa matice herneho pola.
; @param x        X suradnica policka.
; @param y        Y suradnica policka.
; @return         EAX - hodnota policka na danych suradniciach
function matrixGet, p_matrix, x,y
begin
   push ebx
   push ecx
   push edx
   push esi

   mov esi,[p_matrix]

   ;offset je (NUM_Y*x + y) / 4 plus v ramci bytu posun o 8 - 2*zvysok do prava
   ;a and s 00000011
   mov eax, NUM_Y
   mov ebx,[x]
   mul ebx
   add eax,[y]
   mov ebx,4
   div ebx

   ; offset pripocitame k adrese matice
   add esi, eax

   ;nacitame zodpovedajuci bajt
   mov al,[esi]

   ;posun o zvysok doprava
   mov ecx,6
   ;2*edx
   shl edx,1
   sub ecx,edx

   shr eax,cl
   and eax,0x00000003 

   pop esi
   pop edx
   pop ecx
   pop ebx
   return
end 


; Funkcia nastavi hodnotu policka v bitovom poli.
;
; @param p_matrix Adresa matice herneho pola.
; @param x        X suradnica policka.
; @param y        Y suradnica policka.
; @param val      Hodnota na ktoru sa ma policko nastavit (0 - 3). 
function matrixSet, p_mat, posx,posy, val
begin
   pusha

   mov esi,[p_mat]

   ;offset je (NUM_Y*x + y) / 4 plus v ramci bytu posun o zvysok do prava
   ;a and s 00000011
   mov eax, NUM_Y
   mov ebx,[posx]
   mul ebx
   add eax,[posy]
   mov ebx,4
   div ebx

   ; offset pripocitame k adrese matice
   add esi, eax

   xor eax,eax

   ;nacitame zodpovedajuci bajt
   mov al,[esi]

   ;val aj mask treba posunut o zvysok dolava
   mov ecx,6
   ;2*edx
   shl edx,1
   sub ecx,edx
   
   mov edx,[val]
   mov ebx,0x00000003
   and edx,ebx
   not ebx ; prevratime ebx na vyrezavanie do eax

   shl edx,cl
   rol ebx,cl

   ;do eaxu vyrveme dieru
   and eax,ebx

   ; na miesto pridame edx
   or eax,edx
   
   ;zapiseme spat
   mov [esi],al

   popa
   return
end 


; Funkcia na spocitanie za sebou iducich policok daneho typu v linii (ta je obecne dana prirastkami)
;
; ptr_matrix Ukazatel na datovu strukturu hernej matice
; row_step   Prirastok suradnice riadku pri posune o jedno policko v danom smere 
; col_step   Prirastok suradnice stlpca pri posune o jedno policko v danom smere 
; start_row  Suradnica riadku zaciatocneho policka hladania
; start_col  Suradnica stlpca zaciatocneho policka hladania
; cell_type  Typ bunky na spocitanie (X O alebo prazdna)
; cnt_blanks Logicky prepinac ktory urcuje ci sa maju do suctu zahrnut aj prazdne
;            bunky. (vid. funkcia isTie)
;
; @note Prirastky je mozne zadat pomocou makra smeru - vid defs.asm
function countCells, ptr_matrix, row_step, col_step, start_row, start_col, cell_type, cnt_blanks
var sum
begin
   push ebx
   push ecx
   push edx
   push esi
   push edi

   ;trik - vid dokumentacia
   mov esi,[cnt_blanks]
   mov edi,esi
   shl esi,1
   or esi,edi

   ;maska pre predpripravu 
   and esi,[cell_type]

   mov dword [sum],0
   
   mov edx,0 ; indikator ci cyklus prebehol uz dva krat

   ; "for" ktorym raz idem po smere a raz v protismere 
   ; row_step/col_step (inak by som vysetroval len polpriamku)
   .for:
      inc edx

      mov ebx,[start_row]
      mov ecx,[start_col]
      .while:
      ;najprv prehladavanie po smere 
      ; kontrola medzi
      cmp ebx, NUM_Y
      jnb .end
      cmp ebx, 0
      jb  .end
      cmp ecx, NUM_X
      jnb .end
      cmp ecx, 0
      jb  .end

      ;je toto policko spravneho typu ?
      call matrixGet, [ptr_matrix], ebx, ecx
      ;pouzitie masky INST_,[ esi
      or eax,esi
      cmp eax,[cell_type]
      jne .end
      ; ak su podmienky splnene zvacsime pocet
      inc dword [sum]
      add ebx,[row_step] ; posunutie o step po kazdej osy
      add ecx,[col_step]
      jmp .while

      .end:


   ; zmenenie smeru na opacny - staci prosta negacia
   neg dword [row_step]
   neg dword [col_step]

   cmp edx,2
   jne .for
   
   ;startovacia pozicia je zapocitana v oboch smeroch
   ; = dvakrat -> nutne znizit pocitadlo o 1
   mov eax,[sum]
   dec eax

   pop edi
   pop esi
   pop edx
   pop ecx
   pop ebx
   return
end

; Funkcia spocita dlzky linii daneho typu vo vsetkych smeroch a vrati dlzku najdlhsej
;  
; start_row  Suradnica riadku zaciatocneho policka hladania
; start_col  Suradnica stlpca zaciatocneho policka hladania
; cell_type  Typ bunky na spocitanie (X O alebo prazdna)
; cnt_blanks Logicky prepinac ktory urcuje ci sa maju do suctu zahrnut aj prazdne
;            bunky. (vid. funkcia isTie)
function getMaxLineSize, p_matr, s_row, s_col, cell_t, cnt_blank
begin
   push ebx
   
   call countCells, [p_matr], DIR_E, [s_row], [s_col], [cell_t] , [cnt_blank]
   ;ebx - maximum, prve maximum je prvy pocet
   mov ebx,eax

   call countCells, [p_matr], DIR_NE, [s_row], [s_col], [cell_t] , [cnt_blank]
   cmp ebx,eax
   ja .cmp2
   mov ebx,eax

   .cmp2:
   call countCells, [p_matr], DIR_N, [s_row], [s_col], [cell_t] , [cnt_blank]
   cmp ebx,eax
   ja .cmp3
   mov ebx,eax

   .cmp3:
   call countCells, [p_matr], DIR_NW, [s_row], [s_col], [cell_t] , [cnt_blank]
   cmp ebx,eax
   ja .end
   mov ebx,eax

   .end:
   
   ;return = max value
   mov eax,ebx

   pop ebx
   return
end


; Funkcia zisti ci dany hrac vyhral hru.
;
; @param ptr_mat     Ukazatel na strukturu herneho pola.
; @param turn_row    Riadkova suradnica policka kde hrac urobil tah.
; @param turn_col    Stlpcova suradnica policka kde hrac urobil tah.
; @param cell        Typ policka (hrac) pre ktore hladame vyhru - CELL_X alebo CELL_O.
; @param winning_num Pocet symbolov umiestnenych na linii potrebny pre vyhru.
; @return            EAX - 1 ak dany hrac vyhral, inak 0.
function isWinner, ptr_mat, turn_row, turn_col, cell, winning_num
begin


   ;ak je maximalny pocet symbolov daneho hraca v lubovolnom smere na linii
   ;vacsi alebo rovny vyhernemu poctu - vyhral
   call getMaxLineSize, [ptr_mat], [turn_row], [turn_col], [cell] , 0
   cmp eax, [winning_num]
   jnb .winner

   ;loser
   mov eax,0
   return

   .winner:
   mov eax,1
   return
end


