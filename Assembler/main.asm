; Tic tac toe
; 
; autor: Stanislav Smatana
; mail:  xsmata01@stud.fit.vutbr.cz
; 
; main.asm: Subor obsahujuci hlavne telo programu.

bits 32
%include 'include\win32.inc'
%include 'include\general.inc'
%include 'include\defs.asm'


; Funkce z knihovny gdi32.dll 
dllimport ChoosePixelFormat, gdi32.dll
dllimport SetPixelFormat, gdi32.dll
dllimport GetStockObject, gdi32.dll

; Funkce z knihovny glu32.dll 
dllimport gluPerspective, glu32.dll

; Funkce z knihovny kernel32.dll
dllimport GetModuleHandle, kernel32.dll, GetModuleHandleA
dllimport GetCommandLine, kernel32.dll, GetCommandLineA
dllimport ExitProcess, kernel32.dll

; Funkce z knihovny user32.dll 
dllimport RegisterClassEx, user32.dll, RegisterClassExA
dllimport CreateWindowEx, user32.dll, CreateWindowExA
dllimport CheckMenuItem, user32.dll, CheckMenuItem
dllimport ShowWindow, user32.dll
dllimport UpdateWindow, user32.dll
dllimport TranslateMessage, user32.dll
dllimport DispatchMessage, user32.dll, DispatchMessageA
dllimport PostQuitMessage, user32.dll
dllimport GetMessage, user32.dll, GetMessageA
dllimport DefWindowProc, user32.dll, DefWindowProcA
dllimport GetMessage, user32.dll, GetMessageA
dllimport PeekMessage, user32.dll, PeekMessageA
dllimport LoadMenu, user32.dll, LoadMenuA
dllimport LoadImage, user32.dll, LoadImageA
dllimport InvalidateRect, user32.dll
win32fn ReleaseDC, user32.dll 
win32fn GetDC, user32.dll
dllimport MessageBox, user32.dll, MessageBoxA
dllimport LoadCursor, user32.dll, LoadCursorA




; Zakladne definicie
%define xsize 325
%define ysize 370


global game_matrix, hWnd,hDC,hCompatibleDC, hCell_bmp, hCello_bmp, hCellx_bmp, turn, win_cnt, num_x_cells, num_y_cells
extern drawMatrix, matrixSet, matrixGet, isWinner, windowResize


[section .data use32 class=data]

   string szWndClassName,"tictac"

   ; Menu
   menuName db "menu", 0

   WndClass:
   istruc WNDCLASSEX
       at WNDCLASSEX.cbSize,          dd  WNDCLASSEX_size
       at WNDCLASSEX.style,           dd  CS_VREDRAW + CS_HREDRAW  
       at WNDCLASSEX.lpfnWndProc,     dd  WndProc
       at WNDCLASSEX.cbClsExtra,      dd  0
       at WNDCLASSEX.cbWndExtra,      dd  0
       at WNDCLASSEX.hInstance,       dd  NULL
       at WNDCLASSEX.hIcon,           dd  NULL
       at WNDCLASSEX.hCursor,         dd  NULL
       at WNDCLASSEX.hbrBackground,   dd  NULL
       at WNDCLASSEX.lpszMenuName,    dd  0
       at WNDCLASSEX.lpszClassName,   dd  szWndClassName
       at WNDCLASSEX.hIconSm,         dd  NULL
   iend

   hInstance dd 0
   hDC dd 0 ; handle kontextu za��zen�
   hMenu dd 0 ; handle menu
   hCompatibleDC            dd  0 
   hWnd  dd 0 ; handle okna

   ; handle bitmap
   hCell_bmp dd 0
   hCellx_bmp dd 0
   hCello_bmp dd 0

   string szWndCaption,"Tic Tac Toe by Stanislav Smatana"
   dwWndWidth dw xsize ; ���ka okna
   dwWndHeight dw ysize ; v��ka okna
   Message: resb MSG_size

   ; Retezce - chybove hlasky
   string sErrorCaption, "An error occured"
   string sErrorLoadImage, "Can't load image."

   string sWinCaption, "WINNER !!!"
   string sWinnerX, "Player X is winner !"
   string sWinnerO, "Player O is winner !"
   string sTieCaption, "Tie !"
   string sTie, "I'm sorry, nobody wins jedno :("


   ; herna plocha je reprezentovana 128 bitmi
   ; na jedno policko zodpovedaju 2 bity, t.j. 64 policok => 8x8
   ; pozn double = 64bit
   game_matrix dq 0.0, 0.0

   ;indikacia ktory hrac je na tahu
   turn dd CELL_X

   ;win count 
   win_cnt dd 4

   ;rozmery hracej plochy
   num_x_cells dd 8
   num_y_cells dd 8

   ; Nazvy suborov obrazkov
   cell_bmp db "img\cell.bmp", 0
   cellx_bmp db "img\cellx.bmp", 0
   cello_bmp db "img\cello.bmp", 0

[section .code use32 class=code]

   ;****************************** FUNCTIONS *****************************
   function WndProc,hHWnd,wMsg,wParam,lParam
   begin
     mov eax,dword [wMsg];s hodnotou wMsg budeme �asto pracovat, pro
                       ;urychlen� si ji ulo��me do registru EAX
     cmp eax,WM_DESTROY ;je-li zpr�va WM_DESTROY, sko��me na .Destroy
     je near .Destroy
   cmp eax,WM_CLOSE ;tot� ud�l�me v p��pad� WM_CLOSE, tedy
     je near .Destroy ;zav�en� okna
   cmp eax, WM_PAINT
      je near .Paint
   cmp eax, WM_LBUTTONDOWN 
      je near .Mouse
   cmp eax, WM_COMMAND
      je near .menu ; prikazy menu
   cmp eax, WM_CHAR
      je near .char

   ;nena�la se zpr�va, kterou bychom um�li/cht�li obslou�it, po�leme 
   ;ji tedy funkci DefWindowProc:
     invoke DefWindowProc,[hHWnd],[wMsg],[wParam],[lParam]
   ;a jej� v�sledek vr�t�me jako v�sledek na�� funkce
     return eax

   .menu:
      mov eax, [wParam] 
      cmp eax, IDM_NEW
      je near .restart_game
      cmp eax, IDM_EXIT
      je near .Destroy

      cmp eax, IDM_SIZE_3
      jne .SIZE4
      mov dword [win_cnt], 3
      call ClearSizeMenu
      invoke CheckMenuItem, [hMenu], IDM_SIZE_3, MF_CHECKED
      jmp .restart_game

      .SIZE4:
      cmp eax, IDM_SIZE_4
      jne .SIZE5
      mov dword [win_cnt], 4
      call ClearSizeMenu
      invoke CheckMenuItem, [hMenu], IDM_SIZE_4, MF_CHECKED
      jmp .restart_game

      .SIZE5:
      cmp eax, IDM_SIZE_5
      jne .AREA5x5
      mov dword [win_cnt], 5
      call ClearSizeMenu
      invoke CheckMenuItem, [hMenu], IDM_SIZE_5, MF_CHECKED
      jmp .restart_game

      .AREA5x5:
      cmp eax, IDM_AREA_5x5
      jne .AREA_6x6
      mov dword [num_x_cells],5
      mov dword [num_y_cells],5
      call windowResize
      call ClearAreaMenu
      invoke CheckMenuItem, [hMenu], IDM_AREA_5x5, MF_CHECKED
      jmp .restart_game

      .AREA_6x6:
      cmp eax, IDM_AREA_6x6
      jne .AREA_7x7
      mov dword [num_x_cells],6
      mov dword [num_y_cells],6
      call windowResize
      call ClearAreaMenu
      invoke CheckMenuItem, [hMenu], IDM_AREA_6x6, MF_CHECKED
      jmp .restart_game

      .AREA_7x7:
      cmp eax, IDM_AREA_7x7
      jne .AREA_8x8
      mov dword [num_x_cells],7
      mov dword [num_y_cells],7
      call windowResize
      call ClearAreaMenu
      invoke CheckMenuItem, [hMenu], IDM_AREA_7x7, MF_CHECKED
      jmp .restart_game

      .AREA_8x8:
      mov dword [num_x_cells],8
      mov dword [num_y_cells],8
      call windowResize
      call ClearAreaMenu
      invoke CheckMenuItem, [hMenu], IDM_AREA_8x8, MF_CHECKED
      jmp .restart_game
      

   .Destroy: ;sem se dostaneme, chceme-li zav��t
     invoke PostQuitMessage,0 ;okno nebo je-li okno jinak zni�eno
   ;po�leme p��kaz k ukon�en� aplikace

   .Mouse: ;uzivatel stlacil lave tlacidlo mysi
      xor ebx,ebx
      mov eax,[lParam]
      mov bx,ax ; low - word lparam je x suradnica 
      shr eax,16 ;y -suradnica

      xor edx,edx ; nutne nulovat !! div berie edx:eax alebo dx:ax
      mov cx, CELL_SIZE ;predelime velkostou bunky -> ziskame suradnicu bunky kde doslo k msg
      div cx

      xor edx,edx ; nutne nulovat !! div berie edx:eax alebo dx:ax
      mov esi,eax ;eax potrebujem  na dalsie delenie

      mov eax,ebx
      div cx
      
      mov edi,eax

      ;kontrola ci nie je policko obsadene
      call matrixGet, game_matrix, esi,edi
      cmp eax,CELL_BLANK

      jne .cell_full ;ak to nie je prazdne policko koniec
         
      ;nastavenie policka v matici 
      call matrixSet, game_matrix, esi,edi, [turn]


      ;oznamenie win ze je nutne prekreslit okno
      invoke InvalidateRect, [hWnd], NULL, TRUE

      ;kontrola ci dany hrac tymto tahom nezvitazil
      call isWinner, game_matrix, esi,edi,[turn], WIN_COUNT  
      test eax,eax
      jnz .winner


      ;zmenenie tahu
      mov eax,[turn]
      cmp eax, CELL_X 
      jne .else
         mov dword [turn],CELL_O
      jmp .endif
      .else:
         mov dword [turn],CELL_X
      .endif:


      .cell_full: ;policko bolo obsadene
      

      jmp .Finish

      .winner:
         cmp dword [turn],CELL_X 
         jne .cell_o_win 
         invoke MessageBox, NULL, sWinnerX, sWinCaption, MB_OK
         jmp .restart_game
         
      .cell_o_win:
         invoke MessageBox, NULL, sWinnerO, sWinCaption, MB_OK
         jmp .restart_game

      .tie:
         invoke MessageBox, NULL, sTie, sTieCaption, MB_OK
         jmp .restart_game

   ;znak z klavesnice - sledujem ci to nie je escape
   .char:
   mov eax,[wParam]
   cmp eax, VK_ESCAPE
   je .Destroy ;ak ano -> koniec
   jmp .Finish 


   .Paint:
      call drawMatrix ; zavolame vykreslenie hernej plochy
   .Finish:
     return 0 ;vr�t�me hodnotu 0



   .restart_game:

      ;restart stavu hry - herne pole ma 128 bitov
      mov dword [game_matrix], 0
      mov dword [game_matrix + 4], 0
      mov dword [game_matrix + 8], 0
      mov dword [game_matrix + 12], 0

      ;tah nastavime opat na hraca X
      mov dword [turn], CELL_X

      ;oznamenie win ze je nutne prekreslit okno
      ;aby nezostalo na obrazovke stare pole
      invoke InvalidateRect, [hWnd], NULL, TRUE

   jmp .Finish
      
   end ;WndProc


;*********************************STARTPOINT**********************************
..start:
   

   invoke GetModuleHandle,NULL ; zjist�me handle modulu (programu)
   mov [hInstance],eax         ; a hodnotu ulo��me do [hInstance]
   mov [WndClass + WNDCLASSEX.hInstance],eax ; a jednu kopii ulo��mei do struktury t��dy na�eho okna

   invoke GetStockObject, WHITE_BRUSH          ; Ziskame handle na white_brush    
   mov [WndClass + WNDCLASSEX.hbrBackground], eax

   invoke LoadCursor, NULL, IDC_ARROW
   mov [WndClass + WNDCLASSEX.hCursor],eax

   invoke RegisterClassEx,WndClass ; zaregistrujeme na�i t��du
   test eax,eax                    ; je-li EAX=0, pak nastala chyba
   jz Error                 ; = ukon��me program


   ; nacitanie menu
   invoke LoadMenu, NULL, menuName
   mov [hMenu],eax

   ;zaskrtnutie defaultnej plochy 8x8 a vyherneho poctu 4 v menu
   invoke CheckMenuItem, eax, IDM_AREA_8x8, MF_CHECKED
   invoke CheckMenuItem, [hMenu], IDM_SIZE_4, MF_CHECKED

   ;nahravanie obrazkov
   ; prazdna bunka
   invoke LoadImage, NULL, cell_bmp, IMAGE_BITMAP, 0, 0, 0x00000010
   test eax,eax
   jz .ImageLoadFailed 
   mov [hCell_bmp],eax

   ; bunka s krizikom
   invoke LoadImage, NULL, cellx_bmp, IMAGE_BITMAP, 0, 0, 0x00000010
   test eax,eax
   jz near .ImageLoadFailed 
   mov [hCellx_bmp],eax

   ;bunka s koleckom
   invoke LoadImage, NULL, cello_bmp, IMAGE_BITMAP, 0, 0, 0x00000010
   test eax,eax
   jz near .ImageLoadFailed 
   mov [hCello_bmp],eax

   invoke CreateWindowEx, 0, szWndClassName, szWndCaption, WS_CAPTION + WS_SYSMENU + WS_VISIBLE  , CW_USEDEFAULT, CW_USEDEFAULT, xsize, ysize, NULL, [hMenu], [hInstance], NULL
   test eax,eax ; otestujeme �sp�nost vol�n�
   jz Error ; p�i ne�sp�chu (EAX=0) ukon��me program
   mov [hWnd],eax; a nakonec schov�me handle na�eho okn�



   invoke ShowWindow, eax, SW_SHOWDEFAULT	      ; okno zobraz�me
   invoke UpdateWindow, [hWnd]				          ; a nech�me prekreslit.
   
   .MessageLoop:
     invoke GetMessage,Message,NULL,0,0 ; p�e�teme zpr�vu
     test eax,eax ; je-li nula, kon��me
     jz near .Finish2
   cmp eax,-1 ; je-li -1, nastala chyba, ukon��me
     jz near .Finish2 ; tedy program
     invoke TranslateMessage,Message ; p�elo��me virtu�ln� kl�vesy
     invoke DispatchMessage,Message ; po�leme zpr�vu obslu�n� funkci
     jmp .MessageLoop

   .Finish2:
     invoke ExitProcess,[Message + MSG.wParam]

   mov eax,0
   ret

   .ImageLoadFailed:   
     invoke MessageBox, NULL, sErrorLoadImage, sErrorCaption, MB_OK
     jmp .Finish2


Error:
   mov eax,1
   ret


; Funkcia odznaci vsetky polozky v menu velkosti.
function ClearAreaMenu
begin
   invoke CheckMenuItem, [hMenu], IDM_AREA_8x8, MF_UNCHECKED
   invoke CheckMenuItem, [hMenu], IDM_AREA_7x7, MF_UNCHECKED
   invoke CheckMenuItem, [hMenu], IDM_AREA_6x6, MF_UNCHECKED
   invoke CheckMenuItem, [hMenu], IDM_AREA_5x5, MF_UNCHECKED
   return
end

; Funkcia odznaci vsetky polozky v menu vyherneho poctu.
function ClearSizeMenu
begin
   invoke CheckMenuItem, [hMenu], IDM_SIZE_3, MF_UNCHECKED
   invoke CheckMenuItem, [hMenu], IDM_SIZE_4, MF_UNCHECKED
   invoke CheckMenuItem, [hMenu], IDM_SIZE_5, MF_UNCHECKED
   return
end
