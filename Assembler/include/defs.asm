; Tic tac toe
; 
; autor: Stanislav Smatana
; mail:  xsmata01@stud.fit.vutbr.cz
; 
; defs.asm: Globalne definicie.

; Typy policok na hernej ploche
%define CELL_BLANK 0
%define CELL_X 1
%define CELL_O 2

; pocet buniek je ulozeny v globalnych premennych
; definicia je pouzita preto aby bolo jednoduche 
; zmenit nastavitelny pocet na fixny
%define NUM_X [num_x_cells]
%define NUM_Y [num_y_cells]

; Velkost bunky v px
%define CELL_SIZE 40

;vyherny pocet policok
%define WIN_COUNT [win_cnt]


;definicia smerov pre priechody maticou
%define DIR_E 0,1
%define DIR_NE -1,1
%define DIR_N -1,0
%define DIR_NW -1,-1
;ostatne vzniknu len negaciou :)

; Identifikatory poloziek menu
%define IDM_NEW 1
%define IDM_ABOUT 2
%define IDM_EXIT 3
%define IDM_SIZE_3 4
%define IDM_SIZE_4 5
%define IDM_SIZE_5 6
%define IDM_AREA_5x5 7
%define IDM_AREA_6x6 8
%define IDM_AREA_7x7 9
%define IDM_AREA_8x8 10


;posun o velkost menu
%define Y_OFFSET 50
