# C Header Analysis, Stanislav Smatana (xsmata01@stud.fit.vutbr.cz)
# Skript na analyzu hlaviciek jazyka C s vystupom vo formate XML.
#CHA:xsmata01
# 13.3.2013

use strict;
use warnings;
use Getopt::Long;
use File::Find;
use XML::Writer;
no warnings 'closure';

#konstanty navratovych kodov programu
sub E_OK          { return 0};
sub E_ARGS        { return 1};
sub E_INPUT       { return 2};
sub E_OUTPUT      { return 3};
sub E_INPUT_FILE   { return 4};

#chybove hlasky
our @err_msgs = (
   "K tomuto by nemalo dojst",
   "Zadali ste nespravne argumenty.",
   "Vstupny subor/priecinok neexistuje/neda sa otvorit.",
   "Vystupny subor nie je mozne vytvorit",
   "Hlavicku nemozno otvorit",
);

#help string
our $help_str = << 'HELP_TXT';
C Header Analysis, Stanislav Smatana (xsmata01@stud.fit.vutbr.cz)
Usage: cha.pl [--help] [--input=file] [--output=file] [--pretty-xml=k]
              [--no-inline] [--max-par=k] [--no-duplicates]
              [--remove-whitespace] 

Description: Program analyses given C header/headers and produces corresponding xml
             output.

Options: --help Displays this help message.
         --input=file Sets input file to analyse. If directory is given all
                      headers in it and it's subdirectories are analysed. 
                      If this argument is not set, ./ is scanned.
         --output=file Sets file to store output xml. If not given stdout is used.
         --pretty-xml=k If set, output xml will have indentation and newlines. 
                        Optional value k sets number of spaces for indent.
         --no-inline Do not include inline functions in output.
         --max-par=k Include functions with k or less parameters only.    
         --no-duplicates If there are multiple functions with same name,
                         only first occurence is used.
         --remove-whitespace Removes extra whitespace in function declarations.
HELP_TXT

#deklaracie prototypov
sub ProcessArgs();
sub GetFileList($);
sub error($);
sub ReadHeader($);
sub GetFunctions($$$$$);
sub SplitArgs($);
sub createXml($);
sub OutputXml($$$$);
sub OutputFunction($$$$);
sub DelParamVars($);
sub DelParamVars($);
sub DelWrappingWhitespace($);
sub ParamsDelWrapWhitespace($);


###############################################################################
#MAIN
###############################################################################

ProcessArgs();

if($main::HELP)
{
   print $main::help_str;
   exit E_OK;
}

my @files = GetFileList($main::INPUT);
my @funclists;


foreach(@files)
{
   my $contents = ReadHeader($_);
   my %func_list = GetFunctions($contents,$_,$main::INLINE,$main::NO_WHITESPACE,$main::MAX_PAR);

   #pole zoznamov funkcii
   push @funclists,\%func_list;

}

OutputXml(\@funclists, $main::OUTPUT, $main::PRETTY, $main::DUPLICATES);

exit(E_OK);

###############################################################################
# Definicie funkcii
###############################################################################

# Procedura spracuje parametre programu a nastavi globalne premenne.
#
# Nema argumenty, dodane ignoruje.
sub ProcessArgs(){

   our $HELP = "";
   our $INPUT = "./"; #implicitne tento priecinok
   our $OUTPUT = "";
   our $PRETTY = "";
   our $INLINE = 1; #implicitne zahrnam inline funkcie
   our $MAX_PAR = "";
   our $DUPLICATES = 1;
   our $NO_WHITESPACE ="";

   if(!GetOptions("help" => \$HELP, "input=s" => \$INPUT, "output=s" => \$OUTPUT,
              "pretty-xml:4" => \$PRETTY, "max-par=i" => \$MAX_PAR, "inline!" =>\$INLINE,
              "duplicates!" => \$DUPLICATES, "remove-whitespace" =>\$NO_WHITESPACE ))
  {
      error(E_ARGS);
  }

}

# Procedura vytvori zoznam suborov na spracovanie v danej ceste.
# 
# Postupuje rekrzivne aj do podpriecinkov.
# @param $ Priecinok v ktorom sa ma zacat hladanie
# Ak je argument priamo cesta k suboru, funkcia nastavi pole
# s jednym prvkom - menom suboru
sub GetFileList($)
{
   my $dir = $_[0];
   my @files;

   if(! -e $dir)
   {
      error(E_INPUT);
   }

   if( -d $dir )
   {#ide o priecinok
      find(\&wanted, $dir);
   }
   else
   { #ide o obycajny subor
      $files[0] = $dir;
   }

   return @files;

   #pomocna procedura wanted na pridavanie vyhovujucich suborov do  @files
   sub wanted
   {
      #vyberame do zoznamu
      if( -f and /^.*\.h$/){ push @files, $File::Find::name };
   }

}


# Procedura vypise chybovu hlasku a ukonci program s chybovou spravou.
sub error($)
{
   print STDERR "Chyba: $err_msgs[$_[0]]\n";
   exit($_[0]);
}


# Funkcia nacita hlavickovy subor, odstrani komentare a 
# preprocesorove direktivy a vysledok vrati ako retazec
# na jednom riadku.
sub ReadHeader($)
{
   open(HEADER,"<$_[0]") or error(E_INPUT_FILE);
   binmode HEADER,':utf8';

   my @lines = <HEADER>;

   #odstranenie jednoriadkovych komentarov a preprocesovych direktiv
   @lines = grep (!/^\s*(#|\/\/)/, @lines);

   #ak obsahuje header definicie moze sa tam vyskytnut return func(x);
   #to by dalej skript rozoznal ako funkciu s navratovym kodom return 
   #nutne odstranit
   @lines = grep (!/^\s*return/, @lines);


   #spojenie celeho suboru do retazca - umozni to odstranit viacriadkove
   #komentare
   my $whole = join "",@lines;

   #nahrada novych riadkov za medzery
   $whole =~ s/\n/ /g;

   #odstranenie viacriadkovych komentarov
   $whole =~ s/\/\*[^\/]*\*\///g;

   return $whole;

   close(HEADER);
}

# Funkcia zo zadaneho !jednoriadkoveho! retazca !bez komentarov a definov! ziska
# zoznam deklaracii funkcii. Druhy parameter je nazov suboru - pre henerovanie
# xml dokumentu.
sub GetFunctions($$$$$)
{
   my ($functions, $file, $inline, $remove_whitespace, $max_param) = @_;
   my $func_regex=qr/
   ((?:\w+\s+)+ #pred kazdou funkciou musi byt jedno alebo viac slov
   \*?)       #moze a nemusi ist o pointer
   \s*       #za tym moze a nemusi ist volne miesto
   (\w+)       #nazov funkcie
   \s*
   \(([^)]*)\) #zoznam parametrov
   \s* #moze nasledovat dalsie volne miesto
   [\{;] #bud ide o definiciu alebo deklaraciu
   /x;

   my %matches;
   my $params;
   my $varargs;
   while($_[0] =~ /$func_regex/g)
   {

      #bez inline -> ak typ obsahuje inline ako samostatne slovo funkciu
      #neberiem
      if(!$inline)
      {
         if( $1 =~ /\binline\b/)
         {
            next;
         }
      }
      
      #povodne premenne su read-only
      my ($type, $param_raw) = ($1,$3);

      #podrobne odstranovanie bielych znakov
      if($remove_whitespace)
      {
         #kazdu sekvenciu bielych znakov nahradim jednou medzerou
         $type =~ s/\s+/ /g; 
         $param_raw =~ s/\s+/ /g; 

         #v pripade pointra medzi nazvom typu a hviezdickou nie je medzera
         $type =~ s/(\b[^\s]+\b)\s+\*/$1\*/g; 
         $param_raw =~ s/(\b[^\s]+\b)\s+\*/$1\*/g; 

         #moze sa stat ze doslo k a*const -> po hviezdicke musi byt medzera
         $type =~ s/\*([^\s])/* $1/g; 
         $param_raw =~ s/\*([^\s])/* $1/g; 
      }

      #rozsekanie parametrov do pola
      ($params,$varargs) = SplitArgs($param_raw);

      #odstranenie nazvu premennych z parametrov
      $params = DelParamVars($params);

      #odstranenie bielych znakov pred a po deklaraciach parametrov a 
      #navratovom type funkcie 
      $type = DelWrappingWhitespace($type);

      #odstranenie bieleho miesta zo zaciatku a konca deklaracie parametrov
      $params = ParamsDelWrapWhitespace($params);

      #ak je nastavene max param a funkcia ma viac parametrov tak sa ignoruje a
      #pokracujem dalsou
      if(!($max_param eq ""))
      {
         if(scalar @$params > $max_param)
         {
            next;
         }
      }

      #ak ide o prvu funkciu vytvorime pole, inak do neho pridame novu
      #polozklu 
      if(!exists $matches{$2})
      {
      	my @arr;
        $matches{$2} = \@arr;	
      }

      my $arr_ref = $matches{$2};
      unshift @$arr_ref, {'file' => $file, 'rettype' => $type, 'params' => $params, 'varargs' =>
			    $varargs? "yes" :"no"};


   }

   
   return %matches;
}

# Pomocna funkcia ktora zo zoznamu parametrov vytvori pole a urci ci ma funkcia
# varargs.
# @param Retazec obsahujuci zoznam parametrov bez zatvorky.
sub SplitArgs($)
{
   my $args = $_[0];
   my $varargs = "";
   my @splitted = split(",",$args);

   # tri bodky znacia var args
   if ( grep ( /\.\.\./, @splitted))
   {
      $varargs = 1;
   }

   #odstranenie ... z parametrov
   @splitted = grep (!/\.\.\./,@splitted);

   return (\@splitted, $varargs);
}

# Funkcia vypise vysledne xml na vystup.
#
# @param Datova struktura zachytavajuca funkcie
# @param Vystupny subor, v pripade false - stdout.
# @param Velkost indentacie, ak je false - cely xml dokument
#        bude neformatovany na jednom riadku.
# @param Duplicates -> ak true xml moze obsahovat viacere funkcie s rovnakym
#        nazvom, inak uvazujem len prvy vyskyt 
sub OutputXml($$$$)
{
   my ($funclists, $outfile, $pretty, $duplicates) = @_;
   my $writer;
   my $dir;
   my ($out,$out_str);
   #ak je $outfile false => stdout

   if($outfile)
   {
      open(DATA,">$outfile") or error(E_OUTPUT);
      $out = *DATA;
   }
   else
   {
      $out = *STDOUT;
   }

   binmode $out,':utf8';

   #pri pouziti xml::writer dochadza k pridaniu extra noveho riadku za
   #deklaraciu <?xml ... > - toto spravanie je implicitne, preto musim cele xml
   #dostat do stringu a rucne novy riadok odstranit
   if(!($pretty eq ""))
   {
      $writer = XML::Writer->new(OUTPUT => \$out_str, ENCODING=>"utf-8", DATA_MODE => 1, DATA_INDENT => $pretty);
   }
   else
   {
      $writer = XML::Writer->new(OUTPUT => \$out_str, ENCODING=>"utf-8");
   }

   #top element
   $writer->xmlDecl("utf-8");
   
   #ak bol vstupny subor subor -> dir je prazdny 
   $dir = -d $main::INPUT ? $main::INPUT : "";
   $writer->startTag("functions", "dir" => $dir);

   # pole hashov poli hashov -> [{x => [{}]}]
   my ($name,$attribs);
   foreach(@$funclists)
   {
      while( ($name,$attribs) = each(%$_))
      {
         #dve moznosti -> ak je attribs pole -> su povolene duplicity a treba ho
         #prejst, ak je hash -> nie su povolene duplicity a staci vypisat 1
         #funkciu 
         if($duplicates)
         {
            foreach(@$attribs)
            {
               OutputFunction($writer,$name,$_,$dir);
            }
         }
         else
         {
               OutputFunction($writer,$name,@$attribs[0],$dir);
         }

      }
   }
   $writer->endTag("functions");
   $writer->end();

   #odstranenie implicitneho extra riadka od xml writera
   if($pretty)
   {
      #odstranime extra prazdny riadok
      $out_str =~ s/\n\n/\n/;
   }
   else
   {
      #aj pri neformatovanom mode xml writer dava za xml hlavicku novy riadok
      $out_str =~ s/\?>\n/\?>/;
   }
   #vystup
   print $out $out_str;
}

# Funkcia zapise zaznam o funkcii z datovej struktury do formatu xml.
#
# @param Objekt xml writer.
# @param Meno funkcie.
# @param Atributy funkcie v hashi (parametre, navratovy typ, ...)
# @param Priecinok od ktoreho sa zacinalo hladanie (kvoli vytvoreniu relativnej
#        cesty)
sub OutputFunction($$$$)
{
   my ($writer,$name,$attribs,$dir) = @_;

   #cesta k suboru ma byt relativne od daneho priecinka
   $attribs->{'file'} =~ s/^$dir//;
   $writer->startTag("function", "file" => $attribs->{'file'}, name =>
                     $name, varargs => $attribs->{'varargs'}, rettype => $attribs->{'rettype'});

   #vypis parametrov
   my $params = $attribs->{'params'};
   my $param_cnt = 1;
   foreach(@$params)
   {
      $writer->emptyTag("param", number => $param_cnt, type => $_);
      $param_cnt++;
   }

   $writer->endTag("function");
}

# Funkcia odstrani nazvy premennych z definicii typov parametrov.
# 
# @param   Referencia na pole deklaracii parametrov.
# @return  Pole deklaracii parametrov bez ich nazvov.
sub DelParamVars($)
{
   my $in_arr = $_[0];
   #mam vsetko ? 
   my %keywords = ( "void" => "", "int" => "" , "float" => "" , "bool" => "" , "char" => "" ,
      "short" => "" , "long" => "" , "double" => "" , "signed" => "" , "unsigned" => "" ); 

   my @out_arr;
   foreach(@$in_arr)
   {
      #pokial je typ argumentu obsahuje jedine slovo -> nesmiem mazat, urcite to
      #nie je nazov argumentu
      my @tmp = split(/\s+/, $_);
      if( (scalar @tmp) == 1)
      {
         next;
      }

      #vezmi posledne slovo
      $_ =~ /(\b[^\s]+\b)\s*$/; 

      #ak sa nejedna o zname klucove slovo beriem to ako nazov premennej
      if(defined $1)
      {
         if(!exists $keywords{$1})
         {
            $_ =~ s/(\b[^\s]+\b)\s*$//; 
	 }
      }

      unshift @out_arr, $_;
   }

   return \@out_arr;
}

#Funkcia vymaze biele znaky na konci a zaciatku retazca.
#
#@param Retazec.
#@return Upraveny retazec.
sub DelWrappingWhitespace($)
{
   my $str = $_[0];
   
   $str =~ s/^\s+//;
   $str =~ s/\s+$//;
   return $str;
}

#Funkcia vymaze biele znaky na konci a zaciatku retazca v celom poli.
#
#@param Referencia na pole.
#@return Upravene pole.
sub ParamsDelWrapWhitespace($)
{
   my $in_arr = $_[0];
   my @new_arr;

   foreach(@$in_arr)
   {
      unshift @new_arr, DelWrappingWhitespace($_);
   }

   return \@new_arr;
}
