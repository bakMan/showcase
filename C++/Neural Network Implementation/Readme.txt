Backpropagation neural network
Stanislav Smatana 2016
=================================
!!! Prebuilt binary provided in the dist directory is for
    64bit linux and will not work on 32 bit machine. 

Building:
make - !!NOTE!! Output will be in dist/ directory

Execution:
./bpnn -h (for details of usage)


Directories:
./ 	   - all the sources

data - example data for net training, including two 
       trained networks (xor_net.txt,adult_net.txt)

helper - helper scripts for performance evaluation

protocols - experiment protocol description for 
            helper/protocol_runner.sh helper

dist - directory for output of built binaries