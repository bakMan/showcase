/* 
 * File:   main.cpp
 * Author: Stanislav Smatana
 *
 * Created on October 19, 2016, 11:04 AM
 */

#include "matrix.hpp"
#include "bpnn.hpp"
#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <functional>
#include <istream>
#include <sstream>
#include <stdint.h>
#include <fstream>
#include <arpa/inet.h>
#include <string>

using namespace std;
const int32_t TRAINING_FLAG = 2051;


string HELPSTRING = 
        "bpnn - Backpropagation neural network implementation\n"
        "Stanislav Smatana 2016, xsmata01@stud.fit.vutbr.cz\n"
        "\n"
        "Usage: bpnn -t/c [options] file1 file2\n"
        "\n"
        "-t Training mode       - first file is matrix of training vectors, second is\n"
        "                         matrix of expected output vectors. Vectors are in rows.\n"
        "                         Program will output trained neural network to stdin.\n"
        "\n"
        "-c Classification mode - first file is matrix of vectors for classification.\n"
        "                         Second file containes trained neural network (output\n"
        "                         of training mode). Program will output matrix of output\n"
        "                         vectors.\n"
        "\n"
        "Options:\n"
        "\n"
        "-l learning rate       - learning rate of training, defaults to 0.5\n"
        "-m max iterations      - maximal number of iterations for learning algo-\n"
        "                         rithm. Defaults to 10 000.\n"
        "-e minimal error       - minimal error, defaults to 0.0001. Upon reaching\n"
        "                         this value, or lower value, the training stops.\n"
        "-s shape               - REQUIRED FOR TRAINING. Comma separated list of \n"
        "                         integer values. Each represents size of one layer.\n"
        "                         First value specifies dimension of the input.\n"
        "\n"
        "Example:"
        "\n"
        "bpnn -t -s 2,2,1 file1 file2 \n"
        "Train network with 2 dimensional input, 2 neurons in the hidden layer and\n"
        "one in the output layer. Training vectors are in file1, labels are in \n"
        "file 2.\n";        


    double epsilon = 0.0001;
    double rate = 0.5;
    int max_iter = 10000;

/** Exception class for commandline argument processing*/
class args_error : public std::runtime_error
{
public:
    args_error(const string& __arg) :
    runtime_error(__arg) {
    }

    
};

/** Function reads matrix from MNIST binary file
 * @param a Binary stream to read from
 * @note This function is not actually used in program.
 */
Matrix readMNIST(std::istream &a)
{
    int32_t flag;
    int32_t nrows;
    int32_t ncols = 1;
    
    a.read((char *) &flag,4);
    a.read((char *) &nrows,4);
    
    flag = ntohl(flag);
    nrows = ntohl(nrows);
    
    //training data have number of rows and columns specified
    //this is not the case in labels - label is only one num
    if(flag == TRAINING_FLAG )
    {
        int32_t x;
        int32_t y;
        a.read((char *) &x,4);
        a.read((char *) &y,4);
        
        x = ntohl(x);
        y = ntohl(y);
        
        ncols = x*y; //whole image is one row
    }
    
    t_shape s = {nrows,ncols};
    Matrix ret(s);
    
    unsigned char val;
    unsigned cnt = 0;
    while(a.read((char *) &val,1))
    {
        ret.set(cnt,(double) val);
        cnt++;
    }
    
    return ret;
}


/** Function converts label with n values into n-nary vector where each item 
 *  has value 0 or 1.
 * 
 * @param labels 1D column vector of labels to be vectorized
 * @return Vectorized label
 */
Matrix vectorizeLabels(Matrix &labels)
{
    int max = (int) labels.max();
    //cout  << max << endl;
    t_shape s = {labels.shape.rows,max + 1};
    
    Matrix res(s);
    
    for(int r=0;r < labels.shape.rows;r++)
    {
        for(int c=0;c<max;c++)
            res.set(r,c,0.0);
        
        int ind = (int) labels.get(r,0);
        res.set(r,ind,1.0);
    }
    
    return res;
    
}

/** Reads matrix from a file. Both idx binary matrices and ascii space separated
 *  are accepted.
 * @param filename Path to matrix file.
 * @return Loaded matrix, throws exception on error.
 */
Matrix readFile(string filename)
{
    Matrix res;
    
    string idx_suf = ".idx";
    bool is_idx = false;
    
    //check if filename ends with ".idx" suffix
    if(filename.length() >= idx_suf.length())
        is_idx = (0 == filename.compare(filename.length() - idx_suf.length(),idx_suf.length(),idx_suf));
    
    //binary idx file
    ifstream file;
    file.open(filename,ios::binary | ios::in);
    if(!file.is_open())
        throw runtime_error("Unable to open file '" + filename + "'");
        
    if(is_idx)
    {
        return readMNIST(file);
    }
    else
    {
        res.read(file);
        return res;
    }
}




/** Structure to hold program commandline arguments */
struct program_args
{
    bool help = false;
    bool training;
    Matrix mat1;
    Matrix mat2;
    vector<unsigned int> structure;
    double epsilon = 0.0001;
    double rate = 0.5;
    int max_iter = 10000;
    bpnn network; //network - only upon classification
};
typedef struct program_args t_program_args;

/** Function processes commandline arguments and outputs t_program_args structure
 * 
 * @param argc 
 * @param argv
 * @return t_program_args
 */
t_program_args processArgs(int argc, char **argv)
{
    bool train = false;
    bool classify = false;
    char c;
    string shape_str;
    program_args args;
    stringstream ss;
    
    while ((c = getopt (argc, argv, "cte:l:s:m:h")) != -1){
    
    ss.clear();
    
    switch (c)
      {
      case 'h':
        args.help=true;
        return args; //no need to continue
      case 't':
        train = true;
        break;
      case 'c':
        classify = true;
        break;
      case 'l':
          ss << optarg;
          ss >> args.rate;
          if(ss.fail())
              throw args_error("Error while reading learning rate.");
          if(args.rate < 0.0 || args.rate > 1.0)
              throw args_error("Learning rate must be a number between 0.0 and 1.0");
                      
        break;
      case 'm':
          ss << optarg;
          ss >> args.max_iter;
          if(ss.fail())
              throw args_error("Error while reading learning rate.");
          break;
      case 'e':
          ss << optarg;
          ss >> args.epsilon;
          if(ss.fail())
              throw args_error("Error while reading epsilon.");
          break;
        case 's':
          shape_str = optarg;
          break;
      case '?':
        throw args_error("Wrong parameter, check the help");
        break;
      default:
        abort ();
    }}

    //program always takes two positional arguments
    if(argc - optind != 2)
        throw args_error("Program requires two positional arguments.");
    
    if (train && classify)
        throw args_error("Only one option - either c or t is allowed.");
        
    if (!train && !classify)   
        throw args_error("Action must be specified. Please use option c or t.");
        
    
    args.training = train;
    
    //training takes two files, classification only one
    Matrix mats[2];
    try
    {
        for(int i=optind; i < optind + train * 2 + classify;i++)
        {
            if(i == argc)
                throw args_error("Missing file name after options.");
            
            if(string(argv[i]) == "")
                throw args_error("Filename cannot be empty.");
            
            cerr << "Loading matrix file '" << argv[i] << "'" << endl;
            
            mats[i - optind] = readFile(argv[i]);
        }
    }
    catch(runtime_error &e)
    {
        throw args_error("Could not open matrix file, please check path.");
    }
    args.mat1 = mats[0];
    args.mat2 = mats[1];
    
    //if classification is to be commenced -> load whole network
    if(classify)
    {
        cerr << "Loading network from file: " << argv[optind+1];
        ifstream file;
        file.open(argv[optind+1],ios::in);
        
        //read network
        args.network.read(file);        
        file.close();
        
        //test if shape of network and input data fits
        auto net_dim = args.network.getDimensionality();
        
        if(net_dim.first != args.mat1.shape.cols)
            throw args_error("Dimensionality of input data and neural network does not match.");
    }
    
    //shape string parsing
    ss.clear();
    ss << shape_str;
    string tok;
    while(getline(ss,tok,','))
    {
        stringstream tmp;
        tmp << tok;
        int val;
        tmp >> val;
        
        if(tmp.fail())
              throw args_error("Error while reading shape string '" + shape_str + "'");
        
        args.structure.push_back(val);
    }
    
    //at least two integers required
    if(args.training && args.structure.size() < 2)
        throw args_error("Network shape must contain at least 2 values - input dimensionality and size of one layer.");
    
    return args;
}

/******************************************************************************
 **************************** MAIN ********************************************
 ******************************************************************************/
int main(int argc, char** argv) {

    //args processing
    t_program_args args;
    try
    {
        args = processArgs(argc,argv);
    }catch(args_error &e)
    {
        cerr << "\nError while processing commandline arguments: " << endl;
        cerr << e.what() << endl;
        return 1;
    }
    
    //program action dispatch
    if(args.help)
    {
        cerr << HELPSTRING << endl;
        return 0;
    }
    else if(args.training)
    {
        
        //training
        try
        {
            bpnn net(args.structure);
            //if needed - vectorize labels
            if(args.mat2.shape.cols == 1)
            {
                cerr << "Auto vectorizing second matrix." << endl;
                if(args.mat2.max() > 1.0)
                    args.mat2 = vectorizeLabels(args.mat2);
            }
        
            net.train(args.mat1,args.mat2,args.rate,args.epsilon,args.max_iter);
            net.printNN();
            
        }catch(runtime_error &e)
        {
            cerr << "\nError during training, check the shape string (-s parameter):" << endl << e.what() << endl;
            return 1;
        }
    }
    else
    {
        //classification
        
        //if classifying first file is network
        //second file is actual data for classification
        for(int r=0;r < args.mat1.shape.rows;r++)
            args.network.classify(args.mat1.getRow(r).transpose()).printMat();
    }
}

