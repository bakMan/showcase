/* Neural network implementation
 * File:   bpnn.hpp
 * Author: Stanislav Smatana
 *

 */

#ifndef BPNN_HPP
#define BPNN_HPP
#include <cmath>
#include <vector>
#include "matrix.hpp"

class bpnn{
private:
    std::vector<Matrix> layers;
    std::vector<Matrix> biases;
    std::vector<Matrix> outputs;
public:
    /** Creates bp neural network with no layers*/
    bpnn(){};
    
    /** Creates bp neural network of desired shape with random initial weights
     * from interval -0.5 0.5
     * 
     * @param Vector of numbers, each representing the number of neurons in given
     *        layer. With exception of the first number which is the size of the
     *        input vector.
     */
    bpnn(std::vector<unsigned>);
    
    /** Initializes neural network using matrices specified in initializer list
     * 
     * @param List of lists of matrices
     */
    bpnn(std::initializer_list<std::initializer_list<std::initializer_list<double> > >);
    
    void printNN();
    std::vector<Matrix> classify_full(Matrix vec);
    
    /** Trains neural network using backpropagation
    * 
    * @param input      Matrix of training samples
    * @param expected   Matrix of expected outputs
    * @param rate       Learning rate
    * @param eps        Minimal error below which the learning ends
    * @param max_iters  Maximal number of algorithm iterations
    */
    void train(Matrix &input,Matrix &expected,double learning_rate,double eps,int max_iters=-1);
    
    /**Classify input vector using the nn
     */
    Matrix classify(Matrix vector);
    
    /** Deserialize network from given stream
    * 
    * @param stream Stream to read
    */ 
    void read(std::istream &stream);
    
    /** Returns network input and output dimension as std::pair
     * 
     * @return dimension as std::pair
     */
    std::pair<int,int> getDimensionality(){
        return std::make_pair(layers.begin()->shape.cols,layers.rbegin()->shape.cols);
    }
};

const double E = 2.718281828459045;

double sigmoid(double x);

#endif /* BPNN_HPP */

