/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "matrix.hpp"
#include <algorithm>
#include <iostream>
#include <functional>
#include <algorithm>
#include <string>
#include <sstream>
#include <iomanip>
#include <cassert>

const char *MSG_ROWCOLS = "For dot product first matrix have to have the same "
                          "number of columns as is the number of rows of "
                          "the second";

Matrix::Matrix(){
    shape = {0,0};
    data = nullptr;
}

Matrix::Matrix(t_shape s){
    //1+ because there is a hidden column where value 1.0 can be quickly added
    //this is an optimalization to avoid reallocation in case of extra column
    //with one addition
    data = new double[s.rows * s.cols + 1];
    shape = s;
}

Matrix::Matrix(t_shape s,double *data){
    //Data was allocated elsewhere
    this->data = data;
    shape = s;
    owner = false; //prevent destruction
}

Matrix::Matrix( std::initializer_list< std::initializer_list<double> > init){
    //I'm doing it a bit pythonishly, but it is a convience constructor so
    //no need for extreme performance
    auto iter = std::max_element(init.begin(),init.end(),
            [](std::initializer_list<double> a,std::initializer_list<double> b)
            { return a.size() < b.size();});
            
   unsigned max_size = iter->size();
   
   //All members of initializer list have to have the same number of elements
   if(!std::all_of(init.begin(),init.end(),[&](std::initializer_list<double> a){return a.size() == max_size;}))
   {
       throw std::runtime_error("Members of the initializator list don't have same number of elements.");
   }
   
   //Initializer list is row-wise
   shape = {(unsigned) init.size(),max_size};
   data = new double[shape.rows * shape.cols + 1];
   
   //initialize
   unsigned cnt=0;
   for(auto row:init)
   {
       std::copy(row.begin(),row.end(),&data[cnt*shape.cols]);
       cnt ++;
   }
}

Matrix::Matrix(std::initializer_list<double> init){
    //if initializing only with list -> treat as vector
    shape.cols = init.size();
    shape.rows = 1;
    data = new double[shape.cols + 1];
    std::copy(init.begin(),init.end(),data);
}

Matrix::~Matrix(){
    if(owner)
    {
        if (data != nullptr)
            delete data;
    
        data = nullptr;
    }
}

Matrix::Matrix(const Matrix &other):Matrix(other.shape){
    std::copy(other.data,other.data + other.shape.rows*other.shape.cols,data);
}

Matrix::Matrix(Matrix &&m):Matrix(){
    swap(*this,m);
}

Matrix& Matrix::operator=(Matrix other){
    swap(*this,other);
    return *this;
}
    
double Matrix::get(unsigned r,unsigned c) const {
    return data[shape.cols*r +c];
}

void Matrix::set(unsigned r,unsigned c, double val){
    data[shape.cols * r + c] = val;
}

void Matrix::setRow(Matrix &in,unsigned r){
    if(in.shape.rows != 1)
        throw std::runtime_error("Argument to row setting must be a vector.");
                
    if(in.shape.cols != shape.cols)
        throw std::runtime_error("Vector in row setting has to have same number of columns as matrix");
    
    std::copy(in.data,in.data + in.shape.cols,&data[r*shape.cols]);
}

Matrix Matrix::getRow(unsigned r){
    
    t_shape s = {1,shape.cols};
    Matrix ret = Matrix(s);
    std::copy(&data[r*shape.cols],&data[r*shape.cols] + shape.cols,ret.data);
    return ret;
}

void Matrix::getRowNocpy(unsigned r,Matrix &out) const {
    
    out.shape = {1,shape.cols};
    out.data = &data[r*shape.cols];
    out.owner = false;
}

double Matrix::sum()
{
    double s=0.0;
    for(int i=0;i < shape.rows*shape.cols;i++)
    {
        s += data[i];
    }
    
    return s;
}

double Matrix::ssum()
{
    double s=0.0;
    for(int i=0;i<shape.rows*shape.cols;i++)
        s+= data[i]*data[i];
    
    return s;
}

Matrix &Matrix::map(std::function<double(double)> f){
    for(int r=0;r < shape.rows;r++)
    {
        for(int c=0;c < shape.cols;c++)
            set(r,c,f(get(r,c)));
    }
    return *this;
}

Matrix Matrix::map_new(std::function<double(double)> func)
{
    Matrix ret = Matrix(*this);
    ret.map(func);
    return ret;
}

void Matrix::printMat(){
    for(int r=0;r < shape.rows;r++)
    {
        for(int c=0;c < shape.cols;c++)
            std::cout << std::setw(3) <<  get(r,c) << " ";
        std::cout << std::endl;
    }
}

void performDot(const MatrixView &a,const MatrixView &b,Matrix &out,const t_shape &s_a,const t_shape &s_b)
{
    for(unsigned r=0;r < s_a.rows;r++)
    {
        //second matrix column-wise
        for(unsigned c=0;c < s_b.cols;c++)
        {
            //product calculation, by columns in the first one, by rows in second
            double res=0;
            for(unsigned i=0;i < s_a.cols;i++)
                res += a.get(r,i)*b.get(i,c);
            
            out.set(r,c,res);
        }
    }
}

void dot(const MatrixView &a,const MatrixView &other, Matrix &out) 
{
    //First matrix row-wise
    assert(a.shape.cols == other.shape.rows);
    performDot(a,other,out,a.shape,other.shape);
}

Matrix dot(const MatrixView &a,const MatrixView &other) 
{
    //First matrix row-wise
    t_shape s = {a.shape.rows,other.shape.cols};
    Matrix mat(s);
    performDot(a,other,mat,a.shape,other.shape);
    return mat;
}

void implicitDot(const MatrixView &a,const Matrix &other,Matrix &out) 
{
    //perform dot without bias - last row
    t_shape s= other.shape;
    s.rows--;
    performDot(a,other,out,a.shape,s);
    
    //add bias
    Matrix last;
    other.getRowNocpy(other.shape.rows - 1,last);
    applyElementwise(last,out,[](double a, double b){return a+b;});
    
}

/** Method calculates dot product of two matrices
 * 
 * @param Matrix on the right side of calculation
 * @return Matrix of vector-wise dot products.
 */
Matrix Matrix::operator*(const MatrixView &other)const {
    if(shape.cols != other.shape.rows)
        throw std::runtime_error(MSG_ROWCOLS);
    
    //create new empty matrix
    t_shape s = {shape.rows,other.shape.cols};
    Matrix ret = Matrix(s);
    dot(*this,other,ret);
    return ret;
}

Matrix Matrix::operator+(const Matrix &other){
    return applyElementwise(*this,other,[](double a, double b){return a+b;});
}

Matrix& Matrix::operator+=(MatrixView &other)
{
    if(shape.cols != other.shape.cols || shape.rows != other.shape.rows)
        throw std::runtime_error("The number of elements in both matrices must"
                "be same for elementwise operation");

    for(int r=0;r < shape.rows;r++)
        for(int c=0;c < shape.cols;c++)
            set(r,c,get(r,c) + other.get(r,c));
    
    return *this;
}

Matrix Matrix::operator-(){
    Matrix ret(*this);
    ret.map([](double x){return -x;});
    return ret;
}

Matrix Matrix::operator-(const Matrix &other)
{
    return applyElementwise(*this,other,[](double a, double b){return a-b;});
}

void Matrix::implicitONE()
{
    if (shape.rows != 1)
        throw std::runtime_error("Implicit one is supported only in case of vectors");
    
    shape.cols++;
    set(0,shape.cols - 1,1.0);
    
}

void swap(Matrix &first,Matrix &second)
{
    using std::swap;
    swap(first.shape, second.shape);
    swap(first.data, second.data);
}

void applyElementwise(const Matrix &a,const Matrix &b,Matrix &out,std::function<double(double,double)> fun)
{
     if(a.shape.cols != b.shape.cols || a.shape.rows != b.shape.rows)
        throw std::runtime_error("The number of elements in both matrices must"
                "be same for elementwise operation");

    std::transform( a.data, a.data + a.shape.rows*a.shape.cols , b.data, out.data,fun);
}


/** Function takes another function and applies it on pairs of values. One values
 *  from matrix a, one from matrix 2. Requires a and b to be of same shape
 * 
 * @param a Matrix 
 * @param b Matrix
 * @param fun Function of form double f(double,double)
 * @return Matrix of results.
 */
Matrix applyElementwise(const Matrix &a,const Matrix &b,std::function<double(double,double)> fun)
{
     if(a.shape.cols != b.shape.cols || a.shape.rows != b.shape.rows)
        throw std::runtime_error("The number of elements in both matrices must"
                "be same for elementwise operation");

    Matrix res = Matrix(a.shape);
    
    //shapes match, I can treat matrix as a single array
    applyElementwise(a,b,res,fun);
    
    return res;
}

Matrix elementMultiply(const Matrix &a,const Matrix &b)
{
    return applyElementwise(a,b,[](double a, double b){return a*b;});
}

void elementMultiply(const Matrix &a,const Matrix &b,Matrix &out)
{
    applyElementwise(a,b,out,[](double a, double b){return a*b;});
}

Matrix Matrix::transpose()
{
    t_shape s = {shape.cols,shape.rows};
    Matrix ret = Matrix(s);
    
    for(int r=0;r < shape.rows;r++)
        for(int c=0; c < shape.cols;c++)
            ret.set(c,r,get(r,c));
            
    return ret;
}

Matrix::operator double(){
    if (shape.rows != 1 && shape.cols != 1)
        throw std::runtime_error("Atempt to cast non-singular matrix to double");
    else
        return data[0];
             
}
void Matrix::popBack()
{
    if (shape.rows != 1)
        throw std::runtime_error("Pop back operation is only supported in vectors");
    
    shape.cols--;
}

bool Matrix::read(std::istream &stream)
{
    std::vector<double> tmp;
    int row=0;
    int cols = -1;
    
    std::string line;
    while (std::getline(stream, line))
    {
        //empty line or eof is the end of matrix
        if(line == "")
            break;
        
        std::stringstream iss(line);
        
        double val;
        int col =0;
        
        while(iss >> val)
        {
            tmp.push_back(val);
            col++;
        }
        
        if(cols == -1)
            cols=col;
        else if(col != cols)
            throw std::runtime_error("Every matrix row has to have same number of columns.");
        
        row++;
    }
    
    if(data != nullptr)
    {
        delete data;
        data = nullptr;
    }
    
    //In case nothing was read - matrix is empty
    if(tmp.size() == 0)
        return false;
    
    shape = {row,(unsigned) cols};
    data = new double[row*cols];
    
    std::copy(tmp.begin(),tmp.end(),data);
    return true;
}

Matrix Matrix::cutAtColumn(unsigned col)
{
    if(col >= shape.cols)
        throw std::runtime_error("Attempted to cut matrix at column bigger than its number of columns");
    
    unsigned cut_cols = shape.cols - col;
    unsigned stay_cols = shape.cols - cut_cols;
    
    //create new matrix
    t_shape s = {shape.rows,cut_cols};
    Matrix ret = Matrix(s);
    
    //fill new matrix
    for(int r=0;r < shape.rows;r++)
        for(int c=col;c < shape.cols;c++)
            ret.set(r,c - col,get(r,c));
    
    //fill new array for this matrix
    double *ndata = new double[stay_cols * shape.rows];
    for(int r=0;r < shape.rows;r++)
        for(int c=0;c < stay_cols;c++)
            ndata[r*stay_cols + c] = get(r,c);
    
    if(data != nullptr)
        delete data;
    data = ndata;
    shape = {shape.rows,stay_cols};
    
    return ret;
}

void Matrix::set(unsigned i,double val){
    data[i] = val;
}

void Matrix::reshape(t_shape n_shape){
    if( (shape.cols * shape.rows) != (n_shape.cols * n_shape.rows))
        throw std::runtime_error("Number of elements of new shape must be same as of old shape in reshaping.");
    
    shape = n_shape;
}

double Matrix::max(){
    double *m_el = std::max_element(data,data + shape.rows*shape.cols);
    return *m_el;
}