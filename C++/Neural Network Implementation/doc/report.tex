\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[slovak]{babel}
\usepackage[a4paper]{geometry}
\usepackage[boxed,czech]{algorithm2e}
\usepackage{fancyvrb}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{pdflscape}
\usepackage{pdfpages}
\usepackage{hyperref}

\SetKwRepeat{Do}{do}{while}

\begin{document}
\title{Klasifikácia ľudí do príjmových tried pomocou neurónovej siete backpropagation}
\author{Stanislav Smatana}
\maketitle

Cieľom môjho projektu bolo vytvoriť nástroj, ktorý umožňuje tréning neurónovej siete pomocou 
metódy backpropagation. Jeho návrh a vnútornú implementáciu podrobne rozoberám v sekcii \ref{sec:spec}. Následne,
v sekcii \ref{sec:howto}, opisujem ovládanie výsledného programu a jeho praktické použitie ilustrujem
krátkym príkladom. 

Okrem toho musí byť program schopný natrénovanú sieť aj použiť pri riešení
praktickej klasifikačnej úlohy. Funkcionalitu som testoval na verejne dostupnej dátovej sade Adult,
ktorá je podmnožinou dát zo sčítania ľudu v USA z roku 1994. Za cieľ klasifikačnej úlohy som si
zvolil klasifikáciu ľudí do príjmových tried (nad 50 000 dolárov ročne a pod 50 000 dolárov ročne) 
na základe iných informácii, ako sú ich vzdelanie, rodinný stav, či pracovné zameranie. Informácie
o formáte dát a ich úprave opisujem v sekcií \ref{sec:data}. V poslednej sekcií (\ref{sec:res}) 
prezentujem postup trénovania siete a dosiahnuté výsledky.

\section{Návrh a implementácia klasifikačného nástroja}
\label{sec:spec}
Výsledný nástroj musí byť schopný natrénovať neurónovú sieť a následne pomocou nej klasifikovať ľubovolné dáta. Z tohoto dôvodu som sa rozhodol implementovať dva základné režimy funkcie programu – trénovací režim a klasifikačný režim.

V trénovacom režime program dostane na vstup množinu trénovacích vektorov spolu s očakávanými výstupnými vektormi a pomocou algoritmu backpropagation trénuje neurónovú sieť zvolenej topológie. Pri implementácii
som použil verziu algoritmu, ktorá bola prezentovaná na prednáškach predmetu Softcomputing transformovanú do  maticovej formy (algoritmus \ref{alg:train}). Transformáciu
som sa rozhodol urobiť, pretože maticový zápis algoritmu je podľa môjho názoru prehľadnejší a umožňuje mi problém rozložiť na fázu implementácie potrebných maticových operácii a fázu implementácie samotného algoritmu. Ďalšou výhodou je, že obe fázy môžu byť testované oddelene. Okrem algoritmu trénovania je možné
previesť do maticovej formy je algoritmus klasifikácie (algoritmus \ref{alg:classify}).

Pri implementácii som zistil, že štandardná knižnica jazyka C++ neobsahuje podporu pre matice a maticové operácie. Preto
som som sa rozhodol implementovať svoju vlastnú triedu \texttt{Matrix} (súbor \texttt{Matrix.cpp} a
\texttt{Matrix.hpp}), ktorá okrem dátovej štruktúry a funkcií pre prístup k dátam obsahuje
aj sadu niekoľkých metód reprezentujúcich množinu maticových operácií potrebných na implementáciu
algoritmu backpropagation. Najdôležitejšími metódami sú metóda \texttt{dot} implementujúca štandardný maticový súčin, metóda \texttt{elementMultiply} implementujúca súčin vektorov po jednotlivých elementoch a metódy pre sčítanie a odčítanie matíc. Okrem nich obsahuje trieda \texttt{Matrix} aj ďalšie pomocné metódy.

Samotná sieť backpropagation je implementovaná v triede \texttt{bpnn}. Každá vrstva siete sa skladá z
matice váh neurónov (neuróny sú uložené po riadkoch) a vektoru váh nultých vstupov neurónov (v angličtine je tento vektor známy ako "bias"). Okrem toho trieda obsahuje aj predpripravené polia
pre výstupy jednotlivých vrstiev aby nedochádzalo k spomaleniu programu kvôli ich opakovanej alokácii. Hlavnými metódami triedy sú metódy \texttt{train} a \texttt{classify}. Metóda \texttt{train} slúži na tréning siete a jej vstupom je matica, ktorej riadky sú jednotlivé trénovacie
vektory, matica, ktorej riadky predstavujú očakávane výstupy, miera učenia $\mu$, minimálna chyba eps a maximálny počet iterácii algoritmu. Trénovacie vektory sú algoritmu predkladané v poradí v akom sú uložené v trénovacej matici a počiatočné váhy siete sú inicializované na náhodné hodnoty v intervale $<-0.5,0.5>$. 

Metóda \texttt{classify} očakáva na vstupe maticu, ktoré riadky predstavujú vektory pre klasifikáciu a jej výstupom je matica, ktorej riadky predstavujú odozvu siete na jednotlivé vstupy.
Táto metóda je základom funkcie programu v klasifikačnom režime. Podrobný návod na obsluhu programu
a kritéria pre jeho vstupné dáta sú uvedené v sekcii \ref{sec:howto}.

\begin{algorithm}[h]
\SetAlgoLined
 \KwData{

 Množina trénovacích vektorov $T = \{\vec{x}_1\vec{x}_2,\dots,\vec{x}_n\}$
 
 Množina očakávaných výstupných vektorov $P = \{\vec{p}_1,\vec{p}_2,\dots,\vec{p}_n\}$

Miera učenia $\mu$

Minimálna chyba eps
 }
 \Do{ ($(GlErr > eps)$ and (Nebol dosiahnutý maximálny počet iterácií))}{
    \For{($\vec{x}_i$ in T)}{
      $(\vec{y}_L,\vec{y}_{L-1},\dots,\vec{y_1}) = classify(\vec{x})$\;      
      $\vec{e} = \vec{p}_i - \vec{y}_L$\;      
      $\vec{\delta }_L = (\vec{e} \odot \vec{y}_L)\odot (1 - \vec{y}_L)$\;
      \For{($l = L-1;l>0;l--$)}{
         $\vec{\delta_l} = (W^T_{l+1} \cdot \vec{\delta}_{l+1}) \odot y_l \odot ( 1 - \vec{y}_l) $\;
      }
      \For{$(l=1;l<=L;l++)$}{
         $\Delta W_l = \mu(\vec{\delta}_l \cdot \vec{x}^T_l)$\;
         $W_l = W_l + \Delta W_l$\;         
         $\vec{b}_l = \vec{b}_l + \mu \vec{\delta}_l$\;
      }
    }
 }
 \caption{Algoritmus backpropagation v maticovej forme. V prvej fáze je aktuálny výstup siete porovnaný s očakávaným výstupom a ich rozdiel je použitý na výpočet delty výstupnej vrstvy. Následne je chyba propagovaná k predošlým vrstvám a vo finálnom kroku dochádza k úprave váh. Symbol $\odot$ značí súčin dvoch vektorov po elementoch ($\vec{x} \odot \vec{y} = (x_0 y_0,x_1 y_1,\dots ,x_n y_n$)).}
\label{alg:train}  
\end{algorithm}

\begin{algorithm}[H]
\label{alg:classify} 
\KwData{Vstupný vektor $\vec{x}$}
$\vec{y_0} = \vec{x}$\;
\For{$(i=0;i<L;i++)$}{
   $\vec{y}_{l+1} = sigmoid(W_{l+1} \cdot \vec{y}_l + \vec{b}_{l+1})$\;
}
\caption{Algoritmus klasifikácie v maticovom zápise. Vektor $\vec{b}_l$ obsahuje váhy nultých
vstupov jednotlivých neurónov. Keďže nulté vstupy sú implicitne nastavené na hodnotu 1 pripočítanie
vektora $\vec{b}_l$ je ekvivalentné so situáciou kedy by bol súčasťou matice $W_{l+1}$ a vektor $\vec{y}_l$ by obsahoval v prvom riadku implicitnú hodnotu 1. Pre uvedenú možnosť som sa rozhodol aby som predišiel nutnosti rozširovať vstupný vektor.}
\end{algorithm}


\section{Ovládanie programu a formát vstupných dát}
\label{sec:howto}
Program pracuje v dvoch základných režimoch - trénovacii (\texttt{-t}) a klasifikačný (\texttt{-c}). V oboch prípadoch
očakáva dva pozičné parametre - \texttt{súbor1} a \texttt{súbor2}. V prípade trénovania musí prvý súbor obsahovať trénovacie vektory a druhý očakávané výstupy. V prípade klasifikácie musí prvý súbor obsahovať 
vstupné vektory a druhý popis natrénovanej neurónovej siete. 

Pri tréningu je ďalej nutné špecifikovať požadovanú topológiu siete pomocou parametra \texttt{-s}. Po ukončení tréningu program vypíše popis natrénovanej siete na štandardný výstup. Užívateľ môže sieť uložiť do súboru pomocou presmerovania výstupu programu (v linuxovom príkazovom riadku '$>$') a následne použiť v klasifikačnom režime. Program príjma tieto parametre:
\begin{figure}[h]
\begin{tabular}{|l|p{12cm}|}
\hline
\multicolumn{2}{|l|}{\textbf{Použitie:}}\\
\multicolumn{2}{|l|}{bpnn \textbf{-c/t [Parametre]} súbor1 súbor2}\\
\hline
-t & Trénovací režim. Program očakáva dva pozičné parametre - súbor s trénovacími vektormi a súbor s očakávanými výstupmi. Výstupom je natrénovaná neurónová sieť. V trénovacom režime je nutné nastaviť topológiu siete pomocou
parametra -s.\\
\hline
-c & Klasifikačný režim. Program očakáva dva pozičné parametre - súbor
s vektormi na klasifikáciu a súbor s natrénovanou neurónovou sieťou. Výstupom sú výstupné vektory pre dané vstupné vektory.\\
\hline
\multicolumn{2}{|l|}{\textbf{Parametre:}}\\
\hline
-s $n_0,n_1,\dots,n_x$ & Tvar siete - čiarkou oddelený zoznam minimálne dvoch celých čísel. Prvé číslo udáva dimenzionalitu vstupu a ďalšie udávajú počty neurónov v jednotlivých vrstvách. Príklad: 2,2,1 (vstup sú dvojrozmerné vektory, 2 neuróny skrytej vrstvy a 1 neurón na výstupnej vrstve).\\
\hline
-l [float] & Miera učenia $\mu$. Predvolená hodnota: 0.5.\\
\hline
-m [integer] & Maximálny počet iterácií. Predvolená hodnota: 10 000.\\
\hline
-e [float] & Minimálna chyba, po ktorej dosiahnutí učenie končí. Predvolená hodnota: 0.0001.\\
\hline
\end{tabular}
\end{figure}

Použitie programu a formát vstupných dát budem ilustrovať na riešení problému XOR. Nasledujúca
tabuľka ukazuje príklad vstupných dát pre tréning siete riešiacej daný problém:


\begin{figure}[h!]
\centering
\begin{subfigure}{0.5\textwidth}
  \centering
  \begin{Verbatim}[frame=single]
0 0
0 1
1 0
1 1
  \end{Verbatim}
  \caption{Trénovacie vektory.}
  \label{fig:sub1}
\end{subfigure}%1
\begin{subfigure}{0.5\textwidth}
  \centering
  \begin{Verbatim}[frame=single]
0
1
1
0
  \end{Verbatim}
  \caption{Očakávané výstupné vektory.}
  \label{fig:sub1}
\end{subfigure}%
\caption{Ukážka vstupných dát programu. Vľavo je zobrazený súbor s trénovacími vektormi pre 
problém XOR. Každý trénovací vektor je uvedený v jednom riadku a hodnoty jeho zložiek sú oddelené práve jednou medzerou. Na konci riadku nie sú prebytočné medzery. Vpravo je zobrazený súbor s očakávanými výstupnými vektormi. Pravidlá formátovania sú rovnaké. Poradie vektorov v súboroch musí byť totožné. V tomto prípade pre vstup (0,0) očakávame výstup (0), pre vstup (0,1) výstup (1), atď \dots Uvedené súbory sa nachádzajú v priečinku \texttt{data}.}
\end{figure}

Problém XOR nemožno efektívne riešiť pomocou jednovrstvovej siete, preto je nutné zvoliť minimálne
dvojvrstvovú topológiu. V tomto príklade som sa rozhodol použiť sieť s 2 neurónmi na skrytej vrstve a jedným neurónom na výstupnej vrstve. Zodpovedajúca hodnota parametra \texttt{-s} je "2,2,1" (prvá hodnota 2 určuje, že vstup siete sú dvojrozmerné vektory). Ďalej som mieru učenia nastavil 
na 0.5 (parameter \texttt{-l}), maximálny počet iterácii na 500 (parameter \texttt{-m}) a minimálnu chybu pre ukončenie tréningu na 0.001 (parameter -e). Výstup programu, ktorý predstavuje popis natrénovanej siete, som presmeroval do súboru xor\_net.txt. Celý príkaz:

\begin{verbatim}
bpnn -t -s 2,2,1 -l 0.5 -m 500 -e 0.001 data/xor.txt data/xor_labels.txt > xor_net.txt
\end{verbatim}
Výstup príkazu (obsah súboru xor\_net.txt - natrénovaná sieť):
\begin{Verbatim}[frame=single]
-4.51158 -4.37345 
-7.95533 -6.30571 

6.54199 
2.60245 

9.39533 -9.60257 

-4.39352 

\end{Verbatim}
Klasifikáciu pomocou naučenej siete umožňuje spustenie programu s parametrom \texttt{-c}. V tomto príklade
klasifikujem rovnaké dáta pomocou akých som sieť učil, avšak program umožňuje klasifikovať akékoľvek dáta, pokiaľ je ich dimenzionalita rovnaká ako dimenzionalita dát, s ktorými bola
sieť trénovaná. V súbore xor{\_}net.txt sa nachádza popis siete získaný predošlým príkazom.
Celý príkaz:
\begin{verbatim}
bpnn -c data/xor.txt xor_net.txt
\end{verbatim}
Výstup príkazu:

\begin{Verbatim}[frame=single]
0.0188559 
0.978264 
0.979493 
0.0273747 
\end{Verbatim}


\section{Definícia klasifikačnej úlohy a predspracovanie dát }
\label{sec:data}
Ako klasifikačnú úlohu som si zvolil klasifikáciu ľudí do príjmových tried na základe iných atribútov, ako sú vzdelanie, rodinný stav, národnosť, apod. Údaje som zobral z verejne dostupnej
databázy Adult\footnote{Dátová sada je dostupná na adrese https://archive.ics.uci.edu/ml/datasets/Adult}, ktorá je podmnožinou sčítania ľudu v USA z roku 1994 (Census 1994). Dáta sú distribuované vo formáte CSV, v ktorom je každý človek charakterizovaný jedným riadkom pozostávajúcim
z hodnôt týchto atribútov oddelených čiarkou: age, workclass, fnlwgt, education, education-num, marital-status, occupation, relationship, race, sex, capital-gain, capital-loss, hours-per-week, native-country a income. Pretože veľkosť databázy (cca. 30 000 záznamov) mi neumožňovala dostatočne
rýchle učenie, rozhodol som sa dáta najprv predspracovať.

V prvom kroku úprav som z databázy náhodne vybral jej podmnožinu (cca. 8 000 záznamov). Ďalej som odstránil atribút fnlwgt, ktorý sa využíva pri sčítaní ľudu a nepredstavuje hodnotnú informáciu o skúmanom jedincovi. Okrem neho som sa rozhodol odstrániť aj atribúty education.num a relationship, pretože obsahujú rovnakú informáciu ako atribúty education a marital-status. Atribúty capital-gain a capital-loss som zlúčil do nového atribútu capital-income, ktorý som spočítal ako capital-income = capital-gain $-$ capital-loss.  

Ďalej som zúžil množiny hodnôt atribútov marital-status na {married, was-married, never-married} a education na {no-education, basic-education, high-school, associate, bachelor, master, prof-school, doctorate}. V atribúte native-country som všetky hodnoty okrem United-States zlúčil pod hodnotou Non-US.

Nakoniec som pomocou výsledku štatistického testu chí-kvadrát, zoradil atribúty podľa ich signifikantnosti vo vzťahu k príjmovej triede. Ako najsignifikatnejšie sa ukázali atribúty marital-status, education, capital-income, occupation a age. Všetky ostatné atribúty som z datasetu odstránil aby som redukoval dimenzionalitu problému a skrátil čas potrebný na učenie.

Vo finálnom kroku som hodnoty kategorických atribútov zakódoval pomocou celých čísel a všetky hodnoty som normalizoval do intervalu $<0,1>$. Tento krok, sa ukázal ako kritický, pretože
pri mojich prvých pokusoch s nenormalizovanými dátami bola doba učenia príliš dlhá. Tabuľka \ref{tab:data} zhŕňa atribúty použité pri učení a ich hodnoty.

\begin{figure}[h]
\centering
\begin{tabular}{|l|p{7cm}|p{3cm}|}
\hline
\textbf{Atribút} & \textbf{Hodnoty} & \textbf{Rola} \\
\hline
income & $>50$K, $<=50$K & Príjmová trieda \\
\hline
marital-status & Married, Was-Married, Never-Married & Rodinný stav \\
\hline
education & No-Education, Basic-education, High-school, Bachelor, Master, Doctorate, Prof-school, & Dosiahnuté vzdelanie\\
\hline
occupation & Tech-support, Craft-repair, Sales, Exec-managerial, Prof-specialty, Handlers-cleaners, Machine-op-inspct, Adm-clerical, Farming-fishing, Transport-moving, Priv-house-serv, Protective-serv, Armed-Forces, Other-service & Pracovné zameranie \\
\hline
age & celé číslo & Vek\\
\hline
capital-income & reálne číslo & Zisk z investovania (neráta sa do príjmu)\\
\hline
\end{tabular}
\caption{Atribúty z datasetu Adult zahrnuté pri trénovaní neurónovej siete. Atribút \emph{income} predstavuje príjmovú triedu človeka a preto som ho použil ako klasifikačný atribút.}
\label{tab:data}
\end{figure}

\newpage
\section{Vyhodnotenie klasifikačného nástroja}
\label{sec:res}
Po predspracovaní dát som ich rozdelil v pomere 7:3 na trénovaciu a testovaciu sadu. Najprv som sa rozhodol určiť optimálnu topológiu siete. Experimentoval som s jednovrstvovou sieťou pozostávajúcou z jedného neurónu a s dvojvrstvovými sieťami s 5, 10, 15 a 20 neurónmi skrytej vrstvy\footnote{Pretože každý človek v dátovej sade patrí do jednej z dvoch príjmových tried, musí mať výstupná vrstva vždy práve 1 neurón.}. Každú sieť som natrénoval 100 krát na trénovacej sade a vyhodnotil jej výkonnosť na testovacej sade pomocou metriky AUC. Ako najlepšie sa ukázali siete s 10, 15 a 20 neurónmi skrytej vrstvy. Nakoniec som ako výslednú topológiu zvolil sieť s 15 neurónmi, pretože jej priemerná hodnota metriky AUC sa signifikantne nelíši od priemernej hodnoty AUC siete s 20  neurónmi (t-test: $p=0.16$) a zároveň je rozptyl hodnôt AUC oproti sieti s 10 neurónmi menší a preto výsledky jej trénovania považujem za stabilnejšie (graf v prílohe A). 

Ďalej som sa rozhodol experimentovať s počtom iterácii učiaceho algoritmu. Sieť som 100 krát natrénoval s maximálnym počtom iterácii v rozmedzí 50 až 1000. Pre počty iterácii 50 - 100 (graf v prílohe B) dochádza k postupnému nárastu rozlišovacej
schopnosti klasifikátora. Naopak, od hodnoty 100 do hodnoty 1000 sa hodnota už signifikatne nemení (graf v prílohe C).  Pretože pri žiadnom počte iterácii nedošlo k náhlemu
signifikantnému poklesu miery AUC oproti predošlým hodnotám, pokladám prípadné preučenie pri 0 - 1000 iteráciach algoritmu za nepravdepodobné. 

Najlepšia hodnota metriky AUC, ktorú som počas testovania pozoroval bola 0{,}8922 (500 iterácii, $\mu=0.5$, 15 neurónov skrytej vrstvy, 1 neurón výstupnej vrstvy). ROC krivka zodpovedajúcej neurónovej siete je znázornená na grafe \ref{fig:roc}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.63]{fig/roc.pdf}
\caption{ROC krivka najlepšej nájdenej neurónovej siete zo všetkých sietí, ktoré som natrénoval počas experimentovania. }
\label{fig:roc}
\end{figure}

\newpage


\includepdf[landscape=true]{fig/shape.pdf}
\includepdf[landscape=true]{fig/50_100box.pdf}
\includepdf[landscape=true]{fig/100_1000box.pdf}


\end{document}