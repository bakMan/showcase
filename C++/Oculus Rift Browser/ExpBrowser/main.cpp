#include <iostream>
#include <vector>
#include <voce.h>
#include <windows.h>
#include <irrlicht.h>
#include <thread>
#include <mutex>

#include "voice.h"
#include "controller.h"
#include "CWheel.h"
#include "datamodel.h"
#include "OculusRenderer.h"


using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;


using namespace std;
void voce_main();
void browser_main(bool, std::string webdir);

SKeyMap keyMapb[4] = {
	{ EKA_MOVE_FORWARD, KEY_KEY_W },
	{ EKA_MOVE_BACKWARD, KEY_KEY_S },
	{ EKA_STRAFE_LEFT, KEY_KEY_A },
	{ EKA_STRAFE_RIGHT, KEY_KEY_D }
};

vector<int> gCOMMAND_BUFFER;
std::mutex gCOMMAND_MUTEX;
void add_vcommand(int com);

// CONSTANTS
const int COMM_BUF_SIZE = 100;

int main(int argc,char **argv)
{
	//occulus rift rendering switch
	bool occulus_option = false;
	std::string web_root = "web";

	if (argc  > 1)
	{
		bool skip = false;
		for (int i = 1; i < argc; i++)
		{
			bool bad_arg = false;
			std::string arg(argv[i]);

			if (skip)
			{
				skip = false;
				continue;
			}

			if (arg == "--occulus" || arg == "-o")
			{
				occulus_option = true;
			}
			else if (arg == "--webroot" || arg == "-w" )
			{
				if (i + 1 < argc)
				{
					web_root = std::string(argv[i+1]);
					skip = true;
				}
				else
				{
					bad_arg = true;
				}
			}
			else
			{
				bad_arg = true;
			}
			
			if (bad_arg)
			{
				cerr << "Bad commandline options " << endl;
				return 1;
			}

		}

	}

	//preallocate voice command buffer
	gCOMMAND_BUFFER.reserve(COMM_BUF_SIZE);
	
	//Voce initialisation (has to start jvm .. slow)
	voce::init("./lib", false, true, "./grammar", "commands");

	//Start browser and voce thread
	std::thread minion(std::bind(browser_main,occulus_option,web_root));
	
	//VOCE speech recognition has to run in the main thread, otherwise crashes
	//Some bug in VOCE/ JNI ?
	voce_main();

	//wait for browser thread to end
	minion.join();

	return 0;
}


/** Main for speech processing thread
*/
void voce_main()
{

	bool active = false;
	bool quit = false;

	while (!quit)
	{
		//Make it less demanding
		::Sleep(10);


		while (voce::getRecognizerQueueSize() > 0)
		{

			std::string s = voce::popRecognizedString();
			cerr << "Heard: " << s << endl;

			//translate into command
			int command = voiceToCommand(s);

			//voice command logic
			if (command == V_HELLO)
			{
				//Activate listening
				active = true;
				add_vcommand(command);
				
			}
			else if (command == V_BYE)
			{
				//Deactivate listening
				active = false;
				add_vcommand(command);
			}
			else if (active)
			{
				//Send other commands only if active
				if (command != V_NOP)
				{
					//If it is quit -> exit thread
					if (command == V_QUIT)
					{
						active = false;
						quit = true;
					}
					//Yay ! Valid voice command ! Send it out !
					add_vcommand(command);
				}
				else
				{
					//Not recognized command -> should not happend
					cerr << "[Warn] Voice command not recognized, check grammar" << endl;
				}

			}
			else
			{
				//Voice command recieved, but inactive
				cerr << "Sorry not active" << endl;
			}
			
		}
	}

	voce::destroy();

}

/** Main function for browser threas
*/
//debugging flag for occulus dbg on screen
const bool OCULUS_DBG = false;
void browser_main(bool USE_OCCULUS,std::string webdir)
{


	irr::video::E_DRIVER_TYPE driverType = video::EDT_OPENGL;
	int OCCULUS_ADAPTER = 1;
	unsigned OCCULUS_X;
	unsigned OCCULUS_Y;


	if(OCULUS_DBG)
	{
		USE_OCCULUS = true;
		OCCULUS_X = 1280;
		OCCULUS_Y = 720;
		OCCULUS_ADAPTER = 0;
	}
	else
	{
		OCCULUS_X = 1920;
		OCCULUS_Y = 1080;
	}

	const int MONITOR_X = 1280;
	const int MONITOR_Y = 720;

	//main device
	IrrlichtDevice *device;

	//Settings for occulus renderer
	// Get the window handle for Oculus Rift SDK
	void *window = 0;

	if (USE_OCCULUS)
	{

		SIrrlichtCreationParameters params;
		params.Bits = 32;
		params.DriverType = driverType;
		params.DisplayAdapter = 1;
		//during occulus debug run only in windowed mode
		params.Fullscreen = OCULUS_DBG ? false : false;
		params.WindowSize = dimension2d<u32>(OCCULUS_X,OCCULUS_Y);
		params.DeviceType = EIDT_WIN32;

		device = createDeviceEx(params);
	}
	else
	{
		//non occulus rendering
		device = createDevice(driverType, dimension2d<u32>(MONITOR_X,MONITOR_Y), 32,
				 true, false, true, 0);
	}
	
	

	std::map<std::string, tPage> *web;

	if (!device)
	{
		cerr << "Unable to set video device" << endl;
	}

	try
	{
		web = readSimWeb(webdir,device->getVideoDriver());
	}
	catch (std::exception &e)
	{
		cerr << "Error: " << e.what() << endl;
		cin.get();
		return ;
	}




	IVideoDriver* driver = device->getVideoDriver();
	
	ISceneManager* smgr = device->getSceneManager();
	IGUIEnvironment* guienv = device->getGUIEnvironment();

	OculusRenderer *oculusRenderer = 0;

	//setup occulus
	if (USE_OCCULUS)
	{

		if (driverType == irr::video::EDT_DIRECT3D9)
			window = driver->getExposedVideoData().D3D9.HWnd;
		else if (driverType == irr::video::EDT_OPENGL) // OpenGL under windows - no idea how it's done in Linux
			window = driver->getExposedVideoData().OpenGLWin32.HWnd;

		oculusRenderer = new OculusRenderer(window, driver, smgr, 1.0f);
	}
	

	//smgr->addCameraSceneNode(0, vector3df(0, 0, 60), vector3df(0, 0, 0));
	//smgr->addCameraSceneNodeFPS(0, 0, -10, -1, NULL, 0);

	ICameraSceneNode *n = smgr->addCameraSceneNodeFPS(0, 50, 0.1, -1, keyMapb, 4, true, 0.f, false, true);
	n->setTarget(vector3df(0.0,0.0,-1.0));
	

	//add skydome
	scene::ISceneNode* skydome = smgr->addSkyDomeSceneNode(driver->getTexture("media/skydome.jpg"), 16, 8, 0.95f, 2.0f);


	//Create main game controller (and event listener)
	Controller controller(device, web);

	//Set main game controller as listener for events
	device->setEventReceiver(&controller);
	
	while (device->run())
	{
		if (!device->isWindowActive())
		{
			device->yield();
			continue;
		}

		//collision detection
		if (!USE_OCCULUS)
		{
			//NOT occulus ->use active camera
			ISceneNode *node = detectCameraCollision(smgr, smgr->getActiveCamera());
			controller.onCollision(node);
		}
		else
		{
			//occulus, have to use occulus camera
			ISceneNode *node = detectCameraCollision(smgr, oculusRenderer->camera_,vector3df(0.35,0.95,0.0));
			controller.onCollision(node);

		}


		//get voicecommands from other thread and plug them into irrlicht
		if (!gCOMMAND_BUFFER.empty())
		{
			//locking !
			std::lock_guard<mutex> l(gCOMMAND_MUTEX);
			vcommandsToEvents(gCOMMAND_BUFFER,device);
			gCOMMAND_BUFFER.clear();
		}

		driver->beginScene(true, true, SColor(0,0, 0, 0));

		if (USE_OCCULUS)
		{
			//draw to occulus
			ICameraSceneNode *camera = smgr->getActiveCamera();
			camera->OnAnimate(device->getTimer()->getTime());
			camera->updateAbsolutePosition();

			oculusRenderer->drawAll(camera->getAbsolutePosition(), camera->getRotation().Y,
				irr::video::SColor(255, 255, 255, 255));
			
		}
		else
		{
			//draw normally
			smgr->drawAll();
			guienv->drawAll();
		}

		driver->endScene();
	}

	device->drop();

}

//*** UTILITIES

//safely adds command to command buffer
void add_vcommand(int com)
{
	std::lock_guard<mutex> lock(gCOMMAND_MUTEX);
	gCOMMAND_BUFFER.push_back(com);
}