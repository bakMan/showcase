#ifndef CWHEEL_H
#define CWHEEL_H

#include <irrlicht.h>
#include <functional>

#include "datamodel.h"
#include "gmodel.h"


using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;


const double BEG_PHASE = (1 / 2.0) * PI;
const double ROT_SPEED = PI / 4.0;
const double CAM_DIST = 10.0;


//States of the page wheel automaton
enum WHEEL_STATES
{
	S_READY,
	S_TRANSFER,
	S_GOING,
	S_DISAPPEARED,
	S_RISE,

};


class CWheel
{
private:
	//Vector of data model pages
	tPage *p_page;

	//radius of wheel
	int R = 50;

	//Vector of graphical pages
	vector<GSection*> sections;

	//Scene manager
	ISceneManager *smg;


	//Index of active page
	int active_index;

	vector3df pos;

	//radian distance between two pages
	double rads_per_page;

	//mapping between link nodes and target pages
	std::map<unsigned long, std::string> target_map;

	//Callback for new page opening
	std::function<void(std::string)> sel_clbk;

	//state of the wheel
	int state;

	//currently selected scene node using collision detection
	GSection *selected;

	//Creates graphical link representations
	//registers them for collision detection
	//adds them to map for node->target address
	//translation 
	void addLinks(vector<tLink> &lnks, ISceneNode *node);

	// optional notify to outside force at the end of animation
	std::function<void()> outer_anim_end_callback;

public:
	/** Constructor
	m - pointer to scene manager 
	p - pointer to data model for wheel
	sel_clbk - callback to be callled on page change

	*/
	CWheel(ISceneManager *m, tPage *p,std::function<void(std::string)> sel_clbk = std::function<void(std::string)>());
	
	//Sets new data model for a wheel
	void SetModel(tPage *p);

	//Input signals of the controller automaton
	void next();
	void previous();
	void open();
	void up();
	void down();
	void appear(std::function<void()> clbk = std::function<void()>());
	void disappear(std::function<void()> clbk = std::function<void()>());

	//signal that current animation is finished
	void onFinishedAnim();

	//collision detection signaling
	void onCollision(ISceneNode *node);

	//graphically rotates all sections
	void rotateAll(double stop, double vel,double beg_phase);

	//hides all sections
	void toggleVis();

	//updates position of all sections using given displacement vector
	void displaceAll(vector3df disp);

	//displace all -> but using animator
	void animDisplaceAll(vector3df disp,double vel);
};

#endif