#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <string>
#include <vector>
#include <map>
#include <irrlicht.h>

using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

using namespace std;

struct tLink
{
	double tlx;
	double tly;
	double brx;
	double bry;
	std::string target;
};

struct tSection
{
	vector<tLink> links;
	ITexture *view;
	unsigned id;
};

typedef vector<tSection> tPage;

std::map<std::string, tPage> *readSimWeb(std::string path,IVideoDriver * d);


/** Exception for invalid path / etc
*/
class LoadException: public std::exception
{
public:
	LoadException(std::string m = "exception!") : msg(m) {}
	~LoadException() throw() {}
	const char* what() const throw() { return msg.c_str(); }

private:
	std::string msg;
};

//Function for reading links out of json file
vector<tLink> readLinksFromJSON(std::string path);
#endif