#include "voice.h"
#include <string>
#include <map>
#include <IEventReceiver.h>


int voiceToCommand(std::string said)
{
	auto iter = VCOM_TABLE.find(said);

	if (iter != VCOM_TABLE.end())
	{
		//known command
		return iter->second;

	}
	else
	{
		//command not recognized
		return V_NOP;
	}
}

void vcommandsToEvents(vector<int> &commands, IrrlichtDevice *device)
{
	for (int com : commands)
	{
		//send events
		SEvent e;
		e.UserEvent.UserData1 = com;
		e.EventType = EET_USER_EVENT;
		device->postEventFromUser(e);
	}
}