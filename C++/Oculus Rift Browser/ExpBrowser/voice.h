#ifndef H_VOICE
#define H_VOICE

#include<map>
#include<string>
#include<vector>
#include<irrlicht.h>


using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

//List of voice command codes
enum V_COMMANDS
{
	V_NOP =  -1,
	V_HELLO = 0,
	V_BYE,
	V_LEFT,
	V_RIGHT,
	V_UP,
	V_DOWN,
	V_OPEN,
	V_BACK,
	V_QUIT,
	V_HOME
	
};

//Translation table for voice commands into voice command codes
static std::map<std::string, int> VCOM_TABLE = {

	{ "hello", V_HELLO },
	{ "bye", V_BYE },
	{ "left", V_LEFT },
	{ "right", V_RIGHT },
	{ "back", V_BACK },
	{ "up", V_UP },
	{ "down", V_DOWN }, 
	{ "quit", V_QUIT }, 
	{ "open", V_OPEN },
	{ "home", V_HOME}

};

/** Translates given string into command code accordint to VCOM_TABLE
*/
int voiceToCommand(std::string said);

/**Translates set of voice commands into irrlicht events 
*/
void vcommandsToEvents(vector<int> &commands, IrrlichtDevice *device);

#endif