#include <irrlicht.h>
#include <functional>
#include <iostream>
#include <algorithm>

#include "datamodel.h"
#include "CWheel.h"
#include "gmodel.h"


using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

//const double PI = 3.1415926535897;


//TODO: Animation control
//TODO: "Web" loading
//TODO: Implement state machine


SKeyMap keyMap[4] = { 
	{ EKA_MOVE_FORWARD, KEY_KEY_W }, 
	{ EKA_MOVE_BACKWARD,KEY_KEY_S }, 
	{ EKA_STRAFE_LEFT, KEY_KEY_A  },
	{ EKA_STRAFE_RIGHT, KEY_KEY_D  }
};

CWheel::CWheel(ISceneManager *m, tPage *p, std::function<void(std::string)> sel_clbk) :
				smg(m)
{
	active_index = 0;

	//Set selection callback
	this->sel_clbk = sel_clbk;

	
	//Recreate graphical wheel using new data model
	SetModel(p);

	//initial automaton state
	state = S_DISAPPEARED;
	//make all disappear
	toggleVis();
	//move them down
	displaceAll(vector3df(0.0f, -2*Y_SIZE, 0.0f));
}


//adding links
void CWheel::addLinks(vector<tLink> &lnks, ISceneNode *node)
{
	for (tLink &lnk : lnks)
	{
		//create rectangle for link
		GSection *l = new GSection(node, smg, -1);

		//set size of the link rectangle
		double x = lnk.brx - lnk.tlx;
		double y = lnk.bry - lnk.tly;
		l->set_size(x,y);

		//set position to the centre of the link, above the parent
		l->setPosition(vector3df((X_SIZE - lnk.brx + x/2) - X_SIZE/2.0, (Y_SIZE - lnk.bry + y/2) - Y_SIZE/2.0, 0.0001));

		

		//set up collision detection on the link
		ITriangleSelector * selector = smg->createTriangleSelectorFromBoundingBox(l);
		l->setTriangleSelector(selector);

		//set up material properties
		l->setMaterialType(E_MATERIAL_TYPE::EMT_TRANSPARENT_ALPHA_CHANNEL);
		
		//set invisible
		//DEBUG CHANGE
		//l->set_color(SColor(0, 255, 0, 0));
		l->set_color(SColor(50, 0, 255, 0));
		
		

		//insert link into link map
		target_map.insert(std::make_pair((unsigned int) l, lnk.target));
	}
}

//TODO: Create and attach graphical objects
void CWheel::SetModel(tPage *p)
{

	p_page = p;
	active_index = 0;
	
	//clear target mapping
	target_map.clear();

	if (p != NULL)
	{

		//calculate radius
		R = (p->size() / 2 * (X_SIZE))/2;


		//smg->addCameraSceneNode(smg->getRootSceneNode(),vector3df(0.0, 0.0, CAM_DIST + R), vector3df(0.0, 0.0, 0.0));
		
		ICameraSceneNode *n = smg->getActiveCamera();

		vector3df cam_pos(0.0, 0.0, CAM_DIST + R);
		pos = -cam_pos;
		//n->setPosition(vector3df(0.0, 0.0, CAM_DIST + R));
		//n->addAnimator(smg->createFlyStraightAnimator(n->getPosition(),cam_pos,2000));
		
		
		
		//n->setTarget(vector3df(0.0, 0.0, 0.0));

		//get former y position and construct wheel there
		double old_y = 0.0;

		if (sections.size())
		{
			old_y = (*(sections.begin()))->getPosition().Y;
		}

		//destroy current graphical model 
		for (GSection *s : sections)
			s->remove();

		sections.clear();

		//Avoid division by zero
		if (p_page->size() == 0)
			return;
		//radian distance on unit circle that must be between pages
		rads_per_page = (2 * PI) / p_page->size();

		//start at 90 degrees = "active display"
		double phase = BEG_PHASE;
		int id = 10;

		//iterator over model for link assigning
		auto m_iter = p_page->begin();

		for (tSection & sec: *p_page)
		{
			GSection *s = new GSection(smg->getRootSceneNode(), smg, id);
			s->setPosition(vector3df(cos(phase)*R, old_y, sin(phase)*R));
			s->setMaterialTexture(0, sec.view);

			//add selectable links
			addLinks(m_iter->links, s);
		
			//add section to vector
			sections.push_back(s);
			phase += rads_per_page;

			//advance model iterator to next section
			m_iter++;
		}

		//move the wheel so we see the first one
		for (GSection *sec : sections)
			sec->setPosition(sec->getPosition() + pos);
	}
}

void CWheel::rotateAll(double stop, double vel,double beg_phase)
{ 
	double phase = beg_phase;
	for (GSection *ptr : sections)
	{
		//set it to rotate around R, starting phase according to position
		//rotate until next section position , speed determined
		circleSecAnimator * anim = new circleSecAnimator(pos, R, phase, vel, stop,std::bind(&CWheel::onFinishedAnim,this));
		cout << R << " " << phase << " " << vel << " " << stop << endl;

		//ISceneNodeAnimator *anim = smg->createFlyCircleAnimator(core::vector3df(0, 0, 30), 20.0f);
		ptr->addAnimator(anim);
		

		phase += rads_per_page;
	}


}

//Displace all using animation
void CWheel::animDisplaceAll(vector3df disp,double vel)
{
	//length divided by velocity -> *1000 for miliseconds
	double t = (disp.getLength() / vel) * 1000;

	for (GSection *ptr : sections)
	{
		//create animator, make callback out of this object's member
		ISceneNodeAnimator *anim = new clbkFlyStraightAnimator(ptr->getPosition(),
			ptr->getPosition() + disp, t,smg,std::bind(&CWheel::onFinishedAnim,this));
		
		
		ptr->addAnimator(anim);
		anim->drop();
	}
}

//toggle visibility of all units
void CWheel::toggleVis()
{
	std::for_each(sections.begin(), sections.end(), [](GSection *g)
	{
		g->setVisible(!g->isVisible());
	});
}

//move all units at once
void CWheel::displaceAll(vector3df disp)
{
	std::for_each(sections.begin(), sections.end(), [&disp](GSection *g)
	{
		g->setPosition(g->getPosition() + disp);
	});
}

//Wheel control functions
//only respond in READY state
void CWheel::next()
{
	//no point in rotating empty wheel
	if (!sections.size())
		return;

	//double r = rads_per_page;
	//delayed_shit = [this, r](){ this->rotateAll(rads_per_page, PI / 4.0); cout << "Delayed shit" << endl; };
	if (state == S_READY)
	{
		state = S_TRANSFER;
		cout << BEG_PHASE - active_index*rads_per_page << endl;
		rotateAll(rads_per_page, -ROT_SPEED,BEG_PHASE - active_index*rads_per_page);
		
		//rotation
		active_index++;
		cout << active_index << endl;
		if (active_index == sections.size())
			active_index = 0;

	}

}
void CWheel::previous()
{

	//no point in rotating empty wheel
	if (!sections.size())
		return;

	if (state == S_READY)
	{
		state = S_TRANSFER;
		rotateAll(rads_per_page, +ROT_SPEED, BEG_PHASE - active_index * rads_per_page);

		//rotation
		active_index--;
		if (active_index < 0)
			active_index = sections.size() - 1;
	}

}

void CWheel::open()
{
	
	if (state == S_READY)
	{
		//check if anything is selected
		if (selected != NULL)
		{
			//check if there exists target for selected item
			auto iter = target_map.find((unsigned long) selected);

			if (iter == target_map.end())
			{
				//this should not happen
				throw invalid_argument("Redirect to non existent target");
			}
			else
			{
				//we are redirecting !
				sel_clbk(iter->second);
			}
		}

	}
}

void CWheel::up()
{

}

void CWheel::down()
{

}

//displace during appearing and disappearing
const double DISP = 5 * Y_SIZE;
//velocity for appearing and disappearing
const double D_VEL = 30;

void CWheel::appear(std::function<void()> clbk)
{
	//do something only in S_DISAPPEAR state
	if (state == S_DISAPPEARED)
	{
		//rising animation in progress
		state = S_RISE;
		//let us see
		toggleVis();

		//move them up
		animDisplaceAll(vector3df(0.0, 2 * Y_SIZE, 0.0), 10);
		
		//set recieved callback on finish
		outer_anim_end_callback = clbk;
	}
}


void CWheel::disappear(std::function<void()> clbk)
{
	if (state == S_READY)
	{
		state = S_GOING;
		animDisplaceAll(vector3df(0.0, -2 * Y_SIZE, 0.0), 10);

		outer_anim_end_callback = clbk;
	}
}

void CWheel::onFinishedAnim()
{
	//if state was rising or rotating -> we go to ready
	if (state == S_RISE || state == S_TRANSFER)
	{
		state = S_READY;
		
	}
	else if (state == S_GOING)
	{
		//finished disappearing
		state = S_DISAPPEARED;
		toggleVis();
	}

	//if notification for outside force is desired
	//do it
	if (outer_anim_end_callback)
		outer_anim_end_callback();
}

void CWheel::onCollision(ISceneNode *node)
{

	//WARN -> error handling
	//cast node into gsection
	GSection *sec = dynamic_cast<GSection *>(node);
	
	//hide old link
	if (selected)
		selected->set_color(SColor(50, 0, 255, 0));

	//show new link
	selected = sec;

	if (sec != NULL)
		sec->set_color(SColor(50, 255, 0, 0));
}