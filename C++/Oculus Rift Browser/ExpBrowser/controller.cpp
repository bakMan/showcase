#include <irrlicht.h>
#include <iostream>

#include "controller.h"
#include "voice.h"



Controller::Controller(IrrlichtDevice *dev, std::map<std::string, tPage> *web) :device(dev), data(web)
{
	//Get shortcut pointers to important modules
	driver = device->getVideoDriver();
	smgr = device->getSceneManager();
	guienv = device->getGUIEnvironment();

	//iterator pointing to root page
	auto iter = web->find("root");
	
	//web is empty -> should not happen
	if (iter == web->end())
		throw std::invalid_argument("No root page in web structure");
	
	//add root to backstack
	pg_backstack.push_back("root");

	//if it fails, no sound whateva
	engine = snd::createIrrKlangDevice();

	//play ambience loop
	//engine->play2D("media/sound/ambience.ogg", true);
	//
	//create wheel, callback for redirect is OnRedirect, for some reason
	//i had to create lambda, bind didn't work ...
	wheel = new CWheel(smgr, &(iter->second), [this](std::string s){this->OnRedirect(s); });
	

	//make the wheel show up
	wheel->appear();
}


//Main event loop
bool Controller::OnEvent(const SEvent& event)
{
	if (event.EventType == EET_USER_EVENT)
	{
		switch (event.UserEvent.UserData1)
		{
		case V_HELLO:
			//activation sound
			engine->play2D("media/sound/activating.ogg");
			break;
		case V_LEFT:
			wheel->previous();
			break;
		case V_RIGHT:
			wheel->next();
			break;
		case V_UP:
			wheel->up();
			break;
		case V_DOWN:
			wheel->down();
			break;
		case V_OPEN:
			//loading sound
			engine->play2D("media/sound/loading.ogg");
			wheel->open();
			break;
		case V_BACK:
			//loading sound
			engine->play2D("media/sound/loading.ogg");
			//check if we are not on the bottom of stack
			if (pg_backstack.size() != 1)
			{
				pg_backstack.pop_back();
				OnRedirect(*(pg_backstack.rbegin()));
			}
			break;
		case V_BYE:
			//bye sound
			engine->play2D("media/sound/deactivating.ogg");
			break;
		case V_HOME:
			engine->play2D("media/sound/loading.ogg");
			pg_backstack.clear();
			OnRedirect("root");
			break;
		case V_QUIT:
			device->closeDevice();
			break;
		}

		return true;
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		switch (event.KeyInput.Key)
		{
		case EKEY_CODE::KEY_RIGHT:
			wheel->next();
			break;

		case EKEY_CODE::KEY_LEFT:
			wheel->previous();
			break;
		case EKEY_CODE::KEY_RETURN:
			wheel->open();
			break;
		case EKEY_CODE::KEY_BACK:
			//check if we are not on the bottom of stack
			if (pg_backstack.size() != 1)
			{
				pg_backstack.pop_back();
				OnRedirect(*(pg_backstack.rbegin()));
			}
			break;
		case EKEY_CODE::KEY_HOME:
			pg_backstack.clear();
			OnRedirect("root");
			break;
		}
	}

	return false;
}


void Controller::onCollision(ISceneNode *n)
{
	wheel->onCollision(n);
}

void Controller::OnRedirect(std::string target)
{
	//nicely go down
	//and afterwards change model
	wheel->disappear([this,target]()
	{
		//find new target in the map
		auto iter = this->data->find(target);

		//this should not happen 
		if (iter == this->data->end())
		{
			cerr << "[Warn] Bad link address: " << target << endl;
			return;
		}

		//set new data model
		wheel->SetModel(&(iter->second));
		
		//appear will make it visible
		//if I havent done this, it would toggle it back to invisible
		wheel->toggleVis();

		//add to backstack
		pg_backstack.push_back(target);

		//make it appear
		wheel->appear();
	});
}