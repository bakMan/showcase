#ifndef GMODEL_H
#define GMODEL_H

#include <irrlicht.h>
#include <functional>




using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;

#define X_SIZE 16
#define Y_SIZE 9

//Function for collision detection using ray coming from camera
ISceneNode *detectCameraCollision(ISceneManager *man, ICameraSceneNode *camera,vector3df adjust = vector3df(0.0,0.0,0.0));

/* Class representing visualisation of one section of page wheel
*/
class GSection : public ISceneNode
{
private:
	//BBox required by irrlicht
	core::aabbox3d<f32> Box;
	
	//4 verticles of rectangle
	video::S3DVertex Vertices[4];

	//Material
	video::SMaterial Material;

	double x_size;
	double y_size;

public:
	GSection(scene::ISceneNode* parent, scene::ISceneManager* mgr, s32 id);
	
	//Rendering of element
	void render();
	void OnRegisterSceneNode();


	const core::aabbox3d<f32>& GSection::getBoundingBox() const;
	u32 GSection::getMaterialCount() const;
	video::SMaterial& GSection::getMaterial(u32 i);
	void set_size(double x, double y);
	void set_color(SColor col);
};

/** Class for circular fly animator with limited trajectory
*/
class circleSecAnimator : public ISceneNodeAnimator
{
private:
	vector3df center;
	double rads_per_ms;
	double radius;
	double starting_phase;
	double current_phase;

	double stop;

	bool finished;
	bool first;

	u32 init_time;

	std::function<void(void)> finished_callback;
	
public:
	circleSecAnimator(vector3df c, double r, double sp = 0.0, double vel=1, double s = 2 * PI, std::function<void(void)> clb = std::function<void(void)>()) :
		center(c), radius(r), starting_phase(sp), finished_callback(clb), stop(s),finished(false){
		//convert rads/s na rads/ms
		rads_per_ms = vel / 1000;
		first = true;
	}

	//Node animation code
	void animateNode(ISceneNode *node, u32 timeMs);
	ISceneNodeAnimator* createClone(ISceneNode *node, ISceneManager * newManager = 0);
	bool hasFinished();
};


//Wrapper for fly straight animator, with callback
//Unfortunately I had to do it via composition 
class clbkFlyStraightAnimator : public ISceneNodeAnimator
{
private:
	ISceneNodeAnimator *anim;
	std::function<void()> clbk;

public:

	clbkFlyStraightAnimator(){}

	clbkFlyStraightAnimator(const core::vector3df& startPoint,
		const core::vector3df& endPoint,
		u32 timeForWay,
		ISceneManager *smg,
		std::function<void()> finished_callback = std::function<void()>()
		)
	{
		anim = smg->createFlyStraightAnimator(startPoint, endPoint, timeForWay);
		clbk = finished_callback;
	}

	void animateNode(ISceneNode *node, u32 timeMs)
	{
		anim->animateNode(node, timeMs);
		
		if (anim->hasFinished() && clbk)
		{
			clbk();
			//autoremove yourself
			node->removeAnimator(this);
		}
	}

	ISceneNodeAnimator* createClone(ISceneNode *node, ISceneManager * newManager = 0)
	{
		clbkFlyStraightAnimator *a = new clbkFlyStraightAnimator();
		a->anim = anim;
		a->clbk = clbk;
		return a;
	}

	bool hasFinished()
	{
		return anim->hasFinished();
	}
};


#endif