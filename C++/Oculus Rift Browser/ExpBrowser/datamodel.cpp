
#include "datamodel.h"
#include "gmodel.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include <libjson.h>

namespace fs = boost::filesystem;

std::map<std::string, tPage> *readSimWeb(std::string str,IVideoDriver *vid)
{
	fs::path dir(str.c_str());

	if (!fs::exists(dir))
		throw LoadException("Invalid directory");

	if (!fs::is_directory(dir))
		throw LoadException("Not a directory");

	vector<fs::directory_entry> contents;

	copy(fs::directory_iterator(dir), fs::directory_iterator(), back_inserter(contents));

	//All files in it must be directories
	bool check = all_of(contents.begin(), contents.end(), [](fs::directory_entry &d){
		return fs::is_directory(d.path());
	});

	if (!check)
		throw LoadException("Root web directory contains non-directories");

	std::map<std::string, tPage> *out = new std::map<std::string, tPage>();

	tPage root;

	//scan files in every directory
	for (fs::directory_entry &d : contents)
	{
		tPage pg;
		
		//read subdirectory
		vector<fs::directory_entry> subcon;

		copy(fs::directory_iterator(d.path()), fs::directory_iterator(), back_inserter(subcon));

		//iterate over subdirectory
		for (fs::directory_entry &el : subcon)
		{
			std::string stem = el.path().filename().stem().string();
			std::string extension = el.path().filename().extension().string();

			//only json and jpg
			if (extension == ".jpg" || extension == ".jpeg")
			{
				//create new section
				tSection section;
				
				//load image
				section.view = vid->getTexture(el.path().string().c_str());
				
				//image size
				double img_x = section.view->getSize().Width;
				double img_y = section.view->getSize().Height;

				
				//get id
				section.id = atoi(stem.c_str());

				//find corresponding json and parse it
				auto iter = std::find_if(subcon.begin(), subcon.end(), [stem](fs::directory_entry &de){
					//filename + json extension
					return de.path().filename().string() == stem + ".json";
				});

				//not JSON ! Fatal
				if (iter == subcon.end())
					throw LoadException("Unable to find corresponding JSON file");


				//make the link file out of json
				vector<tLink> links;
				try
				{
					links = readLinksFromJSON(iter->path().string());
				}
				catch (invalid_argument &e)
				{
					std::string s;
					s.append("Error while parsing ");
					s.append(iter->path().string());
					s.append(": ");
					s.append(e.what());
					throw LoadException(s.c_str());
				}
				
				//recalculate link coordinates to match ours
				for_each(links.begin(), links.end(), [img_x, img_y](tLink &l)
				{
					l.brx = (l.brx / img_x) * X_SIZE;
					l.bry = (l.bry / img_y) * Y_SIZE;

					l.tlx = (l.tlx / img_x) * X_SIZE;
					l.tly = (l.tly / img_y) * Y_SIZE;

				});

				section.links = links;

				pg.push_back(section);
			}
			else if (extension != ".json")
			{
				cerr << "[Warn] web contains unsupported file: " << el.path().string() << endl;
			}
			
		}

		//SECTIONS MUST BE SORTED according to id
		sort(pg.begin(), pg.end(), [](tSection &a, tSection &b){return a.id < b.id; });
		
		//add first section to preview page
		root.push_back(*(pg.begin()));

		//add link covering whole page to root page
		tLink lnk;
		lnk.brx = X_SIZE;
		lnk.bry = Y_SIZE;
		lnk.tlx = 0.0;
		lnk.tly = 0.0;

		lnk.target = d.path().stem().string();
		vector<tLink> lnks;
		lnks.push_back(lnk);
		root.rbegin()->links = lnks;

		//add page to map
		out->insert(std::pair<std::string, tPage>(d.path().stem().string(), pg));
		

	}

	//insert default root page if explicit was not provided
	if (out->find("root") == out->end())
		out->insert(std::pair<std::string, tPage>("root",root));

	return out;

}



vector<tLink> readLinksFromJSON(std::string path)
{
	
	std::ifstream t(path);

	if (!t.is_open())
		throw invalid_argument("Unable to open json file");

	std::string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());
	
	vector<tLink> links;
	JSONNode root = libjson::parse(str);

	cout << "Here" << endl;

	//error in parsing
	if (root.size() == 0)
	{
		//empty json == no links
		return links;
	}
	if (root.size() != 1)
	{
		cerr << "[Warn] Wrong json format: " << path << endl;
		return links;
	}

	JSONNode child = *(root.begin());

	if (child.name() != "links")
	{
		cerr << "[Warn] Json does not contain links array: " << path << endl;
		return links;
	}

	//look through link array
	for (JSONNode el : child)
	{
		
		tLink lnk;
		lnk.tlx = el["tlx"].as_int();
		lnk.tly = el["tly"].as_int();

		lnk.brx = el["brx"].as_int();
		lnk.bry = el["bry"].as_int();

		lnk.target = el["address"].as_string();
		links.push_back(lnk);
	}

	return links;
	

}