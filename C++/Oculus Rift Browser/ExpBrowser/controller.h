#ifndef EVENTS_H
#define EVENTS_H

#include <irrlicht.h>
#include <irrKlang.h>
#include "CWheel.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;


using namespace std;
namespace snd = irrklang;

/** Main controller of the application
* Listens to IrrLicht events and responds accordingly
*/
class Controller : public IEventReceiver
{
	private:
		IrrlichtDevice *device;
		IVideoDriver* driver;
		ISceneManager* smgr;
		IGUIEnvironment* guienv;
		snd::ISoundEngine* engine;

		//Wheel object controller
		CWheel *wheel;
		std::map<std::string, tPage> *data;

		//Backstack of visited pages
		vector<std::string> pg_backstack;
		
	public:
		bool OnEvent(const SEvent& event);
		
		//Callback for opening new link
		void OnRedirect(std::string target);

		Controller(IrrlichtDevice *dev, std::map<std::string, tPage> *web);
		
		//collision detection callback, just redirects info to wheel
		void onCollision(ISceneNode *n);
};

#endif