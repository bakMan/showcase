#include <irrlicht.h>
#include <functional>
#include <iostream>

#include "gmodel.h"

using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;
using namespace std;



GSection::GSection(scene::ISceneNode* parent, scene::ISceneManager* mgr, s32 id)
: scene::ISceneNode(parent, mgr, id)
{
	Material.Wireframe = false;
	Material.Lighting = false;

	//set size of box
	set_size(X_SIZE, Y_SIZE);

}

void GSection::set_size(double x, double y)
{
	x_size = x;
	y_size = y;

	Vertices[0] = video::S3DVertex(-x_size / 2, -y_size / 2, 0, 0, 0, -1,
		video::SColor(100, 255, 255, 255), 1, 1);
	Vertices[1] = video::S3DVertex(x_size / 2, -y_size / 2, 0, 0, 0, -1,
		video::SColor(100, 255, 255, 255), 0, 1);
	Vertices[2] = video::S3DVertex(-x_size / 2, y_size / 2, 0, 0, 0, -1,
		video::SColor(100, 255, 255, 255), 1, 0);
	Vertices[3] = video::S3DVertex(x_size / 2, y_size / 2, 0, 0, 0, -1,
		video::SColor(100, 255, 255, 255), 0, 0);

	//set the bbox
	Box.reset(Vertices[0].Pos);
	for (s32 i = 1; i<4; ++i)
		Box.addInternalPoint(Vertices[i].Pos);
}

//set color of all verticles
void GSection::set_color(SColor col)
{
	for (int i = 0; i < 4; i++)
	{
		Vertices[i].Color = col;
	}
}

//On registration to rendering
void GSection::OnRegisterSceneNode()
{
	//register for rendering
	if (IsVisible)
		SceneManager->registerNodeForRendering(this);

	ISceneNode::OnRegisterSceneNode();
}

//Rendering itself
void GSection::render()
{
	//Building a rectangle out of two triangles
	u16 indices[] = { 0,1,2,1,3,2};
	video::IVideoDriver* driver = SceneManager->getVideoDriver();

	driver->setMaterial(Material);
	driver->setTransform(video::ETS_WORLD, AbsoluteTransformation);
	driver->drawVertexPrimitiveList(&Vertices[0], 4, &indices[0], 2, video::EVT_STANDARD, scene::EPT_TRIANGLES, video::EIT_16BIT);

}

const core::aabbox3d<f32>& GSection::getBoundingBox() const
{
	return Box;
}

u32 GSection::getMaterialCount() const
{
	return 1;
}

video::SMaterial& GSection::getMaterial(u32 i)
{
	return Material;
}


//Animator
void circleSecAnimator::animateNode(ISceneNode *node, u32 timeMs)
{
	//Ta da da daa ... time was not what you thought
	if (first)
	{
		init_time = timeMs;
		first = false;
	}

	if (node == 0)
		return;

	u32 time = timeMs - init_time;
	current_phase = rads_per_ms * time;

	//If we are at the end position or further -> finish
	if (abs(current_phase) >= stop)
	{
		//current_phase = stop;
		finished = true;
	}

	double phase = starting_phase + current_phase;

	node->setPosition(center + vector3df(cos(phase)*radius, 0.0, sin(phase)*radius));

	if (finished && finished_callback)
	{
		node->removeAnimator(this);
		finished_callback();
	}
}

ISceneNodeAnimator* circleSecAnimator::createClone(ISceneNode *node, ISceneManager * newManager)
{
	circleSecAnimator *anim = new circleSecAnimator(center, radius, starting_phase, rads_per_ms * 1000, stop, finished_callback);
	return anim;
}

bool circleSecAnimator::hasFinished()
{
	return finished;
}

//Performs collision detection on scene from coordinates of camera
ISceneNode *detectCameraCollision(ISceneManager *man,ICameraSceneNode *camera,vector3df adjust)
{
	//detect collisions
	core::line3d<f32> ray;
	ISceneCollisionManager *collMan = man->getSceneCollisionManager();

	//calculate collision line using camera coordinates
	ray.start = adjust + camera->getPosition();
	ray.end = ray.start + (adjust + camera->getTarget() - ray.start).normalize() * 1000.0f;

	// Tracks the current intersection point with the level or a mesh
	core::vector3df intersection;
	// Used to show with triangle has been hit
	core::triangle3df hitTriangle;


	scene::ISceneNode * selectedSceneNode =
		collMan->getSceneNodeAndCollisionPointFromRay(
		ray,
		intersection, // This will be the position of the collision
		hitTriangle, // This will be the triangle hit in the collision
		0,
		0); // Check the entire scene (this is actually the implicit default)
	return selectedSceneNode;
}