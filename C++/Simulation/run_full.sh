#! /bin/bash

rm -r -f doc/week
rm -r -f doc/weekend

echo -n "Running weekday: "
./run.sh 1000 trains.json 
if [[ "$?" != 0 ]]; then
	echo "FAIL\n"
	exit 1
fi
mv experiments doc/week
echo "OK\n"

echo -n "Running weekend: "
./run.sh 1000 trains_weekend.json
if [[ "$?" != 0 ]]; then
	echo "FAIL\n"
	exit 1
fi
mv experiments doc/weekend
echo "OK\n"

echo -n "Concatenate tables: "
cd doc/week
rm -f -r ../tables
mkdir ../tables
for file in *; do
	paste -d '&' "$file/run.log" "../weekend/$file/run.log" | sed 's/$/\\\\/g' > "../tables/$file.tex"
done
echo "OK\n"