#ifndef TRAIN_H
#define TRAIN_H

#include <vector>
#include <string>
#include <exception>
#include <stdexcept>
#include "simlib.h"

using namespace std;

class JSONException: public runtime_error 
{ 
	public: 
		JSONException(string m="exception!") : std::runtime_error(m){}
};


class Train
{
private:
	int id;
	std::vector<int> arrivals;
	std::vector<int> departures;
	string name;
	Stat *average_time;


public:
	//DEBUG
	string type;

	Train(){average_time = new Stat();}
	Train(int id,int arrival, int deperature,string name);
	Train(int id,int arrival,string name);
	void addArrival(int arriv);
	void addDeparture(int dep);
	void setName(string name);
	void setId(int id);
	string dumpToStr();
	vector<int> getArrivals(){return arrivals;}
	vector<int> getDepartures  (){return departures;}
	int debugGetTime();
	int getId(){return id;}
	string getName(){return name;}
	void recordTime(unsigned int time);
	void dumpStat(){average_time->Output();}
	double getStatMean(){return average_time->MeanValue();}

	
};

class Route
{
private:
	int id;
	std::vector<Train*> trains;

public:

	 Route(int i):id(i){};
	 void addTrain(Train *t);
	 std::vector<Train*> getTrains(){return trains;}
	 int getId(){return id;}
	 ~Route();

	
};


typedef struct trainData
{
	vector<Train*> train_list;
	std::vector<Route> route_list;

} t_trainData;



/** Function parses file describing trains in json format and returns data structure of trains
*/
t_trainData parseJsonTrains(string input_file);
bool trainListStatComparator(Train *t1, Train *t2);

#endif
