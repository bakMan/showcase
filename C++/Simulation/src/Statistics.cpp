#include "simlib.h"
#include "Statistics.h"
#include <iostream>

using namespace std;

StatsPerPeriod::StatsPerPeriod(int p,unsigned int n_pers):period(p), n_periods(n_pers)
{
	stats_arr = new Stat[n_pers];
}

StatsPerPeriod::~StatsPerPeriod()
{
	delete stats_arr;
}

void StatsPerPeriod::addData(int data,int time)
{

	int index = time / period;
	//time is past all bins
	if (index >= n_periods)
	{
		return;
	}


	
	//if(index == 1)
	//	cout << data <<endl;
	//Stat x(time) - operator () overload
	(stats_arr[index])(data);
}

void StatsPerPeriod::OutputDetailed()
{
	Print("------------------------- ");
	Print("- Hourly detailed stats - ");
	Print("------------------------- ");

	for (int i = 0; i < n_periods; ++i)
	{
		stats_arr[i].Output();
	}
}

Stat * StatsPerPeriod::getStatArr()
{
	return stats_arr;
}

void StatsPerPeriod::operator()(int data,int time)
{
	addData(data,time);
}

void StatsPerPeriod::operator()(double data,double time)
{
	addData((int) data,(int) time);
}

void StatsPerPeriod::Output()
{
	for (int i = 0; i < n_periods; ++i)
	{
		if(stats_arr[i].Number() != 0)
		{
			cout << i << ":00-" << i+1 << ":00 " << stats_arr[i].MeanValue() << endl;
		}
	}
}