///////////////////////////////////////////////////////////////////////////////
//  Authors:   Viliam Serecun, xserec00@stud.fit.vutbr.cz                    //
//             Stanislav Smatana, xsmata01@stud.fit.vutbr.cz                 //
//  Name:      SimTrain.cpp                                                  //
//  Subject:   IMS                                                           //
//  Project:   Simulation railway station                                    //
//  Date:      06.12.2013                                                    //
//  Compiler:  gcc version 4.8.2 20131017 (Red Hat 4.8.2-1) (GCC)            //
//  Version:   1.0                                                           //
//  Notes:                                                                   //
///////////////////////////////////////////////////////////////////////////////

#include "Train.h"
#include "SimTrain.h"
#include "Statistics.h"
#include "simlib.h"
#include <cmath>
#include <iostream>

#define ONE_R 4
#define PLATFORM_R  6
#define H_RAIL 4

Store One_rails("rails 5k 9k 11k 13k", ONE_R);         // rails to platform 5,9,11,13 k
Store H_rails("rails 1a 2a 4a 3b", H_RAIL);            // rails from Herstice direction
Store Platform_rails("rails 1 2 3 4 5 6", PLATFORM_R); // rails next to platforms
Facility Crossover_One("Crossover One_rails");         // crossover to/from One_rails
Facility Crossover_P("Crossover rails 1 2 3 4 5 6");   // crossover to/from platform 1 2 3 4 5 6
Facility Crossover_Z("Crossover rails 1t 2t");   // crossover to rails 2t and 1t
Facility Rail_inoutZ("Rail 2t");                 // output rail 2t
Facility Rail_inZ("Rail 1t");                    // input rail 1t
// queue definitions
Queue Hb_Input;
Queue Hz_Input;
Queue Bh_Input;
Queue Zh_Input;
Queue B_Platform_Out;
Queue Z_Platform_Out;


//Histogram Processing("Zdrzanie vlakov na stanici v hodinovych intervaloch",0,60,120);

//Hourly time spent in system statistics
StatsPerPeriod TimeStat(3600,24);

int trains_left = 0;
int train_one = 0;

void incOne(unsigned int t)
{
	if(t < 3600 * 2 && t >= 3600)
	{
		train_one ++;
	}
}

using namespace std;

int getTrainNum(){return trains_left;}
void zeroTrains(){trains_left = 0;}

void dumpQueues()
{
	Queue *qarr[] ={&Hb_Input,&Hz_Input,&Bh_Input,&Zh_Input,&B_Platform_Out,&Z_Platform_Out}; 
	string qstrs[] = {"Herspice -> hlavas","Herspice -> Zidenice","Hlavas -> Herspice", "Zidenice -> Herspice","B Platform","Z_Platform_Out"};
	
	for (int i = 0; i < 6; ++i)
	{
		Queue *q = qarr[i];
		Print(qstrs[i].c_str());
		Print("\n");
		q->Output();
		Print("-------------------------------------------------------\n");

		for (Queue::iterator i = q->begin(); i != q->end(); ++i)
		{
			SimTrain *train = (SimTrain *) *i;
			train->dump();
		}
		Print("-------------------------------------------------------\n");
	}

}

void dumpFacilities()
{
	Facility *facs[] = { &Crossover_One ,&Crossover_P ,&Crossover_Z ,&Rail_inoutZ ,&Rail_inZ};
	Store *stores[] = { &One_rails,&H_rails, &Platform_rails};
	string stornames[] = { "&One_rails","&H_rails", "&Platform_rails"};
	string fstrs[] = { "Crossover_One" ,"Crossover_P" ,"Crossover_Z" ,"Rail_inoutZ" ,"Rail_inZ"};

	for (int i = 0; i < 5; ++i)
	{
		facs[i]->Output();
	}

	for (int i = 0; i < 3; ++i)
	{
		stores[i]->Output();
	}

	
}



void zhTrain::Behavior()
{
	enter_time = Time;
	p_data->type = "ZH";
	//cout << "Train zh " << p_data->getId() << " entering " << Time << endl;
	ZH_ASSIGN_SOURCE:
		// rail 1t is free and rail 2t is not
		if (Rail_inoutZ.Busy() && !Rail_inZ.Busy())
			goto ZH_DIRECTION_1T;
		// rail 2t is free and 1t is not
		else if (!Rail_inoutZ.Busy() && !Platform_rails.Full() && Rail_inZ.Busy())
			goto ZH_DIRECTION_2T;
		// both rails are free
		else if (!Rail_inoutZ.Busy() && !Platform_rails.Full() && !Rail_inZ.Busy())
			if (Random() <= 0.7)
				goto ZH_DIRECTION_1T; // 1t
			else 
				goto ZH_DIRECTION_2T; //2t
		// both rails are full, train must wait for sources in queue
		else {
			Zh_Input.Insert(this); // add train into queue
			Passivate();           // passivate train
			goto ZH_ASSIGN_SOURCE; // try again to assign sources
		}
	
	ZH_DIRECTION_1T:
		Seize(Rail_inZ);    // arrive to rail 1t
		Enter(Platform_rails,1); // reserve platform rail
		//MODIFY
		Wait(genZDelay());
		//END_MODIFY
		Seize(Crossover_Z); // waiting for crossover to platform
		Release(Rail_inZ);  // train is out from rail 1t

		// activate train whitch is waiting from direction Zidenice
		if (Zh_Input.Length()>0)
			(Zh_Input.GetFirst())->Activate();

		//MODIFY
		Wait(genZCrossoverDelay());
		//END_MODIFY
		Release(Crossover_Z); // Crossover from/to direction Zidenice

		// activate train whitch is waiting from platform to Zidenice
		if (B_Platform_Out.Length() > 0)
			(B_Platform_Out.GetFirst())->Activate();

		// activate train whitch is waiting from platform to Zidenice
		if (Z_Platform_Out.Length() > 0)
			(Z_Platform_Out.GetFirst())->Activate();


		
		Wait(genPlatformDelay());

		Enter(H_rails, 1); // reservation rail to leave system
		Seize(Crossover_P); // enter to part of rail where is crossover
		
		Wait(genPCrossoverDelay());

		Leave(Platform_rails, 1); // platform 1 2 3 4 5 6 is free

		// activate train whitch is waiting from direction Zidenice
		if (Zh_Input.Length()>0)
			(Zh_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length()>0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length()>0)
			(Hz_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Brno to Herspice
		if (Bh_Input.Length()>0)
			(Bh_Input.GetFirst())->Activate();
		
		Release(Crossover_P); // leave crossover to/from platform
		
		Wait(genHDelay());

		Leave(H_rails, 1); // tain is out of system

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length()>0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length()>0)
			(Hz_Input.GetFirst())->Activate();

		goto ZH_END;


	ZH_DIRECTION_2T:
		Seize(Rail_inoutZ);       // reservation rail 2t
		Enter(Platform_rails, 1); // reserrvation platform
		//MODIFY
		Wait(genZDelay());
		//END_MODIFY
		Seize(Crossover_Z);       // waiting for crossover to platform
		Release(Rail_inoutZ);     // train is out from rail 2t

		// activate train whitch is waiting from direction Zidenice
		if (Zh_Input.Length()>0)
			(Zh_Input.GetFirst())->Activate();

		// activate train whitch is waiting from platform to Zidenice
		if (B_Platform_Out.Length() > 0)
			(B_Platform_Out.GetFirst())->Activate();

		// activate train whitch is waiting from platform to Zidenice
		if (Z_Platform_Out.Length() > 0)
			(Z_Platform_Out.GetFirst())->Activate();

		//MODIFY
		Wait(genZCrossoverDelay());
		//END_MODIFY
		Release(Crossover_Z);

		// activate train whitch is waiting from platform to Zidenice
		if (B_Platform_Out.Length() > 0)
			(B_Platform_Out.GetFirst())->Activate();

		// activate train whitch is waiting from platform to Zidenice
		if (Z_Platform_Out.Length() > 0)
			(Z_Platform_Out.GetFirst())->Activate();

		
		Wait(genPlatformDelay());

		Enter(H_rails, 1); // reservation rail for output system
		Seize(Crossover_P); // witing for crossover to rails 1a 2a 4a 3b

		Wait(genPCrossoverDelay());

		Leave(Platform_rails, 1);

		// activate train whitch is waiting from direction Zidenice
		if (Zh_Input.Length()>0)
			(Zh_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length()>0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length()>0)
			(Hz_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Brno to Herspice
		if (Bh_Input.Length()>0)
			(Bh_Input.GetFirst())->Activate();
		
		Release(Crossover_P); // leave crossover to/from platform

		Wait(genHDelay());
		Leave(H_rails, 1); // tain is out of system

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length()>0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length()>0)
			(Hz_Input.GetFirst())->Activate();

	ZH_END:
		//leave
		//cout << "Train zh " << p_data->getId() << " leaving " << Time << endl;
		trains_left++;

		TimeStat(Time - enter_time,(double) Time);

		p_data->recordTime(Time - enter_time);
		incOne(Time);
		return;
}

void bhTrain::Behavior()
{
	enter_time = Time;
	p_data->type = "BH";
	BH_ALLOCATE_RAILS:
		//if One_rails are empty an platform is Full -> go for one
		if(Platform_rails.Full() && !One_rails.Full())
			goto BH_ONE_RAIL;
		//One_rails are Full but platform is avalible -> go there
		else if(!Platform_rails.Full() && One_rails.Full())
			goto BH_PLATFORM_RAIL;
		// both platform and one rails are avalible -> let's go by chance
		else if(!Platform_rails.Full() && !One_rails.Full())
			if(Random() <= 0.7)
				goto BH_ONE_RAIL;
			else
				goto BH_PLATFORM_RAIL;
		else {
			Bh_Input.Insert(this);
			Passivate();
			goto BH_ALLOCATE_RAILS;
		}

	BH_ONE_RAIL:
		Enter(One_rails, 1); // train on rail of platform 5k/9k/11k/13k
		
		Wait(genOneRailDelay());
		Enter(H_rails, 1); // reservation rail for out system
		//crossover from one rails

		Seize(Crossover_One); // waiting for crossover to exit 
		Wait(genOneCrossoverDelay());

		Leave(One_rails, 1); // one rail platform is free

		// activate train whitch is waiting from direction Brno to Herspice
		if (Bh_Input.Length() > 0)
			(Bh_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Brno
		if (Hb_Input.Length() > 0) {
			(Hb_Input.GetFirst())->Activate();
		}

		Release(Crossover_One); // crossover from/to one-rails is free
		Wait(genHDelay());

		// activate train whitch is waiting from direction Herspice to Brno
		if (Hb_Input.Length() > 0) {
			(Hb_Input.GetFirst())->Activate();
		}

		Leave(H_rails, 1); // exit system

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length()>0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length()>0)
			(Hz_Input.GetFirst())->Activate();

		goto BH_END;

	BH_PLATFORM_RAIL:
		Enter(Platform_rails, 1); // waiting form rail and try to reservate it
	
		Wait(genPlatformDelay());

		Enter(H_rails, 1); // reservation outing rail 
		Seize(Crossover_P); // reservation crossover to H-rails
		
		Wait(genPCrossoverDelay());
		Leave(Platform_rails, 1);

		// activate train whitch is waiting from direction Herspice to Brno
		if(Hb_Input.Length() > 0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if(Hz_Input.Length() > 0)
			(Hz_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Zidenice to Herspice
		if(Zh_Input.Length() > 0)
			(Zh_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Brno to Herspice
		if (Bh_Input.Length()>0)
			(Bh_Input.GetFirst())->Activate();

		Release(Crossover_P); // crossover from/to platform 1,2,3,4,5,6 is free
		Wait(genHDelay());
		Leave(H_rails, 1); // exit system

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length()>0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length()>0)
			(Hz_Input.GetFirst())->Activate();
		

	BH_END:
		//leave
		//cout << "Train  bh leaving" << endl;
		trains_left++;

		TimeStat(Time - enter_time,(double) Time);
		p_data->recordTime(Time - enter_time);
		incOne(Time);
}

void hbTrain::Behavior()
{
	enter_time = Time;
	p_data->type = "HB";
	// assign source for enter to system
	HB_ASSIGN_SOURCE:
		if (!One_rails.Full() && !Crossover_One.Busy() && !H_rails.Full())	
			goto HB_PLATFORM_ONE;
		else if (!H_rails.Full() && !Platform_rails.Full())
			goto HB_PLATFORM_TWO;
		else {
			Hb_Input.Insert(this); // push into wait queue
			Passivate(); // passivate process
			goto HB_ASSIGN_SOURCE; // try to assign sources again 
		}

	// platforms for one rails
	HB_PLATFORM_ONE:
		Enter(H_rails,1); // reservation rail 1a, 2a, 4a, 3b
		Enter(One_rails,1); // reservation rail next to platform on part of main station Nove sady 
		//MODIFY
		Wait(genHDelay());
		//END_MODIFY
		//TODO Possible bug
		Seize(Crossover_One); // reservation crossover to rail 5k, 9k, 11k, 13k
		Leave(H_rails, 1); // train is on crossover

		// activate train whitch is waiting from direction Herspice to Brno
		if (Hb_Input.Length()>0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length() > 0)
			(Hz_Input.GetFirst())->Activate();

		//MODIFY
		Wait(genOneCrossoverDelay());
		//END_MODIFY
		Release(Crossover_One); // train is on platform

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length()>0) {
			(Hb_Input.GetFirst())->Activate();
		}

		
		Wait(genOneRailDelay());

		Leave(One_rails, 1); // rail 5k/9k/11k/13k is free

		// activate train whitch is waiting from direction Herspice to Brno
		if (Hb_Input.Length()>0) 
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Brno to Herspice
		if (Bh_Input.Length()>0)
			(Bh_Input.GetFirst())->Activate();

		goto HB_END;

	// platforms for rails 1 2 3 4 5 6
	HB_PLATFORM_TWO:
		Enter(H_rails, 1); // reservation rail 1a, 2a, 3b, 4a
		Enter(Platform_rails, 1); // reservation rail 1/2/3/4/5/6 next to platform 
		//MODIFY
		Wait(genHDelay());
		//END_MODIFY
		//ERROR: Here Leave H rails should be earlier in my opinion
		Seize(Crossover_P); // witing for crossover to rail 1/2/3/4/5/6
		// going through crossover
		//MODIFY
		Wait(genPCrossoverDelay());
		//END_MODIFY
		Release(Crossover_P); // train is on platform, crossover is free
		Leave(H_rails, 1); // rail 1a, 2a, 3b, 4a is free

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length() > 0)
			(Hb_Input.GetFirst())->Activate();
		
		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length() > 0)
			(Hz_Input.GetFirst())->Activate();

		Wait(genPlatformDelay());

		HB_ASSIGN_SOURCE_OUT:
			// if we have sorces to leave system we leave it
			if (!Crossover_Z.Busy() && !Rail_inoutZ.Busy()) {
				Seize(Crossover_Z);
				Seize(Rail_inoutZ);
			} else { // waiting for sources
				B_Platform_Out.Insert(this);
				Passivate();
				goto HB_ASSIGN_SOURCE_OUT;
			}

		
		Wait(genZCrossoverDelay());
		Leave(Platform_rails, 1);

		// activate train whitch is waiting from direction Herspice to Brno
		if (Hb_Input.Length() > 0)
			(Hb_Input.GetFirst())->Activate();
		
		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length() > 0)
			(Hz_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Zidenice to Herspice
		if (Zh_Input.Length() > 0)
			(Zh_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Brno to Herspice
		if (Bh_Input.Length()>0)
			(Bh_Input.GetFirst())->Activate();


		Release(Crossover_Z); // crossocer from/to Zidenice is free

		// activate train whitch is waiting from direction Herspice
		if (B_Platform_Out.Length() > 0)
			(B_Platform_Out.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice
		if (Z_Platform_Out.Length() > 0)
			(Z_Platform_Out.GetFirst())->Activate();
		
		Wait(genZDelay());
		Release(Rail_inoutZ); // rail 2t is free, train is out of system

		// activate train whitch is waiting from direction Herspice
		if (B_Platform_Out.Length() > 0)
			(B_Platform_Out.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice
		if (Z_Platform_Out.Length() > 0)
			(Z_Platform_Out.GetFirst())->Activate();

		// activate train whitch is waiting from direction Zidenice to Herspice
		if (Zh_Input.Length() > 0)
			(Zh_Input.GetFirst())->Activate();

	HB_END:
	//cout << "Train  hb leaving" << endl;
	trains_left++;

	TimeStat(Time - enter_time,(double) Time);
	p_data->recordTime(Time - enter_time);
	incOne(Time);
		return;
}

void hbzTrain::Behavior()
{
	p_data->type = "HBZ";
	enter_time = Time;
	ALLOCATE_RAILS:
		if(!H_rails.Full() && !Platform_rails.Full()) {
			Enter(H_rails, 1); // reservation rail 1a/2a/3b/4a
			Enter(Platform_rails, 1); // reservation platform 1/2/3/4/5/6
		} else {
			Hz_Input.Insert(this);
			Passivate();
			goto ALLOCATE_RAILS;
		}

	//MODIFY
	Wait(genHDelay());
	//END_MODIFY
	Seize(Crossover_P); // waiting for crossover to rail 1/2/3/4/5/6 next to platforms
	// Goes to platform -> release in rails and crossover
	Leave(H_rails, 1);  // leave rail 1a/2a/3b/4a

	// activate train whitch is waing from direction from Herspice to Brno
	if(Hb_Input.Length() > 0)
		(Hb_Input.GetFirst())->Activate();

	// activate train whitch is waing from direction from Herspice to Zidenice
	if(Hz_Input.Length() > 0)
		(Hz_Input.GetFirst())->Activate();

	//MODIFY
	Wait(genPCrossoverDelay());
	//END_MODIFY
	Release(Crossover_P); // crossover from/to rail 1/2/3/4/5/6 is free

	Wait(genPlatformDelay());

	ALLOCATE_LEAVE_RES:
		if(!Crossover_Z.Busy() && !Rail_inoutZ.Busy()) {
			Seize(Crossover_Z);
			Seize(Rail_inoutZ);
		} else {	
			Z_Platform_Out.Insert(this);
			Passivate();
			goto ALLOCATE_LEAVE_RES;
		}

	
	Wait(genZCrossoverDelay());
	Release(Crossover_Z);

	// activate train whitch is waiting from direction Herspice to Zidenice
	if(Z_Platform_Out.Length() > 0)
		(Z_Platform_Out.GetFirst())->Activate();

	// activate train whitch is waiting from direction Herspice to Brno
	if(B_Platform_Out.Length() > 0)
		(B_Platform_Out.GetFirst())->Activate();

	Leave(Platform_rails,1);

	// activate train whitch is waiting from direction Zidenice
		if (Zh_Input.Length()>0)
			(Zh_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice
		if (Hb_Input.Length()>0)
			(Hb_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Herspice to Zidenice
		if (Hz_Input.Length()>0)
			(Hz_Input.GetFirst())->Activate();

		// activate train whitch is waiting from direction Brno to Herspice
		if (Bh_Input.Length()>0)
			(Bh_Input.GetFirst())->Activate();


	Wait(genZDelay());
	Release(Rail_inoutZ);

	// activate train whitch is waiting from direction Herspice to Zidenice
	if(Z_Platform_Out.Length() > 0)
		(Z_Platform_Out.GetFirst())->Activate();

	// activate train whitch is waiting from direction Herspice to Brno
	if(B_Platform_Out.Length() > 0)
		(B_Platform_Out.GetFirst())->Activate();

	// activate train whitch is waiting from direction Zidenice to Herspice
		if (Zh_Input.Length() > 0)
			(Zh_Input.GetFirst())->Activate();

	//Leave system -> write to histogram
	//cout << "Train  hbz leaving" << endl;
	trains_left++;
	TimeStat(Time - enter_time,(double) Time);
	p_data->recordTime(Time - enter_time);
	incOne(Time);
}


/******************************  GENERATORS  *********************************/
void genTrain::Behavior()
{
	p_train->Activate();
}

//Function creates appropriate train process type based on route and arrival/departure times
SimTrain *createSimTrain(Train* p_train,Route &r)
{
	int departure = -1;
	int arrival = -1;

	//well data shown there is always only one dep or arrival
	//nevertheless we are prepared for everyhing
	if((p_train->getDepartures()).size() != 0)
		departure = (p_train->getDepartures()).at(0);

	if((p_train->getArrivals()).size() != 0)
		arrival = (p_train->getArrivals()).at(0);

	switch(r.getId())
	{
		case 240:
		case 244:
		case 300:
		case 340:
			//based on arrival or departure hb or bh
			if(arrival != -1)
				return new hbTrain(p_train);
			else 
				return new bhTrain(p_train);
		break;
		
		
		case 250:
			//if only arrival zh
			if(departure == -1 && arrival != -1)
			{
				return new zhTrain(p_train);
			}
			else if (departure != -1 && arrival == -1)
			{
				return new hbzTrain(p_train);
			}
			else if (departure != -1 && arrival != -1)
			{
				//random
				double guess = Random();
				if(guess > 0.5)
					return new zhTrain(p_train);
				else
					return new hbzTrain(p_train);


			}
			else
			{
				//something wrong
				cerr << "Train " << p_train->getId() << "has wrong routing on route 250" << endl;
			}

		break;
		case 260:
			//always zh
			return new zhTrain(p_train);

		break;

	}
	return NULL;
}

//Function scheuldes trains to simulation schedule
void scheduleTrains(vector<Route> &routes)
{
	for (std::vector<Route>::iterator i = routes.begin(); i != routes.end(); ++i)
	{
		
		Route route = (*i);
		std::vector<Train*> vec = route.getTrains();

		for (std::vector<Train*>::iterator i = vec.begin(); i != vec.end(); ++i)
		{
			Train *p_data = *i;
			SimTrain *train = createSimTrain(p_data,route);

			if(train == NULL)
			{
				cerr << "Train with wrong route " << endl;
				continue;
			}
			int time = p_data->debugGetTime() + genDelay();
			time = time < 0 ? 0 : time; // if negative delay goes past this day -> ignore
			(new genTrain(train)) -> Activate(time);
		}
	}

}

int genDelay()
{


	double d_type = Random();
	double positive_mu  = 6.49763;
	double negative_mu  = 3.73585;

	//90 % chance of positive delay
	if(d_type <= 0.9)
	{
		return (int) round(Exponential(positive_mu) * 60);
	}
	else
	{
		return -1 * ((int) round(Exponential(negative_mu) * 60));	
	}
		


	/*
	double sigma = 19.3057;
	double mu = 2.17658;
	return round(Normal(mu,sigma)) * 60;
	*/
	
}

//TODO: Can I ?
int genPlatformDelay()
{
	return (int) round(Uniform(2,5)) * 60;
	
	double alfa = 2.68491;
	double beta = 1.42182;
	int delay = (int) round(Gama(alfa,beta) * 60);
	cout << delay << endl;
	return delay < 0 ?  0 : delay;
}


int genOneRailDelay()
{
	return (int) round(Uniform(2,5)) * 60;

	double alfa = 2.68491;
	double beta = 1.42182;
	int delay = (int) round(Gama(alfa,beta) * 60);
	return delay < 0 ? 0 : delay;
}

int genZCrossoverDelay()
{
	return (int) round(Normal(50,5));	
}

int genZDelay()
{
	return (int) round(Normal(20,3));		
}

int genPCrossoverDelay()
{
	return (int) round(Normal(55,5));		
}

int genHDelay()
{
	return (int) round(Normal(15,2));		
}

int genOneCrossoverDelay()
{
	return (int) round(Normal(10,1));		
}