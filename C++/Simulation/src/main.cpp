#include "Train.h"
#include "SimTrain.h"
#include "simlib.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <sstream>
#include <ctime>
#include <algorithm>
#include <cstring>

#define SIM_MAX 25 * 60 * 60 // Whole day + one hour
#define DEF_SIM_COUNT 1000
#define DELAYED_TRAIN_NUM 5

using namespace std;

string USAGE_MSG = "USAGE brno_sim train_file";

int main(int argc, char const *argv[])
{

	int sim_runs = DEF_SIM_COUNT;

	if(argc == 6)
	{ 
		//specified rail counts
		
		//Platform One H
		int index = 2; 
		int railcnts[4];

		for (int j = index; j < 6; ++j)
		{
			int num;
			stringstream(argv[j]) >> num;
			railcnts[j - 2] = num;

		}

		Platform_rails.SetCapacity(railcnts[0]);
		One_rails.SetCapacity(railcnts[1]);
		H_rails.SetCapacity(railcnts[2]);
		sim_runs = railcnts[3];
		

	}
	else if(argc != 2)
	{
		cerr << "Not enough params provided\n" << USAGE_MSG <<  endl;
		return EXIT_FAILURE;
	}

	if(strcmp(argv[1],"-h") == 0)
	{
		cout << "Simulation tool for simulation of main railroad station in Brno" <<endl;
		cout << "Authors: Stanislav Smatana , Viliam Serecun" <<endl;
		cout << "USAGE: ./brno_sim train_file (simulation of normal state, 1000 runs)" <<endl;
		cout << "       ./brno_sim train_file platform_rail_num nove_sady_rail_num" <<endl;
		cout << "                  incoming_rail_num num_of_sim_runs " <<endl;
		exit(0);
	}

	// Load trains 
	vector<Route> routes;
	t_trainData data;

	try
	{
		data = parseJsonTrains(argv[1]);

	}
	catch(JSONException &e)
	{
		cerr << e.what() << endl;
		return EXIT_FAILURE;
	}

	routes = data.route_list;
	//DebugON();

	SetOutput("run.log");

		RandomSeed(time(NULL));
		for (int i = 0; i < sim_runs; i++)
		{

				/* code */
			zeroTrains();
			Init(0,SIM_MAX);
			scheduleTrains(routes);
			Run();

			int t_num = getTrainNum();


			// if(t_num != 582)
			// {
				
			// 	dumpQueues();
			// 	dumpFacilities();
			// 	cerr << "Only " << t_num << " trains passed in run "  << endl;

			// 	exit(1);
			// }



		}

		
		/* code */

		
		TimeStat.Output();
		//dumpFacilities();
		//dumpQueues();
		Print("\n\n");

		//dump trains
		// for (std::vector<Route>::iterator i = routes.begin(); i != routes.end(); ++i)
		// {
		// 	Route r = *i;
		// 	std::vector<Train*> t = r.getTrains();

		// 	for (std::vector<Train*>::iterator j = t.begin(); j != t.end(); ++j)
		// 	{
		// 		Print(((*j)->dumpToStr()).c_str());
		// 		(*j)->dumpStat();
		// 	}
		// }
		
		sort(data.train_list.begin(),data.train_list.end(),trainListStatComparator);
		
		//Print max 5 most delayed trains
		int x=0;
		for (std::vector<Train*>::iterator i = data.train_list.begin(); i != data.train_list.end(); ++i)
		{
			Print((*i)->getName().c_str());
			Print(" & ");
			Print((*i)->getStatMean());
			Print(" \n");

			x++;
			if(x == 5)
				break;
		}

	return 0;
}