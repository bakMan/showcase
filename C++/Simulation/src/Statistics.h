#ifndef HTATISTICS_H
#define HTATISTICS_H
#include "simlib.h"

class StatsPerPeriod
{
protected:
	Stat *stats_arr;
	int period;
	int n_periods;

public:
	StatsPerPeriod(int period,unsigned int n_periods);
	~StatsPerPeriod();
	void addData(int data,int  time);
	Stat *getStatArr();
	void operator()(int data,int time);
	void operator()(double data,double time);
	void Output();
	void OutputDetailed();
	/* data */
};


#endif // HTATISTICS_H