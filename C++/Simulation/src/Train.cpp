#include "Train.h"
#include "json/json.h"
#include <exception>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <iterator>
#include <algorithm>
#include "simlib.h"

using namespace std;
using namespace Json;


Train::Train(int id,int arrival, int departure,string name)
{
	average_time = new Stat();
	this->id = id;
	arrivals.push_back(arrival);
	departures.push_back(departure);
	name = name;
}

Train::Train(int id,int arrival,string name)
{
	average_time = new Stat();
	this->id = id;
	arrivals.push_back(arrival);
	name = name;	
}

void Train::addArrival(int arriv)
{
	arrivals.push_back(arriv);
}

void Train::addDeparture(int dep)
{
	departures.push_back(dep);
}

void Train::setName(string name)
{
	this->name = name;
}

void Train::setId(int id)
{
	this -> id = id;
}

void Train::recordTime(unsigned int time)
{
	(*average_time)(time);
}

//DEBUG
int Train::debugGetTime()
{
	if(arrivals.size() == 0)
		return departures.at(0);
	else
		return arrivals.at(0);
}
string Train::dumpToStr()
{
	stringstream ss;
	ss << id << " : " << name ;

	vector<int>::iterator it;
	ss << endl;
	ss << "Arrivals: ";
	for (std::vector<int>::iterator i = arrivals.begin(); i != arrivals.end(); ++i)
	{
		ss << *i << " ";
	}
	ss << endl;
	ss << "Departures: ";

	for (std::vector<int>::iterator i = departures.begin(); i != departures.end(); ++i)
	{
		ss << *i << " ";
	}
	ss << endl;

	return ss.str();
}

void Route::addTrain(Train *t)
{
	trains.push_back(t);
}

Route::~Route()
{

	for (std::vector<Train *>::iterator ptr = trains.begin(); ptr != trains.end(); ++ptr)
	{
		//delete *ptr;
	}
}

/** Function converts time in format H:M to number of minutes from 00:00
*/
int timeToMinutes(string time)
{
	std::vector<string> tokens;
	std::stringstream ss(time);
    std::string item;

    while (std::getline(ss, item, ':')) {
        tokens.push_back(item);
    }
    


	if(tokens.size() != 2)
		return -1;

	int hours;
	int minutes;

	istringstream (tokens[0]) >> hours;
	istringstream (tokens[1]) >> minutes;

	//return ((hours*60 + minutes));
	return ((hours*60 + minutes)) * 60; //seconds
}

/** Function parses trains in json format and outputs corresponding data structs in c++
*/
t_trainData parseJsonTrains(string input_file)
{
	Json::Value root;
	Json::Reader reader;

	vector<Route> routes;
	t_trainData data;
	std::vector<Train *> train_list;

	std::ifstream ifs(input_file.c_str());

	if(!ifs.is_open())
		throw JSONException("Unable to open file");

  	std::string json( (std::istreambuf_iterator<char>(ifs) ),
                       (std::istreambuf_iterator<char>()    ) );

 
	bool parsingSuccessful = reader.parse( json, root );

	if ( !parsingSuccessful )
	{

    	throw JSONException("JSON Error: " + reader.getFormatedErrorMessages());
	}

	vector<string> vec = root.getMemberNames();
	
	//iterate over names of routes
	for (std::vector<string>::iterator i = vec.begin(); i != vec.end(); ++i)
	{
		//rout id is stored as text -> get number
		int route_id;
		istringstream ( *i ) >> route_id;
		Route r(route_id);
		
		// iterate over trains on current route access root by route id
		Value v = root[*i];

		//Trains on route
		for (Value::iterator i = v.begin(); i != v.end(); ++i)
		{
			Value train = *i;
			Train *p_train = new Train();

			//train id
			int train_id;
			string train_name;

			istringstream ( train["id"].asString() ) >> train_id;
			train_name = train["name"].asString();

			//iterate over departures and arrivals
			Value arrivals = train["arrivals"];
			for ( unsigned int index = 0; index < arrivals.size(); ++index )
   				p_train -> addArrival(timeToMinutes(arrivals[index].asString()));

   			Value departures = train["departures"];
			for ( unsigned int index = 0; index < departures.size(); ++index )
   				p_train -> addDeparture(timeToMinutes(departures[index].asString()));

			p_train->setId(train_id);
			p_train->setName(train_name);	

			r.addTrain(p_train);
			train_list.push_back(p_train);
		}

		routes.push_back(r);

	}

	data.train_list = train_list;
	data.route_list = routes;
	return data;
}


//Function for comparison of train statistics
bool trainListStatComparator(Train *t1, Train *t2)
{
	double mean1 = t1->getStatMean();
	double mean2 = t2->getStatMean();

	return mean1 > mean2;
}