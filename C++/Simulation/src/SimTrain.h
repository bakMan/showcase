#ifndef SIM_TRAIN_H
#define SIM_TRAIN_H

#include "simlib.h"
#include "Train.h"
#include "Statistics.h"
#include <iostream>

using namespace std;

extern int zh_cnt ;
extern int bh_cnt ;
extern int hb_cnt ;
extern int hbz_cnt;

extern Histogram Processing;
extern StatsPerPeriod TimeStat;

extern unsigned int ONE_R_NUM;
extern unsigned int PLATFORM_R_NUM;
extern unsigned int H_RAIL_NUM;

extern Store One_rails;
extern Store H_rails;
extern Store Platform_rails;

extern int train_one;

/** Virtual class of simulation train */
class SimTrain : public Process
{
protected:
	//Train *p_data;
	int enter_time;
public:
	Train *p_data;
	virtual void Behavior() = 0;
	SimTrain(Train *t):p_data(t){}
	void dump(){cout << p_data -> dumpToStr() << endl;}
};


/** Event of train generation */
class genTrain : public Event
{
	protected:
		SimTrain *p_train;

	public:
		genTrain(SimTrain *t):p_train(t){}
		void Behavior();
};

//Function scheuldes trains to simulation schedule
void scheduleTrains(vector<Route> &routes);

/** Function returns train delay from normal distribution
*   based on statistical data
*/
int genDelay();
int genPlatformDelay();
int genOneRailDelay();
int genZCrossoverDelay();
int genZDelay();
int genPCrossoverDelay();
int genHDelay();
int genOneCrossoverDelay();

/********************************************** VILO ***********************

/** Zidenice -> Brno and possible continue
*/
class zhTrain : public SimTrain
{
	void Behavior();

public:
	zhTrain(Train *t):SimTrain(t){}
};

/** Brno -> Herpice 
*/
class bhTrain : public SimTrain
{
	void Behavior();

public:
	bhTrain(Train *t):SimTrain(t){}
};

/** Herpice -> Brno 
*/
class hbTrain : public SimTrain
{
	void Behavior();

public:
	hbTrain(Train *t):SimTrain(t){}
};

/** Herpice -> Brno -> Zidenice 
*/
class hbzTrain : public SimTrain
{
	void Behavior();

public:
	hbzTrain(Train *t):SimTrain(t){}
};

void dumpQueues();
void dumpFacilities();
int getTrainNum();
void zeroTrains();
#endif // SIM_TRAIN_H
