#ifndef SIM_TRAIN_H
#define SIM_TRAIN_H

#include "simlib.h"

Store One_rails("rails 5k 9k 11k 13k", 4);   // rails to platform 5,9,11,13 k
Store H_rails("rails 1a 2a 4a 3b" 4);        // rails from Herstice direction
Store Platform_rails("rails 1 2 3 4 5 6" 6); // rails next to platforms
Facility Crossover_One; // crossover to/from one-rails
Facility Crossover_P;   // crossover to/from platform 1 2 3 4 5 6
Facility Crossover_Z;   // crossover to rails 2t and 1t
Facility Rail_outZ;     // output rail 2t
Facility Rail_inZ;      // input rail 1t

#endif
