#! /bin/bash

run_num=6
declare -a one_rail_cnt=(4 4 4 4 0 4) 
declare -a h_rail_cnt=(4 4 4 4 4 4)
declare -a platform_rail_cnt=(6 5 4 3 6 2)
declare -a titles=("Priemerné zdržania vlakov na stanici počas bežnej prevádzky"
				   "Priemerné zdržania vlakov na stanici pri výpadku jednej koľaje"
				   "Priemerné zdržania vlakov na stanici pri výpadku dvoch koľají"
				   "Priemerné zdržania vlakov na stanici pri výpadku troch koľají"
				   "Priemerné zdržania vlakov na stanici pri výpadku nástupištia Nové Sady"
				   "Priemerné zdržania vlakov na stanici pri výpadku štyroch koľají"
				   )

PLOT_OPTS=$( cat <<EOF
set boxwidth 0.9 relative\n
set style fill solid 1.00 border 0\n
set style data histograms\n
set ylabel "Zdržanie v sekundách"\n
set xtics rotate by 45 right\n
set grid ytics;\n
set nokey\n
set terminal pdf\n
set output 'plot.pdf'\n
plot 'data' using 2:xticlabels(1) title "Priemerne zdrzanie"\n
set terminal wxt \n
replot\n
pause mouse key\n
EOF
)

PLOT_REF_OPTS=$( cat <<EOF
set boxwidth 0.9 relative\n
set style fill solid 1.00 border 0\n
set style data histograms\n
set ylabel "Zdržanie v sekundách"\n
set nokey;\n
set grid ytics;\n
set xtics rotate by 45 right\n
set terminal pdf\n
set output 'plot.pdf'\n
plot 'data' using 2:xticlabels(1) lc rgb"red" title "Výpadok", 
	 '../experiment0/data' using 2 lc rgb"green" title "Bežná prevádzka"\n
set terminal wxt \n
replot\n
pause mouse key\n
EOF
)

if [[ "$#" != "2" ]]; then
	echo "Not enough parameters, provide simulation run number"
	exit 1
fi

make
SIM_RUNS=$1
TRAINS="$2"

if [[ "$?" != "0" ]]; then
	echo "Compilation error"
	exit 1
fi
rm -r -f tmp
rm -r -f experiments
mkdir experiments

for (( i = 0; i < $run_num; i++ )); do
	mkdir tmp
	cd tmp
	cp "../$TRAINS" ./
	cp ../brno_sim ./

	platform_rails=${platform_rail_cnt[$i]}
	h_rails=${h_rail_cnt[$i]}
	one_rails=${one_rail_cnt[$i]}

	./brno_sim $TRAINS $platform_rails $one_rails $h_rails $SIM_RUNS > data 2> /dev/null

	if [[ "$?" != "0" ]]; then
		echo "Simulation ended with error"
		exit 1;
	fi
	cd ..

	dir="experiment$i"
	mkdir "experiments/$dir"

	cat tmp/run.log | sed '/WARNING/d' | sed '/^$/d' > "experiments/$dir/run.log"
	cp tmp/data "experiments/$dir/"

	echo "#! /usr/bin/gnuplot" >> "experiments/$dir/plot.plt" 
	echo "set title \"${titles[$i]}\"" >> "experiments/$dir/plot.plt"
	
	if [[ "$i" != 0 ]]; then
		#statements
		echo -e $PLOT_REF_OPTS >>"experiments/$dir/plot.plt"
	else	
		echo -e $PLOT_OPTS >>"experiments/$dir/plot.plt"
	fi

	cd "experiments/$dir/"
	gnuplot plot.plt &
	cd ..
	cd ..

	rm -r tmp
done
