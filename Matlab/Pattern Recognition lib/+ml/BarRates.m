% Pattern Recognition
%
% Project: Implementation of Bayes' classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function output_args = BarRates( classified,correct,labels,name )
%Function plots per class classification success rates.
%
%classified - labels resulted from classification
%correct - ground truth labels
%labels - list of used labels
%name - title for figure

    rates = zeros(1,size(labels,1));
    
    %calculate success rate for each class
    for i=1:size(labels,1)
        sub_correct = correct(correct == labels(i));
        sub_class = classified(correct == labels(i));
        
        rates(i) = sum(sub_class == sub_correct) / size(sub_class,1);
    end
    
    figure();
    rates = rates * 100;
    bar(rates);
    title(strcat(name,' - success rates by class'));

end