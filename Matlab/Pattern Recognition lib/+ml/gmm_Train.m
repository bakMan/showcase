%TODO PRIORS


function [ means, sigmas, weights, labels, priors] = gmm_Train( X,y,ngauss,eps )
%GMM_TRAIN Trains gaussian mixture model
%   X - matrix of training data
%   y - vector of training labels
%   ngauss - number of gaussians per class

    %extract label list in order to chunk training data
    labels = unique(y);
    label_cnt = size(labels,1);
    ddim = size(X,2);
    
    %for each class ngauss means of ddim
    means = zeros(ngauss,ddim,label_cnt);
    %for each class ngauss ddim x ddim sigmas
    %initialized as random between 0 1
    %THREAT: IS IT BETWEEN 0 and 1 ? 
    sigmas = zeros(ddim,ddim,ngauss,label_cnt);
    priors = zeros(label_cnt,ngauss);
        
    %sigmas = zeros(ddim,ddim,ngauss,label_cnt);
    %for each class ngauss weights
    %weights = ones(label_cnt,ngauss);
    weights = repmat(1/ngauss,label_cnt,ngauss);
    
    %for each class
    for cn = 1:label_cnt
        old_score = -inf;
        
        %select only vectors for this label
        train_data = X(y == labels(cn),:);
        covm = cov(train_data);
        logdet = -0.5*log(det(covm));
        covm = inv(covm);
        
        for j=1:ngauss
            sigmas(:,:,j,cn) = covm;
            priors(cn,j) = logdet;
        end
        
        %responsibility matrix
        ndata = size(train_data,1);
        resp = zeros(ndata,ngauss);

        %initialize means and weights
        for m=1:ngauss
            rand_idx = randi([1,ndata],[1 1]);
            means(m,:,cn) = train_data(rand_idx,:);
            weights(cn,m) =  1/ngauss; %equivalen w, ok ?
        end
        
        while 1

            %classify all vectors and output responsibilities
            for n=1:ndata
                [~,scores] = ml.classify_Bayes_normal(train_data(n,:),means(:,:,cn),sigmas(:,:,:,cn),priors(cn,:),zeros(1,ngauss));            
                %move scores so everything is positive
                m = min(scores);
                scores = scores + abs(m);
                resp(n,:) = scores ./ sum(scores);
            end

            %recalculate means TODO
            for m=1:ngauss
                s = zeros(1,ddim);
                for n=1:ndata
                    %weighted average
                    s = s + resp(n,m)*train_data(n,:);
                end
                %calculate average
                means(m,:,cn) = s / sum(resp(:,m));
            end

            %recalculate sigmas TODO
            for m=1:ngauss
                sigmas(:,:,m,cn) = zeros(ddim,ddim);
                for n=1:ndata
                    %weighted average
                    tmp = train_data(n,:) - means(m,:,cn);
                    sigma = tmp' * tmp;
                    sigmas(:,:,m,cn) = sigmas(:,:,m,cn) + resp(n,m)*sigma;
                end

                %calculate average sigma
                sigmas(:,:,m,cn) = sigmas(:,:,m,cn) / sum(resp(:,m));
                %calculate logdet
                priors(cn,m) = -0.5*log(det(sigmas(:,:,m,cn)));
                %inverse
                sigmas(:,:,m,cn) = inv(sigmas(:,:,m,cn));
            end
            
            %recalculate weights TODO
            total = sum(sum(resp));
            for m=1:ngauss
                summed = sum(resp);
                weights(cn,:) = summed ./ total;
            end


            %calculate score of all data
            new_score = 0;
            for n=1:ndata
                [~,scores] = ml.classify_Bayes_normal(train_data(n,:),means(:,:,cn),sigmas(:,:,:,cn),priors(cn,:),zeros(1,ngauss));                
                scores = scores' .* weights(cn,:);
                new_score = new_score + sum(scores);
            end
            
            if isnan(abs(old_score - new_score))
                sigmas(:,:,:,cn)
                sum(resp)
                pause
            end
            fprintf('Class %d Old %.2f New %.2f Delta %.2f\n',cn,old_score,new_score,abs(old_score - new_score));
            if abs(old_score - new_score) < eps
                break
            end
            old_score = new_score;
        end
    end
    
  
end

