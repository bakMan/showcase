function new_labels  = translate_Mapped_Labels( labels, cluster_map )
%TRANSLATE_MAPPED_LABELS Summary of this function goes here
%   Detailed explanation goes here
    new_labels = zeros(size(labels,1),1);
    for i=1:size(cluster_map,1)
        new_labels(cluster_map(i,1) == labels) = cluster_map(i,2);
    end

end

