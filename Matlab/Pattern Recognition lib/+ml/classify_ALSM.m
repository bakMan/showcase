% Pattern Recognition
%
% Project: Implementation of ALSM classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function [ out_labs ] = classify_ALSM( X,eigen_vecs,labels )
%CLASSIFY_ALSM Function classifies vector x using ALSM
%eigen_vecs - eigenvectors of classes from train_ALSM
%labels - vector of class labels from train_ALSM

    %try to classify using each class
    scores = zeros(1,length(labels));
    out_labs = zeros(size(X,1),1);
    
    %go throug all vectors to classify
    for j=1:size(X,1)
        %extract vector
        x = X(j,:);
        
        for i=1:length(labels)

            %should produce vector of projection values
            projections = x*eigen_vecs(:,:,i);

            %make sure it's positive
            projections = projections .* projections;
            scores(i) = sum(projections);
            
        end
        
        [~, max_idx] = max(scores);
        out_labs(j) = labels(max_idx);
    end

end

