function cluster_map  = map_Clusters( training_data,training_labels, means )
%MAP_CLUSTERS Summary of this function goes here
%   Detailed explanation goes here

    %map of "cluster index" -> class label => 2 columns
    cluster_map = zeros(size(means,1),2);
    labels = unique(training_labels);
    
    %calculate mapping for each mean
    for i=1:size(labels,1)
        %extract data for one class
        submat = training_data(training_labels == labels(i),:);
        
        %classify it using discovered clusters
        out_labs = ml.Kmeans_classify(submat,means);
        
        %assign mapping cluster-> class for cluster to which most of
        %vectors of this class belong
        [a b] = hist(out_labs,unique(out_labs));
        [~, ind] = max(a);
        
        cluster_map(i,:) = [b(ind) labels(i)];
    end

end

