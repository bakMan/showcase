% Pattern Recognition
%
% Project: Implementation of Bayes' classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function [dataset means]= generateGaussianDataset( vec_size,n_classes,n_vec_class)
%generateGaussianDataset Generates dataset matrix of random multivariate gaussian data
%
%vec_size - size of training vectors
%n_classes - number of classes to generate
%n_vec_class - number of vectors per class, function generates n_vec_class
%              testing vectors and n_vec_class training vectors for each
%              class
%
%dataset - generated dataset
%means - means of gaussians used for generation

    %testing and training data, each feature_vec + one test/train label
    %+ class label
    dataset = zeros(n_classes * n_vec_class*2, vec_size + 1 + 1);
    means = zeros(n_classes,vec_size);

    for cls=0:n_classes - 1
        %values for random mean were chosen using visual inspection to ensure
        %classes which can be easily distinguished
        mu    = randi(50,1,vec_size);
        means(cls+1,:) = mu;
        sigma = [1.2 0.5; 0.5 3];
        dist = gmdistribution(mu,sigma,1);

        for t_label=0:1

            %first is train/test label
            start_i = 1 + (cls*2*n_vec_class) + t_label*n_vec_class;
            end_i = start_i + n_vec_class - 1;

            %label testing/training data
            dataset(start_i:end_i,1) = t_label;
            %generate vectors
            data = dist.random(n_vec_class);
            dataset(start_i:end_i,2:2+vec_size -1) = data;
            %assign vec label
            dataset(start_i:end_i,2+vec_size) = cls;

        end
    end
end

