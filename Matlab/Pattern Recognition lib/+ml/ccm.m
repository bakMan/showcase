function  m  = ccm( data )
%CCM Compute correlation matrix for given vectors
%   Detailed explanation goes here

%     m = zeros(size(data,2),size(data,2));
%     for x=1:size(data,1)
%          m = m + data(x,:)'*data(x,:);
%     end

    m = data' * data;
end

