function mat = conditional_CCM( data,classified,expected,class1,class2 )
%CONDITIONAL_CCM Function calculates conditional correlation matrix for 
%                data of class1 mistakenly classified as data of class2
%   Detailed explanation goes here
  
    %select data from class class1 mistakenly classified as class2
    mistake_data = data(expected == class1 & classified == class2,:);
        
    %no data no fun
    if size(mistake_data,1) == 0
        %correlation matrix has size of vec_size * vec_size
        mat = zeros(size(data,2),size(data,2));
        return;
    end
    
    %correlation matrix out of selected data
    mat = ml.ccm(mistake_data);
     

end

