% Pattern Recognition
%
% Project: Implementation of Bayes' classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function [mus sigmas priors labels] = train_Bayes_normal(X, y)
%train_Bayes_normal Function trains single gaussian, bayes classifier for
%each class
%X - matrix where each row is training vector
%y - array of labels for each training vector
%
%mus - means of Gaussians
%simgas - inverse covariance matrices of Gaussians
%priors - prior probabilities of classes, combined with determinants of
%         sigma
%labels - vector of class labels
    
    %extract label list in order to chunk training data
    labels = unique(y);
    label_cnt = size(labels,1);
        
    %there must be some labels provided and number of labels and training
    %data must match
    if label_cnt == 0
        throw(MException('trainGaussian:noLabels','No labels for training data provided'));
    end
    
    if size(y,1) ~= size(X,1)
        throw(MException('trainGaussian:badNum','Number of data vectors and labels dont match'));
    end
    
    %preallocation
    mus = zeros(label_cnt,size(X,2));
    priors = zeros(label_cnt,1);
    
    %Covariance matricies will have constant size of n_features x
    %n_features
    sigmas = zeros(size(X,2),size(X,2),label_cnt);
    
    %for each class
    for i=1:length(labels)
        
        %select only vectors for this label
        train_data = X(y == labels(i),:);
        
        if size(train_data,1) == 0
            throw(MException('trainGaussian:emptyClass','Class with no training data'));
        end
        
        %calculate and store mean and covariance
        mus(i,:) = mean(train_data,1);
        sigma = cov(train_data);
        sigmas(:,:,i) = inv(sigma);
                        
        %prior probability = n_vectors_of_class / n_all_vectors
        %term includes also determinant of cov matrix
        priors(i) = log(size(train_data,1) / size(X,1)) - 0.5*log(det(sigma));
    end

end

