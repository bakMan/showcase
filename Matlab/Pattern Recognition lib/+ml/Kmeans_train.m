% Pattern Recognition
%
% Project: Implementation of k-means
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function means = Kmeans_train(X,nmeans)
%KMEANS_TRAIN Function discovers nmeans clusters in data
%   nmeans - maximal number of clusters
%   X - matrix of data vectors

    means = zeros(nmeans, size(X,2));
    
    %number of current closest cluster for each sample
    closest = zeros(size(X,1),1);
    
    %randomly choose n clusters
    for i=1:nmeans
        %starting means are randomly chosen from dataset
        means(i,:) = X(randi([1 size(X,1)]),:);
    end

    tmp_dist  = zeros(nmeans,1);
    old_labs = 0;
    
    while 1        
        %classify all training data
        out_labs = ml.Kmeans_classify(X,means);
        
        %if there was no change in labels - return
        if old_labs == out_labs
            break;
        end
        
        old_labs = out_labs;
                
        %calculate new means
        for i=1:nmeans
            %calculate new mean from closest points for each cluster
            submat = X(out_labs == i,:);
            if size(submat,1) == 0
                %we have cluster gone
                means(i,:) = nan;
            else
                means(i,:) = mean(submat);
            end
            
        end
        %clear deleted means
        means = means(~isnan(means(:,1)),:);
    end

end

