% Pattern Recognition
%
% Project: Implementation of Bayes' classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function [class scores] = classify_Bayes_normal(x, mus, sigmas, priors, labels)
%classify_Bayes_normal Classifies selected vector using single gaussian
%bayes classifiers
%
%mus - means of Gaussians
%simgas - inverse covariance matrices of Gaussians
%priors - prior probabilities of classes, combined with determinants of
%         sigma
%labels - vector of class labels
%
%class - resulting class label
%scores - vector of likelihoods of vector x belonging to each class

    %preallocation
    scores = zeros(size(mus,1),1);
    
    %calculate score for each class
    for i=1:size(mus,1)
        %gaussian log discriminant function
        vec = (x - mus(i,:))';
        scores(i) = -0.5*(vec')*sigmas(:,:,i)*vec + priors(i);
    end
    
    %class with maximal score is returned
    [ max_value, max_index ] = max(scores);
    class = labels(max_index);
    
end

