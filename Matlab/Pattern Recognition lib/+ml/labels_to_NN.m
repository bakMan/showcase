function out_labels = labels_to_NN(labels)
%LABELS_TO_NN Summary of this function goes here
%   Detailed explanation goes here

    out_labels = zeros(size(labels,1),length(unique(labels)));
    for i=1:size(labels,1)
        out_labels(i,labels(i) + 1) = 1;
    end

end

