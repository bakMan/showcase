function weights = perceptronTrain( X,y,rate,MAX_ITER )
    weights = zeros(1,size(X,2) + 1);
    weights(end) = 0.5;
    
    for j=1:MAX_ITER
        error = 0;
        for i=1:size(X,1)
            label = ml.perceptronClassify(weights,X(i,:));
            weights = weights + rate*(y(i) - label)*[X(i,:) 1];
            
            if y(i) - label ~= 0
                error=1;
            end
        end
        
        if error == 0
            break;
        end
    end
end

