function out_labs = gmm_classify( X,means,sigmas,ngauss,weights,priors,labels)
%GMM_CLASSIFY Summary of this function goes here
%   Detailed explanation goes here

    scores = zeros(1,length(labels));
    
    for i=1:size(X,1)
        for n=1:length(labels)
            [~,s] = ml.classify_Bayes_normal(X(i,:),means(:,:,n),sigmas(:,:,:,n),priors(n,:),ones(1,ngauss));
            s = s' .*weights(n,:);
            scores(n) = sum(s);
        end
        [~,idx] = max(scores);
        out_labs(i) = labels(idx);
    end
    out_labs = out_labs';
end

