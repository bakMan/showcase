% Pattern Recognition
%
% Project: Implementation of k-means
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function out_labels  = Kmeans_classify( X,means)
%KMEANS_CLASSIFY Outputs cluster membership for data vectors 
%   X - data vectors
%   means - means of clusters

    out_labels = zeros(size(X,1),1);
    tmp_dist  = zeros(size(means,1),1);
    
    for i=1:size(X,1)
        %calculate distances
        for j=1:size(means,1)
            tmp_dist(j) = norm(means(j,:) - X(i,:));
        end

        %centroid id with minimal distance is label for vector
        [~, ind] = min(tmp_dist);
        out_labels(i) = ind;
    end

end

