% Pattern Recognition
%
% Project: Implementation of Bayes' classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function [training_set training_labels test_set test_labels ] = extractSet(dataset, label_index,feature_range)
%Function extracts training vectors, training labels, testing vectors and 
%testing labels from dataset 

    %features are on columns 4 - 13, last column is label
    %training set vectors have 0 in first column
    training_set = dataset(dataset(:,1) == 0,:);
    training_labels = training_set(:,label_index);
    training_set = training_set(:,feature_range(1):feature_range(2));

    %testing set extraction
    test_set = dataset(dataset(:,1) == 1,:);
    test_labels = test_set(:,label_index);
    test_set = test_set(:,feature_range(1):feature_range(2));

end

