function output = perceptronClassify( weights, data )

    output = zeros(size(data,1),1);
    for i=1:size(data,1)
        res = weights(1:end -1)*data(i,:)';
        res = res + weights(end);
        
        if res > 0
            output(i)=1;
        else
            output(i)=0;
        end
    end

end

