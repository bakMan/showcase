function  eigens  = getStrongestEigens( matrix, n)

    [eigens, ~] = eigs(matrix,n,'lm');
end

