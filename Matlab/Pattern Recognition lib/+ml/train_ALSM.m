% Pattern Recognition
%
% Project: Implementation of ALSM classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

function [ eigen_vecs labels rates] = train_ALSM(X,y,alpha,beta,max_runs,eigen_vec_num)
%TRAIN_ALSM Function trains ALSM classifier
%   X - training vectors
%   y - training labels
%   alpha - alpha learning coefficient
%   beta - beta learning coefficient
%   eigen_vec_num - number of eigenvectors
    
    %extract label list in order to chunk training data
    labels = unique(y);
    label_cnt = size(labels,1);
    
    %initialize, vec_size * vec_size matrix for each class
    correlations = zeros(size(X,2),size(X,2),label_cnt);
    
    %TODO is this good number of eigen vectors ?
    %eigen_vec_num = round(size(X,2) / 2);
    %eigen_vec_num = round(size(X,2) / 2);
    eigen_vecs = zeros(size(X,2),eigen_vec_num,label_cnt);
        
    %initialization step
    %for each class
    for i=1:length(labels)
        %vector autocorrelation
        train_data = X(y == labels(i),:);
        
        %calculate correlation matric for this class
        correlations(:,:,i) = ml.ccm(train_data);
                
        %eigen vectors are sorted, biggest on the right
        eigen_vecs(:,:,i) = ml.getStrongestEigens(correlations(:,:,i),eigen_vec_num);
    end
    
    rates = zeros(1,max_runs);
    
    %iterative learning
    for k=1:max_runs
        %classify
        tmp_labels = ml.classify_ALSM(X,eigen_vecs,labels);
        rates(k) = sum(tmp_labels == y)/ length(y);
         
        %no point in training if everything was classified
        if sum(y == tmp_labels) == size(y,1)
            break;
        end
                
        %adjust correlation matrices
        for i=1:label_cnt
            
            for j=1:label_cnt
                if labels(i) == labels(j)
                    continue;
                end
                
                %adjust correlation matrix according to conditional
                %correlations
                correlations(:,:,i) = correlations(:,:,i) + alpha*ml.conditional_CCM(X,tmp_labels,y,labels(i),labels(j));
                correlations(:,:,i) = correlations(:,:,i) - beta*ml.conditional_CCM(X,tmp_labels,y,labels(j),labels(i));
            end
            
        
            %eigen vectors are sorted, biggest on the right
            %eig_v
            eigen_vecs(:,:,i) = ml.getStrongestEigens(correlations(:,:,i),eigen_vec_num);
                        
        end
        
    end

end

