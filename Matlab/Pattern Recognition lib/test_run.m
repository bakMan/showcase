% Pattern Recognition
%
% Project: Implementation of Bayes' classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

%TODO: Dodokumentuj
%TODO: Uprac ak treba
close all
clear all

N_GAUSSIANS = 8;
EPS = 0.001;

dataset = load('data/vowel.data');


label_idxes(1).id = 14;
label_idxes(1).range = [4 13];
label_idxes(1).name = 'Vowel recoginition';
label_idxes(1).data = dataset;

for run=1:length(label_idxes)
    
    %chop matrix into training dataset, training labels, testing dataset
    %testing labels
    [training_set training_labels test_set test_labels ] = ml.extractSet(label_idxes(run).data, label_idxes(run).id,label_idxes(run).range);
    
   
    
    [ means sigmas weights labels priors] = ml.gmm_Train( training_set,training_labels,N_GAUSSIANS,EPS);
    x = ml.gmm_classify(test_set,means,sigmas,N_GAUSSIANS,weights,priors,labels);
    sum(x == test_labels) / length(test_labels)
    ml.BarRates(x,test_labels,labels,'Class classification accurancies');
end
