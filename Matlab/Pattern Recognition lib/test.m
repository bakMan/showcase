% Pattern Recognition
%
% Project: Implementation of Bayes' classifier
% Stanislav Smatana, 2014
% staniss@student.uef.fi / xsmata01@stud.fit.vutbr.cz

%TODO: Dodokumentuj
%TODO: Uprac ak treba
close all

dataset = load('data/vowel.data');

%Data structure for test runs
%id - column index of labels
%range - range of column indexes, where features lie
%name - name of recognition task
%data - dataset matrix to extract data from
label_idxes(1).id = 2;
label_idxes(1).range = [4 13];
label_idxes(1).name = 'Speaker recognition';
label_idxes(1).data = dataset;

label_idxes(2).id = 3;
label_idxes(2).range = [4 13];
label_idxes(2).name = 'Gender recognition';
label_idxes(2).data = dataset;

label_idxes(3).id = 14;
label_idxes(3).range = [4 13];
label_idxes(3).name = 'Vowel recoginition';
label_idxes(3).data = dataset;

%simple gaussian generated dataset for validity testing
%4 well-separable 2d gaussians
label_idxes(4).id = 4;
label_idxes(4).range = [2 3];
label_idxes(4).name = 'Simple gaussian generated data';
label_idxes(4).data = generateGaussianDataset(2,4,300);

for run=1:length(label_idxes)
    
    %chop matrix into training dataset, training labels, testing dataset
    %testing labels
    [training_set training_labels test_set test_labels ] = extractSet(label_idxes(run).data, label_idxes(run).id,label_idxes(run).range);
    
    %training
    [mus sigmas priors labels] = train_Bayes_normal(training_set, training_labels);
    
    %number of test set vectors
    test_cnt = size(test_set,1);
    classified = zeros(test_cnt,1);

    %number of correct classifications
    num_correct = 0;
    for i=1:size(test_set,1)
        %classification of vector
        classified(i) = classify_Bayes_normal(test_set(i,:),mus,sigmas,priors,labels);
        
        %correctness check
        if classified(i) == test_labels(i)
            num_correct = num_correct + 1;
        end    
        
    end
    
    %bar plot of success rates per class
    BarRates(classified,test_labels,labels,label_idxes(run).name);
    
    %print classification rate in percent
    fprintf('%s: %.2f\n%',label_idxes(run).name,(num_correct / test_cnt)*100);
end

pause;
close all;
