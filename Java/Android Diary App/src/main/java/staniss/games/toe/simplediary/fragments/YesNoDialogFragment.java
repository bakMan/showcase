package staniss.games.toe.simplediary.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

/** Simple yes/no dialog fragment with callback on positive reply
 * Created by Stanislav Smatana on 11/05/2015.
 */
public class YesNoDialogFragment extends DialogFragment {
    Context mContext;

    public IPositiveAnswer getListener() {
        return listener;
    }

    public void setListener(IPositiveAnswer listener) {
        this.listener = listener;
    }

    public interface IPositiveAnswer
    {
        public void action();

    }

    private IPositiveAnswer listener;

    public YesNoDialogFragment() {
        super();
        mContext = getActivity();
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Are you sure?");
        alertDialogBuilder.setMessage("Do you really want to do it?");
        //null should be your on click listener
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(listener != null)
                    listener.action();
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        return alertDialogBuilder.create();
    }
}
