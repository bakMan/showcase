package staniss.games.toe.simplediary.db.mappers;

import android.view.View;
import android.widget.TextView;

/** Setter maps both views from GUI to DB and DB to GUI, from TextView element to textual field of
 * DB
* Created by Stanislav Smatana on 29/04/2015.
*/ //predefined setters for textview and edittext
public class TextViewSetter extends GUIElementSetter
{

    public TextViewSetter(int id) {
        super(id);
    }

    @Override
    public void set(String db_data,View base_view) {
        TextView t = (TextView) base_view.findViewById(id);
        t.setText(db_data);
    }

    public String get(View base_view)
    {
        TextView t = (TextView) base_view.findViewById(id);
        return t.getText().toString();
    }

}
