package staniss.games.toe.simplediary.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBOpenHelper;
import staniss.games.toe.simplediary.db.mappers.AlarmAdjustmentSetter;
import staniss.games.toe.simplediary.db.mappers.AlarmTextSetter;
import staniss.games.toe.simplediary.db.mappers.DateTimeSetter;
import staniss.games.toe.simplediary.R;
import staniss.games.toe.simplediary.db.mappers.GUIElementSetter;
import staniss.games.toe.simplediary.db.mappers.IfBooleanMapper;
import staniss.games.toe.simplediary.db.mappers.StaticTextSetter;
import staniss.games.toe.simplediary.db.mappers.TextViewSetter;
import staniss.games.toe.simplediary.db.mappers.VisibilitySetter;


/** Fragment showing details about event
 * Created by Stanislav Smatana on 27/04/2015.
 */
//TODO delete
//todo info
public class DetailFragment extends ARecordFragment {



    final static String YESNO_TAG = "YESNO";

    //DB -> GUI mapping
    String[] from = {
            DBOpenHelper.TITLE_FIELD,
            DBOpenHelper.DESC_FIELD,
            DBOpenHelper.DATE_FIELD,
            DBOpenHelper.IS_TIME_FIELD,
            DBOpenHelper.IS_TIME_FIELD,
            DBOpenHelper.DATE_FIELD,
            DBOpenHelper.ALARM_MINS_FIELD,
            DBOpenHelper.IS_ALARM_FIELD,
    };

    //AlarmTextSetter must be instantiated separately, because it is needed by AlarmAdjustmentSetter
    //together they form one virtual setter for time till alarm text
    AlarmTextSetter a_setter = new AlarmTextSetter(R.id.text_alarm);
    GUIElementSetter[] to = {
            new TextViewSetter(R.id.text_name),
            new TextViewSetter(R.id.text_description),
            new DateTimeSetter(R.id.text_date,R.id.text_time),
            new VisibilitySetter(R.id.text_time),
            new VisibilitySetter(R.id.text_coma),
            a_setter,
            new AlarmAdjustmentSetter(R.id.text_alarm,a_setter),
            new IfBooleanMapper(R.id.text_alarm,null,new StaticTextSetter(R.id.text_alarm,"No alarm"))
    };

    //Transfer mapping to parent
    public DetailFragment()
    {
        super();
        super.setMapping(from,to);
    }

    //listener for edit events
    private EditEventListener e_listener = null;

    public EditEventListener getEditListener() {
        return e_listener;
    }
    public void setEditListener(EditEventListener e_listener) {
        this.e_listener = e_listener;
    }

    //Interface for edit button event reciever
    public interface EditEventListener
    {
        public void onEdit();
    }



    //Fragment view creation
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_details, container, false);


        //button action -> tell to edit event listener (hosting activity in this case)
        //that edit button was pressed
        Button b = (Button) v.findViewById(R.id.btn_edit);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(e_listener != null)
                    e_listener.onEdit();
            }
        });


        //delete button callback
        Button d = (Button) v.findViewById(R.id.btn_delete);
        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YesNoDialogFragment frag = new YesNoDialogFragment();
                frag.setListener(new YesNoDialogFragment.IPositiveAnswer() {
                    @Override
                    public void action() {
                        //delete event
                        DBAdapter.getInstance(getActivity()).deleteEvent(id);
                        //finish activity
                        getActivity().finish();
                    }
                });
                frag.show(getFragmentManager(),YESNO_TAG);
            }
        });

        return v;

    }

}
