package staniss.games.toe.simplediary.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.GregorianCalendar;

import staniss.games.toe.simplediary.db.mappers.CheckBoxSetter;
import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBOpenHelper;
import staniss.games.toe.simplediary.db.mappers.DateTimeSetter;
import staniss.games.toe.simplediary.db.mappers.EditTextSetter;
import staniss.games.toe.simplediary.db.mappers.EnableSetter;
import staniss.games.toe.simplediary.db.GUIRecordMapper;
import staniss.games.toe.simplediary.R;
import staniss.games.toe.simplediary.db.mappers.GUIElementSetter;
import staniss.games.toe.simplediary.db.mappers.VisibilitySetter;

/** Fragment for editing of events
 * Created by Stanislav Smatana on 28/04/2015.
 */
public class EditFragment extends ARecordFragment implements DatePickerDialogFragment.IDateChangeListener, TimePickerDialogFragment.ITimeChangeListener {

    static final String TAG = "EDIT_FRAGMENT";
    String TITLE = "";

    //Gui elements
    EditText date;
    CheckBox check;
    EditText time;
    CheckBox check_remind;
    TextView txt_remind_1;
    TextView txt_remind_2;
    EditText field_remind;

    public IFinishedEditingListener getListener() {
        return listener;
    }

    public void setListener(IFinishedEditingListener listener) {
        this.listener = listener;
    }

    @Override
    //Called when user changes date in the date field
    public void onDateChange(int year, int monthOfYear, int dayOfMonth) {

        GregorianCalendar cal = new GregorianCalendar(year,monthOfYear,dayOfMonth);
        String str = DateFormat.getDateInstance(DateFormat.SHORT).format(cal.getTime());
        date.setText(str);
    }

    @Override
    //Called when user changes time in the time field
    public void onTimeChange(int hourOfday, int minute) {

        //I would say that this time format is quite international
        String minutes = Integer.toString(minute);

        //add leading zero if missing
        if(minutes.length() == 1)
            minutes = "0" + minutes;

        time.setText(Integer.toString(hourOfday) + ":" + minutes );
    }

    //When users saves the edited event, listener with this interface is called
    public interface IFinishedEditingListener
    {
        public void onEditingFinished();
    }

    private IFinishedEditingListener listener;

    //DB -> GUI mapping
        String[] from = {DBOpenHelper.TITLE_FIELD,
                        DBOpenHelper.DESC_FIELD,
                        DBOpenHelper.IS_TIME_FIELD,
                        DBOpenHelper.DATE_FIELD,
                        DBOpenHelper.IS_TIME_FIELD,
                        DBOpenHelper.IS_ALARM_FIELD,
                        DBOpenHelper.IS_ALARM_FIELD,
                        DBOpenHelper.IS_ALARM_FIELD,
                        DBOpenHelper.IS_ALARM_FIELD,
                        DBOpenHelper.ALARM_MINS_FIELD
    };



    GUIElementSetter[] to = {
            new EditTextSetter(R.id.field_title),
            new EditTextSetter(R.id.field_description),
            new EnableSetter(R.id.field_time), //in case of whole-day event, time field is disabled
            new DateTimeSetter(R.id.field_date,R.id.field_time),
            new CheckBoxSetter(R.id.checkBox,true),
            new CheckBoxSetter(R.id.chk_remind),
            new VisibilitySetter(R.id.txt_remind1), //if alarm is not set, alarm minutes field and
            new VisibilitySetter(R.id.txt_remind2), //text next to it is hidden
            new VisibilitySetter(R.id.field_remind),
            new EditTextSetter(R.id.field_remind)
    };

    //Transfer mapping to parent
    public EditFragment()
    {
        super();
        super.setMapping(from,to);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_edit, container, false);

        txt_remind_1 = (TextView) v.findViewById(R.id.txt_remind1);
        txt_remind_2 = (TextView) v.findViewById(R.id.txt_remind2);
        field_remind = (EditText) v.findViewById(R.id.field_remind);
        check_remind = (CheckBox) v.findViewById(R.id.chk_remind);

        //set title of the fragment
        ((TextView) v.findViewById(R.id.title)).setText(TITLE);

        //Checkbox turning alarm settings on/off
        //should make the alarm options visible/invisible
        check_remind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int visibility;
                if(check_remind.isChecked())
                {
                    visibility = View.VISIBLE;
                }
                else
                {
                    visibility = View.INVISIBLE;
                }

                txt_remind_1.setVisibility(visibility);
                txt_remind_2.setVisibility(visibility);
                field_remind.setVisibility(visibility);
            }
        });

        //Date field click should result in the datepicker dialog popping up
        date = (EditText) v.findViewById(R.id.field_date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"Date picker dialog popping up");
                DatePickerDialogFragment dialog = new DatePickerDialogFragment();
                dialog.setListener(EditFragment.this);
                dialog.show(getFragmentManager(),"DATE");
            }
        });

        //Time field click should result in the timepicker dialog popping up
        time = (EditText) v.findViewById(R.id.field_time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"Time picker dialog popping up");
                TimePickerDialogFragment dialog = new TimePickerDialogFragment();
                dialog.setListener(EditFragment.this);
                dialog.show(getFragmentManager(),"TIME");
            }
        });

        //set the save callback
        Button b = (Button) v.findViewById(R.id.btn_save);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //title can't be blank
                TextView t = (TextView) ((View) v.getParent()).findViewById(R.id.field_title);

                if(t.getText() == null || t.getText().toString().matches("\\s*"))
                {
                    t.setError("Title field can not be empty");
                    return;
                }

                DB_Mapper.write(getView());
                if(listener != null)
                    listener.onEditingFinished();
            }
        });

        //Unchecking the "whole day" checkbox enables user to choose time
        check = (CheckBox) v.findViewById(R.id.checkBox);
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check.isChecked())
                {
                    time.setEnabled(false);
                }
                else
                {
                    time.setEnabled(true);
                }

            }
        });

        return v;
    }

    @Override
    //Override is needed so the title can be set based on if this is insertion/edit
    public void populate(long id,DBAdapter db)
    {
        //actual populating is delegated to the parent class
        super.populate(id,db);

        if(id == GUIRecordMapper.NO_ID)
            TITLE = "Add new event";
        else
            TITLE = "Edit existing event";

    }


}
