package staniss.games.toe.simplediary.db.mappers;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBOpenHelper;

/** Sets text view for date and text view for date from db data. Uses locale format for gui output.
 *  And DB_FORMAT for db input
 * Created by Stanislav Smatana on 14/05/2015.
 */
public class DateTimeSetter extends GUIElementSetter {

    int id_date;
    int id_time;


    final static String TAG = "DB_DATE";


    static final int DATE_PART = 0;
    static final int TIME_PART = 1;


    public DateTimeSetter(int id_date, int id_time) {
        super(id_date);
        this.id_date = id_date;
        this.id_time = id_time;
    }

    @Override
    public void set(String db_data, View v) {

        Log.i(TAG, "Parsing date from db: " + db_data);

        TextView date = (TextView) v.findViewById(id_date);
        TextView time = (TextView) v.findViewById(id_time);

        //Datetime is in the YYYY:MM:DD HH:MM:SS format -> split on space
        String[] parts = db_data.split(" ");

        //it has to have two parts
        if(parts.length != 2 && parts.length != 1) {
            Log.e(TAG,"Database returned date in the format " + db_data);
            throw new IllegalArgumentException();
        }

        //time is ok as it is (maybe)
        if(parts.length == 2) {
            Log.i(TAG,"Setting time " + parts[TIME_PART]);
            time.setText(parts[TIME_PART]);
        }

        //date has to be parsed from the db string
        DateFormat in_format = new SimpleDateFormat(DBAdapter.DATE_FORMAT);
        Date d;

        try {
            d = in_format.parse(parts[DATE_PART]);
        }
        catch (ParseException e)
        {
            Log.e(TAG,"Wrong format of the date in the DB " + parts[DATE_PART]);
            return;
        }

        //format according to locale
        String s_date = DateFormat.getDateInstance().format(d);

        date.setText(s_date);

    }

    @Override
    public String get(View v) {
        TextView date = (TextView) v.findViewById(id_date);
        TextView time = (TextView) v.findViewById(id_time);

        DateFormat format = DateFormat.getDateInstance(DateFormat.SHORT);
        Date d = new Date();

        try
        {
            d = format.parse(date.getText().toString());
        }
        catch(ParseException e)
        {
            Log.e(TAG,"Date in the field has wrong format: " + date.getText());
        }


        DateFormat out_format = new SimpleDateFormat(DBAdapter.DATE_FORMAT);
        String out = out_format.format(d);

        Log.i(TAG,"Saving date " + out);

        //!!!Important -> time has to have a leading zero, otherwise SQLITE is unable to parse it
        String time_str = time.getText().toString();

        //only one number in the beginning
        if(time_str.matches("[0-9]:[0-9][0-9]"))
        {
            Log.i(TAG,"Adding leading zero");
            time_str = "0" + time_str;
        }

        //glue date and time back together and add seconds (we don't use them)
        return out + " " + time_str;

    }

    @Override
    public void setDefault(View v) {
        //set current time and date
        Calendar cal = new GregorianCalendar();
        String date_str = DateFormat.getDateInstance().format(cal.getTime());
        String time_str = DBAdapter.db_time_format.format(cal.getTime());

        TextView date = (TextView) v.findViewById(id_date);
        TextView time = (TextView) v.findViewById(id_time);

        date.setText(date_str);
        time.setText(time_str);

    }
}
