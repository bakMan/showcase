package staniss.games.toe.simplediary.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBChangeManager;
import staniss.games.toe.simplediary.db.GUIRecordMapper;
import staniss.games.toe.simplediary.db.mappers.GUIElementSetter;

/**
 * Created by Stanislav Smatana on 29/04/2015.
 */

/** Parent abstract class for fragments showing/editing DB records
 *
 */
public abstract class ARecordFragment extends Fragment implements DBChangeManager.DBChangeListener {

    //Mapper between gui and db
    GUIRecordMapper DB_Mapper;
    String TAG = "ARECORD_FRAGMENT";

    //db fields to get data from
    String[] from = null;

    //respective GUIElement setters for the db data
    GUIElementSetter[] to = null;
    long id;

    /** Establishes mapping between gui and db
     *
     * @param from Db fields to map from
     * @param to GUIElement setters to map to
     */
    protected void setMapping(String[] from,GUIElementSetter[] to)
    {
        Log.i(TAG,"Setting mapping");
        this.from = from;
        this.to = to;
    }

    /** Populates fragment with data from db
     *
     * @param id Id of the record
     * @param db Db adapter
     */
    public void populate(long id,DBAdapter db) {
        this.id = id;
        //create db->gui mapping
        try {
            //create mapping
            DB_Mapper = new GUIRecordMapper(from, to, db);
        } catch (GUIRecordMapper.MappingException e) {
            Log.e(TAG, "Error in the db gui mapping.");
            return;
        }

        try {
            //ask mapper to load data from db into cahce
            DB_Mapper.load(id);
        } catch (GUIRecordMapper.MappingException e)
        {
            Log.e(TAG, "Unable to load record from db");
            return;
        }



    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //on creation -> reload data
        if(DB_Mapper != null)
        {
            try {
                DB_Mapper.load(id);
            } catch (GUIRecordMapper.MappingException e) {
                Log.e(TAG, "Unable to load record from db");
                return;
            }

        }
    }

    @Override
    public void onResume() {
        Log.i(TAG,"Fragment onResume()");
        super.onResume();

        //populate gui with loaded db data
        if(DB_Mapper != null) {

            DB_Mapper.show(getView());
        }
    }

    @Override
    public void onDBChange(long id) {

        //on change of the db, reload fragment
        Log.i(TAG,"Fragment " + this.toString() + " recieved DB change notify");
        //reload data from the db -> in case of change
        try {
            DB_Mapper.reload();
        } catch (GUIRecordMapper.MappingException e) {
            Log.e(TAG,"Wrong id on reload");
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
