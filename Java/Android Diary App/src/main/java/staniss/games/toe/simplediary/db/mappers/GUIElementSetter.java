package staniss.games.toe.simplediary.db.mappers;

import android.view.View;

/** Abstract class of all GUIElement setters
* Created by Stanislav Smatana on 16/05/2015.
*/ //Basic abstract class for gui elements/setters/getters
public abstract class GUIElementSetter
{
    int id;

    /** Constructor
     *
     * @param id resource id of the gui element
     */
    public GUIElementSetter(int id)
    {
        this.id = id;
    }

    /** Set element id using data_db, v is the containing view
     *
     * @param db_data Data from db
     * @param v Containing view
     */
    public abstract void set(String db_data, View v);

    /** Get data from the GUI element in the db-friendly string
     *
     * @param v Containing view
     * @return db-friendly string with data
     */
    public abstract  String get(View v);

    /** Sets gui element to the default value in case of NO_ID (see GUIRecordMapper)
     * By default does nothing
     * @param v Containing view
     */
    public void setDefault(View v)
    {

    }
}
