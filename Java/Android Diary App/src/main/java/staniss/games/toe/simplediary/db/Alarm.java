package staniss.games.toe.simplediary.db;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import staniss.games.toe.simplediary.services.AlarmService;

/** This "class" contains static method for manipulating alarms
 * Created by Stanislav Smatana on 19/05/2015.
 */
public class Alarm {

    static final String TAG = "ALARM_SETTING";

    /** Method creates pending intent for alarm setting.
     *
     * @param context Context of alarm
     * @param id Id of the event from db
     * @return PendingIntent representing alarm
     */
    private static PendingIntent alarm_create_pending_intent(Context context,long id)
    {
        Intent intent = new Intent(context, AlarmService.class);
        intent.putExtra("ID",id);

        //Pending id request code is id
        PendingIntent pintent = PendingIntent.getService(context,(int) id,intent,PendingIntent.FLAG_ONE_SHOT);
        return pintent;
    }

    /** Function sets alarm on desired date and time for requested id
     *
     * @param context Context of alarm
     * @param id Id of the event from db
     * @param date Date and time in the db format (see DBAdapter static members)
     * @param alarm_minutes Minutes to subtract from alarm time
     */
    public static void alarm_set(Context context,long id, String date, String alarm_minutes) {

        //contains current date
        GregorianCalendar cal = new GregorianCalendar();
        int i_alarm_minutes;

        try {
            //parse date using db format parser
            Date d = DBAdapter.db_date_format.parse(date);
            //parse alarm minutes
            i_alarm_minutes = Integer.parseInt(alarm_minutes);
            cal.setTime(d);

            //subtract alarm advance time
            cal.add(Calendar.MINUTE, -i_alarm_minutes);
        } catch (ParseException e) {
            Log.e(TAG, "Unable to parse date from database on alarm setting " + date);
        }

        //prepare pending intent to be realized at time
        PendingIntent pintent = alarm_create_pending_intent(context,id);

        Log.i(TAG, "Setting alarm for \"" + Long.toString(id) +
                "\" on " + DBAdapter.db_date_format.format(cal.getTime()) + " original date in db: " + date);

        //set alarm in system
        AlarmManager man = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        man.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pintent);
    }

    /** Removes alarm from the alarmservice. Call on non-existent alarm will do nothing
     *
     * @param context Context of alarm
     * @param id Id of the event from db
     */
    public static void alarm_remove(Context context, long id)
    {
        PendingIntent pintent = alarm_create_pending_intent(context,id);

        //cancel alarm
        AlarmManager man = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        man.cancel(pintent);
    }

}
