package staniss.games.toe.simplediary.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.GregorianCalendar;

import staniss.games.toe.simplediary.DetailScreen;
import staniss.games.toe.simplediary.R;
import staniss.games.toe.simplediary.db.DBAdapter;
import staniss.games.toe.simplediary.db.DBChangeManager;
import staniss.games.toe.simplediary.db.DBOpenHelper;
import staniss.games.toe.simplediary.db.GUIRecordMapper;
import staniss.games.toe.simplediary.db.mappers.GUIElementSetter;

/**
 * Created by Stanislav Smatana on 16/05/2015.
 */
public class AlarmService extends Service {

    static final String TAG = "ALARM_SERVICE";
    static final int NOTIFY_ID = 10;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //this must be done if the app is out of memory
        DBChangeManager.set_context(getApplicationContext());

        long id = intent.getLongExtra("ID", GUIRecordMapper.NO_ID);

        if(id == GUIRecordMapper.NO_ID)
        {
            Log.e(TAG,"Alarm service was started, but no ID provided");
            return super.onStartCommand(intent, flags, startId);
        }

        Log.i(TAG,"Alarm fired off");
        DBAdapter db = DBAdapter.getInstance(this);

        Cursor c = db.getEventById(id);

        if(c.getCount() != 1)
        {
            Log.e(TAG,"Requested ID yielded wrong amount of results: " + Integer.toString(c.getCount()));
            return super.onStartCommand(intent, flags, startId);
        }

        c.moveToFirst();
        String title = c.getString(c.getColumnIndex(DBOpenHelper.TITLE_FIELD));
        String desc = c.getString(c.getColumnIndex(DBOpenHelper.DESC_FIELD));
        String date = c.getString(c.getColumnIndex(DBOpenHelper.DATE_FIELD));
        String time_part = "";

        //extract time frome date
        //date is not neccessary (it's obvious :P )
        String[] parts = date.split(" ");
        if(parts.length == 2)
        {
            time_part = parts[1];
        }

        //intent that opens details of the event
        Intent a_int = new Intent(this, DetailScreen.class);
        a_int.putExtra("ID",id);

        //Request id must be unique because otherwise android will manage only single PendingIntent
        int request = (int) GregorianCalendar.getInstance().getTimeInMillis();
        PendingIntent pintent = PendingIntent.getActivity(this,request,a_int,PendingIntent.FLAG_ONE_SHOT);

        //Morse code lengths
        int dot = 200;      // Length of a Morse Code "dot" in milliseconds
        int dash = 500;     // Length of a Morse Code "dash" in milliseconds
        int short_gap = 200;    // Length of Gap Between dots/dashes
        int medium_gap = 500;   // Length of Gap Between Letters
        int long_gap = 1000;    // Length of Gap Between Words
        long[] pattern = {
                0,  // Start immediately
                dash,short_gap,dash,short_gap,dash,short_gap,dot};


                //get notification manager
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(time_part + " " + title)
                .setContentText(desc)
                .setSmallIcon(R.drawable.icon_alarm)
                .setVibrate(pattern)
                .setContentIntent(pintent);

        Notification notification = mBuilder.build();

        //notification has to close after click
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Gets an instance of the NotificationManager service
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Builds the notification and issues it.
        mNotifyMgr.notify(0, notification);

        Log.i(TAG,"Sent notification for \""+ title + "\" on " + time_part + " for id " + Long.toString(a_int.getLongExtra("ID", -1)));

        return super.onStartCommand(intent, flags, startId);

    }
}
