package staniss.games.toe.simplediary.db.mappers;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import staniss.games.toe.simplediary.db.DBAdapter;

/** In cooperation with AlarmAdjustmentSetter sets the text to the remaining to alarm field in the detail
 * fragment
 * Created by Stanislav Smatana on 15/05/2015.
 */
public class AlarmTextSetter extends GUIElementSetter {

    static final String TAG = "SET_ALARM";

    SimpleDateFormat db_format = new SimpleDateFormat(DBAdapter.DATE_FORMAT + " " + DBAdapter.TIME_FORMAT);
    GregorianCalendar future = null;

    public AlarmTextSetter(int id) {
        super(id);
    }

    @Override
    //Does not actually set the value, just stores the time
    //Setting is delegated to AlarmAdjustmentSetter
    public void set(String db_data, View v) {

        TextView txt = (TextView) v.findViewById(id);
        Date d;

        try {
            d = db_format.parse(db_data);
        } catch (ParseException e)
        {
            Log.e(TAG, "Wrong date format in db " + db_data);
            txt.setText("ERROR");
            return;
        }

        future = new GregorianCalendar();
        future.setTime(d);
    }


    public GregorianCalendar getCal()
    {
        return future;
    }

    @Override
    public String get(View v) {
        return null;
    }
}
