package staniss.games.toe.simplediary.db.mappers;

import android.view.View;
import android.widget.EditText;

/** Sets editText value based on db, works both ways, should be merged with TextViewSetter
* Created by Stanislav Smatana on 29/04/2015.
*/
public class EditTextSetter extends GUIElementSetter
{
    EditText t;

    public EditTextSetter(int id) {
        super(id);
    }

    @Override
    public void set(String db_data,View base_view) {
        EditText t = (EditText) base_view.findViewById(id);
        t.setText(db_data);
    }

    public String get(View base_view)
    {
        EditText t = (EditText) base_view.findViewById(id);
        return t.getText().toString();
    }
}
