package staniss.games.toe.simplediary.db.mappers;

import android.util.Log;
import android.view.View;

/** One way DB -> GUI setter, which sets GUIElement enabled based on the DB boolean data
 * Created by Stanislav Smatana on 14/05/2015.
 */
public class EnableSetter extends GUIElementSetter {
    final static String TAG = "ENABLESETTER_DB";
    boolean inverse = false;

    public EnableSetter(int id) {
        super(id);
    }

    /** Constructor
     *
     * @param id GUI element id
     * @param inv If true enable element on "false" value
     */
    public EnableSetter(int id,boolean inv) {
        super(id);
        inverse = inv;
    }

    @Override
    public void set(String db_data, View v) {
        View ch =  v.findViewById(id);
        Log.i(TAG, "Enable value from db: " + db_data);

        if(db_data != null && (db_data.equals("TRUE") || db_data.equals("true")))
        {
            ch.setEnabled(!inverse);
        }
        else
        {
            ch.setEnabled(inverse);
        }
    }

    @Override
    public String get(View v) {
        return null;
    }
}

