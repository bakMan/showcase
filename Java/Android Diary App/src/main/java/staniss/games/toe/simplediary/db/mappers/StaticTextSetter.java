package staniss.games.toe.simplediary.db.mappers;

import android.view.View;
import android.widget.TextView;

/** This setter simply sets provided static text into textView when requested to set, ignores DB
 * Created by Stanislav Smatana on 16/05/2015.
 */
public class StaticTextSetter extends GUIElementSetter {

    String text = "";

    public StaticTextSetter(int id,String text) {
        super(id);
        this.text = text;
    }

    @Override
    public void set(String db_data, View v) {
        TextView txt = (TextView) v.findViewById(id);
        txt.setText(text);
    }

    @Override
    public String get(View v) {
        return null;
    }
}
