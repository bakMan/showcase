package staniss.games.toe.simplediary.db;

import android.database.Cursor;
import android.util.Log;
import android.view.View;

import staniss.games.toe.simplediary.db.mappers.GUIElementSetter;

/** This class maps data between the db and GUI Elements
 * Owerall scheme: DB -> DBAdapter -> GUIRecordMapper -> GUI
 * Created by Stanislav Smatana on 29/04/2015.
 */
public class GUIRecordMapper {

    static final String TAG = "DB_MAPPER";

    //NO_ID is used for records that are to be inserted into db and are not there yet
    public static final int NO_ID = -1;

    //column names from which to map
    String[] db_from = null;

    //result cache to reduce number of db requests
    String[] cache;
    boolean cache_empty = true;

    //corresponding setters which are responsible for setting fields
    GUIElementSetter[] to = null;

    long id = NO_ID;
    DBAdapter db;

    //exception applicable where mappings provided are wrong
    public class MappingException extends Exception{}

    /** Creates new GUIRecordmapper for requested mapping
     *
     * @param from DB fields to map from
     * @param to GUI element setters to map into (see GUIElementSetter)
     * @param db Database adapter
     * @throws MappingException When there is no mapping provided or number of fields is not equal
     *         to number of setters
     */
    public GUIRecordMapper(String[] from, GUIElementSetter[] to,DBAdapter db) throws MappingException
    {
        //mapping must be present
        if(from == null || to == null) {
            Log.e(TAG,"Provided null mapping");
            throw new MappingException();
        }

        this.db_from = from;
        this.to = to;
        this.db = db;

        //prepare db connection
        db.open();

        //mappings must have equal size
        if(db_from.length != to.length) {
            Log.e(TAG,"Mapping length does not match");
            throw new MappingException();
        }

        //allocate cache
        cache = new String[db_from.length];
    }

    /** Method loads data for record with id from db and stores in the cache
     *
     * @param id Id of the record to load
     */
    public void load(long id) throws MappingException
    {
        Cursor c = db.getEventById(id);

        //if there is nothing -> error
        if (c.getCount() == 0) {
            Log.w(TAG, "Provided ID yielded no results id=" + Long.toString(id));
            return;
        }

        //get data from DB
        Log.i(TAG,"Number of columns " + Integer.toString(c.getColumnCount()));

        //loading of data
        c.moveToFirst();
        try {

            int cnt = 0;
            for (String field : db_from)
            {
                cache[cnt] = c.getString(c.getColumnIndexOrThrow(field));
                cnt++;
            }

        }catch (Exception e)
        {
            //wrong field name in mapping
            throw new MappingException();
        }

        this.id = id;
        cache_empty = false;
    }

    /** Function reloads data from db to cache
     *
     * @throws MappingException
     */
    public void reload() throws MappingException
    {
        if(id != NO_ID)
        {
            load(id);
        }
    }
    /** Shows data loaded from DB to gui elements
     *
     */
    public void show(View base)
    {
        if(cache_empty)
        {
            Log.i(TAG,"Result cache is empty. Have you called load before show ? Using defaults");
        }

        //set gui elements to values
        int cnt=0;
        for (GUIElementSetter s : to)
        {

            //In the case of NO_ID, there is nothing to show from db
            //request default value from the setter
            if(id == NO_ID)
                s.setDefault(base);
            else
                s.set(cache[cnt],base);
            cnt++;
        }
    }

    /** Inserts new row into eventlist, used by write
     *
     */
    private void insert(View base)
    {
        //Load data from gui
        GUItoCache(base);

        //perform insert
        db.insertEvent(db_from,cache);
        DBChangeManager.getMgr().notify(NO_ID);
    }

    /** Writes data from gui elements to db
     *
     */
    public void write(View base)
    {
        //if id is NO_ID, insert new row instead
        if(id == NO_ID)
        {
            insert(base);
            return;
        }

        //load data from gui elements
        GUItoCache(base);

        //update db
        db.updateEvent(id,db_from,cache);

        //notify about change
        DBChangeManager.getMgr().notify(id);
    }

    //Loads data from gui to the cache
    private void GUItoCache(View base) {
        //set gui elements to values
        int cnt = 0;
        for (GUIElementSetter s : to) {

            String val = s.get(base);
            cache[cnt] = val;

            cnt++;

        }
    }

}
