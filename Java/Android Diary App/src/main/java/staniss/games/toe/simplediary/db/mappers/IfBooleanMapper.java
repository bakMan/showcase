package staniss.games.toe.simplediary.db.mappers;

import android.view.View;

/** Setter which takes another two setters and if database data is equal to true set's gui using the
 * first one, otherwise, using the second one
 * Created by Stanislav Smatana on 16/05/2015.
 */
public class IfBooleanMapper extends GUIElementSetter {

    GUIElementSetter if_true;
    GUIElementSetter if_false;

    public IfBooleanMapper(int id,GUIElementSetter if_true,GUIElementSetter if_false) {
        super(id);
        this.if_false = if_false;
        this.if_true = if_true;
    }

    @Override
    public void set(String db_data, View v) {

        GUIElementSetter out = null;

        if (db_data != null && (db_data.equals("true") || db_data.equals("TRUE")))
        {
            out = if_true;
        }
        else
        {
            out = if_false;
        }

        if(out != null)
            out.set(db_data,v);
    }

    @Override
    public String get(View v) {
        return null;
    }
}
