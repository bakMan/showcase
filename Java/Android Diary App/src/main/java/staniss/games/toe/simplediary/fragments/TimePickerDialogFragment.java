package staniss.games.toe.simplediary.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;

/** Simple Time picker
 * Created by Stanislav Smatana on 13/05/2015.
 */
public class TimePickerDialogFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{



    public ITimeChangeListener getListener() {
        return listener;
    }

    public void setListener(ITimeChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if(listener!=null)
            listener.onTimeChange(hourOfDay, minute);
    }

    public interface ITimeChangeListener
    {
        public void onTimeChange(int hourOfday,int minute);
    }

    private ITimeChangeListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int hour_of_day = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of DatePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour_of_day,minute,true);
    }

}
