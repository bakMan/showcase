package staniss.games.toe.simplediary.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import staniss.games.toe.simplediary.R;

/** Fragment that shows welcome message and option to add first event
 * Created by Stanislav Smatana on 12/05/2015.
 */
public class WelcomeFragment extends Fragment {

    public IFirstEventListener getListener() {
        return listener;
    }

    public void setListener(IFirstEventListener listener) {
        this.listener = listener;
    }

    public interface IFirstEventListener
    {
        public void onFirstItemAdd();
    }

    private IFirstEventListener listener;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.welcome_screen, container, false);


        //button action -> tell to edit event listener (hosting activity in this case)
        //that edit button was pressed
        Button b = (Button) v.findViewById(R.id.btn_add_first);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getListener() != null)
                    getListener().onFirstItemAdd();
            }
        });


        return v;

    }

}
