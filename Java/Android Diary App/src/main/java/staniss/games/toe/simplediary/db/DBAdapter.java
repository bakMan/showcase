package staniss.games.toe.simplediary.db;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.util.Pair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import staniss.games.toe.simplediary.services.AlarmService;

/**  Adapter between db and application
 * Created by Stanislav Smatana on 27/04/2015.
 */
public class DBAdapter {


    //singleton instance of this class
    static DBAdapter instance = null;

    /** Enumeration of query time ranges
     * today events
     * events of this week
     * future events (tomorrow+)
     * past event (yesterday-)
     */
    public enum TIME_FRAME
    {
        TODAY,THIS_WEEK,FUTURE,PAST
    }

    private static long UNLIMITED = -1;

    /** Translates TIME_FRAME into actual sql where string
     *
     * @param frame TIME_FRAME enum member
     * @return SQL string
     */
    private String timeRangeToSQL(TIME_FRAME frame)
    {
        //#date# = date field from db, for convinience
        String[] queries = {
                "DATE(#date#) = DATE('now')", //today
                "DATE(#date#) >= DATE('now', 'weekday 0', '-7 days') AND DATE(#date#) <= DATE('now', 'weekday 0')", //this week
                "DATE(#date#) > DATE('now')",
                "DATE(#date#) < DATE('now')"

        };


        //replace #date# with actual date field name
        return queries[frame.ordinal()].replace("#date#",DBOpenHelper.DATE_FIELD);
    }

    /** Get actual singleton of this class
     *
     * @param context Db Context
     * @return Singleton object of this class
     */
    static public DBAdapter getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new DBAdapter(context);
            instance.open();
        }

        return instance;
    }

    //this date format is storable in SQLITE
    public final static String DATE_FORMAT = "yyyy-MM-dd";
    public final static String TIME_FORMAT = "HH:mm";

    private String TAG = "DB_ADAPTER";

    Context context;

    private DBOpenHelper db_helper;
    private SQLiteDatabase db;

    //date and time formatters according to db format
    public static SimpleDateFormat db_date_format = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT);
    public static SimpleDateFormat db_time_format = new SimpleDateFormat(TIME_FORMAT);

    public DBAdapter(Context _context) {
        context = _context;
        db_helper = new DBOpenHelper(context);
    }

    /** Method opens connection to database
     *
     * @throws SQLiteException
     */
    public void open() throws SQLiteException
    {
        Log.i(TAG, "Opening db connection");
        db = db_helper.getWritableDatabase();
    }

    /** Method gets number of rows in the events table
     *
     * @return Number of rows
     */
    public int getEventsNum()
    {
        return db.query(DBOpenHelper.TABLE_NAME,null,null,null,null,null,null).getCount();
    }

    /** Method gets all events in the db
     *
     * @return DB Cursor pointing to the first row of events table
     */
    public Cursor getAllEvents()
    {
        return getAllEvents(DBOpenHelper.DATE_FIELD,true);
    }


    /** Get all future events
     *
     * @param order_by_coulumn By which column to order
     * @param ascending True if order should be ascending
     * @return
     */
    public Cursor getAllEvents(String order_by_coulumn,boolean ascending)
    {
        //delegate to full function
        return getAllEvents(order_by_coulumn,ascending,TIME_FRAME.FUTURE);
    }

    /** Get all events in the time range from db
     *
     * @param order_by_coulumn By which column to order
     * @param ascending True if order should be ascending
     * @param frame TIME_FRAME enum member determining time frame of query
     * @return Result db cursor
     */
    public Cursor getAllEvents(String order_by_coulumn,boolean ascending,TIME_FRAME frame) {
        String mode;
        if (ascending)
            mode = " ASC";
        else
            mode = " DESC";

        //hack for date, ordering wasnt working properly
        //instead of actual date value it sorts the dates using unix epoch time
        if (order_by_coulumn.equals(DBOpenHelper.DATE_FIELD))
        {
            //order first by date, than within a day put whole day events first (is_time = 0) and than consider time
            order_by_coulumn = "DATE(" + DBOpenHelper.DATE_FIELD + ")" +mode+", " + DBOpenHelper.IS_TIME_FIELD + " " + mode + ", TIME("+DBOpenHelper.DATE_FIELD +") " ;
            Log.i(TAG,"Hack for date field " + order_by_coulumn);
        }

        String order = order_by_coulumn + mode;
        String where = timeRangeToSQL(frame);


        Cursor c = db.query(DBOpenHelper.TABLE_NAME,null,where,null,null,null,order);
        Log.i(TAG,"Queried db for events, found " + Integer.toString(c.getCount()) +" records");

        return c;
    }

    //get future alarm events
    public Cursor getFutureAlarms()
    {
        //todo TEST
        Date d = new Date();
        String date_str = db_date_format.format(d);

        String where = DBOpenHelper.DATE_FIELD + " > " + "? AND " + DBOpenHelper.IS_ALARM_FIELD + " = ?";
        String[] where_args = {date_str,"true"};

        Cursor c = db.query(DBOpenHelper.TABLE_NAME,null,where,where_args,null,null,null);
        return c;
    }


    /** Get single event with a given id
     *
     * @param id Id of row to fetch
     * @return Cursor pointing to desired row
     */
    public Cursor getEventById(long id)
    {

        String[] sel_args = {Long.toString(id)};
        Cursor c = db.query(DBOpenHelper.TABLE_NAME,null,"_id=?",sel_args,null,null,null);

        if(c.getCount() > 1)
        {
            Log.e(TAG,"More events ("+Integer.toString(c.getCount())+") have the same id!");
        }

        return c;
    }

    /** Packs data into ContentValues object
     *
     * @param fields Field names
     * @param data Data
     * @return ContentValue object containing data
     */
    private ContentValues pack(ArrayList<String> fields,ArrayList<String> data)
    {
        ContentValues args = new ContentValues();

        int cnt=0;

        for(String field : fields)
        {
            if(field == null)
            {
                Log.w(TAG,"Null in the field name");
            }

            args.put(field,data.get(cnt));
            cnt++;
        }

        return args;

    }


    /** Function filters nulls out of data array and erases corresponding items in fields array
     *
     * Some of the GUI <-> DB mappers work only in the DB -> GUI fashion. These return null on
     * the get request from GUIRecordMapper. They are ignored in actuall db write, therefore this
     * function exists :)
     *
     * @param fields Input fieldnames
     * @param data Input data
     * @param out_fields Output arraylist of fieldnames
     * @param out_data Output arraylist of data
     */
    private void filterNulls(String[] fields,String[] data,ArrayList<String> out_fields,ArrayList<String> out_data)
    {
        //filter out nulls -> these should be ignored in write
        int cnt=0;
        for(String d : data)
        {
            if(d != null)
            {
                out_fields.add(fields[cnt]);
                out_data.add(d);
            }

            cnt++;
        }
    }


    //Debugging function that turns update/insert query request into readable string
    private String queryToString(ArrayList<String> fields,ArrayList<String> data)
    {
        StringBuilder bld = new StringBuilder();
        bld.append("{");

        int cnt = 0;
        for(String f : fields)
        {
            bld.append(f);
            bld.append(" -> \"");
            bld.append(data.get(cnt));
            bld.append("\",");

            cnt++;
        }

        bld.append("}");

        return bld.toString();
    }

    /** Function updates event in the db
     *
     * @param id Id of row to update
     * @param fields Fieldnames to update
     * @param data Corresponding data
     */
    public void updateEvent(long id,String[] fields,String[] data)
    {
        alarm_remove(id);

        Log.i(TAG,"Update for event id=" + Long.toString(id));

        ArrayList<String> f_fields = new ArrayList<String>();
        ArrayList<String> f_data = new ArrayList<String>();

        //Nulls come from setters which are in the direction DB -> GUI only
        filterNulls(fields,data,f_fields,f_data);

        String dbg = queryToString(f_fields,f_data);
        Log.i(TAG,"Updating: " + dbg);

        //Pack  for update
        ContentValues args = pack(f_fields,f_data);
        String[] tmp =  {Long.toString(id)};
        db.update(DBOpenHelper.TABLE_NAME,args,DBOpenHelper.ID_FIELD+"=?",tmp);

        //check if alarm should be set
        int alarm_fld = f_fields.indexOf(DBOpenHelper.IS_ALARM_FIELD);
        if(alarm_fld != -1 && f_data.get(alarm_fld).equals("true"))
        {
            //set alarm
            Log.i(TAG,"Setting alarm for new event");
            int date_idx = f_fields.indexOf(DBOpenHelper.DATE_FIELD);
            int alarm_mins_idx = f_fields.indexOf(DBOpenHelper.ALARM_MINS_FIELD);

            alarm_set(id,f_data.get(date_idx),f_data.get(alarm_mins_idx));
        }

    }


    /** Function inserts new event into db
     *
     * @param fields Input fieldnames
     * @param data Input data
     */
    public void insertEvent(String[] fields,String[] data)
    {

        ArrayList<String> f_fields = new ArrayList<String>();
        ArrayList<String> f_data = new ArrayList<String>();

        //Nulls come from setters which are in the direction DB -> GUI only
        filterNulls(fields,data,f_fields,f_data);

        String dbg = queryToString(f_fields,f_data);
        Log.i(TAG,"Inserting new row: " + dbg);

        ContentValues args = pack(f_fields,f_data);
        long id = db.insert(DBOpenHelper.TABLE_NAME,null,args);

        Log.i(TAG,"Row got id " + Long.toString(id));
        //check if alarm should be set
        int tmp = f_fields.indexOf(DBOpenHelper.IS_ALARM_FIELD);
        if(tmp != -1 && f_data.get(tmp).equals("true"))
        {
            //set alarm
            Log.i(TAG,"Setting alarm for new event");
            int date_idx = f_fields.indexOf(DBOpenHelper.DATE_FIELD);
            int alarm_mins_idx = f_fields.indexOf(DBOpenHelper.ALARM_MINS_FIELD);

            alarm_set(id,f_data.get(date_idx),f_data.get(alarm_mins_idx));
        }

    }

    /** Function deletes event from db
     *
     * @param id Id of row to delete
     */
    public void deleteEvent(long id) {
        alarm_remove(id);
        String[] args = {Long.toString(id)};
        db.delete(DBOpenHelper.TABLE_NAME,DBOpenHelper.ID_FIELD+"=?",args);
    }


    //These members delegate to Alarm set and remove functions of the Alarm class
    private void alarm_set(long id, String date, String alarm_minutes)
    {
        Alarm.alarm_set(context,id,date,alarm_minutes);
    }


    private void alarm_remove(long id)
    {
        Alarm.alarm_remove(context,id);
    }
}
